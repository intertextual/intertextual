use clap::Parser;
use diesel::prelude::*;
use diesel::PgConnection;

use intertextual::models::error::IntertextualError;
use intertextual::schema::*;

#[derive(Parser, Debug)]
pub struct UserParams {
    /// The subcommand to use
    #[clap(subcommand)]
    pub command: UserSubCommand,
}

#[derive(Parser, Debug)]
pub enum UserSubCommand {
    /// Creates an user account
    Create(UserCreationParameters),
    /// Immediately deletes an user account
    Delete(SingleUserParameters),
    /// Promotes the given user to administrator status
    Promote(SingleUserParameters),
    /// Demotes the given user back to standard user status
    Demote(SingleUserParameters),
}

#[derive(Parser, Debug)]
pub struct SingleUserParameters {
    /// Name of the target user
    pub username: String,
}

#[derive(Parser, Debug)]
pub struct UserCreationParameters {
    /// The username of the user to create.
    #[clap(short, long)]
    pub username: String,

    /// Request a password from the command line.
    /// If this option is not present, a random password will be generated automatically.
    #[clap(short, long)]
    pub password_from_command_line: bool,

    /// The display name of the user.
    /// If no value is given, the display name is initialized to the username
    #[clap(short, long)]
    pub display_name: Option<String>,

    /// Whether to bypass the reset request.
    /// Note : this will be ignored if the -p / --password option is not provided
    #[clap(long)]
    pub do_not_request_reset: bool,
}

pub fn handle(params: UserParams, conn: &PgConnection) -> Result<(), IntertextualError> {
    match params.command {
        UserSubCommand::Create(params) => create_user(params, conn),
        UserSubCommand::Delete(params) => delete_user(params, conn),
        UserSubCommand::Promote(params) => promote_user(params, conn),
        UserSubCommand::Demote(params) => demote_user(params, conn),
    }
}

pub fn create_user(
    options: UserCreationParameters,
    conn: &PgConnection,
) -> Result<(), IntertextualError> {
    // Check if the username is not already in use
    let username = options.username;
    let existing_user =
        intertextual::actions::users::find_user_by_username_for_login(&username, conn)?;
    if existing_user.is_some() {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Username",
            message: "This username is already in use".to_string(),
        });
    }

    const MAX_CHARACTERS_FOR_USERNAME: usize = 32;

    if username.len() > MAX_CHARACTERS_FOR_USERNAME {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Username",
            message: format!(
                "Usernames should be at most {} characters long.",
                MAX_CHARACTERS_FOR_USERNAME
            ),
        });
    }

    let re = regex::Regex::new(r"^[a-zA-Z0-9_]+$").unwrap();
    if !re.is_match(&username) {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Username",
            message: "Usernames can only contain uppercase and lowercase characters, numbers and underscores. Use a simpler username, and set any other characters as your display name.".to_string(),
        });
    }

    let password = if options.password_from_command_line {
        use std::io::Read;
        use std::io::Write;
        let mut buffer = String::new();
        std::io::stdout().write_all(b"enter password:")?;
        std::io::stdout().flush()?;
        std::io::stdin().read_to_string(&mut buffer)?;
        buffer.as_mut().trim().to_string()
    } else {
        use rand::Rng;
        let rand_string: String = rand::thread_rng()
            .sample_iter(&rand::distributions::Alphanumeric)
            .take(30)
            .collect();
        rand_string
    };
    let display_name = options.display_name.unwrap_or_else(|| username.clone());

    let pw_sk = std::env::var("PASSWORD_SECRET_KEY").expect("PASSWORD_SECRET_KEY needs to be set");
    let new_user = intertextual::actions::users::modifications::insert_user(
        username,
        display_name.trim().to_string(),
        password.clone(),
        pw_sk,
        conn,
    )?;
    log::info!(
        "User {} (id: {}) has been created with password {}.",
        new_user.username,
        new_user.id,
        password
    );

    if !options.do_not_request_reset {
        diesel::update(users::table.find(new_user.id))
            .set((users::password_reset_required.eq(true),))
            .get_result::<intertextual::models::users::User>(conn)?;
        log::info!("A new password will be requested on first login.");
    }

    Ok(())
}

pub fn delete_user(
    options: SingleUserParameters,
    conn: &PgConnection,
) -> Result<(), IntertextualError> {
    let user =
        intertextual::actions::users::find_user_by_username_for_login(&options.username, conn)?;
    let user = match user {
        Some(user) => user,
        None => {
            return Err(IntertextualError::FormFieldFormatError {
                form_field_name: "Username",
                message: "This user does not exist".to_string(),
            })
        }
    };
    let user = intertextual::actions::users::automated_actions::delete_user(user, conn)?;
    diesel::insert_into(moderation_actions::table)
        .values(intertextual::models::admin::NewModerationAction {
            username: "system".to_string(),
            action_type: format!("Immediately removed account of user id={}", user.id),
            message: "System action from command line".to_string(),
        })
        .get_result::<intertextual::models::admin::ModerationAction>(conn)?;
    log::info!(
        "User {} (id: {}) has been deleted successfully.",
        user.username,
        user.id,
    );

    Ok(())
}

pub fn promote_user(
    options: SingleUserParameters,
    conn: &PgConnection,
) -> Result<(), IntertextualError> {
    let user = diesel::update(users::table)
        .filter(users::username.ilike(options.username))
        .set(users::administrator.eq(true))
        .get_result::<intertextual::models::users::User>(conn)?;
    diesel::insert_into(moderation_actions::table)
        .values(intertextual::models::admin::NewModerationAction {
            username: "system".to_string(),
            action_type: format!("Added user id={} to administrators", user.id),
            message: "System action from command line".to_string(),
        })
        .get_result::<intertextual::models::admin::ModerationAction>(conn)?;
    log::info!("User {} promoted succcessfully", user.id);
    Ok(())
}

pub fn demote_user(
    options: SingleUserParameters,
    conn: &PgConnection,
) -> Result<(), IntertextualError> {
    let user = diesel::update(users::table)
        .filter(users::username.ilike(options.username))
        .set(users::administrator.eq(false))
        .get_result::<intertextual::models::users::User>(conn)?;
    diesel::insert_into(moderation_actions::table)
        .values(intertextual::models::admin::NewModerationAction {
            username: "system".to_string(),
            action_type: format!("Removed user id={} from administrators", user.id),
            message: "System action from command line".to_string(),
        })
        .get_result::<intertextual::models::admin::ModerationAction>(conn)?;
    log::info!("User {} demoted succcessfully", user.id);
    Ok(())
}
