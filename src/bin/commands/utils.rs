use clap::Parser;
use diesel::prelude::*;
use diesel::PgConnection;

use intertextual::models::error::IntertextualError;
use intertextual::models::stories::Chapter;
use intertextual::schema::*;
use intertextual::utils::htmlize::remove_leading_whitespaces_from_paragraphs_in_html;

#[derive(Parser, Debug)]
pub struct UtilsParameters {
    /// The subcommand to use
    #[clap(subcommand)]
    pub command: UtilSubCommand,
}

#[derive(Parser, Debug)]
pub enum UtilSubCommand {
    /// Recompute the word count for all chapters on the website
    ///
    /// Use this if the word counting algorithm has changed.
    /// Note : This operation will take a while to compute
    RecomputeWordCounts,
    /// Planify a recompute for cached story fields such as publication date and/or update date
    ///
    /// Use this if the story update algorithm has changed.
    /// Note : This operation will impact the next event loop of the server
    PlanifyRecomputeStoryCachedFields,
    /// Execute a recompute of notifications for all planned stories
    ///
    /// Use this if the notifications broke somehow.
    /// Note : This operation will impact the next event loop of the server
    RecomputeFutureChaptersNotifications,
    /// Resanitize all chapters, forewords and afterwords on the website
    ///
    /// Use this if the sanitation step changed.
    /// Note : This operation will take a while to compute
    ResanitizeAllChapters,
    /// Remove all leading non-breaking whitespaces in paragraphs of all the existing stories. This is a one-shot command.
    RemoveLeadingWhitespaces,
}

pub fn handle(params: UtilsParameters, conn: &PgConnection) -> Result<(), IntertextualError> {
    match params.command {
        UtilSubCommand::RecomputeWordCounts => recompute_word_counts(conn),
        UtilSubCommand::PlanifyRecomputeStoryCachedFields => {
            planify_recompute_story_cached_fields(conn)
        }
        UtilSubCommand::RecomputeFutureChaptersNotifications => {
            recompute_future_chapters_notifications(conn)
        }
        UtilSubCommand::ResanitizeAllChapters => resanitize_all_chapters(conn),
        UtilSubCommand::RemoveLeadingWhitespaces => remove_leading_whitespaces(conn),
    }
}

pub fn recompute_word_counts(conn: &PgConnection) -> Result<(), IntertextualError> {
    let all_chapters = chapters::table
        .select(chapters::id)
        .load::<uuid::Uuid>(conn)?;
    let total_count = all_chapters.len();
    for (processed_count, chapter_id) in all_chapters.iter().enumerate() {
        log::info!("Chapter {}/{}", processed_count, total_count);
        if let Ok(chapter) = chapters::table.find(chapter_id).get_result::<Chapter>(conn) {
            let word_count =
                intertextual::utils::word_count::count_words_from_html(&chapter.content);
            let _ = diesel::update(chapters::table.find(chapter_id))
                .set(chapters::word_count.eq(word_count))
                .execute(conn);
        }
    }
    log::info!("{} chapters have been processed", total_count);
    Ok(())
}

pub fn planify_recompute_story_cached_fields(conn: &PgConnection) -> Result<(), IntertextualError> {
    diesel::delete(last_story_cache_check::table).execute(conn)?;
    log::info!("Previous planification recompute date has been deleted. Recompute will run in the next 30 seconds.");
    Ok(())
}

pub fn recompute_future_chapters_notifications(
    conn: &PgConnection,
) -> Result<(), IntertextualError> {
    for chapter_id in chapters::table
        .filter(chapters::show_publicly_after_date.gt(chrono::Utc::now().naive_utc()))
        .filter(chapters::moderator_locked.eq(false))
        .select(chapters::id)
        .load::<uuid::Uuid>(conn)?
    {
        intertextual::actions::notifications::modifications::add_chapter_publication_notification(
            chapter_id, conn,
        )?;
    }
    log::info!("Future chapter notifications have been replanified. Recompute will run in the next 30 seconds.");
    Ok(())
}

pub fn remove_leading_whitespaces(conn: &PgConnection) -> Result<(), IntertextualError> {
    let all_chapters = chapters::table
        .select(chapters::id)
        .load::<uuid::Uuid>(conn)?;
    let total_count = all_chapters.len();
    for (processed_count, chapter_id) in all_chapters.iter().enumerate() {
        log::info!("Chapter {}/{}", processed_count, total_count);
        if let Ok(chapter) = chapters::table.find(chapter_id).get_result::<Chapter>(conn) {
            let content = remove_leading_whitespaces_from_paragraphs_in_html(chapter.content);
            let _ = diesel::update(chapters::table.find(chapter_id))
                .set(chapters::content.eq(content))
                .execute(conn);
        }
    }
    log::info!("{} chapters have been processed", total_count);
    Ok(())
}

// The FromSql implementation for SanitizedHtml includes sanitization,
// so we just need to read the values from the database and then write
// them back in.
pub fn resanitize_all_chapters(conn: &PgConnection) -> Result<(), IntertextualError> {
    let all_chapters = chapters::table
        .select(chapters::id)
        .load::<uuid::Uuid>(conn)?;
    let total_count = all_chapters.len();
    for (processed_count, chapter_id) in all_chapters.iter().enumerate() {
        log::info!("Chapter {}/{}", processed_count, total_count);
        if let Ok(chapter) = chapters::table.find(chapter_id).get_result::<Chapter>(conn) {
            let _ = diesel::update(chapters::table.find(chapter_id))
                .set((
                    chapters::foreword.eq(chapter.foreword),
                    chapters::content.eq(chapter.content),
                    chapters::afterword.eq(chapter.afterword),
                ))
                .execute(conn);
        }
    }
    Ok(())
}
