use clap::Parser;
use diesel::prelude::*;
use diesel::PgConnection;

use intertextual::models::error::IntertextualError;
use intertextual::models::formats::*;
use intertextual::models::stories::CreativeRole;
use intertextual::schema::*;

const PARAGRAPH_PER_STORY: usize = 43;

#[derive(Parser, Debug)]
pub struct StressTestParameters {
    /// The subcommand to use
    #[clap(subcommand)]
    pub command: StressTestModeSubCommand,
}

#[derive(Parser, Debug)]
pub enum StressTestModeSubCommand {
    /// Create an authors stress test environment
    Authors(StressTestAuthorParameters),
    /// Clean up behind the authors stress test
    CleanAuthors(CleanStressTestParameters),
    /// Create a followers stress test environment
    Followers(StressTestFollowerParameters),
    /// Clean up behind the followers stress test
    CleanFollowers(CleanStressTestParameters),
    /// Create an user approval stress test environment
    Approvals(StressTestApprovalParameters),
    /// Clean up behind the user approval stress test
    CleanApprovals(CleanStressTestParameters),
    /// Create a comment stress test environment
    Comments(StressTestCommentParameters),
    /// Clean up behind the comment stress test
    CleanComments(CleanStressTestParameters),
    /// Create a recommendation stress test environment
    Recommendations(StressTestRecommendationParameters),
    /// Clean up behind the recommendation stress test
    CleanRecommendations(CleanStressTestParameters),
    /// Create a stress test for hits on a chapter
    ///
    /// Warning : This cannot be undone
    Hits(StressTestHitParameters),
}

#[derive(Parser, Debug)]
pub struct CleanStressTestParameters {
    /// The number of users to clean up
    #[clap(long)]
    pub user_count: i64,
}

#[derive(Parser, Debug)]
pub struct StressTestAuthorParameters {
    /// The number of users to create
    #[clap(long)]
    pub user_count: i64,

    /// The number of stories to create per user
    #[clap(long)]
    pub story_per_user: i64,

    /// The number of stories to create per user
    #[clap(long)]
    pub chapter_per_story: Option<i64>,

    /// The number of tags per created story
    #[clap(long)]
    pub tags_per_story: i64,

    /// The number of overall tags to create
    #[clap(long)]
    pub overall_tags: i64,
}

#[derive(Parser, Debug)]
pub struct StressTestFollowerParameters {
    /// The number of users to create
    #[clap(long)]
    pub user_count: i64,

    /// The username of the author to follow
    #[clap(long)]
    pub target_author: String,
}

#[derive(Parser, Debug)]
pub struct StressTestApprovalParameters {
    /// The number of users to create
    #[clap(long)]
    pub user_count: i64,

    /// The username of the author to approve
    #[clap(long)]
    pub target_author: String,

    /// The URL of the story to approve
    #[clap(long)]
    pub target_story: String,
}

#[derive(Parser, Debug)]
pub struct StressTestCommentParameters {
    /// The number of users to create
    #[clap(long)]
    pub user_count: i64,

    /// The username of the author to comment on
    #[clap(long)]
    pub target_author: String,

    /// The URL of the story to comment on
    #[clap(long)]
    pub target_story: String,

    /// The chapter of the story to comment on
    #[clap(long)]
    pub target_chapter: i32,
}

#[derive(Parser, Debug)]
pub struct StressTestRecommendationParameters {
    /// The number of users to create
    #[clap(long)]
    pub user_count: i64,

    /// The username of the author to recommend
    #[clap(long)]
    pub target_author: String,

    /// The URL of the story to recommend
    #[clap(long)]
    pub target_story: String,
}

#[derive(Parser, Debug)]
pub struct StressTestHitParameters {
    /// The number of users to create
    #[clap(long)]
    pub total_hit_count: i64,

    /// The username of the author to comment on
    #[clap(long)]
    pub target_author: String,

    /// The URL of the story to comment on
    #[clap(long)]
    pub target_story: String,

    /// The chapter of the story to comment on
    #[clap(long)]
    pub target_chapter: i32,
}

pub fn handle(params: StressTestParameters, conn: &PgConnection) -> Result<(), IntertextualError> {
    use StressTestModeSubCommand::*;
    match params.command {
        Authors(params) => prepare_stress_test_authors(params, conn),
        Followers(params) => prepare_stress_test_followers(params, conn),
        Approvals(params) => prepare_stress_test_approvals(params, conn),
        Comments(params) => prepare_stress_test_comments(params, conn),
        Recommendations(params) => prepare_stress_test_recommendations(params, conn),
        Hits(params) => prepare_stress_test_hits(params, conn),
        CleanAuthors(params) => clean_stress_test_authors(params, conn),
        CleanFollowers(params) => clean_stress_test_followers(params, conn),
        CleanApprovals(params) => clean_stress_test_approvals(params, conn),
        CleanComments(params) => clean_stress_test_comments(params, conn),
        CleanRecommendations(params) => clean_stress_test_recommendations(params, conn),
    }
}

pub fn prepare_stress_test_authors(
    params: StressTestAuthorParameters,
    conn: &PgConnection,
) -> Result<(), IntertextualError> {
    let usernames: Vec<String> = (0..params.user_count)
        .map(|i| format!("stress_test_user_{}", i))
        .collect();

    let category = {
        let category = tag_categories::table
            .filter(tag_categories::display_name.eq("stress_tags"))
            .get_result::<intertextual::models::tags::TagCategory>(conn)
            .optional()?;
        if let Some(category) = category {
            category
        } else {
            diesel::insert_into(tag_categories::table)
                .values(intertextual::actions::tags::admin::NewTagCategory {
                    allow_user_defined_tags: false,
                    description: "".to_string(),
                    display_name: "stress_tags".to_string(),
                    is_always_displayed: false,
                })
                .get_result::<intertextual::models::tags::TagCategory>(conn)?
        }
    };

    let tags: Vec<String> = (0..params.overall_tags)
        .map(|i| format!("stress_test_tag_{}", i))
        .collect();
    let chapter_count = params.chapter_per_story.unwrap_or(1);

    let password = "123456".to_string();
    let pw_sk = std::env::var("PASSWORD_SECRET_KEY").expect("PASSWORD_SECRET_KEY needs to be set");
    let pw_hash = argonautica::Hasher::default()
        .with_password(password.as_str())
        .with_secret_key(pw_sk.as_str())
        .hash()
        .unwrap();
    let mut tag_index: i64 = 0;
    let mut example_text_index: usize = 0;
    for username in usernames {
        if let Ok(new_user) = get_or_create_dummy_user(&username, &pw_hash, conn) {
            for story_index in 0..params.story_per_user {
                let publication_date = get_random_date(chrono::Duration::days(14));
                let example_html = SanitizedHtml::from(&generate_example_text(
                    &mut example_text_index,
                    PARAGRAPH_PER_STORY,
                    true,
                ));
                let word_count =
                    intertextual::utils::word_count::count_words_from_html(&example_html);
                let description =
                    intertextual::utils::text_trim::limit_bytes_with_ellipses_if_needed(
                        &example_html.text()[3..200],
                        100,
                    );
                let new_story = intertextual::models::stories::NewSingleAuthorStory {
                    author_id: new_user.id,
                    comments_enabled: true,
                    recommendations_enabled:
                        intertextual::models::stories::RecommendationsState::Enabled,
                    foreword: SanitizedHtml::from(&generate_example_text(
                        &mut example_text_index,
                        1,
                        true,
                    )),
                    content: example_html,
                    afterword: SanitizedHtml::from(&generate_example_text(
                        &mut example_text_index,
                        1,
                        true,
                    )),
                    description: RichTagline { inner: description },
                    title: format!("Lorem Ipsum {}", story_index),
                    url_fragment: format!("LoremIpsum{}", story_index),
                    word_count,
                    is_multi_chapter: false,
                    ongoing_status: intertextual::models::stories::OngoingState::NeverOngoing,
                };
                if let Ok((new_story, new_chapter)) =
                    intertextual::actions::stories::modifications::create_new_single_author_story(
                        new_story, conn,
                    )
                {
                    // Set the creation & publication date manually, to bypass the checks around the publication time
                    diesel::update(stories::table.find(new_story.id))
                        .set((
                            stories::story_internally_created_at.eq(publication_date),
                            stories::cached_first_publication_date.eq(publication_date),
                            stories::cached_last_update_date.eq(publication_date),
                        ))
                        .execute(conn)?;
                    diesel::update(chapters::table.find(new_chapter.id))
                        .set((
                            chapters::chapter_internally_created_at.eq(publication_date),
                            chapters::official_creation_date.eq(publication_date),
                            chapters::show_publicly_after_date.eq(publication_date),
                        ))
                        .execute(conn)?;

                    for _ in 1..chapter_count {
                        let example_chapter_html = SanitizedHtml::from(&generate_example_text(
                            &mut example_text_index,
                            PARAGRAPH_PER_STORY,
                            true,
                        ));
                        let word_count = intertextual::utils::word_count::count_words_from_html(
                            &example_chapter_html,
                        );
                        let new_chapter = intertextual::models::stories::NewChapter {
                            title: None,
                            word_count,
                            story_id: new_story.id,
                            foreword: SanitizedHtml::from(&generate_example_text(
                                &mut example_text_index,
                                1,
                                true,
                            )),
                            content: example_chapter_html,
                            afterword: SanitizedHtml::from(&generate_example_text(
                                &mut example_text_index,
                                1,
                                true,
                            )),
                        };
                        if let Ok(chapter) =
                            intertextual::actions::stories::modifications::create_new_chapter(
                                new_chapter,
                                conn,
                            )
                        {
                            // Set the creation & publication date manually, to bypass the checks around the publication time
                            diesel::update(chapters::table.find(chapter.id))
                                .set((
                                    chapters::chapter_internally_created_at.eq(publication_date),
                                    chapters::official_creation_date.eq(publication_date),
                                    chapters::show_publicly_after_date.eq(publication_date),
                                ))
                                .execute(conn)?;
                        }
                    }

                    for _ in 0..params.tags_per_story {
                        let tag_name = tags.get(tag_index as usize).unwrap();
                        let (internal_tag, _) =
                            intertextual::actions::tags::modifications::get_or_insert_tag(
                                category.id,
                                tag_name,
                                conn,
                            )?;
                        /* Ignore the errors for this operation : these errors are generated when adding a tag several times to a story */
                        let _ = intertextual::actions::tags::modifications::add_tag_to_story(
                            new_story.id,
                            internal_tag,
                            intertextual::models::tags::TagAssociationType::Major,
                            conn,
                        );
                        tag_index += 1;
                        if tag_index >= params.overall_tags {
                            tag_index = 0;
                        }
                    }
                }
            }
        }
    }

    Ok(())
}

pub fn clean_stress_test_authors(
    params: CleanStressTestParameters,
    conn: &PgConnection,
) -> Result<(), IntertextualError> {
    let usernames: Vec<String> = (0..params.user_count)
        .map(|i| format!("stress_test_user_{}", i))
        .collect();
    for username in usernames {
        if let Ok(user) = users::table
            .filter(users::username.ilike(&username))
            .get_result::<intertextual::models::users::User>(conn)
        {
            intertextual::actions::users::automated_actions::delete_user(user, conn)?;
            log::info!("Cleaned up user {}", username);
        }
    }

    Ok(())
}

pub fn prepare_stress_test_followers(
    params: StressTestFollowerParameters,
    conn: &PgConnection,
) -> Result<(), IntertextualError> {
    let usernames: Vec<String> = (0..params.user_count)
        .map(|i| format!("stress_test_follower_{}", i))
        .collect();
    let target_author = users::table
        .filter(users::username.ilike(params.target_author))
        .get_result::<intertextual::models::users::User>(conn)?;
    let password = "123456".to_string();
    let pw_sk = std::env::var("PASSWORD_SECRET_KEY").expect("PASSWORD_SECRET_KEY needs to be set");
    let pw_hash = argonautica::Hasher::default()
        .with_password(password.as_str())
        .with_secret_key(pw_sk.as_str())
        .hash()
        .unwrap();
    for username in usernames {
        if let Ok(new_user) = get_or_create_dummy_user(&username, &pw_hash, conn) {
            intertextual::actions::follows::modifications::follow_author(
                new_user.id,
                target_author.id,
                conn,
            )?;
        }
    }

    Ok(())
}

pub fn clean_stress_test_followers(
    params: CleanStressTestParameters,
    conn: &PgConnection,
) -> Result<(), IntertextualError> {
    let usernames: Vec<String> = (0..params.user_count)
        .map(|i| format!("stress_test_follower_{}", i))
        .collect();
    for username in usernames {
        if let Ok(user) = users::table
            .filter(users::username.ilike(&username))
            .get_result::<intertextual::models::users::User>(conn)
        {
            intertextual::actions::users::automated_actions::delete_user(user, conn)?;
            log::info!("Cleaned up user {}", username);
        }
    }

    Ok(())
}

pub fn prepare_stress_test_approvals(
    params: StressTestApprovalParameters,
    conn: &PgConnection,
) -> Result<(), IntertextualError> {
    let usernames: Vec<String> = (0..params.user_count)
        .map(|i| format!("stress_test_approval_{}", i))
        .collect();
    let target_author = users::table
        .filter(users::username.ilike(params.target_author))
        .get_result::<intertextual::models::users::User>(conn)?;
    let target_story = stories::table
        .inner_join(user_to_story_associations::table)
        .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
        .filter(user_to_story_associations::user_id.eq(target_author.id))
        .filter(stories::url_fragment.ilike(params.target_story))
        .select(stories::all_columns)
        .get_result::<intertextual::models::stories::Story>(conn)?;
    let range_to_consider = std::cmp::min(
        chrono::Duration::days(14),
        chrono::Utc::now().naive_utc() - target_story.story_internally_created_at,
    );
    let password = "123456".to_string();
    let pw_sk = std::env::var("PASSWORD_SECRET_KEY").expect("PASSWORD_SECRET_KEY needs to be set");
    let pw_hash = argonautica::Hasher::default()
        .with_password(password.as_str())
        .with_secret_key(pw_sk.as_str())
        .hash()
        .unwrap();
    for username in usernames {
        if let Ok(new_user) = get_or_create_dummy_user(&username, &pw_hash, conn) {
            let user_approval_date = get_random_date(range_to_consider);
            // Set the user approval with a manual time
            diesel::insert_into(user_approvals::table)
                .values((
                    user_approvals::user_id.eq(new_user.id),
                    user_approvals::story_id.eq(target_story.id),
                    user_approvals::approval_date.eq(user_approval_date),
                ))
                .execute(conn)?;
        }
    }

    Ok(())
}

pub fn clean_stress_test_approvals(
    params: CleanStressTestParameters,
    conn: &PgConnection,
) -> Result<(), IntertextualError> {
    let usernames: Vec<String> = (0..params.user_count)
        .map(|i| format!("stress_test_approval_{}", i))
        .collect();
    for username in usernames {
        if let Ok(user) = users::table
            .filter(users::username.ilike(&username))
            .get_result::<intertextual::models::users::User>(conn)
        {
            intertextual::actions::users::automated_actions::delete_user(user, conn)?;
            log::info!("Cleaned up user {}", username);
        }
    }

    Ok(())
}

pub fn prepare_stress_test_comments(
    params: StressTestCommentParameters,
    conn: &PgConnection,
) -> Result<(), IntertextualError> {
    let usernames: Vec<String> = (0..params.user_count)
        .map(|i| format!("stress_test_comment_{}", i))
        .collect();
    let target_author = users::table
        .filter(users::username.ilike(params.target_author))
        .get_result::<intertextual::models::users::User>(conn)?;
    let target_story = stories::table
        .inner_join(user_to_story_associations::table)
        .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
        .filter(user_to_story_associations::user_id.eq(target_author.id))
        .filter(stories::url_fragment.ilike(params.target_story))
        .select(stories::all_columns)
        .get_result::<intertextual::models::stories::Story>(conn)?;
    let target_chapter = chapters::table
        .filter(chapters::story_id.eq(target_story.id))
        .filter(chapters::chapter_number.eq(params.target_chapter))
        .get_result::<intertextual::models::stories::Chapter>(conn)?;
    let range_to_consider = std::cmp::min(
        chrono::Duration::days(14),
        chrono::Utc::now().naive_utc() - target_story.story_internally_created_at,
    );
    let password = "123456".to_string();
    let pw_sk = std::env::var("PASSWORD_SECRET_KEY").expect("PASSWORD_SECRET_KEY needs to be set");
    let pw_hash = argonautica::Hasher::default()
        .with_password(password.as_str())
        .with_secret_key(pw_sk.as_str())
        .hash()
        .unwrap();
    let mut example_text_index: usize = 0;
    for username in usernames {
        if let Ok(new_user) = get_or_create_dummy_user(&username, &pw_hash, conn) {
            let comment_date = get_random_date(range_to_consider);
            let new_comment = intertextual::models::comments::NewComment {
                chapter_id: target_chapter.id,
                commenter_id: new_user.id,
                message: RichBlock {
                    inner: generate_example_text(&mut example_text_index, 1, false),
                },
            };
            let comment =
                intertextual::actions::comments::modifications::add_new_comment(new_comment, conn)?;
            // Set the comment time manually
            diesel::update(comments::table.find(comment.id))
                .set(comments::created.eq(comment_date))
                .execute(conn)?;
        }
    }

    Ok(())
}

pub fn clean_stress_test_comments(
    params: CleanStressTestParameters,
    conn: &PgConnection,
) -> Result<(), IntertextualError> {
    let usernames: Vec<String> = (0..params.user_count)
        .map(|i| format!("stress_test_comment_{}", i))
        .collect();
    for username in usernames {
        if let Ok(user) = users::table
            .filter(users::username.ilike(&username))
            .get_result::<intertextual::models::users::User>(conn)
        {
            intertextual::actions::users::automated_actions::delete_user(user, conn)?;
            log::info!("Cleaned up user {}", username);
        }
    }

    Ok(())
}

pub fn prepare_stress_test_recommendations(
    params: StressTestRecommendationParameters,
    conn: &PgConnection,
) -> Result<(), IntertextualError> {
    let usernames: Vec<String> = (0..params.user_count)
        .map(|i| format!("stress_test_recommend_{}", i))
        .collect();
    let target_author = users::table
        .filter(users::username.ilike(params.target_author))
        .get_result::<intertextual::models::users::User>(conn)?;
    let target_story = stories::table
        .inner_join(user_to_story_associations::table)
        .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
        .filter(user_to_story_associations::user_id.eq(target_author.id))
        .filter(stories::url_fragment.ilike(params.target_story))
        .select(stories::all_columns)
        .get_result::<intertextual::models::stories::Story>(conn)?;
    let range_to_consider = std::cmp::min(
        chrono::Duration::days(14),
        chrono::Utc::now().naive_utc() - target_story.story_internally_created_at,
    );
    let password = "123456".to_string();
    let pw_sk = std::env::var("PASSWORD_SECRET_KEY").expect("PASSWORD_SECRET_KEY needs to be set");
    let pw_hash = argonautica::Hasher::default()
        .with_password(password.as_str())
        .with_secret_key(pw_sk.as_str())
        .hash()
        .unwrap();
    let mut example_text_index: usize = 0;
    for username in usernames {
        if let Ok(new_user) = get_or_create_dummy_user(&username, &pw_hash, conn) {
            let creation_date = get_random_date(range_to_consider);
            let new_recommendation = intertextual::models::recommendations::NewRecommendation {
                description: RichBlock {
                    inner: generate_example_text(&mut example_text_index, 3, false),
                },
                featured_by_user: false,
                story_id: target_story.id,
                user_id: new_user.id,
            };
            let recommendation =
                intertextual::actions::recommendations::modifications::create_recommendation(
                    new_recommendation,
                    conn,
                )?;
            // Set the comment time manually
            diesel::update(recommendations::table.find(recommendation.id))
                .set(recommendations::recommendation_date.eq(creation_date))
                .execute(conn)?;
        }
    }

    Ok(())
}

pub fn clean_stress_test_recommendations(
    params: CleanStressTestParameters,
    conn: &PgConnection,
) -> Result<(), IntertextualError> {
    let usernames: Vec<String> = (0..params.user_count)
        .map(|i| format!("stress_test_recommend_{}", i))
        .collect();
    for username in usernames {
        if let Ok(user) = users::table
            .filter(users::username.ilike(&username))
            .get_result::<intertextual::models::users::User>(conn)
        {
            intertextual::actions::users::automated_actions::delete_user(user, conn)?;
            log::info!("Cleaned up user {}", username);
        }
    }

    Ok(())
}

pub fn prepare_stress_test_hits(
    params: StressTestHitParameters,
    conn: &PgConnection,
) -> Result<(), IntertextualError> {
    let target_author = users::table
        .filter(users::username.ilike(params.target_author))
        .get_result::<intertextual::models::users::User>(conn)?;
    let target_story = stories::table
        .inner_join(user_to_story_associations::table)
        .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
        .filter(user_to_story_associations::user_id.eq(target_author.id))
        .filter(stories::url_fragment.ilike(params.target_story))
        .select(stories::all_columns)
        .get_result::<intertextual::models::stories::Story>(conn)?;
    let target_chapter = chapters::table
        .filter(chapters::story_id.eq(target_story.id))
        .filter(chapters::chapter_number.eq(params.target_chapter))
        .get_result::<intertextual::models::stories::Chapter>(conn)?;
    let range_to_consider = std::cmp::min(
        chrono::Duration::days(14),
        chrono::Utc::now().naive_utc() - target_story.story_internally_created_at,
    );
    for _ in 0..params.total_hit_count {
        let hit_date = get_random_date(range_to_consider);
        // Set the hit time manually
        diesel::insert_into(story_hits::table)
            .values((
                story_hits::chapter_id.eq(target_chapter.id),
                story_hits::hit_date.eq(hit_date),
            ))
            .execute(conn)?;
    }

    Ok(())
}

fn get_random_date(range: chrono::Duration) -> chrono::NaiveDateTime {
    let publication_date = chrono::Utc::now().naive_utc();
    let duration_range_seconds: i64 = range.num_seconds();
    let duration_size = rand::random::<f64>();
    let duration_nanoseconds = ((duration_range_seconds as f64) * duration_size) as i64;
    let duration = chrono::Duration::seconds(duration_nanoseconds);
    publication_date - duration
}

fn get_or_create_dummy_user(
    username: &str,
    pw_hash: &str,
    conn: &PgConnection,
) -> Result<intertextual::models::users::User, IntertextualError> {
    let target_user = users::table
        .filter(users::username.ilike(username))
        .get_result::<intertextual::models::users::User>(conn)
        .optional()?;
    if let Some(user) = target_user {
        return Ok(user);
    }

    let target_user = diesel::insert_into(users::table)
        .values((
            users::username.eq(username),
            users::display_name.eq(username),
            users::pw_hash.eq(pw_hash),
        ))
        .get_result::<intertextual::models::users::User>(conn)?;
    log::info!(
        "User {} (id: {}) has been created.",
        target_user.username,
        target_user.id
    );

    Ok(target_user)
}

fn generate_example_text(
    example_text_index: &mut usize,
    paragraph_count: usize,
    use_paragraphs: bool,
) -> String {
    /// Note : This is randomly-generated text from the https://www.lipsum.com/ website
    const EXAMPLE_TEXT_SIZE: usize = 50;
    const EXAMPLE_TEXT: [&str; EXAMPLE_TEXT_SIZE] = [
        r"Aenean eu nibh suscipit, ultrices mi eu, consectetur dui. Duis ut arcu ullamcorper, luctus ipsum at, convallis eros. Pellentesque augue libero, consectetur at ipsum sit amet, interdum eleifend ipsum. Nunc neque dolor, porttitor non neque sit amet, tempor dapibus neque. Sed at nunc blandit, vestibulum eros et, suscipit nisl. Etiam eleifend euismod lectus pellentesque tempor. Nullam vulputate vel eros a congue. Maecenas tempus, augue quis aliquet scelerisque, mi orci scelerisque neque, eleifend scelerisque erat dui vel tellus. Vivamus interdum scelerisque nibh, ac pulvinar leo iaculis in. Vivamus sollicitudin justo et diam ultricies eleifend. Pellentesque quis justo lacinia, dignissim nunc non, iaculis tellus.",
        r"Vestibulum consectetur nulla at eros condimentum imperdiet. Cras eu urna vel erat auctor rutrum. Integer quis tortor at neque molestie rhoncus. Nulla nec neque turpis. Nullam vitae interdum ex. Morbi vulputate id libero nec interdum. Suspendisse lobortis ornare purus in efficitur. Nulla non vulputate magna, non malesuada est. Quisque aliquam nibh est. Praesent id justo eros. Aliquam nec ante lacus. Nam sem nulla, volutpat sed ullamcorper nec, mollis in orci. Sed vitae commodo massa, in hendrerit orci.",
        r"Nullam nec sagittis metus. Morbi ut scelerisque est, at sagittis turpis. Integer a justo quis lacus pellentesque tempus non eget nisl. Morbi aliquet arcu nec semper eleifend. Pellentesque sit amet mi ut arcu placerat blandit nec ac magna. Phasellus et tempus sapien. Vivamus fermentum est vel diam tempor eleifend. Vestibulum in dolor sit amet libero pretium tempus ut ut odio. Duis iaculis nisl quis ultrices vulputate. Nunc blandit vitae nisl nec lacinia. Sed in facilisis lacus, a blandit massa. Pellentesque ut consequat nibh. Nullam quam augue, sodales ac sodales porttitor, tristique ut metus. Nam posuere mi id turpis congue, id sagittis velit placerat. Proin consequat lectus ac elit rutrum imperdiet vel ut odio.",
        r"Proin vel nisl erat. Nullam id ultrices velit. Integer pretium nisl magna, in accumsan nulla pretium eu. Duis ac turpis blandit, efficitur est nec, fermentum urna. Pellentesque efficitur turpis ac nulla mollis elementum. Donec volutpat tristique risus, vel eleifend sapien. Aenean ut nunc a tortor sollicitudin laoreet in in odio. Nullam tincidunt tellus arcu, at finibus nibh congue non. Aliquam id nulla rhoncus, feugiat ipsum at, laoreet arcu.",
        r"Morbi at arcu at sapien sollicitudin pulvinar. Nulla facilisi. Suspendisse suscipit, orci at euismod feugiat, quam magna lobortis metus, quis lacinia arcu lorem eget lectus. Ut blandit non velit elementum malesuada. Nam luctus metus ut ornare porttitor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi congue fringilla nunc. Nam dapibus risus erat, at ultricies justo condimentum non. Etiam dictum dui ut iaculis tempor. Nam venenatis facilisis lacus. Donec feugiat egestas massa, sed hendrerit tellus euismod sed.",
        r"Integer aliquam interdum diam non posuere. Integer id luctus turpis. Duis urna neque, luctus ut consectetur eu, gravida eu leo. Cras luctus elit eu diam ullamcorper dignissim. Aliquam non odio faucibus, pretium ipsum in, pretium est. Nunc in magna mi. Donec elit quam, finibus vitae consequat quis, commodo euismod diam. In placerat aliquam placerat. Ut hendrerit aliquet tellus et tincidunt. Praesent eget erat convallis, volutpat enim vitae, sagittis enim. Aliquam aliquet arcu risus, vel congue risus mollis sed. Mauris efficitur et tortor ut lobortis. Nulla facilisi. Quisque fermentum ipsum in maximus volutpat. Integer eu quam mauris.",
        r"Sed finibus nisl nec mauris dignissim venenatis. In hendrerit scelerisque elementum. Integer bibendum eleifend auctor. Donec libero risus, condimentum in scelerisque eu, sodales nec mauris. Pellentesque ut mattis dolor. Sed non enim purus. Nam eget tempor enim, pulvinar vulputate dui. Maecenas tristique lacus ut velit laoreet tempor.",
        r"Mauris at varius metus, quis vulputate lectus. Donec cursus ligula turpis, quis tempor dui ullamcorper at. Suspendisse dapibus at libero pharetra elementum. Vestibulum eget justo convallis, consequat sapien eu, rutrum risus. Aenean porttitor consequat enim, vel gravida sapien mollis at. Curabitur varius semper posuere. Donec ut tristique metus. Maecenas in dui quis dolor lacinia tempor. Sed ut vestibulum ex. Nulla pharetra arcu sit amet massa convallis, mollis faucibus augue finibus. Vestibulum ante erat, porttitor in lorem ut, laoreet sollicitudin metus. Nulla elit justo, vulputate in eros eget, blandit interdum libero. Nam consequat tortor bibendum metus suscipit, quis eleifend nisi ornare. Sed malesuada massa convallis leo congue blandit. Ut vel odio tellus.",
        r"Nullam semper sapien ante, in congue sapien malesuada id. Nulla at elit pellentesque, malesuada nisl ac, consectetur magna. Proin mattis ac turpis et eleifend. Donec nec erat rhoncus nulla ullamcorper aliquet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas porttitor ex eget tellus pharetra, tempor dapibus quam ornare. Aenean in varius leo.",
        r"Vivamus et nisi urna. Nulla at ultrices metus. Praesent quis libero commodo, lacinia ligula et, lacinia mi. Nam dignissim augue at metus imperdiet euismod. Sed ac nisl vel augue vulputate hendrerit. Quisque auctor sagittis sem, a pharetra turpis venenatis ac. Etiam rutrum diam eget metus fermentum, at auctor ex pellentesque. Duis ac malesuada leo. Fusce dolor nulla, fringilla ac malesuada et, condimentum a velit.",
        r"Nullam erat nunc, ultrices vitae fermentum ut, viverra in erat. Aliquam eget velit sit amet est hendrerit pharetra tincidunt ac mauris. Aliquam aliquam, turpis non vehicula porta, magna mi ultricies est, ac aliquam nisi nunc quis ipsum. Donec aliquet quam ac gravida molestie. Nam mollis semper tellus, eu auctor diam porta non. Phasellus cursus orci at auctor pretium. Duis ut elit et nisl elementum rhoncus sed vel orci. Etiam nec vestibulum neque. Vestibulum iaculis enim sit amet magna facilisis venenatis. Quisque dignissim vehicula quam. Ut interdum tellus mollis mauris pellentesque, at egestas enim dapibus.",
        r"Nullam ac lobortis magna, quis tristique lorem. Donec tempor mattis massa ac porta. Vestibulum semper tellus varius libero porttitor aliquet nec id dolor. Fusce consectetur nec tellus a laoreet. Integer pretium eros eu placerat ornare. Cras porta purus ac dignissim gravida. Proin convallis augue non nisi scelerisque semper. Nunc bibendum diam malesuada augue venenatis rutrum. In non dapibus sapien, non venenatis metus. Pellentesque libero tellus, imperdiet non vehicula non, dapibus vel sem. Fusce venenatis accumsan orci sit amet faucibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec ligula quam, varius sit amet vulputate sed, sagittis eu justo. Vestibulum suscipit, dolor id pulvinar tincidunt, risus nisi hendrerit leo, in dignissim ex dui in ligula.",
        r"Phasellus tristique efficitur tellus eget sagittis. Nunc et mattis velit. Curabitur felis metus, efficitur sed dui eu, dignissim hendrerit orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque sed faucibus lorem. Aliquam nulla elit, pretium et turpis sed, posuere maximus erat. Ut ultricies condimentum sollicitudin. Integer ultrices dignissim dui, a consequat tortor sodales molestie.",
        r"Nunc nec porta velit, ac sagittis neque. Pellentesque eu facilisis nisi. Maecenas consectetur, tellus sed interdum facilisis, justo est viverra nulla, id euismod dui dolor et ligula. Fusce facilisis placerat quam sed tempus. Donec eget urna dui. Nullam sapien turpis, porttitor sit amet augue sed, condimentum aliquet neque. Fusce volutpat sem diam, a ullamcorper lorem luctus ut. Quisque luctus ligula nisl, non interdum arcu finibus sed. Nulla malesuada urna augue, vitae sagittis diam efficitur ut. Sed vulputate sagittis nunc, nec consequat ligula interdum quis. Donec vehicula vulputate magna, vehicula placerat velit imperdiet ut. Donec sodales dolor nulla, eget pharetra orci elementum eget. Donec vitae blandit diam, et vulputate nisl. Suspendisse blandit tincidunt nunc fermentum varius. Donec et lorem consequat, tincidunt massa vitae, dictum sem. Aliquam interdum pharetra mauris, vel tincidunt ex cursus id.",
        r"Mauris sit amet lobortis ligula. Ut sagittis ullamcorper gravida. Nunc eleifend malesuada malesuada. Maecenas eu eros sit amet erat auctor sodales. Proin mi enim, placerat eget velit et, ullamcorper pretium nunc. Nunc fringilla viverra porttitor. Suspendisse quis sodales magna, non blandit magna. Vestibulum eu mi sed dolor dictum aliquam.",
        r"Sed hendrerit, neque in iaculis laoreet, mauris metus vulputate enim, non ornare urna lectus quis justo. Nunc vestibulum augue in rutrum consequat. Integer at congue ipsum, vel porta risus. Etiam et neque tellus. Aliquam id cursus lectus. Phasellus vel purus risus. Nam metus sapien, faucibus ut sapien at, sodales placerat massa. Quisque elementum suscipit gravida. Nullam luctus sem ac venenatis pharetra. Quisque mollis laoreet tellus in sollicitudin. Suspendisse venenatis ornare magna, eget congue lorem maximus a. Phasellus pharetra nulla ut mi imperdiet finibus.",
        r"Curabitur congue, lorem at varius dictum, sem elit auctor ex, efficitur suscipit arcu dui at nisl. Quisque arcu nisl, viverra a ligula quis, semper interdum orci. Vivamus auctor eros a mi tincidunt porttitor. Duis tristique, lectus at pretium rhoncus, nunc dolor fermentum erat, eget blandit sapien urna et ligula. Phasellus a odio eget magna ultrices cursus ut quis lorem. Donec posuere volutpat orci. Proin mollis bibendum egestas. In imperdiet et dui sed commodo. Sed a hendrerit quam, quis gravida enim.",
        r"Sed sit amet est ante. Integer vel massa et elit iaculis finibus. Donec sit amet sollicitudin elit. Aliquam vestibulum metus tellus, ac placerat diam tempus sit amet. Integer turpis felis, gravida nec interdum et, dictum id dui. Proin fermentum viverra nibh, ut dignissim neque egestas nec. Phasellus pulvinar neque eget hendrerit accumsan. Proin ornare molestie nunc vitae tempus. Cras imperdiet justo eget hendrerit hendrerit. Proin facilisis consectetur ante in dictum.",
        r"Morbi rhoncus turpis in cursus tincidunt. Donec iaculis luctus diam, eu ornare odio volutpat at. Integer viverra sit amet massa vel mattis. Nulla euismod mattis eros, nec aliquam massa sollicitudin et. Duis a posuere mi. Pellentesque quis sapien varius, interdum leo sit amet, ullamcorper magna. Aenean cursus tortor vitae fermentum viverra. Fusce nec turpis sed tellus maximus auctor. Nunc at magna aliquet, viverra leo vitae, molestie felis. Phasellus euismod lectus sed libero accumsan, eget ullamcorper libero aliquam.",
        r"Proin auctor luctus tellus, ullamcorper dapibus sem blandit vitae. Cras et lacinia ante. Nullam vitae sodales eros, at pharetra nisi. Etiam lacus nunc, hendrerit eget efficitur nec, ultricies vel quam. Cras a nibh a libero scelerisque laoreet. Nunc at est et ex accumsan vulputate. Nulla elementum rhoncus tempus. Sed porta faucibus tortor a consequat. Etiam vehicula pharetra urna a molestie. Vivamus sit amet nibh eget massa efficitur congue. Duis non ex congue, aliquet tortor ut, cursus urna. Quisque pretium ligula sed purus vehicula tempor non et elit.",
        r"Sed sed turpis justo. Nunc non placerat tellus. Cras enim enim, vestibulum eget est vitae, sodales vestibulum turpis. Pellentesque tincidunt sem orci, vel tempor lectus scelerisque sit amet. Phasellus viverra pellentesque mi, id pretium enim venenatis nec. Praesent metus eros, sodales nec pharetra ac, blandit in mauris. Aenean semper massa ut semper sagittis. Aliquam ornare, eros non ultrices tincidunt, orci lorem pulvinar tellus, et pellentesque eros quam at lorem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae;",
        r"Mauris id erat eu orci venenatis commodo. Nulla lacus lacus, tincidunt non velit et, pretium volutpat ex. Phasellus nec augue a libero condimentum aliquam. Vivamus faucibus dolor ac mauris ornare, in elementum lacus varius. Vestibulum sit amet orci purus. Nulla augue ex, tempus at vulputate ut, sagittis eu nunc. Praesent faucibus commodo imperdiet. Aenean in nunc volutpat, consectetur sem sed, dignissim nulla. Suspendisse potenti. Quisque non viverra metus. Cras turpis turpis, faucibus id rutrum at, placerat quis libero. Fusce non venenatis lectus. Vivamus cursus justo eu eros congue placerat. Nullam facilisis justo tortor, at aliquet urna condimentum vel. Sed imperdiet erat vel urna luctus, ut pharetra odio fringilla.",
        r"In eleifend nisi ac ultrices auctor. Duis risus justo, pharetra tincidunt condimentum ac, convallis a neque. Sed non euismod ipsum, ac iaculis metus. Fusce sit amet mi pretium, ornare massa quis, ornare nisl. Ut elementum laoreet interdum. Praesent scelerisque vehicula cursus. Phasellus ultricies lacinia maximus. Duis aliquet justo nec nisl auctor dapibus. Morbi sollicitudin sapien convallis turpis aliquam, eget mollis ligula ultricies. Aliquam urna diam, pharetra eget iaculis at, congue sed nisl. Duis et ligula cursus, porta quam mattis, tempor purus. Aenean vehicula sem ut nunc lobortis, sit amet vehicula nibh eleifend. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec vehicula consequat mi.",
        r"Quisque lacinia non risus ac interdum. Proin lacus massa, congue nec dignissim id, volutpat vel dui. Sed in purus et augue convallis mollis eu quis leo. Pellentesque volutpat massa in eros semper, mattis ornare justo efficitur. Maecenas blandit egestas ipsum id tempus. Donec consequat, lectus in convallis sodales, erat eros volutpat orci, in volutpat nulla nunc in nibh. Morbi tortor magna, tempor at neque eu, viverra sagittis dui. Vivamus ac elit eget metus semper volutpat vel sed enim. Sed imperdiet vulputate dolor vel placerat. Praesent feugiat turpis in eros dignissim semper. Mauris eros orci, pharetra ac lorem nec, congue posuere lorem. Vivamus leo eros, convallis quis feugiat ut, interdum ut magna. Maecenas volutpat magna vel arcu consectetur interdum. Nam purus dui, ultrices sed felis quis, dapibus semper dolor.",
        r"Morbi eget vehicula justo. In scelerisque massa quis nibh bibendum, ut scelerisque ligula varius. Sed molestie aliquam condimentum. Donec et nunc tempor, varius tortor quis, pharetra dui. Sed a fringilla massa, nec efficitur tortor. Aenean efficitur eros erat, quis aliquet sapien dapibus non. Aliquam in eros massa. Donec elementum enim ex, eget tempus augue feugiat quis. Sed tincidunt hendrerit nunc, nec dapibus urna interdum id. Nunc elementum pellentesque arcu, at facilisis libero lobortis quis. Etiam laoreet venenatis purus sodales gravida. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
        r"Sed ut porta ex. Mauris in ex libero. Nulla ut sem dictum lacus scelerisque consequat a non dui. Nullam id est euismod, blandit turpis sed, varius ex. Morbi aliquam augue sit amet commodo tempus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed malesuada nibh mi, quis posuere mi lobortis in. Aliquam at nulla at tortor scelerisque pharetra. Maecenas leo quam, malesuada id aliquam a, tincidunt in magna. Sed et quam non arcu gravida tempor.",
        r"Suspendisse molestie odio quis sem auctor, eget ornare arcu luctus. Phasellus ullamcorper mattis mauris ut tempor. Phasellus ante enim, efficitur in tortor in, consequat aliquet erat. Etiam non metus ipsum. Sed tellus massa, convallis nec dignissim eu, faucibus eu leo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Praesent interdum dapibus nulla, eget iaculis massa congue nec. Aliquam tincidunt aliquam metus, sit amet imperdiet augue efficitur a. Mauris dictum lectus et molestie iaculis. Curabitur ornare, elit in accumsan pulvinar, massa neque finibus felis, bibendum egestas tellus tellus eget magna.",
        r"Morbi neque mauris, scelerisque eget arcu non, consequat porttitor ligula. Duis elit risus, congue sed felis bibendum, sagittis rhoncus augue. Donec id placerat sem. Nullam laoreet lacinia libero, vel rhoncus ipsum ornare sit amet. Pellentesque et urna non tellus dapibus sodales. Ut suscipit lectus auctor elementum ornare. Nulla luctus mi ut ante ullamcorper vulputate. Nam turpis mauris, iaculis eu molestie id, rhoncus eget lacus. Nunc nec ex sed lectus dictum eleifend. Nam eget lacus mauris. Nunc luctus malesuada nisl, quis lacinia erat facilisis vestibulum. Nulla aliquam porttitor sem, non bibendum nisl facilisis ac. Nam eget lorem at dui iaculis vehicula et quis odio. In sit amet commodo purus. Nulla congue vestibulum nulla, et tristique libero suscipit iaculis. In pulvinar velit vel tortor placerat, ut fringilla erat semper.",
        r"Sed ut ultricies justo, vitae aliquet metus. Donec commodo a nulla sit amet venenatis. Mauris et nisl ac orci semper accumsan. Nam nulla augue, porta nec odio quis, tincidunt iaculis lectus. Proin aliquam metus metus, ac egestas nulla pulvinar sed. Cras sodales sagittis nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras vulputate felis tellus, id fermentum mauris tempor at. Sed lobortis nunc eu sem condimentum egestas. Aliquam elementum ante risus, eget pulvinar velit auctor in. Proin at nunc non turpis porta volutpat nec quis massa. In pulvinar porta nunc, a tempus ligula tincidunt eu. Vivamus tempus fermentum dui sit amet fringilla. Nullam nisi leo, aliquet non lacinia sit amet, egestas id magna. Fusce euismod aliquam lacus, ac vestibulum nulla fermentum sit amet. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
        r"Maecenas quis nisi sodales lectus interdum pharetra vel in sem. Pellentesque sed semper dolor. Nam tempor, arcu sit amet ornare tincidunt, arcu orci consequat purus, sit amet gravida neque massa non felis. Suspendisse potenti. Mauris rutrum fermentum sollicitudin. Donec tempor orci dignissim mi molestie pellentesque. Vestibulum posuere ultrices risus, sit amet tristique purus vestibulum ut. Vivamus elit turpis, finibus nec turpis ac, mattis malesuada felis. Vestibulum lacus risus, vulputate at sollicitudin nec, ultricies a nibh. Nam finibus efficitur feugiat. Sed nunc lorem, vulputate sit amet mi vitae, aliquet interdum sem.",
        r"Nullam enim neque, aliquam eget risus ac, facilisis auctor tellus. Duis et tristique dui, nec gravida augue. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus id est commodo, sagittis nibh ac, gravida ex. Donec lobortis posuere nisi, in mattis arcu aliquam ut. Vestibulum enim ligula, tempus at malesuada vel, facilisis vitae dui. Phasellus viverra enim vel elementum dignissim. Proin volutpat venenatis augue, at euismod est facilisis non. Vestibulum a placerat tellus. Etiam pharetra arcu in purus consectetur, et semper ante vestibulum. Aenean rutrum neque sapien, eu vulputate ligula vulputate eget. Integer eget dui neque. Donec eleifend a risus eu vehicula.",
        r"Aenean dui magna, tincidunt non tincidunt quis, mollis nec massa. Ut a dignissim diam. Praesent luctus eleifend elit vel porttitor. Ut nec tortor at orci pellentesque fermentum. Donec fringilla ex a aliquam cursus. Sed malesuada tortor tellus, et vestibulum lorem aliquam at. Donec accumsan quam quis mollis ultrices. Vestibulum quis enim a velit mattis cursus quis sit amet ex. Curabitur ut efficitur lectus. Phasellus sed sodales nisi, a mattis neque. Integer nulla arcu, suscipit nec venenatis eu, cursus sit amet mi.",
        r"Sed venenatis eu dolor non sodales. Maecenas eget urna faucibus, tempor metus eu, malesuada purus. Maecenas eu tellus magna. Donec lobortis dolor sapien, vitae blandit nisl tincidunt in. Maecenas ac venenatis arcu. Cras fermentum, ipsum ut pellentesque molestie, eros tellus rutrum eros, ac rhoncus eros est tincidunt neque. Maecenas erat tellus, accumsan nec varius at, gravida ut risus. Aenean sagittis fringilla fringilla. Fusce lacinia imperdiet neque, et egestas ex lacinia non. Morbi porta dictum sodales. Phasellus vel eleifend nisi, in efficitur est.",
        r"Fusce eu risus eget purus dictum lacinia quis a lorem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nullam vel eleifend tellus. Donec imperdiet ex suscipit sem placerat, id scelerisque erat convallis. Vestibulum quis leo turpis. Curabitur elementum blandit est, at convallis dui. Mauris ut auctor dolor. Sed imperdiet commodo risus, nec convallis nisi pretium eget. Vivamus tristique nunc vestibulum ante posuere pulvinar. Phasellus blandit id elit ut rhoncus.",
        r"Ut feugiat varius enim. Phasellus elementum euismod vehicula. Donec orci dui, pellentesque id felis vitae, convallis blandit arcu. Curabitur eget urna nec quam malesuada scelerisque nec id tortor. Duis tincidunt, eros eu iaculis pulvinar, odio mi pretium arcu, a varius justo ante id enim. Fusce ultricies posuere velit porta venenatis. Curabitur a vestibulum urna, non tincidunt felis.",
        r"Interdum et malesuada fames ac ante ipsum primis in faucibus. Proin consectetur euismod dolor placerat dapibus. Donec egestas pulvinar pulvinar. Aliquam et elementum tortor. Sed id turpis a justo varius varius vel vitae enim. Sed in velit sagittis, imperdiet felis id, consectetur turpis. In hac habitasse platea dictumst. Donec a nisl dui. Aliquam erat volutpat. Sed vel odio elit. Praesent neque nunc, vehicula at mi sit amet, cursus dapibus sapien. Sed elit leo, dapibus in ante nec, rutrum ullamcorper erat. Suspendisse dictum pulvinar eros, eu finibus dui condimentum at. Morbi ac blandit lacus.",
        r"Sed a nulla faucibus, suscipit libero at, fermentum metus. Donec nec convallis tortor. Nunc dapibus, nibh a posuere suscipit, odio tortor scelerisque risus, quis ornare augue massa in leo. Donec a cursus arcu, eu sollicitudin ante. Integer et erat at tortor pretium malesuada. Aenean lobortis venenatis consequat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In posuere metus felis, sed mollis nisi volutpat sed. Cras ullamcorper vulputate quam, eu dictum metus dictum vel. Nulla finibus tellus lorem, a pharetra mauris sollicitudin vel. Vestibulum sit amet dignissim mi, et ornare quam. Interdum et malesuada fames ac ante ipsum primis in faucibus.",
        r"Nam porta quam at libero iaculis congue. Nam quis nunc eget ex finibus scelerisque sed et ex. Etiam id urna neque. Aenean eget dictum dui. Cras a ex at leo laoreet facilisis maximus non arcu. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at pellentesque tellus. Quisque convallis nunc sed mi efficitur, eu pulvinar ipsum malesuada.",
        r"Etiam dui enim, malesuada quis eleifend in, commodo ut sapien. Aenean sollicitudin purus a auctor interdum. Praesent vulputate elit et finibus euismod. Fusce rutrum magna nibh, in consequat velit vestibulum id. Morbi aliquam enim nisl, sit amet tempus nisi facilisis at. Nulla id volutpat ligula, in tristique lorem. Nam imperdiet, quam at malesuada dictum, tortor tortor feugiat quam, at ullamcorper sem urna vitae ante. Praesent ac dui vulputate, pretium nulla vitae, sollicitudin felis. Donec purus risus, luctus ac felis vitae, porta interdum quam. Sed scelerisque libero neque, ut consectetur arcu fringilla et. Sed ex dolor, vulputate sed massa a, suscipit pretium elit. Donec vitae vulputate erat. Morbi elementum, neque viverra venenatis posuere, nisi ante eleifend purus, ut maximus nibh felis nec lacus.",
        r"Nullam sagittis justo ut ex lacinia lobortis. Nunc vitae interdum est. Duis et elit in leo ullamcorper lacinia nec in lacus. Donec eu consequat libero. Aenean vel fermentum tellus. Ut congue, magna eu ullamcorper tincidunt, mi tortor laoreet massa, eget viverra quam nisl at mauris. Sed sit amet lectus suscipit, euismod eros eu, condimentum erat. Integer quis ipsum ligula. Fusce felis arcu, elementum nec bibendum non, pretium non tortor. Aliquam lobortis et tellus ac dictum. Nullam ultrices ac nulla iaculis pretium. Pellentesque sagittis eleifend risus, vulputate ornare ante.",
        r"Nam porta eu nulla vel efficitur. Ut efficitur sem nibh, eu sollicitudin mi aliquet eu. Pellentesque ut mi eget velit pellentesque laoreet. Etiam dignissim, libero quis consectetur consectetur, diam urna dapibus libero, finibus porttitor magna arcu in felis. Vivamus aliquet nulla elit, nec pellentesque libero ultrices eget. Nullam faucibus sodales ligula id tincidunt. Nunc id pulvinar orci. Vestibulum accumsan arcu non est varius porttitor. Proin iaculis vel mauris eu lobortis. Nullam eleifend lectus nec ornare semper. Phasellus congue vulputate malesuada.",
        r"Donec aliquet tempus nibh dapibus efficitur. Fusce sit amet finibus lorem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer nec turpis est. Fusce arcu ipsum, volutpat ut nibh ut, pulvinar gravida enim. Nulla facilisi. Etiam sapien turpis, sollicitudin vel efficitur ut, varius non mi. Integer non ligula facilisis, hendrerit mauris ut, pharetra arcu. Suspendisse potenti. Phasellus nec magna non velit facilisis scelerisque a vel augue. Nullam pretium molestie metus, at ullamcorper nisl ornare a.",
        r"Suspendisse sagittis orci elementum libero pellentesque, ac gravida dolor lobortis. Aenean ut posuere lectus. Ut a ligula sed sapien sollicitudin consequat. Quisque sed ipsum turpis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec fermentum nec dui nec hendrerit.",
        r"Ut eu urna sit amet turpis varius bibendum feugiat sit amet purus. Donec ipsum lorem, viverra et lorem a, dapibus iaculis tellus. Curabitur laoreet pharetra urna, ac luctus arcu porttitor laoreet. Suspendisse potenti. Integer at odio augue. Mauris accumsan pellentesque tincidunt. Fusce sit amet rutrum sem. Pellentesque condimentum imperdiet sem. Integer lacus tortor, dictum eu mollis non, gravida et mauris. Curabitur varius, tortor nec tristique aliquam, ligula sapien volutpat risus, tincidunt semper ipsum mauris bibendum velit.",
        r"Nunc et velit dapibus, auctor arcu non, pretium est. Phasellus molestie scelerisque odio, et malesuada ante aliquam et. Donec a sagittis ligula. Quisque turpis ex, interdum in posuere venenatis, suscipit eu risus. Morbi aliquet ac sapien non fermentum. Vivamus sed massa eget nisl mollis convallis vel quis est. Donec et purus quam. Pellentesque at lacus sed enim pellentesque sagittis. Nullam ut dui placerat, pulvinar tellus in, venenatis libero.",
        r"Aenean volutpat odio vel nisl finibus imperdiet. Praesent in ex eget velit viverra maximus ut a nibh. Donec elementum eleifend sagittis. Phasellus aliquet tincidunt tortor, ac facilisis turpis malesuada sit amet. In neque nulla, laoreet at nulla ac, consectetur ultricies nisl. Proin quis tortor pretium, consectetur tellus ac, scelerisque turpis. Donec consectetur in odio at viverra. Mauris in nisi leo. Aenean nisl sapien, scelerisque sit amet nulla quis, commodo accumsan mi. Sed libero lectus, volutpat sit amet erat non, tristique rutrum nisi. Suspendisse potenti. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras sollicitudin risus felis, a aliquam lectus imperdiet ut. Nam at gravida felis.",
        r"Etiam quis pretium eros, at ullamcorper augue. Etiam imperdiet sollicitudin arcu. Curabitur et convallis lectus. Pellentesque et dapibus massa. Morbi gravida tristique odio sed gravida. Nunc dolor nisi, hendrerit aliquet leo at, scelerisque ullamcorper ex. Nam rhoncus diam vel ex ultricies finibus. Donec imperdiet convallis vestibulum. Proin bibendum dolor eget eros luctus commodo. Etiam ultricies iaculis justo sed laoreet. Sed sodales massa at massa rhoncus rutrum. Pellentesque luctus erat at ornare pharetra.",
        r"Nulla ultricies, lorem et dictum rutrum, nisi felis porttitor orci, ac malesuada sem enim ut purus. In mattis mattis eros vitae ultricies. Vestibulum euismod ultrices nunc suscipit imperdiet. Maecenas hendrerit eleifend fermentum. Vivamus in vulputate elit. Nunc ut tellus consectetur turpis rutrum suscipit. Aliquam magna sem, gravida ut ex vel, iaculis finibus risus. Sed dui mauris, condimentum vitae semper vel, bibendum id ante. Donec a mollis augue.",
        r"Pellentesque pellentesque augue lorem, sit amet consequat augue laoreet eget. Integer faucibus enim eget hendrerit lobortis. Nam ac venenatis sapien. Nunc eu diam vestibulum, euismod sem non, feugiat tortor. Integer nec hendrerit est, eget molestie nunc. Phasellus id odio in diam hendrerit convallis vitae ut felis. Sed scelerisque diam id varius ornare. Nulla id arcu non metus fringilla cursus vel vel libero. Quisque sit amet nibh id dui mattis egestas sit amet eget est. Donec ex metus, scelerisque vel placerat sed, vulputate non arcu. Donec non varius orci, nec pellentesque est. Donec vestibulum, leo quis ullamcorper imperdiet, orci augue accumsan nibh, nec vestibulum ligula urna sit amet elit. Aliquam faucibus tristique justo, eu hendrerit dui. Sed sit amet eros quam. Quisque et lobortis justo.",
        r"Proin elementum velit nulla, nec consequat nulla varius vitae. In hac habitasse platea dictumst. In nec suscipit massa. Fusce eu nisi eget libero lobortis placerat. Maecenas vitae ullamcorper nisi, eget consectetur lacus. Vestibulum vel imperdiet libero. Praesent blandit dolor non sem pellentesque semper. Nulla mattis quam at sapien vestibulum sagittis. Duis tempus ultricies lectus, ut auctor turpis venenatis et. Etiam dictum ultrices eros gravida mollis. Sed id consectetur neque, quis pellentesque nulla. Nulla facilisi. Nulla facilisi. Sed vel convallis neque.",
    ];

    let mut indexes = Vec::with_capacity(paragraph_count);

    for _i in 0..paragraph_count {
        indexes.push(*example_text_index);
        *example_text_index += 1;
        if *example_text_index >= EXAMPLE_TEXT_SIZE {
            *example_text_index = 0;
        }
    }

    if use_paragraphs {
        indexes
            .into_iter()
            .map(|i| format!("<p>{}</p>", EXAMPLE_TEXT[i]))
            .collect::<Vec<String>>()
            .join("\n")
    } else {
        indexes
            .into_iter()
            .map(|i| EXAMPLE_TEXT[i].to_string())
            .collect::<Vec<String>>()
            .join("\n\n")
    }
}
