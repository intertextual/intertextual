use clap::Parser;
use diesel::prelude::*;
use log::{error, info, warn};
use serde::Deserialize;

use std::convert::TryInto;
use std::fs::File;
use std::path::PathBuf;

use intertextual::actions;
use intertextual::models;
use intertextual::utils::htmlize::*;
use intertextual::utils::word_count::count_words_from_html;

#[derive(Parser, Debug)]
pub struct ImportParameters {
    /// If an author doesn't have a publication date specified, publish their stories now. (default is to leave it unpublished)
    #[clap(short, long)]
    pub_now: bool,

    /// force overwriting pre-existing stories and chapters
    #[clap(short, long)]
    force: bool,

    /// If a chapter upload fails due to a conflict, continue uploading remaining chapters, rather than skipping the rest of the story
    #[clap(short, long)]
    ignore_chapter_conflicts: bool,

    /// YAML file describing stories to be imported
    #[clap(name = "IMPORT_SPEC", parse(from_os_str))]
    import_spec: PathBuf,
}

#[derive(Deserialize)]
struct ImportAuthor {
    username: String,
    foreword: Option<String>,
    afterword: Option<String>,
    pub_date: Option<String>,
    stories: Vec<ImportStory>,
}

#[derive(Deserialize)]
struct ImportStory {
    title: String,
    url_fragment: String,
    tags: Vec<String>,
    description: models::formats::RichTagline,
    chapters: Vec<ImportChapter>,
}

#[derive(Deserialize)]
struct ImportChapter {
    title: Option<String>,
    file: PathBuf,
}

macro_rules! err_ret {
    ($($arg:tt)+) => {
        {
            error!($($arg)+);
            return
        }
    }
}
macro_rules! err_con {
    ($($arg:tt)+) => {
        {
            error!($($arg)+);
            continue
        }
    }
}

fn process_author(
    author: ImportAuthor,
    pub_now: bool,
    force: bool,
    ignore: bool,
    conn: &PgConnection,
) {
    let foreword = markdown_to_sanitized_html(&author.foreword.unwrap_or_default());
    let afterword = markdown_to_sanitized_html(&author.afterword.unwrap_or_default());

    for story in author.stories.into_iter() {
        if story.chapters.is_empty() {
            err_ret!(
                "story '{}' by author {} has no chapters",
                &story.title,
                &author.username
            );
        }

        let author_id = match actions::users::find_user_by_username(
            &author.username,
            &models::filter::FilterMode::BypassFilters,
            conn,
        ) {
            Err(e) => err_ret!(
                "Error while looking up author {} of story '{}': \
                 {}. Skipping this story.",
                &author.username,
                &story.title,
                e
            ),
            Ok(None) => err_ret!(
                "Author {} of story '{}' not found. \
                 Skipping this story.",
                &author.username,
                &story.title
            ),
            Ok(Some(u)) => u.id,
        };

        let story_id = match actions::stories::find_story_by_author_and_url(
            author_id,
            &story.url_fragment,
            &models::filter::FilterMode::BypassFilters,
            conn,
        ) {
            Err(e) => err_ret!(
                "Error while looking up story '{}' by author {}: \
                 {}. Skipping this story.",
                &story.title,
                &author.username,
                e
            ),
            Ok(Some(s)) => {
                let update = models::stories::Story {
                    id: s.id,
                    story_internally_created_at: s.story_internally_created_at,
                    url_fragment: story.url_fragment.clone(),
                    title: story.title.clone(),
                    is_collaboration: false,
                    description: story.description,
                    // TODO : make this configurable
                    comments_enabled: false,
                    recommendations_enabled: models::stories::RecommendationsState::Enabled.into(),
                    is_multi_chapter: story.chapters.len() > 1,
                    cached_first_publication_date: s.cached_first_publication_date,
                    cached_last_update_date: s.cached_last_update_date,
                    ongoing_status: models::stories::OngoingState::NeverOngoing.into(),
                };
                if update != s {
                    if force {
                        warn!(
                            "Force-updating story {} by author {}. \
                             Old: {:?}; New: {:?}.",
                            &story.url_fragment, &author.username, s, &update
                        );
                        match actions::stories::modifications::update_story_title_desc(update, conn)
                        {
                            Err(e) => err_ret!(
                                "Error while updating story '{}' \
                                 by author {}: {}. \
                                 Skipping this story.",
                                &story.title,
                                &author.username,
                                e
                            ),
                            Ok(s) => s.id,
                        }
                    } else {
                        err_ret!(
                            "Trying to add story '{}' by author {}, \
                             but it conflicts with existing story {}. \
                             Skipping this story.",
                            &story.title,
                            &author.username,
                            &s.title
                        );
                    }
                } else {
                    warn!(
                        "Story '{}' by author {} already exists, \
                         proceeding to add chapters and tags to this story.",
                        &story.title, &author.username
                    );
                    s.id
                }
            }
            Ok(None) => {
                let ch1_content = match file_to_sanitized_html(story.chapters[0].file.clone()) {
                    Err(_) => err_ret!(
                        "Failed to read file {:?} \
                         containing chapter 1 of story '{}' \
                         by author {}. \
                         Skipping this story.",
                        &story.chapters[0].file,
                        &story.title,
                        &author.username,
                    ),
                    Ok(c) => c,
                };
                let ch1_word_count = count_words_from_html(&ch1_content);
                let new_story = models::stories::NewSingleAuthorStory {
                    author_id,
                    url_fragment: story.url_fragment.clone(),
                    title: story.title.clone(),
                    description: story.description,
                    foreword: foreword.clone(),
                    content: ch1_content,
                    word_count: ch1_word_count,
                    afterword: afterword.clone(),
                    // TODO : make this configurable
                    comments_enabled: true,
                    recommendations_enabled: models::stories::RecommendationsState::Enabled,
                    is_multi_chapter: story.chapters.len() > 1,
                    ongoing_status: intertextual::models::stories::OngoingState::NeverOngoing,
                };

                match actions::stories::modifications::create_new_single_author_story(
                    new_story, conn,
                ) {
                    Err(e) => err_ret!(
                        "Failed to add story '{}' by author {}: {}. \
                         Skipping this story.",
                        &story.title,
                        &author.username,
                        e
                    ),
                    Ok((s, _)) => {
                        info!(
                            "Added story '{}' by author {}",
                            &story.title, &author.username
                        );
                        s.id
                    }
                }
            }
        };

        let other_category = actions::tags::get_category_by_name("Other", conn);
        let other_category = match other_category {
            Ok(Some(category)) => category,
            _ => {
                err_ret!("Could not find default tag category 'Other' in case new tags are created")
            }
        };
        for tag in story.tags.into_iter() {
            let tag = match actions::tags::modifications::get_or_insert_tag(
                other_category.id,
                &tag,
                conn,
            ) {
                Err(e) => err_con!(
                    "Error while looking up tag {}: {}. \
                         Skipping this tag.",
                    &tag,
                    e
                ),
                Ok((t, _)) => t,
            };
            let tag_name = tag.internal_name.clone();
            let res = actions::tags::modifications::add_tag_to_story(
                story_id,
                tag,
                models::tags::TagAssociationType::Standard,
                conn,
            );
            if intertextual::utils::is_unique_violation(&res) {
                warn!(
                    "Story '{}' by author {} already has tag {}",
                    &story.title, &author.username, &tag_name
                );
                continue;
            }
            match res {
                Err(e) => err_con!(
                    "Error adding tag {} to story '{}' \
                     by author {}: {}. Skipping this tag.",
                    &tag_name,
                    &story.title,
                    &author.username,
                    e
                ),
                Ok(_) => info!(
                    "Added tag {} to story '{}' by author {}",
                    &tag_name, &story.title, &author.username
                ),
            }
        }

        for (i, chapter) in story.chapters.into_iter().enumerate() {
            let chnum: i32 = (i + 1).try_into().unwrap();

            let show_publicly_after_date = match author.pub_date {
                Some(ref p) => match chrono::NaiveDateTime::parse_from_str(p, "%Y-%m-%d %H:%M") {
                    Err(_) => err_ret!(
                        "Failed to parse publication date for \
                         chapter {} of story '{}' by author {}. \
                         Skipping this chapter and remaining \
                         chapters in this story.",
                        chnum,
                        &story.title,
                        &author.username
                    ),
                    Ok(dt) => Some(dt),
                },
                None => {
                    if pub_now {
                        Some(chrono::Utc::now().naive_utc())
                    } else {
                        None
                    }
                }
            };

            let content = match file_to_sanitized_html(chapter.file.clone()) {
                Err(_) => err_ret!(
                    "Failed to read file {:?} containing chapter {} \
                     of story '{}' by author {}.
                                Skipping this chapter and remaining chapters \
                                of this story.",
                    &chapter.file,
                    chnum,
                    &story.title,
                    &author.username,
                ),
                Ok(c) => c,
            };
            let word_count = count_words_from_html(&content);
            match actions::stories::find_chapter_by_author_url_and_number(
                author_id,
                &story.url_fragment,
                chnum,
                &models::filter::FilterMode::IncludeDirectAccess {
                    own_user_id: author_id,
                },
                conn,
            ) {
                Err(e) => err_ret!(
                    "Error while looking up chapter {} \
                     of story '{}' by author {}: {}. \
                     Skipping this chapter and remaining chapters \
                     of this story.",
                    chnum,
                    &story.title,
                    &author.username,
                    e
                ),
                Ok(Some((_, c))) => {
                    let update = models::stories::Chapter {
                        id: c.id,
                        story_id,
                        chapter_internally_created_at: c.chapter_internally_created_at,
                        chapter_number: chnum,
                        title: chapter.title,
                        foreword: foreword.clone(),
                        content,
                        word_count,
                        afterword: afterword.clone(),
                        show_publicly_after_date,
                        official_creation_date: c.official_creation_date,
                        last_edition_date: c.last_edition_date,
                        moderator_locked: c.moderator_locked,
                        moderator_locked_reason: c.moderator_locked_reason.clone(),
                    };
                    if update.content == c.content && c.title.is_none() && update.title.is_some() {
                        // if we created a new story, chapter 1 will always start
                        // out untitled and need titling, no need to make a fuss
                        if chnum != 1 {
                            warn!(
                                "Adding title {} to chapter {} of story '{}' \
                                 by author {}",
                                update.title.clone().unwrap(),
                                chnum,
                                &story.title,
                                &author.username
                            );
                        }
                    } else if update != c {
                        if force {
                            warn!(
                                "Force-updating chapter {} of story '{}' by \
                                 author {}",
                                chnum, &story.title, &author.username
                            );
                        } else {
                            error!(
                                "Trying to add chapter {} of story '{}' by \
                                 author {}, but it conflicts with an existing \
                                 version of the chapter. {}",
                                chnum,
                                &story.title,
                                &author.username,
                                if ignore {
                                    "Proceeding to next chapter."
                                } else {
                                    "Skipping this chapter and remaining \
                                     chapters of this story."
                                }
                            );
                            if ignore {
                                continue;
                            } else {
                                return;
                            }
                        }
                    } else {
                        warn!(
                            "Chapter {} of story '{}' by author {} already \
                             exists, proceeding to next chapter",
                            chnum, &story.title, &author.username
                        );
                        continue;
                    }
                    if let Err(e) =
                        actions::stories::modifications::update_chapter(update, false, conn)
                    {
                        err_ret!(
                            "Failed to update chapter {} \
                             of story '{}' by author {}: {}. \
                             Skipping this chapter and \
                             remaining chapters of this story.",
                            chnum,
                            &story.title,
                            &author.username,
                            e
                        );
                    }
                    if show_publicly_after_date.is_some() {
                        info!(
                            "Setting publication date of chapter {} of story \
                             '{}' by author {}.",
                            chnum, &story.title, &author.username
                        );
                        if let Err(e) = actions::stories::modifications::set_public_after(
                            c,
                            show_publicly_after_date,
                            conn,
                        ) {
                            err_ret!(
                                "Failed to set publication date of \
                                 chapter {} of story '{}' by author \
                                 {}: {}. Skipping this chapter and \
                                 remaining chapters of this story.",
                                chnum,
                                &story.title,
                                &author.username,
                                e
                            );
                        }
                    }
                }
                Ok(None) => {
                    if chnum == 1 {
                        warn!(
                            "Chapter 1 of story '{}' by author {} doesn't exist, \
                             even though story exists, which is unexpected. \
                             Proceeding to add chapter 1 from import spec.",
                            &story.title, &author.username
                        );
                    }
                    let new_chapter = models::stories::NewChapter {
                        story_id,
                        title: chapter.title,
                        foreword: foreword.clone(),
                        content,
                        word_count,
                        afterword: afterword.clone(),
                    };
                    let c = match actions::stories::modifications::create_new_chapter(
                        new_chapter,
                        conn,
                    ) {
                        Err(e) => err_ret!(
                            "Failed to add chapter {} \
                                 of story '{}' by author {}: {}. \
                                 Skipping this chapter and \
                                 remaining chapters of this story.",
                            chnum,
                            &story.title,
                            &author.username,
                            e
                        ),
                        Ok(c) => {
                            info!(
                                "Added chapter {} of story '{}' by author {}",
                                chnum, &story.title, &author.username
                            );
                            c
                        }
                    };
                    if show_publicly_after_date.is_some() {
                        if let Err(e) = actions::stories::modifications::set_public_after(
                            c,
                            show_publicly_after_date,
                            conn,
                        ) {
                            err_ret!(
                                "Failed to set publication date of \
                                 chapter {} of story '{}' by author \
                                 {}: {}. Skipping this chapter and \
                                 remaining chapters of this story.",
                                chnum,
                                &story.title,
                                &author.username,
                                e
                            );
                        }
                    }
                }
            }
        }
    }
}

pub fn import(
    parameters: ImportParameters,
    conn: &PgConnection,
) -> Result<(), models::error::IntertextualError> {
    let f = File::open(parameters.import_spec).expect("failed to open import spec file");
    let spec: Vec<ImportAuthor> = serde_yaml::from_reader(f).expect("failed to parse import spec");

    for author in spec.into_iter() {
        process_author(
            author,
            parameters.pub_now,
            parameters.force,
            parameters.ignore_chapter_conflicts,
            conn,
        );
    }

    Ok(())
}
