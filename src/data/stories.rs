//! # Stories retriever
//!
//! This module contains methods used to retrieve informations for stories and chapters
use diesel::PgConnection;
use models::error::IntertextualError;

use crate::actions;
use crate::models;
use crate::prelude::*;

/// Resolves parts of an author-based URL into a story, respecting the given `filter_mode`
pub fn find_author_story_by_url(
    author_username: String,
    story_url: String,
    filter_mode: &models::filter::FilterMode,
    conn: &PgConnection,
) -> Result<(Vec<models::users::User>, models::stories::Story), IntertextualError> {
    let author = actions::users::find_user_by_username(&author_username, filter_mode, conn)?;
    let author = match author {
        Some(author) => author,
        None => {
            return Err(IntertextualError::UserNotFound {
                username: author_username,
            })
        }
    };
    let story_data =
        actions::stories::find_story_by_author_and_url(author.id, &story_url, filter_mode, conn)?;
    let story = match story_data {
        Some(story_data) => story_data,
        None => {
            return Err(IntertextualError::AuthorStoryNotFound {
                author_username,
                url_fragment: story_url,
            })
        }
    };

    let authors = actions::stories::find_authors_by_story(story.id, filter_mode, conn)?;
    Ok((authors, story))
}

/// Resolves parts of a collaboration URL into a story, respecting the given `filter_mode`
pub fn find_collaboration_story_by_url(
    story_url: String,
    filter_mode: &models::filter::FilterMode,
    conn: &PgConnection,
) -> Result<(Vec<models::users::User>, models::stories::Story), IntertextualError> {
    let story_data =
        actions::stories::find_collaboration_story_by_url(&story_url, filter_mode, conn)?;
    let story = match story_data {
        Some(story_data) => story_data,
        None => {
            return Err(IntertextualError::CollaborationStoryNotFound {
                url_fragment: story_url,
            })
        }
    };
    let authors = actions::stories::find_authors_by_story(story.id, filter_mode, conn)?;
    Ok((authors, story))
}

/// Resolves parts of an author-based URL into a chapter, respecting the given `filter_mode`
pub fn find_author_chapter_by_url(
    author_username: String,
    story_url: String,
    chapter_number: i32,
    filter_mode: &models::filter::FilterMode,
    conn: &PgConnection,
) -> Result<
    (
        Vec<models::users::User>,
        models::stories::Story,
        models::stories::Chapter,
    ),
    IntertextualError,
> {
    let author = actions::users::find_user_by_username(&author_username, filter_mode, conn)?;
    let author = match author {
        Some(author) => author,
        None => {
            return Err(IntertextualError::UserNotFound {
                username: author_username,
            })
        }
    };
    let story_data = actions::stories::find_chapter_by_author_url_and_number(
        author.id,
        &story_url,
        chapter_number,
        filter_mode,
        conn,
    )?;
    let (story, chapter) = match story_data {
        Some(story_data) => story_data,
        None => {
            return Err(IntertextualError::AuthorChapterNotFound {
                author_username,
                url_fragment: story_url,
                chapter_number,
            })
        }
    };

    let authors = actions::stories::find_authors_by_story(story.id, filter_mode, conn)?;
    Ok((authors, story, chapter))
}

/// Resolves parts of a collaboration URL into a chapter, respecting the given `filter_mode`
pub fn find_collaboration_chapter_by_url(
    story_url: String,
    chapter_number: i32,
    filter_mode: &models::filter::FilterMode,
    conn: &PgConnection,
) -> Result<
    (
        Vec<models::users::User>,
        models::stories::Story,
        models::stories::Chapter,
    ),
    IntertextualError,
> {
    let story_data = actions::stories::find_collaboration_chapter_url_and_number(
        &story_url,
        chapter_number,
        filter_mode,
        conn,
    )?;
    let (story, chapter) = match story_data {
        Some(story_data) => story_data,
        None => {
            return Err(IntertextualError::CollaborationChapterNotFound {
                url_fragment: story_url,
                chapter_number,
            })
        }
    };
    let authors = actions::stories::find_authors_by_story(story.id, filter_mode, conn)?;
    Ok((authors, story, chapter))
}

pub enum PublicationAvailability {
    CanBePublished,
    BlockedByTags(Vec<BlockingTags>),
}

pub struct BlockingTags {
    pub display_name: String,
    pub blocked_message: Option<SanitizedHtml>,
}

pub fn story_publication_availability(
    story_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<PublicationAvailability, IntertextualError> {
    let tags = actions::tags::find_all_canonical_tags_by_story_id(story_id, conn)?;
    let mut blocked_tags: Vec<BlockingTags> = vec![];
    for (_, tag) in tags {
        if !tag.block_publication {
            continue;
        }
        blocked_tags.push(BlockingTags {
            display_name: tag.display_name,
            blocked_message: tag.block_publication_message.map(|m| m.html(conn)),
        });
    }

    if blocked_tags.is_empty() {
        Ok(PublicationAvailability::CanBePublished)
    } else {
        Ok(PublicationAvailability::BlockedByTags(blocked_tags))
    }
}
