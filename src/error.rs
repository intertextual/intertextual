use actix_web::{error::BlockingError, HttpResponse, ResponseError};
use thiserror::Error;

use intertextual::models::error::IntertextualError;

use crate::app::common_pages::*;
use crate::app::persistent::PersistentData;
use crate::app::persistent::PersistentTemplate;

#[derive(Error, Debug)]
pub enum AppError {
    #[error("{error}")]
    Io {
        error: std::io::Error,
        persistent: PersistentTemplate,
    },

    #[error("{error}")]
    Intertextual {
        error: IntertextualError,
        persistent: PersistentTemplate,
    },

    #[error("{error}")]
    Template {
        error: askama::Error,
        persistent: PersistentTemplate,
    },

    #[error("{error}")]
    Multipart {
        error: actix_multipart::MultipartError,
        persistent: PersistentTemplate,
    },

    #[error("The multipart form field was too long and could not be parsed")]
    MultipartFormFieldTooLong { persistent: PersistentTemplate },

    #[error("Blocking operation failed")]
    Blocking { persistent: PersistentTemplate },
}

pub trait IntoApp {
    fn into_app(self, data: &PersistentData) -> AppError;
}

pub trait MapApp<T> {
    fn map_err_app(self, data: &PersistentData) -> Result<T, AppError>;
}

impl<T, E: IntoApp + std::fmt::Debug> MapApp<T> for Result<T, E> {
    fn map_err_app(self, data: &PersistentData) -> Result<T, AppError> {
        self.map_err(|e| e.into_app(data))
    }
}

impl IntoApp for std::io::Error {
    fn into_app(self, data: &PersistentData) -> AppError {
        AppError::Io {
            error: self,
            persistent: data.into(),
        }
    }
}

impl IntoApp for IntertextualError {
    fn into_app(self, data: &PersistentData) -> AppError {
        AppError::Intertextual {
            error: self,
            persistent: data.into(),
        }
    }
}

impl IntoApp for askama::Error {
    fn into_app(self, data: &PersistentData) -> AppError {
        AppError::Template {
            error: self,
            persistent: data.into(),
        }
    }
}

impl IntoApp for actix_multipart::MultipartError {
    fn into_app(self, data: &PersistentData) -> AppError {
        AppError::Multipart {
            error: self,
            persistent: data.into(),
        }
    }
}

impl IntoApp for BlockingError {
    fn into_app(self, data: &PersistentData) -> AppError {
        AppError::Blocking {
            persistent: data.into(),
        }
    }
}

impl ResponseError for AppError {
    fn error_response(&self) -> HttpResponse {
        match self {
            AppError::Intertextual { error, persistent } => match error {
                IntertextualError::DBError(_) => {
                    eprintln!("{}", error);
                    internal_server_error(persistent.clone())
                }
                IntertextualError::DBPoolError(_) => {
                    eprintln!("{}", error);
                    internal_server_error(persistent.clone())
                }
                IntertextualError::FromUtf8Error(_) => {
                    eprintln!("{}", error);
                    internal_server_error(persistent.clone())
                }
                IntertextualError::IOError(_) => {
                    eprintln!("{}", error);
                    internal_server_error(persistent.clone())
                }
                IntertextualError::PandocError(_) => {
                    eprintln!("{}", error);
                    internal_server_error(persistent.clone())
                }
                IntertextualError::AccessRightsRequired => not_accessible(
                    persistent.clone(),
                    Some("You do not have enough rights to access this page".to_string()),
                ),
                IntertextualError::AccessRightsRequiredObfuscated => {
                    not_found(persistent.clone(), None)
                }
                IntertextualError::AuthorChapterNotFound {
                    author_username,
                    chapter_number,
                    url_fragment,
                } => not_found(
                    persistent.clone(),
                    Some(format!(
                        "The chapter {} of story {} from author @{} does not exist",
                        chapter_number, url_fragment, author_username
                    )),
                ),
                IntertextualError::UserNotFound { username } => not_found(
                    persistent.clone(),
                    Some(format!("The user @{} does not exist", username)),
                ),
                IntertextualError::AuthorStoryNotFound {
                    author_username,
                    url_fragment,
                } => not_found(
                    persistent.clone(),
                    Some(format!(
                        "The story {} from author @{} does not exist",
                        url_fragment, author_username
                    )),
                ),
                IntertextualError::CollaborationStoryNotFound { url_fragment } => not_found(
                    persistent.clone(),
                    Some(format!("The collaboration {} does not exist", url_fragment)),
                ),
                IntertextualError::CollaborationChapterNotFound {
                    url_fragment,
                    chapter_number,
                } => not_found(
                    persistent.clone(),
                    Some(format!(
                        "The chapter {} of collaboration {} does not exist",
                        chapter_number, url_fragment
                    )),
                ),
                IntertextualError::AuthorRecommendationNotFound {
                    recommender_username,
                    author_username,
                    url_fragment,
                } => not_found(
                    persistent.clone(),
                    Some(format!(
                        "The recommendation of story {} by @{} by user @{} does not exist",
                        url_fragment, author_username, recommender_username
                    )),
                ),
                IntertextualError::CollaborationRecommendationNotFound {
                    recommender_username,
                    url_fragment,
                } => not_found(
                    persistent.clone(),
                    Some(format!(
                        "The recommendation of collaboration {} by user @{} does not exist",
                        url_fragment, recommender_username
                    )),
                ),
                IntertextualError::CommentNotFound { comment_id } => not_found(
                    persistent.clone(),
                    Some(format!("The comment {} does not exist", comment_id)),
                ),
                IntertextualError::NotificationNotFound { notification_id } => not_found(
                    persistent.clone(),
                    Some(format!(
                        "The notification {} does not exist",
                        notification_id
                    )),
                ),
                IntertextualError::ModmailNotFound { modmail_id } => not_found(
                    persistent.clone(),
                    Some(format!(
                        "The moderation message {} does not exist",
                        modmail_id
                    )),
                ),
                IntertextualError::FormFieldFormatError {
                    form_field_name,
                    message,
                } => bad_request(
                    persistent.clone(),
                    Some(format!("Error for field {} : {}", form_field_name, message)),
                ),
                IntertextualError::LoginRequired => not_accessible(
                    persistent.clone(),
                    Some("You need to be logged in to access this page".to_string()),
                ),
                IntertextualError::LoginRequiredObfuscated => not_found(persistent.clone(), None),
                IntertextualError::InvalidPassword => not_accessible(
                    persistent.clone(),
                    Some("The provided password is incorrect".to_string()),
                ),
                IntertextualError::PageDoesNotExist => not_found(persistent.clone(), None),
                IntertextualError::InternalServerError => internal_server_error(persistent.clone()),
            },

            AppError::MultipartFormFieldTooLong { persistent } => {
                eprintln!("The multipart form field was longer than expected");
                bad_request(
                    persistent.clone(),
                    Some(
                        "Bad request : the multipart form field was bigger than the accepted size"
                            .to_string(),
                    ),
                )
            }

            AppError::Multipart { error, persistent } => {
                eprintln!("{}", error);
                bad_request(
                    persistent.clone(),
                    Some(
                        "Bad request : the multipart form entry had an invalid format".to_string(),
                    ),
                )
            }

            AppError::Io { error, persistent } => {
                eprintln!("{}", error);
                internal_server_error(persistent.clone())
            }

            AppError::Template { error, persistent } => {
                eprintln!("{}", error);
                internal_server_error(persistent.clone())
            }

            AppError::Blocking { persistent } => {
                eprintln!("Blocking operation failed");
                internal_server_error(persistent.clone())
            }
        }
    }
}
