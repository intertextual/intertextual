use actix_web::{get, web, HttpResponse};
use askama::Template;

use intertextual::models::shared::AppState;

use crate::app::persistent::PersistentData;
use crate::app::sitemap::SitemapEntry;
use crate::error::*;
use crate::prelude::*;

#[derive(Template)]
#[template(path = "sitemap.xml", escape = "none")]
struct SitemapTemplate {
    sitemap: Vec<SitemapEntry>,
}

#[get("/robots.txt/")]
async fn robots_txt(data: web::Data<AppState>) -> Result<HttpResponse, AppError> {
    let response = HttpResponse::Ok().content_type("text/plain").body(format!(
        "User-agent: *
Allow: /

Sitemap: {}/sitemap.xml
",
        data.site.base_url
    ));
    Ok(response)
}

#[get("/sitemap.xml/")]
async fn sitemap_xml(data: web::Data<AppState>) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_no_id(&data);
    let site = data.site.clone();
    let publication_date = data.default_publication_date.clone();
    let sitemap = db_action(&data.pool, &persistent, move |conn| {
        crate::app::sitemap::generate_sitemap_files(&site, &publication_date, &conn)
    })
    .await?;
    let s = SitemapTemplate { sitemap }
        .render()
        .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok()
        .content_type("application/xml; charset=utf-8")
        .body(s))
}
