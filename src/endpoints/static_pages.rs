use actix_web::{get, web, HttpRequest, HttpResponse};
use askama::Template;

use intertextual::actions::static_pages;

use crate::prelude::*;

#[derive(Template)]
#[template(path = "about.html")]
struct AboutPageTemplate {
    persistent: PersistentTemplate,
    has_contact: bool,
    has_faq: bool,
    has_legal: bool,
}

#[derive(Template)]
#[template(path = "static_page.html")]
struct StaticPageTemplate {
    persistent: PersistentTemplate,
    title: String,
    content: SanitizedHtml,
}

#[get("/about/")]
async fn about_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let (has_contact, has_faq, has_legal) = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<(bool, bool, bool), IntertextualError> {
            let contact =
                static_pages::get_static_page(static_pages::StaticPageKind::Contact, &conn)?;
            let faq = static_pages::get_static_page(static_pages::StaticPageKind::Faq, &conn)?;
            let legal = static_pages::get_static_page(static_pages::StaticPageKind::Legal, &conn)?;
            Ok((!contact.is_empty(), !faq.is_empty(), !legal.is_empty()))
        },
    )
    .await?;
    let s = AboutPageTemplate {
        persistent: PersistentTemplate::from(&persistent),
        has_contact,
        has_faq,
        has_legal,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[get("/contact/")]
async fn contact_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
) -> Result<HttpResponse, AppError> {
    static_page_shared(request, id, static_pages::StaticPageKind::Contact, data).await
}

#[get("/faq/")]
async fn faq_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
) -> Result<HttpResponse, AppError> {
    static_page_shared(request, id, static_pages::StaticPageKind::Faq, data).await
}

#[get("/legal/")]
async fn legal_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
) -> Result<HttpResponse, AppError> {
    static_page_shared(request, id, static_pages::StaticPageKind::Legal, data).await
}

#[get("/rules/")]
async fn rules_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
) -> Result<HttpResponse, AppError> {
    static_page_shared(request, id, static_pages::StaticPageKind::Rules, data).await
}

#[get("/terms_of_service/")]
async fn tos_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
) -> Result<HttpResponse, AppError> {
    static_page_shared(
        request,
        id,
        static_pages::StaticPageKind::TermsOfService,
        data,
    )
    .await
}

async fn static_page_shared(
    request: HttpRequest,
    id: Identity,
    kind: static_pages::StaticPageKind,
    data: web::Data<AppState>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;

    let content = db_action(&data.pool, &persistent, move |conn| {
        static_pages::get_static_page(kind, &conn)
    })
    .await?;

    let s = StaticPageTemplate {
        persistent: PersistentTemplate::from(&persistent),
        title: kind.title().to_string(),
        content,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}
