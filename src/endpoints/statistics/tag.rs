use actix_web::{get, web, HttpRequest, HttpResponse};
use askama::Template;

use intertextual::actions;
use intertextual::models;
use intertextual::models::error::IntertextualError;
use intertextual::models::filter::FilterMode;
use intertextual::models::shared::AppState;

use crate::app::identity::Identity;
use crate::app::persistent::*;
use crate::endpoints::statistics::shared::*;
use crate::error::*;
use crate::prelude::*;

#[derive(Template)]
#[template(path = "statistics/tag.html")]
struct TagStatisticsTemplate {
    persistent: PersistentTemplate,
    sort_mode: TagsSortByMode,
    user: models::users::User,
    tag_statistics: Vec<TagStatistics>,
}

#[get("/statistics/tags/")]
async fn main_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<StatisticsQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let username = {
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequired)
            .map_err_app(&persistent)?;
        login_user.username.clone()
    };
    tag_page_shared(data, persistent, username, url_params.tag_sort_by()).await
}

#[get("/statistics/tags/@{user}/")]
async fn user_specific_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    url_params: web::Query<StatisticsQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let username = path.into_inner();
    tag_page_shared(data, persistent, username, url_params.tag_sort_by()).await
}

async fn tag_page_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    username: String,
    sort_mode: TagsSortByMode,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let filter_mode = FilterMode::from_login(login_user);

    let user = {
        if login_user.username == username {
            let id = login_user.id;
            db_action(&data.pool, &persistent, move |conn| {
                actions::users::find_user_by_id(id, &conn)
            })
            .await?
            .ok_or(IntertextualError::UserNotFound { username })
            .map_err_app(&persistent)?
        } else {
            let username_copy = username.clone();
            let filter_mode = filter_mode.clone();
            db_action(&data.pool, &persistent, move |conn| {
                actions::users::find_user_by_username(&username_copy, &filter_mode, &conn)
            })
            .await?
            .ok_or(IntertextualError::UserNotFound { username })
            .map_err_app(&persistent)?
        }
    };

    login_user
        .require_see_statistics_rights_for(std::slice::from_ref(&user))
        .map_err_app(&persistent)?;

    let user_id = user.id;
    let mut tag_statistics = db_action(&data.pool, &persistent, move |conn| {
        get_tag_statistics(user_id, &filter_mode, &conn)
    })
    .await?;

    sort_mode.sort_list(&mut tag_statistics);

    let s = TagStatisticsTemplate {
        persistent: PersistentTemplate::from(&persistent),
        sort_mode,
        user,
        tag_statistics,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}
