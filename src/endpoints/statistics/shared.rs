use std::convert::TryFrom;

use serde::Deserialize;

use intertextual::actions;
use intertextual::models;
use intertextual::models::error::IntertextualError;
use intertextual::models::filter::FilterMode;

use crate::endpoints::statistics::graphs::GraphRangeMode;
use crate::prelude::*;

#[derive(Deserialize)]
pub struct StatisticsQuery {
    pub range: Option<String>,
    pub sort_by: Option<String>,
}

impl StatisticsQuery {
    pub fn range(&self) -> QueryRangeMode {
        match self.range.as_deref() {
            Some("hidden") => QueryRangeMode::Hidden,
            Some("daily") => QueryRangeMode::Daily,
            Some("weekly") => QueryRangeMode::Weekly,
            Some("monthly") => QueryRangeMode::Monthly,
            Some("100") => QueryRangeMode::Last100,
            Some("all") => QueryRangeMode::All,
            _ => QueryRangeMode::Hidden,
        }
    }

    pub fn user_sort_by(&self) -> UserSortByMode {
        UserSortByMode::parse_sort_by(&self.sort_by)
    }

    pub fn story_sort_by(&self) -> StorySortByMode {
        StorySortByMode::parse_sort_by(&self.sort_by)
    }

    pub fn tag_sort_by(&self) -> TagsSortByMode {
        TagsSortByMode::parse_sort_by(&self.sort_by)
    }
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum QueryRangeMode {
    Hidden,
    Daily,
    Weekly,
    Monthly,
    Last100,
    All,
}

impl QueryRangeMode {
    pub fn range(&self, min_start_date: Option<chrono::NaiveDate>) -> GraphRangeMode {
        match self {
            QueryRangeMode::Hidden => GraphRangeMode::Daily { days: 30 },
            QueryRangeMode::Daily => GraphRangeMode::Hourly { hours: 24 },
            QueryRangeMode::Weekly => GraphRangeMode::Daily { days: 7 },
            QueryRangeMode::Monthly => GraphRangeMode::Daily { days: 30 },
            QueryRangeMode::Last100 => GraphRangeMode::Daily { days: 100 },
            QueryRangeMode::All => match min_start_date {
                Some(start_date) => GraphRangeMode::DailyExact { start_date },
                None => GraphRangeMode::Daily { days: 100 },
            },
        }
    }

    pub fn current_amp(&self) -> &'static str {
        match self {
            QueryRangeMode::Hidden => "&range=hidden",
            QueryRangeMode::Daily => "&range=daily",
            QueryRangeMode::Weekly => "&range=weekly",
            QueryRangeMode::Monthly => "&range=monthly",
            QueryRangeMode::Last100 => "&range=100",
            QueryRangeMode::All => "&range=all",
        }
    }

    pub fn description(&self) -> &'static str {
        match self {
            QueryRangeMode::Hidden => "Hide Graphs",
            QueryRangeMode::Daily => "Last 24 hours",
            QueryRangeMode::Weekly => "Last 7 days",
            QueryRangeMode::Monthly => "Last 30 days",
            QueryRangeMode::Last100 => "Last 100 days",
            QueryRangeMode::All => "All time",
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum UserSortByMode {
    TitleAsc,
    TitleDesc,
    HitsAsc,
    HitsDesc,
    UserApprovalsAsc,
    UserApprovalsDesc,
    FollowsAsc,
    FollowsDesc,
    CommentsAsc,
    CommentsDesc,
    RecommendationsAsc,
    RecommendationsDesc,
}

impl UserSortByMode {
    pub fn parse_sort_by(sort_by: &Option<String>) -> UserSortByMode {
        match sort_by.as_deref() {
            Some(t) if t == "title_asc" => UserSortByMode::TitleAsc,
            Some(t) if t == "title_desc" => UserSortByMode::TitleDesc,
            Some(t) if t == "hits_asc" => UserSortByMode::HitsAsc,
            Some(t) if t == "hits_desc" => UserSortByMode::HitsDesc,
            Some(t) if t == "ua_asc" => UserSortByMode::UserApprovalsAsc,
            Some(t) if t == "ua_desc" => UserSortByMode::UserApprovalsDesc,
            Some(t) if t == "follows_asc" => UserSortByMode::FollowsAsc,
            Some(t) if t == "follows_desc" => UserSortByMode::FollowsDesc,
            Some(t) if t == "comms_asc" => UserSortByMode::CommentsAsc,
            Some(t) if t == "comms_desc" => UserSortByMode::CommentsDesc,
            Some(t) if t == "recs_asc" => UserSortByMode::RecommendationsAsc,
            Some(t) if t == "recs_desc" => UserSortByMode::RecommendationsDesc,
            _ => UserSortByMode::TitleAsc,
        }
    }

    pub fn next_title_sort(&self) -> &'static str {
        match self {
            Self::TitleAsc => "title_desc",
            _ => "title_asc",
        }
    }

    pub fn title_symbol(&self) -> &'static str {
        match self {
            Self::TitleAsc => "↓",
            Self::TitleDesc => "↑",
            _ => "",
        }
    }

    pub fn next_hits_sort(&self) -> &'static str {
        match self {
            Self::HitsDesc => "hits_asc",
            _ => "hits_desc",
        }
    }

    pub fn hits_symbol(&self) -> &'static str {
        match self {
            Self::HitsAsc => "↑",
            Self::HitsDesc => "↓",
            _ => "",
        }
    }

    pub fn next_ua_sort(&self) -> &'static str {
        match self {
            Self::UserApprovalsDesc => "ua_asc",
            _ => "ua_desc",
        }
    }

    pub fn ua_symbol(&self) -> &'static str {
        match self {
            Self::UserApprovalsAsc => "↑",
            Self::UserApprovalsDesc => "↓",
            _ => "",
        }
    }

    pub fn next_follows_sort(&self) -> &'static str {
        match self {
            Self::FollowsDesc => "follows_asc",
            _ => "follows_desc",
        }
    }

    pub fn follows_symbol(&self) -> &'static str {
        match self {
            Self::FollowsAsc => "↑",
            Self::FollowsDesc => "↓",
            _ => "",
        }
    }

    pub fn next_comms_sort(&self) -> &'static str {
        match self {
            Self::CommentsDesc => "comms_asc",
            _ => "comms_desc",
        }
    }

    pub fn comms_symbol(&self) -> &'static str {
        match self {
            Self::CommentsAsc => "↑",
            Self::CommentsDesc => "↓",
            _ => "",
        }
    }

    pub fn next_recs_sort(&self) -> &'static str {
        match self {
            Self::RecommendationsDesc => "recs_asc",
            _ => "recs_desc",
        }
    }

    pub fn recs_symbol(&self) -> &'static str {
        match self {
            Self::RecommendationsAsc => "↑",
            Self::RecommendationsDesc => "↓",
            _ => "",
        }
    }

    pub fn current_amp(&self) -> &'static str {
        match self {
            UserSortByMode::TitleAsc => "",
            UserSortByMode::TitleDesc => "&sort_by=title_desc",
            UserSortByMode::HitsAsc => "&sort_by=hits_asc",
            UserSortByMode::HitsDesc => "&sort_by=hits_desc",
            UserSortByMode::UserApprovalsAsc => "&sort_by=ua_asc",
            UserSortByMode::UserApprovalsDesc => "&sort_by=ua_desc",
            UserSortByMode::FollowsAsc => "&sort_by=follows_asc",
            UserSortByMode::FollowsDesc => "&sort_by=follows_desc",
            UserSortByMode::CommentsAsc => "&sort_by=comms_asc",
            UserSortByMode::CommentsDesc => "&sort_by=comms_desc",
            UserSortByMode::RecommendationsAsc => "&sort_by=recs_asc",
            UserSortByMode::RecommendationsDesc => "&sort_by=recs_desc",
        }
    }

    pub fn sort_list(&self, list: &mut Vec<(ShortStoryData, StoryStatistics)>) {
        match self {
            UserSortByMode::TitleAsc => list
                .sort_by(|(s1, _), (s2, _)| s1.title.to_lowercase().cmp(&s2.title.to_lowercase())),
            UserSortByMode::TitleDesc => list
                .sort_by(|(s1, _), (s2, _)| s2.title.to_lowercase().cmp(&s1.title.to_lowercase())),
            UserSortByMode::HitsAsc => {
                list.sort_by(|(_, s1), (_, s2)| s1.hit_count.cmp(&s2.hit_count))
            }
            UserSortByMode::HitsDesc => {
                list.sort_by(|(_, s1), (_, s2)| s2.hit_count.cmp(&s1.hit_count))
            }
            UserSortByMode::UserApprovalsAsc => {
                list.sort_by(|(_, s1), (_, s2)| s1.user_approval_count.cmp(&s2.user_approval_count))
            }
            UserSortByMode::UserApprovalsDesc => {
                list.sort_by(|(_, s1), (_, s2)| s2.user_approval_count.cmp(&s1.user_approval_count))
            }
            UserSortByMode::FollowsAsc => {
                list.sort_by(|(_, s1), (_, s2)| s1.unique_follow_count.cmp(&s2.unique_follow_count))
            }
            UserSortByMode::FollowsDesc => {
                list.sort_by(|(_, s1), (_, s2)| s2.unique_follow_count.cmp(&s1.unique_follow_count))
            }
            UserSortByMode::CommentsAsc => {
                list.sort_by(|(_, s1), (_, s2)| s1.comment_count.cmp(&s2.comment_count))
            }
            UserSortByMode::CommentsDesc => {
                list.sort_by(|(_, s1), (_, s2)| s2.comment_count.cmp(&s1.comment_count))
            }
            UserSortByMode::RecommendationsAsc => list
                .sort_by(|(_, s1), (_, s2)| s1.recommendation_count.cmp(&s2.recommendation_count)),
            UserSortByMode::RecommendationsDesc => list
                .sort_by(|(_, s1), (_, s2)| s2.recommendation_count.cmp(&s1.recommendation_count)),
        };
    }
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum StorySortByMode {
    ChapterAsc,
    ChapterDesc,
    HitsAsc,
    HitsDesc,
    CommentsAsc,
    CommentsDesc,
}

impl StorySortByMode {
    pub fn parse_sort_by(sort_by: &Option<String>) -> StorySortByMode {
        match sort_by.as_deref() {
            Some(t) if t == "chapter_asc" => StorySortByMode::ChapterAsc,
            Some(t) if t == "chapter_desc" => StorySortByMode::ChapterDesc,
            Some(t) if t == "hits_asc" => StorySortByMode::HitsAsc,
            Some(t) if t == "hits_desc" => StorySortByMode::HitsDesc,
            Some(t) if t == "comms_asc" => StorySortByMode::CommentsAsc,
            Some(t) if t == "comms_desc" => StorySortByMode::CommentsDesc,
            _ => StorySortByMode::ChapterAsc,
        }
    }

    pub fn next_chapter_sort(&self) -> &'static str {
        match self {
            Self::ChapterAsc => "chapter_desc",
            _ => "chapter_asc",
        }
    }

    pub fn chapter_symbol(&self) -> &'static str {
        match self {
            Self::ChapterAsc => "↓",
            Self::ChapterDesc => "↑",
            _ => "",
        }
    }

    pub fn next_hits_sort(&self) -> &'static str {
        match self {
            Self::HitsDesc => "hits_asc",
            _ => "hits_desc",
        }
    }

    pub fn hits_symbol(&self) -> &'static str {
        match self {
            Self::HitsAsc => "↑",
            Self::HitsDesc => "↓",
            _ => "",
        }
    }

    pub fn next_comms_sort(&self) -> &'static str {
        match self {
            Self::CommentsDesc => "comms_asc",
            _ => "comms_desc",
        }
    }

    pub fn comms_symbol(&self) -> &'static str {
        match self {
            Self::CommentsAsc => "↑",
            Self::CommentsDesc => "↓",
            _ => "",
        }
    }

    pub fn current_amp(&self) -> &'static str {
        match self {
            StorySortByMode::ChapterAsc => "",
            StorySortByMode::ChapterDesc => "&sort_by=chapter_desc",
            StorySortByMode::HitsAsc => "&sort_by=hits_asc",
            StorySortByMode::HitsDesc => "&sort_by=hits_desc",
            StorySortByMode::CommentsAsc => "&sort_by=comms_asc",
            StorySortByMode::CommentsDesc => "&sort_by=comms_desc",
        }
    }

    pub fn sort_list(&self, list: &mut Vec<ChapterStatistics>) {
        match self {
            StorySortByMode::ChapterAsc => {
                list.sort_by(|c1, c2| c1.chapter.chapter_number.cmp(&c2.chapter.chapter_number))
            }
            StorySortByMode::ChapterDesc => {
                list.sort_by(|c1, c2| c2.chapter.chapter_number.cmp(&c1.chapter.chapter_number))
            }
            StorySortByMode::HitsAsc => list.sort_by(|c1, c2| c1.hits.cmp(&c2.hits)),
            StorySortByMode::HitsDesc => list.sort_by(|c1, c2| c2.hits.cmp(&c1.hits)),
            StorySortByMode::CommentsAsc => list.sort_by(|c1, c2| c1.comments.cmp(&c2.comments)),
            StorySortByMode::CommentsDesc => list.sort_by(|c1, c2| c2.comments.cmp(&c1.comments)),
        };
    }
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum TagsSortByMode {
    TitleAsc,
    TitleDesc,
    StoriesAsc,
    StoriesDesc,
    HitsAsc,
    HitsDesc,
    HitsAverageAsc,
    HitsAverageDesc,
    UserApprovalsAsc,
    UserApprovalsDesc,
    UserApprovalsAverageAsc,
    UserApprovalsAverageDesc,
}

impl TagsSortByMode {
    pub fn parse_sort_by(sort_by: &Option<String>) -> TagsSortByMode {
        match sort_by.as_deref() {
            Some(t) if t == "title_asc" => TagsSortByMode::TitleAsc,
            Some(t) if t == "title_desc" => TagsSortByMode::TitleDesc,
            Some(t) if t == "stories_asc" => TagsSortByMode::StoriesAsc,
            Some(t) if t == "stories_desc" => TagsSortByMode::StoriesDesc,
            Some(t) if t == "hits_asc" => TagsSortByMode::HitsAsc,
            Some(t) if t == "hits_desc" => TagsSortByMode::HitsDesc,
            Some(t) if t == "hits_avg_asc" => TagsSortByMode::HitsAverageAsc,
            Some(t) if t == "hits_avg_desc" => TagsSortByMode::HitsAverageDesc,
            Some(t) if t == "ua_asc" => TagsSortByMode::UserApprovalsAsc,
            Some(t) if t == "ua_desc" => TagsSortByMode::UserApprovalsDesc,
            Some(t) if t == "ua_avg_asc" => TagsSortByMode::UserApprovalsAverageAsc,
            Some(t) if t == "ua_avg_desc" => TagsSortByMode::UserApprovalsAverageDesc,
            _ => TagsSortByMode::HitsAverageDesc,
        }
    }

    pub fn next_title_sort(&self) -> &'static str {
        match self {
            Self::TitleAsc => "title_desc",
            _ => "title_asc",
        }
    }

    pub fn title_symbol(&self) -> &'static str {
        match self {
            Self::TitleAsc => "↓",
            Self::TitleDesc => "↑",
            _ => "",
        }
    }

    pub fn next_stories_sort(&self) -> &'static str {
        match self {
            Self::StoriesDesc => "stories_asc",
            _ => "stories_desc",
        }
    }

    pub fn stories_symbol(&self) -> &'static str {
        match self {
            Self::StoriesAsc => "↑",
            Self::StoriesDesc => "↓",
            _ => "",
        }
    }

    pub fn next_hits_sort(&self) -> &'static str {
        match self {
            Self::HitsDesc => "hits_asc",
            _ => "hits_desc",
        }
    }

    pub fn hits_symbol(&self) -> &'static str {
        match self {
            Self::HitsAsc => "↑",
            Self::HitsDesc => "↓",
            _ => "",
        }
    }

    pub fn next_hits_avg_sort(&self) -> &'static str {
        match self {
            Self::HitsAverageDesc => "hits_avg_asc",
            _ => "hits_avg_desc",
        }
    }

    pub fn hits_avg_symbol(&self) -> &'static str {
        match self {
            Self::HitsAverageAsc => "↑",
            Self::HitsAverageDesc => "↓",
            _ => "",
        }
    }

    pub fn next_ua_sort(&self) -> &'static str {
        match self {
            Self::UserApprovalsDesc => "ua_asc",
            _ => "ua_desc",
        }
    }

    pub fn ua_symbol(&self) -> &'static str {
        match self {
            Self::UserApprovalsAsc => "↑",
            Self::UserApprovalsDesc => "↓",
            _ => "",
        }
    }

    pub fn next_ua_avg_sort(&self) -> &'static str {
        match self {
            Self::UserApprovalsAverageDesc => "ua_avg_asc",
            _ => "ua_avg_desc",
        }
    }

    pub fn ua_avg_symbol(&self) -> &'static str {
        match self {
            Self::UserApprovalsAverageAsc => "↑",
            Self::UserApprovalsAverageDesc => "↓",
            _ => "",
        }
    }

    #[allow(dead_code)]
    pub fn current_amp(&self) -> &'static str {
        match self {
            TagsSortByMode::TitleAsc => "",
            TagsSortByMode::TitleDesc => "&sort_by=title_desc",
            TagsSortByMode::StoriesAsc => "&sort_by=stories_asc",
            TagsSortByMode::StoriesDesc => "&sort_by=stories_desc",
            TagsSortByMode::HitsAsc => "&sort_by=hits_asc",
            TagsSortByMode::HitsDesc => "&sort_by=hits_desc",
            TagsSortByMode::HitsAverageAsc => "&sort_by=hits_avg_asc",
            TagsSortByMode::HitsAverageDesc => "&sort_by=hits_avg_desc",
            TagsSortByMode::UserApprovalsAsc => "&sort_by=ua_asc",
            TagsSortByMode::UserApprovalsDesc => "&sort_by=ua_desc",
            TagsSortByMode::UserApprovalsAverageAsc => "&sort_by=ua_avg_asc",
            TagsSortByMode::UserApprovalsAverageDesc => "&sort_by=ua_avg_desc",
        }
    }

    pub fn sort_list(&self, list: &mut Vec<TagStatistics>) {
        match self {
            TagsSortByMode::TitleAsc => list.sort_by(|t1, t2| {
                t1.display_name
                    .to_lowercase()
                    .cmp(&t2.display_name.to_lowercase())
            }),
            TagsSortByMode::TitleDesc => list.sort_by(|t1, t2| {
                t2.display_name
                    .to_lowercase()
                    .cmp(&t1.display_name.to_lowercase())
            }),
            TagsSortByMode::StoriesAsc => {
                list.sort_by(|t1, t2| t1.story_count.cmp(&t2.story_count))
            }
            TagsSortByMode::StoriesDesc => {
                list.sort_by(|t1, t2| t2.story_count.cmp(&t1.story_count))
            }
            TagsSortByMode::HitsAsc => list.sort_by(|t1, t2| t1.hit_count.cmp(&t2.hit_count)),
            TagsSortByMode::HitsDesc => list.sort_by(|t1, t2| t2.hit_count.cmp(&t1.hit_count)),
            TagsSortByMode::HitsAverageAsc => list.sort_by(|t1, t2| {
                t1.hit_count_avg
                    .partial_cmp(&t2.hit_count_avg)
                    .unwrap_or(std::cmp::Ordering::Less)
            }),
            TagsSortByMode::HitsAverageDesc => list.sort_by(|t1, t2| {
                t2.hit_count_avg
                    .partial_cmp(&t1.hit_count_avg)
                    .unwrap_or(std::cmp::Ordering::Less)
            }),
            TagsSortByMode::UserApprovalsAsc => {
                list.sort_by(|t1, t2| t1.user_approval_count.cmp(&t2.user_approval_count))
            }
            TagsSortByMode::UserApprovalsDesc => {
                list.sort_by(|t1, t2| t2.user_approval_count.cmp(&t1.user_approval_count))
            }
            TagsSortByMode::UserApprovalsAverageAsc => list.sort_by(|t1, t2| {
                t1.user_approval_count_avg
                    .partial_cmp(&t2.user_approval_count_avg)
                    .unwrap_or(std::cmp::Ordering::Less)
            }),
            TagsSortByMode::UserApprovalsAverageDesc => list.sort_by(|t1, t2| {
                t2.user_approval_count_avg
                    .partial_cmp(&t1.user_approval_count_avg)
                    .unwrap_or(std::cmp::Ordering::Less)
            }),
        };
    }
}

pub struct ShortStoryData {
    pub author_path: String,
    pub url_fragment: String,
    pub title: String,
}

pub struct GraphEntries {
    pub hits: Vec<chrono::NaiveDateTime>,
    pub user_approvals: Vec<chrono::NaiveDateTime>,
    pub comments: Vec<chrono::NaiveDateTime>,
    pub recommendations: Vec<chrono::NaiveDateTime>,
}

impl GraphEntries {
    pub fn min_time(&self) -> Option<chrono::NaiveDate> {
        self.hits
            .iter()
            .chain(self.user_approvals.iter())
            .chain(self.comments.iter())
            .chain(self.recommendations.iter())
            .min()
            .map(|t| t.date())
    }
}

pub struct StoryStatistics {
    pub chapter_count: i64,
    pub hit_count: i64,
    pub hit_count_avg: f64,
    pub user_approval_count: i64,
    pub total_follow_count: i64,
    pub unique_follow_count: i64,
    pub comment_count: i64,
    pub recommendation_count: i64,
    pub word_count: i64,
}

pub struct ChapterStatistics {
    pub chapter: models::stories::ChapterMetadata,
    pub title: String,
    pub hits: i64,
    pub comments: i64,
}

pub fn get_story_statistics_and_graphs(
    story: &models::stories::Story,
    filter_mode: &FilterMode,
    exclude_graphs: bool,
    conn: &diesel::pg::PgConnection,
) -> Result<
    (
        StoryStatistics,
        Vec<ChapterStatistics>,
        Option<GraphEntries>,
    ),
    IntertextualError,
> {
    let raw_chapters =
        actions::stories::find_chapters_metadata_by_story_id(story.id, filter_mode, conn)?;
    let user_approvals = actions::user_approvals::find_user_approvals_through_time(
        std::slice::from_ref(&story.id),
        conn,
    )?;
    let recommendations: Vec<chrono::NaiveDateTime> =
        actions::recommendations::find_recommendations_through_time(
            std::slice::from_ref(&story.id),
            conn,
        )?;

    let total_follow_count =
        actions::follows::author_private::get_combined_follow_count_by_story(story.id, conn)?;
    let unique_follow_count =
        actions::follows::author_private::get_unique_follow_count_by_story(story.id, conn)?;
    let mut total_hits: Vec<chrono::NaiveDateTime> = vec![];
    let mut total_comments: Vec<chrono::NaiveDateTime> = vec![];
    let mut chapter_statistics = Vec::with_capacity(raw_chapters.len());
    let mut word_count = 0;
    for chapter in raw_chapters {
        let chapter_id = chapter.id;
        let mut chapter_hits =
            actions::hits::find_hits_through_time(std::slice::from_ref(&chapter_id), conn)?;
        let mut comments =
            actions::comments::find_comments_through_time(std::slice::from_ref(&chapter_id), conn)?;
        let chapter_hits_count = i64::try_from(chapter_hits.len()).unwrap_or(0);
        let chapter_comments_count = i64::try_from(comments.len()).unwrap_or(0);
        word_count += i64::from(chapter.word_count);
        let title = chapter.chapter_title_or_number(true);
        chapter_statistics.push(ChapterStatistics {
            chapter,
            title,
            hits: chapter_hits_count,
            comments: chapter_comments_count,
        });
        total_hits.append(&mut chapter_hits);
        total_comments.append(&mut comments);
    }

    let hit_count = i64::try_from(total_hits.len()).unwrap_or(0);
    let chapter_count = i64::try_from(chapter_statistics.len()).unwrap_or(0);
    let story_statistics = StoryStatistics {
        chapter_count,
        hit_count,
        hit_count_avg: (hit_count as f64) / (chapter_count as f64),
        user_approval_count: i64::try_from(user_approvals.len()).unwrap_or(0),
        total_follow_count,
        unique_follow_count,
        comment_count: i64::try_from(total_comments.len()).unwrap_or(0),
        recommendation_count: i64::try_from(recommendations.len()).unwrap_or(0),
        word_count,
    };

    Ok((
        story_statistics,
        chapter_statistics,
        if exclude_graphs {
            None
        } else {
            Some(GraphEntries {
                hits: total_hits,
                comments: total_comments,
                recommendations,
                user_approvals,
            })
        },
    ))
}

pub fn get_story_statistics(
    story: &models::stories::Story,
    filter_mode: &FilterMode,
    conn: &diesel::pg::PgConnection,
) -> Result<StoryStatistics, IntertextualError> {
    let raw_chapters =
        actions::stories::find_chapters_metadata_by_story_id(story.id, filter_mode, conn)?;
    let user_approval_count =
        actions::user_approvals::total_user_approval_count_for_story(story.id, conn)?;
    let recommendations: Vec<chrono::NaiveDateTime> =
        actions::recommendations::find_recommendations_through_time(
            std::slice::from_ref(&story.id),
            conn,
        )?;

    let mut word_count = 0;
    let mut chapter_ids = Vec::with_capacity(raw_chapters.len());
    let chapter_count = i64::try_from(raw_chapters.len()).unwrap_or(0);
    for chapter in raw_chapters {
        word_count += i64::from(chapter.word_count);
        chapter_ids.push(chapter.id);
    }

    let total_follow_count =
        actions::follows::author_private::get_combined_follow_count_by_story(story.id, conn)?;
    let unique_follow_count =
        actions::follows::author_private::get_unique_follow_count_by_story(story.id, conn)?;
    let hit_count = actions::hits::find_hit_counts_from_several(&chapter_ids, conn)?;
    let comment_count = actions::comments::find_comment_quantity_for_chapters(&chapter_ids, conn)?;
    let story_statistics = StoryStatistics {
        chapter_count,
        hit_count,
        hit_count_avg: (hit_count as f64) / (chapter_count as f64),
        user_approval_count,
        total_follow_count,
        unique_follow_count,
        comment_count,
        recommendation_count: i64::try_from(recommendations.len()).unwrap_or(0),
        word_count,
    };

    Ok(story_statistics)
}

pub struct AuthorStatistics {
    pub total_hit_count: i64,
    pub user_approval_count: i64,
    pub follow_count: i64,
    pub comments_count: i64,
    pub recommendation_count: i64,
    pub word_count: i64,
}

type AuthorStatisticsResult = (
    AuthorStatistics,
    Vec<(ShortStoryData, StoryStatistics)>,
    Option<GraphEntries>,
);

fn get_per_author_graph_contents(
    all_chapter_ids: &[uuid::Uuid],
    story_ids: &[uuid::Uuid],
    conn: &diesel::pg::PgConnection,
) -> Result<GraphEntries, IntertextualError> {
    Ok(GraphEntries {
        hits: actions::hits::find_hits_through_time(&all_chapter_ids, &conn)?,
        user_approvals: actions::user_approvals::find_user_approvals_through_time(
            &story_ids, &conn,
        )?,
        comments: actions::comments::find_comments_through_time(&all_chapter_ids, &conn)?,
        recommendations: actions::recommendations::find_recommendations_through_time(
            &story_ids, &conn,
        )?,
    })
}

pub fn get_author_statistics(
    user_id: uuid::Uuid,
    filter_mode: &FilterMode,
    exclude_graphs: bool,
    conn: &diesel::pg::PgConnection,
) -> Result<AuthorStatisticsResult, IntertextualError> {
    let stories_by_author =
        actions::stories::find_all_stories_by_author(user_id, filter_mode, conn)?;
    let number_of_stories = stories_by_author.len();
    let mut story_statistics = Vec::with_capacity(number_of_stories);
    let mut story_ids: Vec<uuid::Uuid> = Vec::with_capacity(number_of_stories);
    let mut all_chapter_ids: Vec<uuid::Uuid> = Vec::with_capacity(number_of_stories);
    for story in stories_by_author {
        let authors = actions::stories::find_authors_by_story(story.id, filter_mode, conn)?;
        let raw_chapters =
            actions::stories::find_chapters_metadata_by_story_id(story.id, filter_mode, conn)?;

        let mut chapter_ids = Vec::with_capacity(raw_chapters.len());
        let chapter_count = i64::try_from(raw_chapters.len()).unwrap_or(0);
        let mut word_count = 0;
        for chapter in raw_chapters {
            word_count += i64::from(chapter.word_count);
            chapter_ids.push(chapter.id);
        }

        let total_follow_count =
            actions::follows::author_private::get_combined_follow_count_by_story(story.id, conn)?;
        let unique_follow_count =
            actions::follows::author_private::get_unique_follow_count_by_story(story.id, conn)?;

        let user_approval_count =
            actions::user_approvals::total_user_approval_count_for_story(story.id, conn)?;
        let recommendations: Vec<chrono::NaiveDateTime> =
            actions::recommendations::find_recommendations_through_time(
                std::slice::from_ref(&story.id),
                conn,
            )?;
        let hit_count = actions::hits::find_hit_counts_from_several(&chapter_ids, conn)?;
        let comment_count =
            actions::comments::find_comment_quantity_for_chapters(&chapter_ids, conn)?;
        story_statistics.push((
            ShortStoryData {
                author_path: story.author_path(&authors),
                url_fragment: story.url_fragment,
                title: story.title,
            },
            StoryStatistics {
                chapter_count,
                hit_count,
                hit_count_avg: (hit_count as f64) / (chapter_count as f64),
                user_approval_count,
                total_follow_count,
                unique_follow_count,
                comment_count,
                recommendation_count: i64::try_from(recommendations.len()).unwrap_or(0),
                word_count,
            },
        ));
        story_ids.push(story.id);
        all_chapter_ids.append(&mut chapter_ids);
    }

    let total_graphs = if exclude_graphs {
        None
    } else {
        Some(get_per_author_graph_contents(
            &all_chapter_ids,
            &story_ids,
            &conn,
        )?)
    };
    let author_follow_count =
        actions::follows::author_private::get_author_follow_count(user_id, conn)?;

    let mut total_hit_count: i64 = 0;
    let mut user_approval_count: i64 = 0;
    let mut comments_count: i64 = 0;
    let mut recommendation_count: i64 = 0;
    let mut word_count: i64 = 0;
    for (_, s) in story_statistics.iter() {
        total_hit_count += s.hit_count;
        user_approval_count += s.user_approval_count;
        comments_count += s.comment_count;
        recommendation_count += s.recommendation_count;
        word_count += s.word_count;
    }
    let statistics = AuthorStatistics {
        total_hit_count,
        user_approval_count,
        follow_count: author_follow_count,
        comments_count,
        recommendation_count,
        word_count,
    };
    Ok((statistics, story_statistics, total_graphs))
}

pub struct TagStatistics {
    pub display_name: String,
    pub story_count: i64,
    pub chapter_count: i64,
    pub hit_count: i64,
    pub hit_count_avg: f64,
    pub user_approval_count: i64,
    pub user_approval_count_avg: f64,
}

pub fn get_tag_statistics(
    user_id: uuid::Uuid,
    filter_mode: &FilterMode,
    conn: &diesel::pg::PgConnection,
) -> Result<Vec<TagStatistics>, IntertextualError> {
    let mut tag_statistics: Vec<TagStatistics> = vec![];
    for story in actions::stories::find_all_stories_by_author(user_id, filter_mode, conn)? {
        let statistics = get_story_statistics(&story, filter_mode, conn)?;
        let story_tags =
            intertextual::actions::tags::find_all_canonical_tags_by_story_id(story.id, conn)?;
        for (_, tag) in story_tags {
            let entry = {
                if let Some(tag) = tag_statistics
                    .iter_mut()
                    .find(|t| t.display_name == tag.display_name)
                {
                    tag
                } else {
                    tag_statistics.push(TagStatistics {
                        display_name: tag.display_name,
                        story_count: 0,
                        chapter_count: 0,
                        hit_count: 0,
                        hit_count_avg: 0f64,
                        user_approval_count: 0,
                        user_approval_count_avg: 0f64,
                    });
                    tag_statistics
                        .last_mut()
                        .ok_or(IntertextualError::InternalServerError)?
                }
            };
            entry.chapter_count += statistics.chapter_count;
            entry.story_count += 1;
            entry.hit_count += statistics.hit_count;
            entry.user_approval_count += statistics.user_approval_count;
        }
    }

    for stat in tag_statistics.iter_mut() {
        let story_count = stat.story_count as f64;
        let chapter_count = stat.chapter_count as f64;
        let user_approval_count = stat.user_approval_count as f64;
        let hit_count = stat.hit_count as f64;
        stat.hit_count_avg = hit_count / chapter_count;
        stat.user_approval_count_avg = user_approval_count / story_count;
    }

    tag_statistics.sort_by(|tag1, tag2| {
        tag2.hit_count_avg
            .partial_cmp(&tag1.hit_count_avg)
            .unwrap_or(std::cmp::Ordering::Less)
    });

    Ok(tag_statistics)
}
