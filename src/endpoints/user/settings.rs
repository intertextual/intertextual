use actix_web::http::header::LOCATION;
use actix_web::{get, post, web, HttpRequest, HttpResponse};
use argonautica::{Hasher, Verifier};
use askama::Template;
use intertextual::models::tags::TagHighlightRule;
use serde::{Deserialize, Serialize};

use intertextual::actions;
use intertextual::models;
use intertextual::models::filter::FilterMode;
use intertextual::models::notifications::NotificationCategory;
use intertextual::models::users::NotificationLevel;
use intertextual::models::users::ShortUserEntry;
use intertextual::models::users::UserSettings;

use crate::app::validation::validate_display_name;
use crate::app::validation::validate_email;
use crate::app::validation::validate_password;
use crate::app::validation::validate_user_details;
use crate::prelude::*;

#[derive(Template)]
#[template(path = "user/settings.html")]
struct UserSettingsTemplate {
    persistent: PersistentTemplate,
    user: UserSettingsData,
    account_deletion_request: Option<(chrono::NaiveDateTime, bool)>,
    active_tokens: Vec<AuthTokenEntry>,
    excluded_tags: String,
    excluded_authors: Vec<ShortUserEntry>,
    current_tag_highlight_rules: Vec<(usize, TagHighlightRule)>,
    available_tag_highlight_rules_templates: Vec<(String, String)>,
    show_informations_confirmation: bool,
    show_notifications_confirmation: bool,
    show_excluded_tags_confirmation: bool,
    show_author_excluded_confirmation: bool,
    show_author_unexcluded_confirmation: bool,
    show_publication_confirmation: bool,
    show_interaction_confirmation: bool,
    show_author_notes_confirmation: bool,
    show_token_removed_confirmation: bool,
    show_password_changed_confirmation: bool,
    show_activate_confirmation: bool,
    show_deactivate_confirmation: bool,
    show_delete_confirmation: bool,
    show_cancel_delete_confirmation: bool,
    show_tag_highlight_rules_update_confirmation: bool,
}

#[derive(Template)]
#[template(path = "user/password_reset.html")]
struct UserPasswordResetTemplate {
    persistent: PersistentTemplate,
    user: UserSettingsData,
}

struct UserSettingsData {
    username: String,
    display_name: String,
    email: String,
    details: SanitizedHtml,
    settings: models::users::UserSettings,
    notification_settings: Vec<NotificationSettingEntry>,
    default_foreword: SanitizedHtml,
    default_afterword: SanitizedHtml,
    deactivated_by_user: bool,
    banned_by_moderator: bool,
}

struct NotificationSettingEntry {
    category: NotificationCategory,
    level: NotificationLevel,
}

struct AuthTokenEntry {
    name: String,
    last_visit: String,
}

impl NotificationSettingEntry {
    fn new(category: NotificationCategory, settings: &UserSettings) -> NotificationSettingEntry {
        NotificationSettingEntry {
            category,
            level: settings
                .notification_settings
                .get(&category)
                .copied()
                .unwrap_or_else(|| models::users::User::default_notification_level(category)),
        }
    }
}

impl From<models::users::User> for UserSettingsData {
    fn from(model: models::users::User) -> Self {
        let settings = model.get_settings();
        let notification_settings = NotificationCategory::categories_with_settings()
            .iter()
            .map(|&category| NotificationSettingEntry::new(category, &settings))
            .collect();
        UserSettingsData {
            username: model.username,
            display_name: model.display_name,
            email: model.email.unwrap_or_default(),
            details: model.details.unwrap_or_else(SanitizedHtml::new),
            settings,
            notification_settings,
            default_foreword: model.default_foreword,
            default_afterword: model.default_afterword,
            deactivated_by_user: model.deactivated_by_user,
            banned_by_moderator: model
                .banned_until
                .filter(|b| b > &chrono::Utc::now().naive_utc())
                .is_some(),
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct SettingsPageQuery {
    pub confirm: Option<String>,
}

#[derive(Serialize, Deserialize)]
struct UpdateInformationParams {
    pub display_name: String,
    pub email: String,
    pub details: SanitizedHtml,
}

#[derive(Serialize, Deserialize)]
struct UpdateNotificationsParams {
    #[serde(flatten)]
    notifications_entries: std::collections::BTreeMap<String, String>,
}

#[derive(Serialize, Deserialize)]
struct ExcludeAuthorParams {
    pub author_name: String,
}

#[derive(Serialize, Deserialize)]
struct UnexcludeAuthorParams {
    pub author_name: String,
}

#[derive(Serialize, Deserialize)]
struct UpdateExcludedTagsParams {
    pub excluded_tags: String,
}

#[derive(Serialize, Deserialize)]
struct UpdateDefaultInteractionParams {
    pub comments: String,
    pub recommendations: String,
}

#[derive(Serialize, Deserialize)]
struct UpdateDefaultPublicationParams {
    pub default_publication: String,
    pub day: String,
    pub hour: String,
}

#[derive(Serialize, Deserialize)]
struct UpdateDefaultNotesParams {
    pub default_foreword: SanitizedHtml,
    pub default_afterword: SanitizedHtml,
}

#[derive(Serialize, Deserialize)]
struct DeleteTokenParams {
    pub token_name: String,
}

#[derive(Serialize, Deserialize)]
struct UpdatePasswordParams {
    pub old_password: String,
    pub password: String,
    pub password_confirm: String,
}

#[derive(Serialize, Deserialize)]
struct ResetPasswordParams {
    pub password: String,
    pub password_confirm: String,
}

#[derive(Serialize, Deserialize)]
struct ActivationAccountParams {
    pub old_password: String,
}

#[derive(Serialize, Deserialize)]
struct DeleteAccountParams {
    pub old_password: String,
    pub confirm_deletion: Option<String>,
}

#[get("/settings/")]
async fn main_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<SettingsPageQuery>,
) -> Result<HttpResponse, AppError> {
    let params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let username = {
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequired)
            .map_err_app(&persistent)?;
        login_user.username.clone()
    };
    shared_page(data, persistent, username, params.confirm).await
}

#[get("/settings/@{user}/")]
async fn specific_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    url_params: web::Query<SettingsPageQuery>,
) -> Result<HttpResponse, AppError> {
    let params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let username = path.into_inner();
    shared_page(data, persistent, username, params.confirm).await
}

async fn shared_page(
    data: web::Data<AppState>,
    persistent: PersistentData,
    username: String,
    confirm: Option<String>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let filter_mode = FilterMode::from_login(login_user);

    let user = {
        if login_user.username == username {
            let id = login_user.id;
            db_action(&data.pool, &persistent, move |conn| {
                actions::users::find_user_by_id(id, &conn)
            })
            .await?
            .ok_or(IntertextualError::UserNotFound { username })
            .map_err_app(&persistent)?
        } else {
            let username_copy = username.clone();
            let filter_mode = filter_mode.clone();
            db_action(&data.pool, &persistent, move |conn| {
                actions::users::find_user_by_username(&username_copy, &filter_mode, &conn)
            })
            .await?
            .ok_or(IntertextualError::UserNotFound { username })
            .map_err_app(&persistent)?
        }
    };

    login_user
        .require_edit_user_settings_for(&user)
        .map_err_app(&persistent)?;

    let current_tag_highlight_rules = login_user
        .get_tag_highlight_rules()
        .unwrap_or_else(|| {
            data.site
                .default_tag_highlight_rules
                .iter()
                .cloned()
                .collect()
        })
        .into_iter()
        .enumerate()
        .collect();

    let available_tag_highlight_rules_templates = db_action(&data.pool, &persistent, move |conn| {
        actions::tags::get_tag_highlight_rule_templates_list(&conn)
    })
    .await?;

    let user_id = user.id;
    let account_deletion_request = db_action(&data.pool, &persistent, move |conn| {
        actions::users::get_earliest_active_deletion_request(user_id, &conn)
    })
    .await?;

    let days_after_request = i64::from(data.site.account_deletion_request_duration_days);
    let account_deletion_request =
        account_deletion_request.map(|(request_date, can_be_cancelled)| {
            (
                request_date + chrono::Duration::days(days_after_request),
                can_be_cancelled,
            )
        });

    let user_id = user.id;
    let active_tokens: Vec<AuthTokenEntry> = db_action(&data.pool, &persistent, move |conn| {
        actions::users::get_tokens_for_user(user_id, &conn)
    })
    .await?
    .into_iter()
    .map(|(token_name, last_visit)| AuthTokenEntry {
        name: token_name,
        last_visit: format_duration(chrono::Utc::now().naive_utc() - last_visit),
    })
    .collect();

    let user_id = user.id;
    let (excluded_tags, excluded_authors) = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<
            (Vec<models::tags::CanonicalTag>, Vec<models::users::User>),
            IntertextualError,
        > {
            let tag_ids =
                actions::denylist::find_excluded_canonical_tag_ids_for_user(user_id, &conn)?;
            let mut tag_list: Vec<models::tags::CanonicalTag> = Vec::with_capacity(tag_ids.len());
            for id in tag_ids {
                if let Some(tag) = actions::tags::find_canonical_tag_by_id(id, &conn)? {
                    tag_list.push(tag);
                }
            }
            let author_ids = actions::denylist::find_excluded_author_ids_for_user(user_id, &conn)?;
            let mut author_list: Vec<models::users::User> = Vec::with_capacity(author_ids.len());
            for id in author_ids {
                if let Some(user) = actions::users::find_user_by_id(id, &conn)? {
                    author_list.push(user);
                }
            }
            Ok((tag_list, author_list))
        },
    )
    .await?;

    let excluded_tags = excluded_tags
        .into_iter()
        .map(|t| format!("#{}", t.display_name))
        .collect::<Vec<String>>()
        .join(" ");
    let excluded_authors = excluded_authors.iter().map(ShortUserEntry::from).collect();

    let user = UserSettingsData::from(user);
    let s = UserSettingsTemplate {
        persistent: PersistentTemplate::from(&persistent),
        user,
        account_deletion_request,
        active_tokens,
        excluded_tags,
        excluded_authors,
        current_tag_highlight_rules,
        available_tag_highlight_rules_templates,
        show_informations_confirmation: matches!(&confirm, Some(v) if v == "informations"),
        show_notifications_confirmation: matches!(&confirm, Some(v) if v == "notifications"),
        show_excluded_tags_confirmation: matches!(&confirm, Some(v) if v == "excluded_tags"),
        show_author_excluded_confirmation: matches!(&confirm, Some(v) if v == "author_excluded"),
        show_author_unexcluded_confirmation: matches!(&confirm, Some(v) if v == "author_unexcluded"),
        show_publication_confirmation: matches!(&confirm, Some(v) if v == "publication"),
        show_interaction_confirmation: matches!(&confirm, Some(v) if v == "interaction"),
        show_author_notes_confirmation: matches!(&confirm, Some(v) if v == "author_notes"),
        show_token_removed_confirmation: matches!(&confirm, Some(v) if v == "token_removed"),
        show_password_changed_confirmation: matches!(&confirm, Some(v) if v == "password_changed"),
        show_activate_confirmation: matches!(&confirm, Some(v) if v == "activate"),
        show_deactivate_confirmation: matches!(&confirm, Some(v) if v == "deactivate"),
        show_delete_confirmation: matches!(&confirm, Some(v) if v == "delete"),
        show_cancel_delete_confirmation: matches!(&confirm, Some(v) if v == "cancel_delete"),
        show_tag_highlight_rules_update_confirmation: matches!(&confirm, Some(v) if v == "tag_highlight_rules"),
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

fn format_duration(duration: chrono::Duration) -> String {
    if duration.num_days() > 1 {
        format!("{} days ago", duration.num_days())
    } else if duration.num_hours() > 2 {
        format!("{} hours ago", duration.num_hours())
    } else if duration.num_hours() > 1 {
        "An hour ago".to_string()
    } else if duration.num_minutes() > 2 {
        format!("{} minutes ago", duration.num_minutes())
    } else {
        "A moment ago".to_string()
    }
}

#[get("/password_reset/@{user}/")]
async fn password_reset_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let username = path.into_inner();

    let username_copy = username.clone();
    let mode = FilterMode::from_login(login_user);
    let author = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&username_copy, &mode, &conn)
    })
    .await?
    .ok_or(IntertextualError::UserNotFound { username })
    .map_err_app(&persistent)?;

    login_user
        .require_edit_user_settings_for(&author)
        .map_err_app(&persistent)?;

    let s = UserPasswordResetTemplate {
        persistent: PersistentTemplate::from(&persistent),
        user: UserSettingsData::from(author),
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/settings/@{user}/informations/")]
async fn handle_update_information(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,

    form: web::Form<UpdateInformationParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let username = path.into_inner();

    let username_copy = username.clone();
    let mode = FilterMode::from_login(login_user);
    let author = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&username_copy, &mode, &conn)
    })
    .await?
    .ok_or(IntertextualError::UserNotFound { username })
    .map_err_app(&persistent)?;

    login_user
        .require_edit_user_settings_for(&author)
        .map_err_app(&persistent)?;

    let form = form.into_inner();
    let new_display_name = form.display_name;

    validate_display_name(&new_display_name, "Display name").map_err_app(&persistent)?;

    let new_email = if form.email.trim().is_empty() {
        None
    } else {
        validate_email(&form.email, "email").map_err_app(&persistent)?;
        Some(form.email)
    };

    let new_details = if form.details.is_empty() {
        None
    } else {
        validate_user_details(&form.details, "Description").map_err_app(&persistent)?;
        Some(form.details)
    };

    let username = author.username.clone();
    let _ = db_action(&data.pool, &persistent, move |conn| {
        actions::users::modifications::update_user_information(
            author,
            String::from(new_display_name.trim()),
            new_email,
            new_details,
            &conn,
        )
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((
            LOCATION,
            format!("/settings/@{}/?confirm=informations#informations", username,),
        ))
        .finish())
}

#[post("/settings/@{user}/notifications/")]
async fn handle_update_notifications(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,

    form: web::Form<UpdateNotificationsParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let username = path.into_inner();

    let username_copy = username.clone();
    let mode = FilterMode::from_login(login_user);
    let author = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&username_copy, &mode, &conn)
    })
    .await?
    .ok_or(IntertextualError::UserNotFound { username })
    .map_err_app(&persistent)?;

    login_user
        .require_edit_user_settings_for(&author)
        .map_err_app(&persistent)?;

    let form = form.into_inner();
    let mut new_settings = login_user.get_settings();
    for &category in NotificationCategory::categories_with_settings() {
        let key = format!("notification_{}", i32::from(category));
        let level_string = form
            .notifications_entries
            .get(&key)
            .cloned()
            .unwrap_or_default();
        let level = match level_string.as_str() {
            "notification" => NotificationLevel::RedNumber,
            "event" => NotificationLevel::GreyDot,
            _ => NotificationLevel::None,
        };
        new_settings.notification_settings.insert(category, level);
    }

    let username = author.username.clone();
    let serialized_settings = new_settings
        .to_serializable()
        .map_err(|_| IntertextualError::InternalServerError)
        .map_err_app(&persistent)?;
    let _ = db_action(&data.pool, &persistent, move |conn| {
        actions::users::modifications::update_user_settings(
            author.id,
            Some(serialized_settings),
            &conn,
        )
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((
            LOCATION,
            format!(
                "/settings/@{}/?confirm=notifications#notifications",
                username,
            ),
        ))
        .finish())
}

#[post("/settings/@{user}/excluded_tags/")]
async fn handle_update_excluded_tags(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,

    form: web::Form<UpdateExcludedTagsParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let username = path.into_inner();

    let username_copy = username.clone();
    let mode = FilterMode::from_login(login_user);
    let author = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&username_copy, &mode, &conn)
    })
    .await?
    .ok_or(IntertextualError::UserNotFound { username })
    .map_err_app(&persistent)?;

    login_user
        .require_edit_user_settings_for(&author)
        .map_err_app(&persistent)?;

    let form = form.into_inner();
    let tag_names: Vec<String> = form
        .excluded_tags
        .split(' ')
        .filter_map(|entry| {
            let clean_tag = entry.strip_prefix('#').unwrap_or(entry);
            if clean_tag.is_empty() {
                None
            } else {
                Some(clean_tag.to_string())
            }
        })
        .collect();

    let user_id = author.id;
    db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<(), IntertextualError> {
            let mut tags_to_exclude: Vec<uuid::Uuid> = Vec::with_capacity(tag_names.len());
            for tag_name in tag_names {
                if let Some((_category, tag)) = actions::tags::find_tag_by_name(&tag_name, &conn)? {
                    tags_to_exclude.push(tag.id);
                }
            }

            let excluded_tag_ids =
                actions::denylist::find_excluded_canonical_tag_ids_for_user(user_id, &conn)?;

            for &canonical_tag_id in tags_to_exclude.iter() {
                if excluded_tag_ids.contains(&canonical_tag_id) {
                    // The tag is already excluded. No need to try re-excluding it.
                    continue;
                }
                actions::denylist::modifications::exclude_canonical_tag_for_user(
                    user_id,
                    canonical_tag_id,
                    &conn,
                )?;
            }

            for canonical_tag_id in excluded_tag_ids {
                if tags_to_exclude.contains(&canonical_tag_id) {
                    // The tag is in the new list, and therefore rightfully excluded. Do not unexclude it.
                    continue;
                }
                actions::denylist::modifications::remove_excluded_canonical_tag_for_user(
                    user_id,
                    canonical_tag_id,
                    &conn,
                )?;
            }

            Ok(())
        },
    )
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((
            LOCATION,
            format!(
                "/settings/@{}/?confirm=excluded_tags#exclusion_list",
                author.username,
            ),
        ))
        .finish())
}

#[post("/settings/@{user}/exclude_author/")]
async fn handle_exclude_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,

    form: web::Form<ExcludeAuthorParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let username = path.into_inner();

    let username_copy = username.clone();
    let mode = FilterMode::from_login(login_user);
    let author = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&username_copy, &mode, &conn)
    })
    .await?
    .ok_or(IntertextualError::UserNotFound { username })
    .map_err_app(&persistent)?;

    let excluded_username = form.into_inner().author_name;
    let excluded_username_copy = excluded_username.clone();
    let mode = FilterMode::from_login(login_user);
    let excluded_author = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&excluded_username_copy, &mode, &conn)
    })
    .await?
    .ok_or(IntertextualError::UserNotFound {
        username: excluded_username,
    })
    .map_err_app(&persistent)?;

    login_user
        .require_edit_user_settings_for(&author)
        .map_err_app(&persistent)?;

    let user_id = author.id;
    let author_to_exclude_id = excluded_author.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::denylist::modifications::exclude_author_for_user(
            user_id,
            author_to_exclude_id,
            &conn,
        )
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((
            LOCATION,
            format!(
                "/settings/@{}/?confirm=author_excluded#exclusion_list",
                author.username,
            ),
        ))
        .finish())
}

#[post("/settings/@{user}/unexclude_author/")]
async fn handle_unexclude_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,

    form: web::Form<UnexcludeAuthorParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let username = path.into_inner();

    let username_copy = username.clone();
    let mode = FilterMode::from_login(login_user);
    let author = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&username_copy, &mode, &conn)
    })
    .await?
    .ok_or(IntertextualError::UserNotFound { username })
    .map_err_app(&persistent)?;

    let excluded_username = form.into_inner().author_name;
    let excluded_username_copy = excluded_username.clone();
    let mode = FilterMode::from_login(login_user);
    let excluded_author = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&excluded_username_copy, &mode, &conn)
    })
    .await?
    .ok_or(IntertextualError::UserNotFound {
        username: excluded_username,
    })
    .map_err_app(&persistent)?;

    login_user
        .require_edit_user_settings_for(&author)
        .map_err_app(&persistent)?;

    let user_id = author.id;
    let author_to_unexclude_id = excluded_author.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::denylist::modifications::remove_excluded_author_for_user(
            user_id,
            author_to_unexclude_id,
            &conn,
        )
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((
            LOCATION,
            format!(
                "/settings/@{}/?confirm=author_unexcluded#exclusion_list",
                author.username,
            ),
        ))
        .finish())
}

#[post("/settings/@{user}/default_publication/")]
async fn handle_update_default_publication(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,

    form: web::Form<UpdateDefaultPublicationParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let username = path.into_inner();

    let username_copy = username.clone();
    let mode = FilterMode::from_login(login_user);
    let author = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&username_copy, &mode, &conn)
    })
    .await?
    .ok_or(IntertextualError::UserNotFound { username })
    .map_err_app(&persistent)?;

    login_user
        .require_edit_user_settings_for(&author)
        .map_err_app(&persistent)?;

    let form = form.into_inner();
    let mut new_settings = login_user.get_settings();

    match form.default_publication.as_str() {
        "immediate" => {
            new_settings.preferred_publication_mode =
                models::users::PreferredPublicationMode::PublishNow;
        }
        "future_date" => {
            new_settings.preferred_publication_mode =
                models::users::PreferredPublicationMode::PublishLater;

            new_settings.preferred_publication_day = match form.day.as_str() {
                "mon" => Some(chrono::Weekday::Mon),
                "tue" => Some(chrono::Weekday::Tue),
                "wed" => Some(chrono::Weekday::Wed),
                "thu" => Some(chrono::Weekday::Thu),
                "fri" => Some(chrono::Weekday::Fri),
                "sat" => Some(chrono::Weekday::Sat),
                "sun" => Some(chrono::Weekday::Sun),
                _ => None,
            };

            let mut form_hour = form.hour.parse::<u32>().unwrap_or(16);
            if form_hour > 23 {
                form_hour = 16;
            }
            new_settings.preferred_publication_hour = form_hour;
        }
        _ => {
            new_settings.preferred_publication_mode =
                models::users::PreferredPublicationMode::DoNotPublish;
        }
    }

    let username = author.username.clone();
    let serialized_settings = new_settings
        .to_serializable()
        .map_err(|_| IntertextualError::InternalServerError)
        .map_err_app(&persistent)?;
    let _ = db_action(&data.pool, &persistent, move |conn| {
        actions::users::modifications::update_user_settings(
            author.id,
            Some(serialized_settings),
            &conn,
        )
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((
            LOCATION,
            format!("/settings/@{}/?confirm=publication#publication", username,),
        ))
        .finish())
}

#[post("/settings/@{user}/default_interaction/")]
async fn handle_update_default_interaction(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,

    form: web::Form<UpdateDefaultInteractionParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let username = path.into_inner();

    let username_copy = username.clone();
    let mode = FilterMode::from_login(login_user);
    let author = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&username_copy, &mode, &conn)
    })
    .await?
    .ok_or(IntertextualError::UserNotFound { username })
    .map_err_app(&persistent)?;

    login_user
        .require_edit_user_settings_for(&author)
        .map_err_app(&persistent)?;

    let form = form.into_inner();
    let mut new_settings = login_user.get_settings();
    new_settings.preferred_recommendations_enabled = match form.recommendations.as_str() {
        "enabled" => models::stories::RecommendationsState::Enabled,
        "user_only" => models::stories::RecommendationsState::UserOnly,
        _ => models::stories::RecommendationsState::Disabled,
    };
    new_settings.preferred_comments_enabled = form.comments == "enabled";

    let username = author.username.clone();
    let serialized_settings = new_settings
        .to_serializable()
        .map_err(|_| IntertextualError::InternalServerError)
        .map_err_app(&persistent)?;
    let _ = db_action(&data.pool, &persistent, move |conn| {
        actions::users::modifications::update_user_settings(
            author.id,
            Some(serialized_settings),
            &conn,
        )
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((
            LOCATION,
            format!("/settings/@{}/?confirm=interaction#interaction", username,),
        ))
        .finish())
}

#[post("/settings/@{user}/default_notes/")]
async fn handle_update_default_notes(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,

    form: web::Form<UpdateDefaultNotesParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let username = path.into_inner();

    let username_copy = username.clone();
    let mode = FilterMode::from_login(login_user);
    let author = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&username_copy, &mode, &conn)
    })
    .await?
    .ok_or(IntertextualError::UserNotFound { username })
    .map_err_app(&persistent)?;

    login_user
        .require_edit_user_settings_for(&author)
        .map_err_app(&persistent)?;

    let form = form.into_inner();

    let username = author.username.clone();
    let _ = db_action(&data.pool, &persistent, move |conn| {
        actions::users::modifications::update_user_default_notes(
            author.id,
            form.default_foreword,
            form.default_afterword,
            &conn,
        )
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((
            LOCATION,
            format!("/settings/@{}/?confirm=author_notes#author-notes", username,),
        ))
        .finish())
}

#[post("/settings/@{user}/delete_token/")]
async fn handle_delete_token(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,

    form: web::Form<DeleteTokenParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let username = path.into_inner();

    let username_copy = username.clone();
    let mode = FilterMode::from_login(login_user);
    let author = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&username_copy, &mode, &conn)
    })
    .await?
    .ok_or(IntertextualError::UserNotFound { username })
    .map_err_app(&persistent)?;

    let form = form.into_inner();

    login_user
        .require_edit_user_settings_for(&author)
        .map_err_app(&persistent)?;

    let username = author.username.clone();
    let token_name = form.token_name;
    let _ = db_action(&data.pool, &persistent, move |conn| {
        actions::users::modifications::remove_auth_token_from_name(author.id, token_name, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((
            LOCATION,
            format!("/settings/@{}/?confirm=token_removed#tokens", username,),
        ))
        .finish())
}

#[post("/settings/@{user}/password/")]
async fn handle_update_password(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,

    form: web::Form<UpdatePasswordParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let username = path.into_inner();

    let username_copy = username.clone();
    let mode = FilterMode::from_login(login_user);
    let author = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&username_copy, &mode, &conn)
    })
    .await?
    .ok_or(IntertextualError::UserNotFound { username })
    .map_err_app(&persistent)?;

    if login_user.id != author.id {
        return Err(IntertextualError::AccessRightsRequired.into_app(&persistent));
    }

    let form = form.into_inner();
    let old_password = form.old_password;
    if old_password.is_empty() {
        return Err(IntertextualError::InvalidPassword.into_app(&persistent));
    }
    let is_valid = Verifier::default()
        .with_hash(&author.pw_hash)
        .with_password(old_password)
        .with_secret_key(data.password_secret_key.as_str())
        .verify()
        .unwrap();
    if !is_valid {
        return Err(IntertextualError::InvalidPassword.into_app(&persistent));
    }

    let password = form.password.clone();
    if form.password != form.password_confirm {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Password",
            message: "The passwords do not match".to_string(),
        }
        .into_app(&persistent));
    }

    validate_password(&password, "Password").map_err_app(&persistent)?;

    let pw_hash = Hasher::default()
        .with_password(password)
        .with_secret_key(data.password_secret_key.as_str())
        .hash()
        .unwrap();

    let username = author.username.clone();
    let _ = db_action(&data.pool, &persistent, move |conn| {
        actions::users::modifications::update_user_password(author, pw_hash, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((
            LOCATION,
            format!("/settings/@{}/?confirm=password_changed#password", username,),
        ))
        .finish())
}

#[post("/settings/@{user}/password_reset/")]
async fn handle_reset_password(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,

    form: web::Form<ResetPasswordParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let username = path.into_inner();

    let username_copy = username.clone();
    let mode = FilterMode::from_login(login_user);
    let author = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&username_copy, &mode, &conn)
    })
    .await?
    .ok_or(IntertextualError::UserNotFound { username })
    .map_err_app(&persistent)?;

    if login_user.id != author.id {
        return Err(IntertextualError::AccessRightsRequired.into_app(&persistent));
    }

    let form = form.into_inner();
    if !author.password_reset_required {
        return Err(IntertextualError::InvalidPassword.into_app(&persistent));
    }

    let password = form.password.clone();
    if form.password != form.password_confirm {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Password",
            message: "The passwords do not match".to_string(),
        }
        .into_app(&persistent));
    }

    validate_password(&password, "Password").map_err_app(&persistent)?;

    let pw_hash = Hasher::default()
        .with_password(password)
        .with_secret_key(data.password_secret_key.as_str())
        .hash()
        .unwrap();

    let username = author.username.clone();
    let _ = db_action(&data.pool, &persistent, move |conn| {
        actions::users::modifications::update_user_password(author, pw_hash, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, format!("/@{}/?confirm=password_reset", username,)))
        .finish())
}

#[post("/settings/@{user}/activate/")]
async fn handle_activate_account(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,

    form: web::Form<ActivationAccountParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let username = path.into_inner();

    let username_copy = username.clone();
    let mode = FilterMode::from_login(login_user);
    let author = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&username_copy, &mode, &conn)
    })
    .await?
    .ok_or_else(|| IntertextualError::UserNotFound {
        username: username.clone(),
    })
    .map_err_app(&persistent)?;

    if login_user.id != author.id {
        return Err(IntertextualError::AccessRightsRequired.into_app(&persistent));
    }

    let form = form.into_inner();
    let old_password = form.old_password;
    if old_password.is_empty() {
        return Err(IntertextualError::InvalidPassword.into_app(&persistent));
    }

    let is_valid = Verifier::default()
        .with_hash(&author.pw_hash)
        .with_password(old_password)
        .with_secret_key(data.password_secret_key.as_str())
        .verify()
        .unwrap();
    if !is_valid {
        return Err(IntertextualError::InvalidPassword.into_app(&persistent));
    }

    let _ = db_action(&data.pool, &persistent, move |conn| {
        actions::users::modifications::update_user_activation(author, true, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((
            LOCATION,
            format!("/settings/@{}/?confirm=activate#deactivate", username,),
        ))
        .finish())
}

#[post("/settings/@{user}/deactivate/")]
async fn handle_deactivate_account(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,

    form: web::Form<ActivationAccountParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let username = path.into_inner();

    let username_copy = username.clone();
    let mode = FilterMode::from_login(login_user);
    let author = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&username_copy, &mode, &conn)
    })
    .await?
    .ok_or_else(|| IntertextualError::UserNotFound {
        username: username.clone(),
    })
    .map_err_app(&persistent)?;

    if login_user.id != author.id {
        return Err(IntertextualError::AccessRightsRequired.into_app(&persistent));
    }

    let form = form.into_inner();
    let old_password = form.old_password;
    if old_password.is_empty() {
        return Err(IntertextualError::InvalidPassword.into_app(&persistent));
    }

    let is_valid = Verifier::default()
        .with_hash(&author.pw_hash)
        .with_password(old_password)
        .with_secret_key(data.password_secret_key.as_str())
        .verify()
        .unwrap();
    if !is_valid {
        return Err(IntertextualError::InvalidPassword.into_app(&persistent));
    }

    let _ = db_action(&data.pool, &persistent, move |conn| {
        actions::users::modifications::update_user_activation(author, false, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((
            LOCATION,
            format!("/settings/@{}/?confirm=deactivate#activate", username,),
        ))
        .finish())
}

#[post("/settings/@{user}/delete/")]
async fn handle_delete_account(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let username = path.into_inner();

    let username_copy = username.clone();
    let mode = FilterMode::from_login(login_user);
    let author = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&username_copy, &mode, &conn)
    })
    .await?
    .ok_or_else(|| IntertextualError::UserNotFound {
        username: username.clone(),
    })
    .map_err_app(&persistent)?;

    if login_user.id != author.id {
        return Err(IntertextualError::AccessRightsRequired.into_app(&persistent));
    }

    db_action(&data.pool, &persistent, move |conn| {
        actions::users::modifications::request_user_deletion(author, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((
            LOCATION,
            format!("/settings/@{}/?confirm=delete#delete", username,),
        ))
        .finish())
}

#[post("/settings/@{user}/cancel_delete/")]
async fn handle_cancel_delete_account(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let username = path.into_inner();

    let username_copy = username.clone();
    let mode = FilterMode::from_login(login_user);
    let author = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&username_copy, &mode, &conn)
    })
    .await?
    .ok_or_else(|| IntertextualError::UserNotFound {
        username: username.clone(),
    })
    .map_err_app(&persistent)?;

    if login_user.id != author.id {
        return Err(IntertextualError::AccessRightsRequired.into_app(&persistent));
    }

    db_action(&data.pool, &persistent, move |conn| {
        actions::users::modifications::cancel_user_deletion_request(author, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, format!("/settings/@{}/#delete", username,)))
        .finish())
}

pub mod tag_highlight {
    use actix_web::http::header::LOCATION;
    use actix_web::{post, web, HttpRequest, HttpResponse};
    use serde::{Deserialize, Serialize};

    use intertextual::actions;
    use intertextual::models::filter::FilterMode;
    use intertextual::models::tags::TagHighlightRule;
    use intertextual::models::tags::TagHighlightRuleColor;

    use crate::prelude::*;

    #[derive(Serialize, Deserialize)]
    struct CreateRuleParams {
        pub tags: String,
        pub use_custom_color: Option<String>,
        pub select_color: String,
        pub custom_color: String,
        pub symbol: String,
    }

    #[post("/settings/@{user}/tag_highlight_rules_create/")]
    async fn handle_create_rule(
        request: HttpRequest,
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<String>,

        form: web::Form<CreateRuleParams>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequired)
            .map_err_app(&persistent)?;
        let username = path.into_inner();
        let username_copy = username.clone();
        let mode = FilterMode::from_login(login_user);
        let author = db_action(&data.pool, &persistent, move |conn| {
            actions::users::find_user_by_username(&username_copy, &mode, &conn)
        })
        .await?
        .ok_or_else(|| IntertextualError::UserNotFound {
            username: username.clone(),
        })
        .map_err_app(&persistent)?;
        login_user
            .require_edit_user_settings_for(&author)
            .map_err_app(&persistent)?;

        let current_rules = login_user.get_tag_highlight_rules().unwrap_or_else(|| {
            data.site
                .default_tag_highlight_rules
                .iter()
                .cloned()
                .collect()
        });

        let form = form.into_inner();
        let tags = form
            .tags
            .split(' ')
            .map(|t| t.trim())
            .map(|t| t.strip_prefix('#').unwrap_or(t))
            .filter(|t| !t.is_empty())
            .map(|t| t.to_string())
            .collect::<Vec<String>>();
        let use_custom_color = matches!(form.use_custom_color, Some(value) if &value == "on");
        let story_color = if use_custom_color {
            TagHighlightRuleColor::parse_from_str(&form.custom_color).map_err_app(&persistent)?
        } else {
            TagHighlightRuleColor::parse_from_str(&form.select_color).map_err_app(&persistent)?
        };
        let story_symbol = if form.symbol.trim().is_empty() {
            None
        } else {
            Some(form.symbol.trim().to_string())
        };

        let mut new_rules = current_rules;

        new_rules.push(TagHighlightRule {
            tags,
            story_color,
            story_symbol,
        });

        let user_id = author.id;
        db_action(&data.pool, &persistent, move |conn| {
            actions::users::modifications::update_user_tag_highlight_rules(
                user_id,
                Some(new_rules),
                &conn,
            )
        })
        .await?;

        Ok(HttpResponse::SeeOther()
            .insert_header((
                LOCATION,
                format!(
                    "/settings/@{}/?confirm=tag_highlight_rules#tag_highlight_rules",
                    username,
                ),
            ))
            .finish())
    }

    #[derive(Serialize, Deserialize)]
    struct DeleteRuleParams {
        pub template_index: usize,
    }

    #[post("/settings/@{user}/tag_highlight_rules_delete/")]
    async fn handle_delete_rule(
        request: HttpRequest,
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<String>,

        form: web::Form<DeleteRuleParams>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequired)
            .map_err_app(&persistent)?;
        let username = path.into_inner();
        let username_copy = username.clone();
        let mode = FilterMode::from_login(login_user);
        let author = db_action(&data.pool, &persistent, move |conn| {
            actions::users::find_user_by_username(&username_copy, &mode, &conn)
        })
        .await?
        .ok_or_else(|| IntertextualError::UserNotFound {
            username: username.clone(),
        })
        .map_err_app(&persistent)?;
        login_user
            .require_edit_user_settings_for(&author)
            .map_err_app(&persistent)?;

        let current_rules = login_user.get_tag_highlight_rules().unwrap_or_else(|| {
            data.site
                .default_tag_highlight_rules
                .iter()
                .cloned()
                .collect()
        });

        let form = form.into_inner();
        let current_index = form.template_index;
        if current_index >= current_rules.len() {
            return Err(IntertextualError::FormFieldFormatError {
                form_field_name: "Edited rule",
                message: format!(
                    "The edited rule does not exist (no rule with index {})",
                    current_index
                ),
            })
            .map_err_app(&persistent);
        }

        let mut new_rules = current_rules;
        new_rules.remove(current_index);

        let user_id = author.id;
        db_action(&data.pool, &persistent, move |conn| {
            actions::users::modifications::update_user_tag_highlight_rules(
                user_id,
                Some(new_rules),
                &conn,
            )
        })
        .await?;

        Ok(HttpResponse::SeeOther()
            .insert_header((
                LOCATION,
                format!(
                    "/settings/@{}/?confirm=tag_highlight_rules#tag_highlight_rules",
                    username,
                ),
            ))
            .finish())
    }

    #[derive(Serialize, Deserialize)]
    struct EditRuleParams {
        pub template_index: usize,
        pub position: String,
        pub tags: String,
        pub use_custom_color: Option<String>,
        pub select_color: String,
        pub custom_color: String,
        pub symbol: String,
    }

    #[post("/settings/@{user}/tag_highlight_rules_edit/")]
    async fn handle_edit_rule(
        request: HttpRequest,
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<String>,

        form: web::Form<EditRuleParams>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequired)
            .map_err_app(&persistent)?;
        let username = path.into_inner();
        let username_copy = username.clone();
        let mode = FilterMode::from_login(login_user);
        let author = db_action(&data.pool, &persistent, move |conn| {
            actions::users::find_user_by_username(&username_copy, &mode, &conn)
        })
        .await?
        .ok_or_else(|| IntertextualError::UserNotFound {
            username: username.clone(),
        })
        .map_err_app(&persistent)?;
        login_user
            .require_edit_user_settings_for(&author)
            .map_err_app(&persistent)?;

        let current_rules = login_user.get_tag_highlight_rules().unwrap_or_else(|| {
            data.site
                .default_tag_highlight_rules
                .iter()
                .cloned()
                .collect()
        });

        let form = form.into_inner();
        let current_index = form.template_index;
        let target_index = form
            .position
            .parse::<usize>()
            .map_err(|_| IntertextualError::FormFieldFormatError {
                form_field_name: "Position",
                message: "The position must be a number greater or equal to 1".to_string(),
            })
            .map_err_app(&persistent)?;
        if target_index == 0 {
            return Err(IntertextualError::FormFieldFormatError {
                form_field_name: "Position",
                message: "The position must be a number greater or equal to 1".to_string(),
            })
            .map_err_app(&persistent);
        }

        // Note: In the UI, the target index starts at 1. We remove it here.
        let target_index = target_index - 1;

        if current_index >= current_rules.len() {
            return Err(IntertextualError::FormFieldFormatError {
                form_field_name: "Edited rule",
                message: format!(
                    "The edited rule does not exist (no rule with index {})",
                    current_index
                ),
            })
            .map_err_app(&persistent);
        }
        if target_index >= current_rules.len() {
            return Err(IntertextualError::FormFieldFormatError {
                form_field_name: "Position",
                message: format!(
                    "The target position must be less or equal to {} (the current number of rules)",
                    current_rules.len()
                ),
            })
            .map_err_app(&persistent);
        }
        let tags = form
            .tags
            .split(' ')
            .map(|t| t.trim())
            .map(|t| t.strip_prefix('#').unwrap_or(t))
            .filter(|t| !t.is_empty())
            .map(|t| t.to_string())
            .collect::<Vec<String>>();
        let use_custom_color = matches!(form.use_custom_color, Some(value) if &value == "on");
        let story_color = if use_custom_color {
            TagHighlightRuleColor::parse_from_str(&form.custom_color).map_err_app(&persistent)?
        } else {
            TagHighlightRuleColor::parse_from_str(&form.select_color).map_err_app(&persistent)?
        };
        let story_symbol = if form.symbol.trim().is_empty() {
            None
        } else {
            Some(form.symbol.trim().to_string())
        };

        let mut new_rules = current_rules;

        // Reorder if needed
        if current_index != target_index {
            let element = new_rules.remove(current_index);
            new_rules.insert(target_index, element);
        };

        // Note: The item has now been moved to the target_index position
        // Make sure that current_index isn't pointing to an obsolete position
        // to apply the changes there if needed.
        let current_index = target_index;

        // Change data if needed
        let item = new_rules
            .get_mut(current_index)
            .ok_or_else(|| IntertextualError::FormFieldFormatError {
                form_field_name: "Edited rule",
                message: format!(
                    "The edited rule does not exist (no rule with index {})",
                    current_index
                ),
            })
            .map_err_app(&persistent)?;
        item.tags = tags;
        item.story_color = story_color;
        item.story_symbol = story_symbol;

        let user_id = author.id;
        db_action(&data.pool, &persistent, move |conn| {
            actions::users::modifications::update_user_tag_highlight_rules(
                user_id,
                Some(new_rules),
                &conn,
            )
        })
        .await?;

        Ok(HttpResponse::SeeOther()
            .insert_header((
                LOCATION,
                format!(
                    "/settings/@{}/?confirm=tag_highlight_rules#tag_highlight_rules",
                    username,
                ),
            ))
            .finish())
    }

    #[derive(Serialize, Deserialize)]
    struct SelectTemplateParams {
        pub template_name: String,
    }

    #[post("/settings/@{user}/tag_highlight_rules_select_template/")]
    async fn handle_select_template(
        request: HttpRequest,
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<String>,

        form: web::Form<SelectTemplateParams>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequired)
            .map_err_app(&persistent)?;
        let username = path.into_inner();
        let username_copy = username.clone();
        let mode = FilterMode::from_login(login_user);
        let author = db_action(&data.pool, &persistent, move |conn| {
            actions::users::find_user_by_username(&username_copy, &mode, &conn)
        })
        .await?
        .ok_or_else(|| IntertextualError::UserNotFound {
            username: username.clone(),
        })
        .map_err_app(&persistent)?;
        login_user
            .require_edit_user_settings_for(&author)
            .map_err_app(&persistent)?;

        let form = form.into_inner();

        let user_id = author.id;
        let template_name = form.template_name;
        db_action(&data.pool, &persistent, move |conn| {
            let new_rules =
                actions::tags::get_tag_highlight_rule_template_by_name(&template_name, &conn)?;
            actions::users::modifications::update_user_tag_highlight_rules(
                user_id,
                Some(new_rules),
                &conn,
            )
        })
        .await?;

        Ok(HttpResponse::SeeOther()
            .insert_header((
                LOCATION,
                format!(
                    "/settings/@{}/?confirm=tag_highlight_rules#tag_highlight_rules",
                    username,
                ),
            ))
            .finish())
    }
}
