use actix_web::http::header::LOCATION;
use actix_web::{get, post, web, HttpRequest, HttpResponse};
use askama::Template;
use serde::Deserialize;

use intertextual::actions;
use intertextual::models;
use intertextual::models::filter::FilterMode;
use intertextual::models::stories::StoryEntry;
use intertextual::models::stories::StoryQueryExtraInfos;

use crate::prelude::*;

#[derive(Template)]
#[template(path = "user/index.html")]
struct UserTemplate {
    persistent: PersistentTemplate,
    user: models::users::User,
    stories: Vec<FeaturableStoryEntry>,
    excluded_stories: Vec<FeaturableStoryEntry>,
    total_recommendation_count: usize,
    is_self: bool,
    is_administrator: bool,
    follow_status: FollowStatus,
    trimmed_details: String,
    has_featured_stories: bool,
    featured_stories_limit_index: usize,
    sorting_mode: String,
    show_delete_chapter_confirm_box: bool,
    show_password_reset_confirmation: bool,
}

pub struct FeaturableStoryEntry {
    pub story: StoryEntry,
    pub is_story_featured: bool,
}

pub struct FollowStatus {
    show: bool,
    user_is_following_author: bool,
}

impl FollowStatus {
    pub async fn new(
        data: &AppState,
        persistent: &PersistentData,
        author_id: uuid::Uuid,
        user: Option<&models::users::User>,
    ) -> Result<FollowStatus, AppError> {
        let (user_can_follow, user_is_following_author) = match user {
            Some(user) => {
                let user_id = user.id;
                let user_is_following_author = db_action(&data.pool, persistent, move |conn| {
                    actions::follows::is_following_author(user_id, author_id, &conn)
                })
                .await?;
                (user_id != author_id, user_is_following_author)
            }
            None => (false, false),
        };

        Ok(FollowStatus {
            show: user_can_follow,
            user_is_following_author,
        })
    }
}

#[derive(Deserialize)]
pub struct UserPageQuery {
    pub confirm: Option<String>,
    pub sort_by: Option<String>,
}

#[derive(Clone, Copy)]
pub enum SortingMode {
    /// Sort by featured stories first, then by publication date (newest first)
    FeaturedFirst,
    /// Sort by publication date (oldest first)
    PubDateAsc,
    /// Sort by publication date (newest first)
    PubDateDesc,
    /// Sort by username (A first)
    TitleAsc,
    /// Sort by username (Z first)
    TitleDesc,
}

impl std::fmt::Display for SortingMode {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                SortingMode::FeaturedFirst => "featured",
                SortingMode::PubDateAsc => "pub_date_asc",
                SortingMode::PubDateDesc => "pub_date_desc",
                SortingMode::TitleAsc => "title_asc",
                SortingMode::TitleDesc => "title_desc",
            }
        )
    }
}

impl SortingMode {
    fn parse_url(url_param: Option<&str>) -> Option<SortingMode> {
        match url_param {
            Some(s) if s == "featured" => Some(SortingMode::FeaturedFirst),
            Some(s) if s == "pub_date_asc" => Some(SortingMode::PubDateAsc),
            Some(s) if s == "pub_date_desc" => Some(SortingMode::PubDateDesc),
            Some(s) if s == "title_asc" => Some(SortingMode::TitleAsc),
            Some(s) if s == "title_desc" => Some(SortingMode::TitleDesc),
            _ => None,
        }
    }
}

#[get("/@{user}/")]
async fn user_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    url_params: web::Query<UserPageQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();

    let username = path.into_inner();

    let mode = FilterMode::from_login_opt(&login_user);
    let username_copy = username.clone();
    let author = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&username_copy, &mode, &conn)
    })
    .await?
    .ok_or(IntertextualError::UserNotFound { username })
    .map_err_app(&persistent)?;

    let author_settings = author.get_settings();

    let sorting_mode =
        SortingMode::parse_url(url_params.sort_by.as_deref()).unwrap_or(SortingMode::FeaturedFirst);

    let user_info = StoryQueryExtraInfos::from_maybe_user(&data.site, &login_user);
    let featured_story_ids = author_settings.featured_story_ids.clone();
    let author_id = author.id;
    let stories = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<Vec<FeaturableStoryEntry>, IntertextualError> {
            let stories = actions::stories::find_all_stories_by_author(
                author_id,
                &user_info.filter_mode,
                &conn,
            )?;
            let mut result = Vec::with_capacity(stories.len());
            for story in stories {
                let is_story_featured = featured_story_ids.contains(&story.id);
                result.push(FeaturableStoryEntry {
                    story: StoryEntry::from_database(story, &user_info, &conn)?,
                    is_story_featured,
                });
            }
            match sorting_mode {
                SortingMode::FeaturedFirst => result.sort_by(|entry1, entry2| {
                    if entry1.is_story_featured && !entry2.is_story_featured {
                        std::cmp::Ordering::Less
                    } else if !entry1.is_story_featured && entry2.is_story_featured {
                        std::cmp::Ordering::Greater
                    } else {
                        entry2.story.last_update.cmp(&entry1.story.last_update)
                    }
                }),
                SortingMode::TitleAsc => result.sort_by(|entry1, entry2| {
                    entry1
                        .story
                        .title
                        .to_lowercase()
                        .cmp(&entry2.story.title.to_lowercase())
                }),
                SortingMode::TitleDesc => result.sort_by(|entry1, entry2| {
                    entry2
                        .story
                        .title
                        .to_lowercase()
                        .cmp(&entry1.story.title.to_lowercase())
                }),
                SortingMode::PubDateAsc => result.sort_by(|entry1, entry2| {
                    entry1.story.last_update.cmp(&entry2.story.last_update)
                }),
                SortingMode::PubDateDesc => result.sort_by(|entry1, entry2| {
                    entry2.story.last_update.cmp(&entry1.story.last_update)
                }),
            }
            Ok(result)
        },
    )
    .await?;

    let (excluded_stories, stories): (Vec<FeaturableStoryEntry>, Vec<FeaturableStoryEntry>) =
        stories.into_iter().partition(|s| s.story.is_excluded());

    let featured_stories_count = stories.iter().filter(|s| s.is_story_featured).count();
    let has_featured_stories = featured_stories_count > 0;
    let sorting_mode =
        if featured_stories_count == 0 && matches!(sorting_mode, SortingMode::FeaturedFirst) {
            SortingMode::PubDateDesc
        } else {
            sorting_mode
        };

    let trimmed_details = intertextual::utils::text_trim::limit_bytes_with_ellipses_if_needed(
        &(author
            .details
            .as_ref()
            .map(|h| h.text())
            .unwrap_or_default()),
        200,
    );

    let author_id = author.id;
    let total_recommendation_count = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<usize, IntertextualError> {
            let raw_recommendations =
                actions::recommendations::find_recommendations_by_user(author_id, &conn)?;
            Ok(raw_recommendations.len())
        },
    )
    .await?;

    let follow_status = FollowStatus::new(&data, &persistent, author.id, login_user).await?;
    let is_self = login_user.iter().any(|u| u.id == author.id);
    let is_administrator = login_user.iter().any(|u| u.administrator);

    let s = UserTemplate {
        persistent: PersistentTemplate::from(&persistent),
        user: author,
        stories,
        excluded_stories,
        trimmed_details,
        is_self,
        is_administrator,
        total_recommendation_count,
        follow_status,
        has_featured_stories,
        featured_stories_limit_index: if matches!(sorting_mode, SortingMode::FeaturedFirst) {
            featured_stories_count
        } else {
            0
        },
        sorting_mode: sorting_mode.to_string(),
        show_delete_chapter_confirm_box: matches!(&url_params.confirm, Some(v) if v == "delete_chapter"),
        show_password_reset_confirmation: matches!(&url_params.confirm, Some(v) if v == "password_reset"),
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/toggle_featured/@{user}/{story}/")]
async fn handle_author_feature_story_toggle(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        intertextual::data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await?;

    handle_shared_feature_story_toggle(data, persistent, authors, story).await
}

#[post("/toggle_featured/collaboration/{story}/")]
async fn handle_collaboration_feature_story_toggle(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let url_fragment = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        intertextual::data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await?;

    handle_shared_feature_story_toggle(data, persistent, authors, story).await
}

async fn handle_shared_feature_story_toggle(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    if authors.iter().all(|a| a.id != login_user.id) {
        return Err(IntertextualError::AccessRightsRequired).map_err_app(&persistent);
    }

    let username = login_user.username.clone();
    let mut new_settings = login_user.get_settings();

    if new_settings.featured_story_ids.contains(&story.id) {
        new_settings.featured_story_ids = new_settings
            .featured_story_ids
            .into_iter()
            .filter(|v| v != &story.id)
            .collect();
    } else {
        new_settings.featured_story_ids.push(story.id);
    }

    let login_user_id = login_user.id;
    let serialized_settings = new_settings
        .to_serializable()
        .map_err(|_| IntertextualError::InternalServerError)
        .map_err_app(&persistent)?;
    let _ = db_action(&data.pool, &persistent, move |conn| {
        actions::users::modifications::update_user_settings(
            login_user_id,
            Some(serialized_settings),
            &conn,
        )
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, format!("/@{}/#sorting_modes", username,)))
        .finish())
}
