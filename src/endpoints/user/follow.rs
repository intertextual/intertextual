use actix_web::http::header::LOCATION;
use actix_web::{get, post, web, HttpRequest, HttpResponse};
use askama::Template;

use intertextual::actions;
use intertextual::data;
use intertextual::models;
use intertextual::models::error::IntertextualError;
use intertextual::models::filter::FilterMode;
use intertextual::models::shared::AppState;

use crate::app::identity::Identity;
use crate::app::persistent::*;
use crate::error::*;
use crate::prelude::*;

#[derive(Template)]
#[template(path = "user/follow_list.html")]
struct UserFollowsTemplate {
    user: models::users::User,
    persistent: PersistentTemplate,
    follows: Vec<FollowEntry>,
}

struct FollowEntry {
    url: String,
    text: String,
    date: chrono::NaiveDateTime,
    authors: Vec<models::users::ShortUserEntry>,
}
#[get("/follows/")]
async fn main_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let username = {
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequired)
            .map_err_app(&persistent)?;
        login_user.username.clone()
    };
    shared_page(data, persistent, username).await
}

#[get("/follows/@{user}/")]
async fn specific_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let username = path.into_inner();
    shared_page(data, persistent, username).await
}

async fn shared_page(
    data: web::Data<AppState>,
    persistent: PersistentData,
    username: String,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let filter_mode = FilterMode::from_login(login_user);

    let user = {
        if login_user.username == username {
            let id = login_user.id;
            db_action(&data.pool, &persistent, move |conn| {
                actions::users::find_user_by_id(id, &conn)
            })
            .await?
            .ok_or(IntertextualError::UserNotFound { username })
            .map_err_app(&persistent)?
        } else {
            let username_copy = username.clone();
            let filter_mode = filter_mode.clone();
            db_action(&data.pool, &persistent, move |conn| {
                actions::users::find_user_by_username(&username_copy, &filter_mode, &conn)
            })
            .await?
            .ok_or(IntertextualError::UserNotFound { username })
            .map_err_app(&persistent)?
        }
    };

    login_user
        .require_see_statistics_rights_for(std::slice::from_ref(&user))
        .map_err_app(&persistent)?;

    let user_id = user.id;
    let follows = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<Vec<FollowEntry>, IntertextualError> {
            let mut result = Vec::<FollowEntry>::new();

            let raw_authors = actions::follows::private::get_user_author_follows(user_id, &conn)?;
            result.reserve(raw_authors.len());
            for (user, date) in raw_authors {
                let entry = models::users::ShortUserEntry::from(&user);
                result.push(FollowEntry {
                    url: format!("/@{}/", user.username),
                    text: format!("Following author {}", user.display_name),
                    date,
                    authors: vec![entry],
                });
            }

            let raw_recommenders =
                actions::follows::private::get_user_recommender_follows(user_id, &conn)?;
            result.reserve(raw_recommenders.len());
            for (user, date) in raw_recommenders {
                let entry = models::users::ShortUserEntry::from(&user);
                result.push(FollowEntry {
                    url: format!("/@{}/", user.username),
                    text: format!("Following suggestions by {}", user.display_name),
                    date,
                    authors: vec![entry],
                });
            }

            let raw_stories = actions::follows::private::get_user_story_follows(user_id, &conn)?;
            result.reserve(raw_stories.len());
            for (story, date) in raw_stories {
                let authors =
                    actions::stories::find_authors_by_story(story.id, &filter_mode, &conn)?;
                result.push(FollowEntry {
                    url: format!("/{}/{}/", story.author_path(&authors), story.url_fragment),
                    text: format!("Following story {}", story.title),
                    date,
                    authors: authors
                        .iter()
                        .map(models::users::ShortUserEntry::from)
                        .collect(),
                });
            }

            result.sort_unstable_by_key(|m| m.date);
            Ok(result)
        },
    )
    .await?;

    let s = UserFollowsTemplate {
        persistent: PersistentTemplate::from(&persistent),
        user,
        follows,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/follow/@{user}/author/")]
async fn handle_follow_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let author_username = path.into_inner();
    let username = author_username.clone();
    let mode = models::filter::FilterMode::from_login(login_user);
    let author = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&username, &mode, &conn)
    })
    .await?
    .ok_or(IntertextualError::UserNotFound {
        username: author_username,
    })
    .map_err_app(&persistent)?;

    let user_id = login_user.id;
    let author_id = author.id;
    let _ignore_errors = db_action(&data.pool, &persistent, move |conn| {
        actions::follows::modifications::follow_author(user_id, author_id, &conn)
    })
    .await;

    let return_url = format!("/@{}/#follow", author.username,);

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}

#[post("/follow/@{user}/author/unfollow/")]
async fn handle_unfollow_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let author_username = path.into_inner();
    let username = author_username.clone();
    let mode = models::filter::FilterMode::from_login(login_user);
    let author = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&username, &mode, &conn)
    })
    .await?
    .ok_or(IntertextualError::UserNotFound {
        username: author_username,
    })
    .map_err_app(&persistent)?;

    let user_id = login_user.id;
    let author_id = author.id;
    let _ignore_errors = db_action(&data.pool, &persistent, move |conn| {
        actions::follows::modifications::unfollow_author(user_id, author_id, &conn)
    })
    .await;

    let return_url = format!("/@{}/#follow", author.username,);

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}

#[post("/follow/@{user}/story/{story}/")]
async fn handle_follow_story(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let (author_username, url_fragment) = path.into_inner();
    let mode = models::filter::FilterMode::from_login(login_user);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_story_by_url(author_username, url_fragment, &mode, &conn)
    })
    .await?;

    let user_id = login_user.id;
    let story_id = story.id;
    let _ignore_errors = db_action(&data.pool, &persistent, move |conn| {
        actions::follows::modifications::unfollow_author_if_needed_and_follow_story(
            user_id, story_id, &conn,
        )
    })
    .await;

    let return_url = format!(
        "/{}/{}/#follow",
        story.author_path(&authors),
        story.url_fragment,
    );

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}

#[post("/follow/@{user}/story/{story}/unfollow/")]
async fn handle_unfollow_story(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let (author_username, url_fragment) = path.into_inner();
    let mode = models::filter::FilterMode::from_login(login_user);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_story_by_url(author_username, url_fragment, &mode, &conn)
    })
    .await?;

    let user_id = login_user.id;
    let story_id = story.id;
    let _ignore_errors = db_action(&data.pool, &persistent, move |conn| {
        actions::follows::modifications::unfollow_story(user_id, story_id, &conn)
    })
    .await;

    let return_url = format!(
        "/{}/{}/#follow",
        story.author_path(&authors),
        story.url_fragment,
    );

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}

#[post("/follow/collaboration/story/{story}/")]
async fn handle_follow_collaboration_story(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let url_fragment = path.into_inner();
    let mode = models::filter::FilterMode::from_login(login_user);
    let (_authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await?;

    let user_id = login_user.id;
    let story_id = story.id;
    let _ignore_errors = db_action(&data.pool, &persistent, move |conn| {
        actions::follows::modifications::unfollow_author_if_needed_and_follow_story(
            user_id, story_id, &conn,
        )
    })
    .await;

    let return_url = format!("/collaboration/{}/#follow", story.url_fragment,);

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}

#[post("/follow/collaboration/story/{story}/unfollow/")]
async fn handle_unfollow_collaboration_story(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let url_fragment = path.into_inner();
    let mode = models::filter::FilterMode::from_login(login_user);
    let (_authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await?;

    let user_id = login_user.id;
    let story_id = story.id;
    let _ignore_errors = db_action(&data.pool, &persistent, move |conn| {
        actions::follows::modifications::unfollow_story(user_id, story_id, &conn)
    })
    .await;

    let return_url = format!("/collaboration/{}/#follow", story.url_fragment,);

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}
