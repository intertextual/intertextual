use actix_web::{get, web, HttpRequest, HttpResponse};
use askama::Template;
use serde::Deserialize;

use intertextual::actions;
use intertextual::models;
use intertextual::models::filter::FilterMode;
use intertextual::models::stories::StoryEntry;
use intertextual::models::stories::StoryQueryExtraInfos;
use intertextual::utils::page_list::PageListing;

use crate::prelude::*;

const COMMENTS_PER_PAGE: i64 = 20;

#[derive(Template)]
#[template(path = "user/user_comments.html")]
struct UserCommentsTemplate {
    persistent: PersistentTemplate,
    is_administrator: bool,
    user: models::users::User,
    comments: Vec<UserCommentEntry>,
    page_listing: PageListing,
}

pub struct RawUserCommentEntry {
    id: uuid::Uuid,
    story: StoryEntry,
    chapter: models::stories::ChapterMetadata,
    created: String,
    edited: Option<String>,
    message: RichBlock,
    can_edit: bool,
    can_delete: bool,
}

pub struct UserCommentEntry {
    id: uuid::Uuid,
    story: StoryEntry,
    chapter: models::stories::ChapterMetadata,
    created: String,
    edited: Option<String>,
    formatted_comment: SanitizedHtml,
    can_edit: bool,
    can_delete: bool,
}

#[derive(Deserialize)]
struct UserCommentParams {
    start: Option<String>,
}

#[get("/my_comments/")]
async fn main_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<UserCommentParams>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let username = {
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequired)
            .map_err_app(&persistent)?;
        login_user.username.clone()
    };
    let start_index = url_params
        .start
        .and_then(|s| s.parse::<i64>().ok())
        .unwrap_or(0);
    shared_page(data, persistent, username, start_index).await
}

#[get("/my_comments/@{user}/")]
async fn specific_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    url_params: web::Query<UserCommentParams>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let username = path.into_inner();
    let start_index = url_params
        .start
        .and_then(|s| s.parse::<i64>().ok())
        .unwrap_or(0);
    shared_page(data, persistent, username, start_index).await
}

async fn shared_page(
    data: web::Data<AppState>,
    persistent: PersistentData,
    username: String,
    start_index: i64,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let filter_mode = FilterMode::from_login(login_user);

    let user = {
        if login_user.username == username {
            let id = login_user.id;
            db_action(&data.pool, &persistent, move |conn| {
                actions::users::find_user_by_id(id, &conn)
            })
            .await?
            .ok_or(IntertextualError::UserNotFound { username })
            .map_err_app(&persistent)?
        } else {
            let username_copy = username.clone();
            db_action(&data.pool, &persistent, move |conn| {
                actions::users::find_user_by_username(&username_copy, &filter_mode, &conn)
            })
            .await?
            .ok_or(IntertextualError::UserNotFound { username })
            .map_err_app(&persistent)?
        }
    };

    login_user
        .require_see_statistics_rights_for(std::slice::from_ref(&user))
        .map_err_app(&persistent)?;

    let user_id = user.id;
    let user_info = StoryQueryExtraInfos::from_user(&data.site, login_user);
    let (comments_count, raw_comments) = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<(i64, Vec<RawUserCommentEntry>), IntertextualError> {
            let comments_count = actions::comments::find_comment_quantity_by_user(user_id, &conn)?;
            let raw_comments = actions::comments::find_comments_by_user(
                user_id,
                start_index,
                COMMENTS_PER_PAGE,
                &conn,
            )?;
            let mut comments = Vec::with_capacity(std::cmp::min(
                raw_comments.len(),
                COMMENTS_PER_PAGE as usize,
            ));
            for (story, chapter, comment) in raw_comments {
                let story_entry = StoryEntry::from_database(story, &user_info, &conn)?;
                comments.push(RawUserCommentEntry {
                    id: comment.id,
                    story: story_entry,
                    chapter: chapter,
                    created: comment
                        .created
                        .format("%Y-%m-%d at %H:%M (UTC+00)")
                        .to_string(),
                    edited: comment
                        .updated
                        .map(|t| t.format("%Y-%m-%d at %H:%M (UTC+00)").to_string()),
                    message: comment.message,
                    can_edit: true,
                    can_delete: true,
                });
            }
            Ok((comments_count, comments))
        },
    )
    .await?;

    let mut comments = Vec::with_capacity(raw_comments.len());
    for comment in raw_comments {
        let formatted_comment =
            richblock_to_html(&comment.message, &data.pool, &persistent).await?;
        comments.push(UserCommentEntry {
            id: comment.id,
            story: comment.story,
            chapter: comment.chapter,
            created: comment.created,
            edited: comment.edited,
            formatted_comment,
            can_edit: comment.can_edit,
            can_delete: comment.can_delete,
        })
    }

    let is_administrator = login_user.administrator;
    let page_listing = PageListing::get_from_count(comments_count, start_index, COMMENTS_PER_PAGE);

    let s = UserCommentsTemplate {
        persistent: PersistentTemplate::from(&persistent),
        is_administrator,
        user,
        comments,
        page_listing,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}
