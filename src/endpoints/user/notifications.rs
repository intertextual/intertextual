use std::convert::TryFrom;

use actix_web::http::header::LOCATION;
use actix_web::{get, post, web, HttpRequest, HttpResponse};
use askama::Template;
use serde::{Deserialize, Serialize};

use intertextual::actions;
use intertextual::models;
use intertextual::models::error::IntertextualError;
use intertextual::models::notifications::Notification;
use intertextual::models::notifications::NotificationCategory;
use intertextual::models::shared::AppState;
use intertextual::utils::page_list::PageListing;

use crate::app::identity::Identity;
use crate::app::persistent::*;
use crate::error::*;
use crate::prelude::*;

const LIMIT_PER_PAGE: i64 = 50;
const MAX_UNREAD_PER_PAGE: i64 = 100;

#[derive(Template)]
#[template(path = "user/notifications_unread.html")]
struct NotificationsUnreadTemplate {
    persistent: PersistentTemplate,
    user: models::users::User,
    notifications: Vec<NotificationEntry>,
    events: Vec<NotificationEntry>,
    remaining_notifications_count: i64,
    all_notification_count: i64,
}

#[derive(Template)]
#[template(path = "user/notifications_all.html")]
struct NotificationsAllTemplate {
    persistent: PersistentTemplate,
    user: models::users::User,
    unread_notification_count: i64,
    notifications: Vec<NotificationEntry>,
    page_listing: PageListing,
}

#[derive(Serialize, Deserialize)]
struct ReadUnreadParams {
    nt: String,
    mode: Option<String>,
}

pub struct NotificationEntry {
    id: uuid::Uuid,
    time: chrono::NaiveDateTime,
    category: NotificationCategory,
    message: String,
    read: bool,
}

impl From<Notification> for NotificationEntry {
    fn from(notification: Notification) -> NotificationEntry {
        NotificationEntry {
            id: notification.id,
            time: notification.notification_time,
            category: NotificationCategory::try_from(notification.category)
                .unwrap_or(NotificationCategory::Unknown),
            message: notification.message,
            read: notification.read,
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct NotificationListQuery {
    start: Option<i64>,
}

#[get("/notifications/")]
async fn main_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let user_id = login_user.id;
    let user = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_id(user_id, &conn)
    })
    .await?
    .ok_or_else(|| IntertextualError::UserNotFound {
        username: login_user.username.clone(),
    })
    .map_err_app(&persistent)?;

    let user_id = user.id;
    let (unread_notifications, unread_notifications_count) = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<(Vec<models::notifications::Notification>, i64), IntertextualError> {
            Ok((
                actions::notifications::find_latest_unread_notifications_by_user_id(
                    user_id,
                    MAX_UNREAD_PER_PAGE,
                    &conn,
                )?,
                actions::notifications::get_unread_notification_count_by_user_id(user_id, &conn)?,
            ))
        },
    )
    .await?;
    let all_notification_count = db_action(&data.pool, &persistent, move |conn| {
        actions::notifications::get_all_notifications_count_by_user_id(user_id, &conn)
    })
    .await?;
    let remaining_notifications_count =
        unread_notifications_count - (unread_notifications.len() as i64);
    let (notifications, events) = unread_notifications
        .into_iter()
        .map(NotificationEntry::from)
        .partition(|n| user.should_show_red_notify_for(n.category));
    let s = NotificationsUnreadTemplate {
        persistent: PersistentTemplate::from(&persistent),
        user,
        notifications,
        events,
        remaining_notifications_count,
        all_notification_count,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[get("/notifications/all/")]
async fn all_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<NotificationListQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let user_id = login_user.id;
    let user = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_id(user_id, &conn)
    })
    .await?
    .ok_or_else(|| IntertextualError::UserNotFound {
        username: login_user.username.clone(),
    })
    .map_err_app(&persistent)?;

    let user_id = user.id;
    let unread_notification_count = db_action(&data.pool, &persistent, move |conn| {
        actions::notifications::get_unread_notification_count_by_user_id(user_id, &conn)
    })
    .await?;

    let start = url_params.start.unwrap_or(0);
    let user_id = user.id;
    let (all_notifications, all_notifications_count) = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<(Vec<Notification>, i64), IntertextualError> {
            Ok((
                actions::notifications::find_all_notifications_by_user_id(
                    user_id,
                    start,
                    LIMIT_PER_PAGE,
                    &conn,
                )?,
                actions::notifications::get_all_notifications_count_by_user_id(user_id, &conn)?,
            ))
        },
    )
    .await?;
    let page_listing = PageListing::get_from_count(all_notifications_count, start, LIMIT_PER_PAGE);
    let s = NotificationsAllTemplate {
        persistent: PersistentTemplate::from(&persistent),
        user,
        unread_notification_count,
        notifications: all_notifications
            .into_iter()
            .map(NotificationEntry::from)
            .collect(),
        page_listing,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

/// Accesses the target page for the given notification, and sets the notification as "handled"
///
/// Note : This is a GET request to allow opening the notification link in a new page if needed. As the notification GUIDs are
/// still technically unguessable (no known attack) and unique by user, this request is not susceptible to CSRF attacks on its own.
/// The effect of such a CSRF attack would also be of low consequence, only dismissing the notification while still showing the
/// notification target page as the new page.
#[get("/notifications/access/{id}/")]
async fn handle_access(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<uuid::Uuid>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let notification_id = path.into_inner();
    let user_id = login_user.id;
    let notification = db_action(&data.pool, &persistent, move |conn| {
        actions::notifications::find_notification_by_id(user_id, notification_id, &conn)
    })
    .await?
    .ok_or(IntertextualError::NotificationNotFound { notification_id })
    .map_err_app(&persistent)?;

    let return_url = notification.link.clone();

    // Mark the notification as read
    db_action(&data.pool, &persistent, move |conn| {
        actions::notifications::modifications::set_read(notification, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}

#[post("/notifications/mark_all_read/")]
async fn handle_mark_all_read(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::notifications::modifications::set_all_read(user_id, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, "/notifications/#notifications"))
        .finish())
}

#[post("/notifications/mark_read/{id}/")]
async fn handle_mark_read(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<uuid::Uuid>,
    url_params: web::Query<ReadUnreadParams>,
) -> Result<HttpResponse, AppError> {
    let params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let return_url = match (params.nt.as_str(), params.mode.unwrap_or_default().as_str()) {
        ("event", "unread") => "/notifications/#events",
        ("event", _) => "/notifications/all/#events",
        (_, "unread") => "/notifications/#notifications",
        (_, _) => "/notifications/all/#notifications",
    };

    let notification_id = path.into_inner();
    let user_id = login_user.id;
    let notification = db_action(&data.pool, &persistent, move |conn| {
        actions::notifications::find_notification_by_id(user_id, notification_id, &conn)
    })
    .await?
    .ok_or(IntertextualError::NotificationNotFound { notification_id })
    .map_err_app(&persistent)?;

    db_action(&data.pool, &persistent, move |conn| {
        actions::notifications::modifications::set_read(notification, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}

#[post("/notifications/mark_unread/{id}/")]
async fn handle_mark_unread(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<uuid::Uuid>,
    url_params: web::Query<ReadUnreadParams>,
) -> Result<HttpResponse, AppError> {
    let params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let return_url = match (params.nt.as_str(), params.mode.unwrap_or_default().as_str()) {
        ("event", "unread") => "/notifications/#events",
        ("event", _) => "/notifications/all/#events",
        (_, "unread") => "/notifications/#notifications",
        (_, _) => "/notifications/all/#notifications",
    };

    let notification_id = path.into_inner();
    let user_id = login_user.id;
    let notification = db_action(&data.pool, &persistent, move |conn| {
        actions::notifications::find_notification_by_id(user_id, notification_id, &conn)
    })
    .await?
    .ok_or(IntertextualError::NotificationNotFound { notification_id })
    .map_err_app(&persistent)?;

    db_action(&data.pool, &persistent, move |conn| {
        actions::notifications::modifications::set_unread(notification, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}
