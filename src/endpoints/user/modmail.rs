use actix_web::http::header::LOCATION;
use actix_web::{get, post, web, HttpRequest, HttpResponse};
use askama::Template;
use serde::{Deserialize, Serialize};

use intertextual::actions;
use intertextual::models;
use intertextual::models::error::IntertextualError;
use intertextual::models::formats::SanitizedHtml;
use intertextual::models::shared::AppState;
use intertextual::utils::page_list::PageListing;

use crate::app::identity::Identity;
use crate::app::persistent::*;
use crate::app::validation::validate_rich_modmail;
use crate::endpoints::filters;
use crate::error::*;
use crate::prelude::*;

const MESSAGES_PER_PAGE: i64 = 50;

#[derive(Template)]
#[template(path = "user/modmail.html")]
struct ModmailTemplate {
    persistent: PersistentTemplate,
    username: String,
    modmail: Vec<ModmailEntry>,
    page_listing: PageListing,
}

struct ModmailEntry {
    pub is_user_to_mods: bool,
    pub message_date: chrono::NaiveDateTime,
    pub message: SanitizedHtml,
}

#[derive(Serialize, Deserialize)]
pub struct ModmailQuery {
    start: Option<i64>,
}

#[derive(Serialize, Deserialize)]
pub struct ModmailParams {
    message: models::formats::RichBlock,
}

#[get("/moderation_contact/")]
async fn main_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<ModmailQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let start = url_params.start.unwrap_or(0);
    let user_id = login_user.id;
    let (modmail, total_modmail_count) = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<(Vec<ModmailEntry>, i64), IntertextualError> {
            let all_modmails_count = actions::modmail::get_modmail_count_for_user(user_id, &conn)?;
            let modmail_list =
                actions::modmail::get_modmail_for_user(user_id, start, MESSAGES_PER_PAGE, &conn)?;
            let mut modmails = Vec::with_capacity(modmail_list.len());
            for raw_modmail in modmail_list {
                modmails.push(ModmailEntry {
                    is_user_to_mods: raw_modmail.is_user_to_mods,
                    message: raw_modmail.message.html(&conn),
                    message_date: raw_modmail.message_date,
                });
            }
            Ok((modmails, all_modmails_count))
        },
    )
    .await?;

    let page_listing = PageListing::get_from_count(total_modmail_count, start, MESSAGES_PER_PAGE);
    let username = login_user.username.clone();
    let s = ModmailTemplate {
        persistent: PersistentTemplate::from(&persistent),
        username,
        modmail,
        page_listing,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/moderation_contact/send/")]
async fn handle_send(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    params: web::Form<ModmailParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id.clone(), &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let params = params.into_inner();
    let message = params.message;
    validate_rich_modmail(&message, "Moderation Message").map_err_app(&persistent)?;
    let new_modmail = models::modmail::NewModmail {
        user_id: login_user.id,
        message,
    };
    let _ = db_action(&data.pool, &persistent, move |conn| {
        actions::modmail::modifications::send_new_modmail(new_modmail, &conn)
    })
    .await?;
    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, "/moderation_contact/"))
        .finish())
}
