use actix_web::{get, web, HttpRequest, HttpResponse};
use askama::Template;

use intertextual::actions;
use intertextual::data;
use intertextual::models;
use intertextual::models::filter::FilterMode;
use intertextual::models::shared::SharedSiteData;
use intertextual::models::stories::StoryEntry;
use intertextual::models::stories::StoryQueryExtraInfos;
use intertextual::utils::text_trim::limit_bytes_with_ellipses_if_needed;

use crate::prelude::*;

#[derive(Template)]
#[template(path = "story/export-instructions.html")]
struct ExportInstructionsTemplate {
    site: std::sync::Arc<SharedSiteData>,
    persistent: PersistentTemplate,
    authors: Vec<models::users::User>,
    author_path: String,
    story: models::stories::Story,
    chapters: Vec<models::stories::ChapterMetadata>,
}

#[derive(Template)]
#[template(path = "story/otw-export.html")]
struct OtwChapterTemplate {
    persistent: PersistentTemplate,
    story_entry: StoryEntry,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
    is_standalone: bool,
    publication_year: String,
    current_year: String,
    trimmed_description_for_card: String,
}

#[get("/@{user}/{story}/0/export-instructions/")]
async fn instructions_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await?;

    instructions_shared(data, persistent, authors, story).await
}

#[get("/collaboration/{story}/0/export-instructions/")]
async fn instructions_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let url_fragment = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await?;

    instructions_shared(data, persistent, authors, story).await
}

async fn instructions_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_edition_rights_for(story_id, user_id, &conn)
    })
    .await?;

    let story_id = story.id;
    let filter_mode = FilterMode::from_login(login_user);
    let chapters = db_action(&data.pool, &persistent, move |conn| {
        actions::stories::find_chapters_metadata_by_story_id(story_id, &filter_mode, &conn)
    })
    .await?;

    let author_path = story.author_path(&authors);
    let s = ExportInstructionsTemplate {
        site: data.site.clone(),
        persistent: PersistentTemplate::from(&persistent),
        authors,
        author_path,
        story,
        chapters,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[get("/@{user}/{story}/{chapter}/otw-export.html/")]
async fn author_chapter_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, i32)>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();

    let (username, url_fragment, chapter_number) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login_user);
    let (_, story, chapter) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_chapter_by_url(
            username,
            url_fragment,
            chapter_number,
            &mode,
            &conn,
        )
    })
    .await?;

    chapter_page_shared(data, &persistent, story, chapter).await
}

#[get("/collaboration/{story}/{chapter}/otw-export.html/")]
async fn collaboration_chapter_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, i32)>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();

    let (url_fragment, chapter_number) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login_user);
    let (_, story, chapter) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_chapter_by_url(url_fragment, chapter_number, &mode, &conn)
    })
    .await?;

    chapter_page_shared(data, &persistent, story, chapter).await
}

async fn chapter_page_shared(
    data: web::Data<AppState>,
    persistent: &PersistentData,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let filter_mode = FilterMode::from_login_opt(&login_user);

    let story_id = story.id;
    let is_standalone = db_action(
        &data.pool,
        persistent,
        move |conn| -> Result<bool, IntertextualError> {
            actions::stories::story_is_standalone(story_id, &filter_mode, &conn)
        },
    )
    .await?;

    let publication_year = chapter
        .official_creation_date
        .unwrap_or_else(|| chrono::Utc::now().naive_utc())
        .format("%Y")
        .to_string();
    let current_year = chrono::Utc::now().naive_utc().format("%Y").to_string();
    let trimmed_description_for_card =
        limit_bytes_with_ellipses_if_needed(&story.description.inner, 200);
    let user_info = StoryQueryExtraInfos::from_maybe_user(&data.site, &login_user);
    let story_copy = story.clone();
    let story_entry = db_action(&data.pool, persistent, move |conn| {
        StoryEntry::from_database(story_copy, &user_info, &conn)
    })
    .await?;
    let s = OtwChapterTemplate {
        persistent: PersistentTemplate::from(persistent),
        story_entry,
        story,
        chapter,
        is_standalone,
        publication_year,
        current_year,
        trimmed_description_for_card,
    }
    .render()
    .map_err_app(persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}
