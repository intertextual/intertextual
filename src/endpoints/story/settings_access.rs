use actix_web::http::header::LOCATION;
use actix_web::{get, post, web, HttpRequest, HttpResponse};
use askama::Template;
use serde::{Deserialize, Serialize};
use std::str::FromStr;

use intertextual::actions;
use intertextual::actions::stories::modifications;
use intertextual::data;
use intertextual::models;
use intertextual::models::filter::FilterMode;
use intertextual::models::stories::CreativeRole;
use intertextual::models::stories::RecommendationsState;

use crate::prelude::*;

#[derive(Template)]
#[template(path = "story/settings_access.html")]
struct StoryEditTemplate {
    persistent: PersistentTemplate,
    login_user_id: uuid::Uuid,
    authors: Vec<models::users::User>,
    public_authors: Vec<models::users::User>,
    non_public_authors: Vec<models::users::User>,
    editors: Vec<models::users::User>,
    beta_readers: Vec<models::users::User>,
    author_path: String,
    is_collaboration_url_available: bool,
    story: models::stories::Story,
    show_collaboration_confirm_box: bool,
    show_authors_confirm_box: bool,
    show_users_confirm_box: bool,
    show_interaction_confirm_box: bool,
}

#[derive(Serialize, Deserialize)]
pub struct StorySettingsQuery {
    pub confirm: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct UserAccessEditParams {
    username: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct StoryInteractionEditParams {
    comment_mode: String,
    recommendation_mode: String,
}

type AccessVec = Vec<(models::users::User, CreativeRole)>;

#[get("/@{user}/{story}/0/settings_access/")]
async fn main_page_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    url_params: web::Query<StorySettingsQuery>,
) -> Result<HttpResponse, AppError> {
    let params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await?;

    main_page_shared(data, persistent, authors, story, params.confirm).await
}

#[get("/collaboration/{story}/0/settings_access/")]
async fn main_page_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    url_params: web::Query<StorySettingsQuery>,
) -> Result<HttpResponse, AppError> {
    let params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let url_fragment = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await?;

    main_page_shared(data, persistent, authors, story, params.confirm).await
}

async fn main_page_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    confirm: Option<String>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_management_rights_for(story_id, user_id, &conn)
    })
    .await?;

    let url_fragment = story.url_fragment.clone();
    let is_collaboration_url_available = db_action(&data.pool, &persistent, move |conn| {
        actions::stories::is_collaboration_story_url_available(&url_fragment, &conn)
    })
    .await?;

    let story_id = story.id;
    let users_with_roles = db_action(&data.pool, &persistent, move |conn| {
        actions::access::get_all_users_with_creative_roles(story_id, &conn)
    })
    .await?;

    let (public_authors, others): (AccessVec, AccessVec) = users_with_roles
        .into_iter()
        .partition(|(_, access)| matches!(access, CreativeRole::Author));
    let public_authors = public_authors.into_iter().map(|(user, _)| user).collect();

    let (non_public_authors, others): (AccessVec, AccessVec) = others
        .into_iter()
        .partition(|(_, access)| matches!(access, CreativeRole::UnconfirmedAuthor));
    let non_public_authors = non_public_authors
        .into_iter()
        .map(|(user, _)| user)
        .collect();

    let (editors, others): (AccessVec, AccessVec) = others
        .into_iter()
        .partition(|(_, access)| matches!(access, CreativeRole::Editor));
    let editors = editors.into_iter().map(|(user, _)| user).collect();
    let beta_readers = others
        .into_iter()
        .filter(|(_, access)| matches!(access, CreativeRole::BetaReader))
        .map(|(user, _)| user)
        .collect();

    let login_user_id = login_user.id;
    let author_path = story.author_path(&authors);
    let s = StoryEditTemplate {
        persistent: PersistentTemplate::from(&persistent),
        login_user_id,
        authors,
        public_authors,
        non_public_authors,
        editors,
        beta_readers,
        author_path,
        is_collaboration_url_available,
        story,
        show_collaboration_confirm_box: matches!(&confirm, Some(v) if v == "collaboration"),
        show_authors_confirm_box: matches!(&confirm, Some(v) if v == "authors"),
        show_users_confirm_box: matches!(&confirm, Some(v) if v == "users"),
        show_interaction_confirm_box: matches!(&confirm, Some(v) if v == "interaction"),
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/@{user}/{story}/0/settings_access_remove/")]
async fn handle_story_access_remove_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    params: web::Form<UserAccessEditParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await?;

    handle_story_access_remove_shared(data, persistent, authors, story, params).await
}

#[post("/collaboration/{story}/0/settings_access_remove/")]
async fn handle_story_access_remove_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    params: web::Form<UserAccessEditParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let url_fragment = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await?;

    handle_story_access_remove_shared(data, persistent, authors, story, params).await
}

async fn handle_story_access_remove_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    form: web::Form<UserAccessEditParams>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let mode = FilterMode::from_login_opt(&login_user);
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_management_rights_for(story_id, user_id, &conn)
    })
    .await?;

    let form = form.into_inner();
    let username = form
        .username
        .ok_or_else(|| IntertextualError::FormFieldFormatError {
            form_field_name: "Username",
            message: String::from("The username cannot be empty"),
        })
        .map_err_app(&persistent)?;

    let username_to_find = username.clone();
    let target_user = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&username_to_find, &mode, &conn)
    })
    .await?;
    let target_user = target_user
        .ok_or_else(|| IntertextualError::FormFieldFormatError {
            form_field_name: "Username",
            message: format!("The user {} was not found", username),
        })
        .map_err_app(&persistent)?;

    if target_user.id == login_user.id {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Username",
            message: String::from("You cannot remove your own access rights"),
        })
        .map_err_app(&persistent);
    }

    let return_url = format!(
        "/{}/{}/0/settings_access/?confirm=users#users",
        story.author_path(&authors),
        story.url_fragment,
    );

    db_action(&data.pool, &persistent, move |conn| {
        actions::access::modifications::set_creative_role_for_user(
            story,
            target_user,
            CreativeRole::None,
            &conn,
        )
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}

#[post("/@{user}/{story}/0/settings_authors_add/")]
async fn handle_story_authors_add_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    params: web::Form<UserAccessEditParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await?;

    handle_story_authors_add_shared(data, persistent, authors, story, params).await
}

#[post("/collaboration/{story}/0/settings_authors_add/")]
async fn handle_story_authors_add_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    params: web::Form<UserAccessEditParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let url_fragment = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await?;

    handle_story_authors_add_shared(data, persistent, authors, story, params).await
}

async fn handle_story_authors_add_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    form: web::Form<UserAccessEditParams>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let mode = FilterMode::from_login_opt(&login_user);
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_management_rights_for(story_id, user_id, &conn)
    })
    .await?;

    let form = form.into_inner();
    let username = form
        .username
        .ok_or_else(|| IntertextualError::FormFieldFormatError {
            form_field_name: "Username",
            message: String::from("The username cannot be empty"),
        })
        .map_err_app(&persistent)?;

    let username_to_find = username.clone();
    let target_user = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&username_to_find, &mode, &conn)
    })
    .await?;
    let target_user = target_user
        .ok_or_else(|| IntertextualError::FormFieldFormatError {
            form_field_name: "Username",
            message: format!("The user {} was not found", username),
        })
        .map_err_app(&persistent)?;

    let story_id = story.id;
    let user_id = target_user.id;
    let target_user_creative_role = db_action(&data.pool, &persistent, move |conn| {
        actions::access::get_creative_role(story_id, user_id, &conn)
    })
    .await?;
    if !matches!(target_user_creative_role, CreativeRole::None) {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Username",
            message: String::from("The user already has a creative role for this story. Remove other roles before adding a new one."),
        })
        .map_err_app(&persistent);
    }

    let return_url = format!(
        "/{}/{}/0/settings_access/?confirm=authors#authors",
        story.author_path(&authors),
        story.url_fragment,
    );

    db_action(&data.pool, &persistent, move |conn| {
        actions::access::modifications::set_creative_role_for_user(
            story,
            target_user,
            CreativeRole::UnconfirmedAuthor,
            &conn,
        )
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}

#[post("/@{user}/{story}/0/settings_readwrite_add/")]
async fn handle_story_readwrite_add_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    params: web::Form<UserAccessEditParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await?;

    handle_story_readwrite_add_shared(data, persistent, authors, story, params).await
}

#[post("/collaboration/{story}/0/settings_readwrite_add/")]
async fn handle_story_readwrite_add_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    params: web::Form<UserAccessEditParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let url_fragment = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await?;

    handle_story_readwrite_add_shared(data, persistent, authors, story, params).await
}

async fn handle_story_readwrite_add_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    form: web::Form<UserAccessEditParams>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let mode = FilterMode::from_login_opt(&login_user);
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_management_rights_for(story_id, user_id, &conn)
    })
    .await?;

    let form = form.into_inner();
    let username = form
        .username
        .ok_or_else(|| IntertextualError::FormFieldFormatError {
            form_field_name: "Username",
            message: String::from("The username cannot be empty"),
        })
        .map_err_app(&persistent)?;

    let username_to_find = username.clone();
    let target_user = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&username_to_find, &mode, &conn)
    })
    .await?;
    let target_user = target_user
        .ok_or_else(|| IntertextualError::FormFieldFormatError {
            form_field_name: "Username",
            message: format!("The user {} was not found", username),
        })
        .map_err_app(&persistent)?;

    let story_id = story.id;
    let user_id = target_user.id;
    let target_user_creative_role = db_action(&data.pool, &persistent, move |conn| {
        actions::access::get_creative_role(story_id, user_id, &conn)
    })
    .await?;
    if !matches!(target_user_creative_role, CreativeRole::None) {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Username",
            message: String::from("The user already has a creative role for this story. Remove other roles before adding a new one."),
        })
        .map_err_app(&persistent);
    }

    let return_url = format!(
        "/{}/{}/0/settings_access/?confirm=users#users",
        story.author_path(&authors),
        story.url_fragment,
    );

    db_action(&data.pool, &persistent, move |conn| {
        actions::access::modifications::set_creative_role_for_user(
            story,
            target_user,
            CreativeRole::Editor,
            &conn,
        )
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}

#[post("/@{user}/{story}/0/settings_readonly_add/")]
async fn handle_story_readonly_add_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    params: web::Form<UserAccessEditParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await?;

    handle_story_readonly_add_shared(data, persistent, authors, story, params).await
}

#[post("/collaboration/{story}/0/settings_readonly_add/")]
async fn handle_story_readonly_add_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    params: web::Form<UserAccessEditParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let url_fragment = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await?;

    handle_story_readonly_add_shared(data, persistent, authors, story, params).await
}

async fn handle_story_readonly_add_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    form: web::Form<UserAccessEditParams>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let mode = FilterMode::from_login_opt(&login_user);
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_management_rights_for(story_id, user_id, &conn)
    })
    .await?;

    let form = form.into_inner();
    let username = form
        .username
        .ok_or_else(|| IntertextualError::FormFieldFormatError {
            form_field_name: "Username",
            message: String::from("The username cannot be empty"),
        })
        .map_err_app(&persistent)?;

    let username_to_find = username.clone();
    let target_user = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&username_to_find, &mode, &conn)
    })
    .await?;
    let target_user = target_user
        .ok_or_else(|| IntertextualError::FormFieldFormatError {
            form_field_name: "Username",
            message: format!("The user {} was not found", username),
        })
        .map_err_app(&persistent)?;

    let story_id = story.id;
    let user_id = target_user.id;
    let target_user_creative_role = db_action(&data.pool, &persistent, move |conn| {
        actions::access::get_creative_role(story_id, user_id, &conn)
    })
    .await?;
    if !matches!(target_user_creative_role, CreativeRole::None) {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Username",
            message: String::from("The user already has a creative role for this story. Remove other roles before adding a new one."),
        })
        .map_err_app(&persistent);
    }

    let return_url = format!(
        "/{}/{}/0/settings_access/?confirm=users#users",
        story.author_path(&authors),
        story.url_fragment,
    );

    db_action(&data.pool, &persistent, move |conn| {
        actions::access::modifications::set_creative_role_for_user(
            story,
            target_user,
            CreativeRole::BetaReader,
            &conn,
        )
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}

#[post("/@{user}/{story}/0/settings_authors_set_self_as_public/")]
async fn handle_story_authors_set_self_public_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await?;

    handle_story_authors_set_self_public_shared(data, persistent, authors, story).await
}

#[post("/collaboration/{story}/0/settings_authors_set_self_as_public/")]
async fn handle_story_authors_set_self_public_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let url_fragment = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await?;

    handle_story_authors_set_self_public_shared(data, persistent, authors, story).await
}

async fn handle_story_authors_set_self_public_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_management_rights_for(story_id, user_id, &conn)
    })
    .await?;

    let return_url = format!(
        "/{}/{}/0/settings_access/?confirm=authors#authors",
        story.author_path(&authors),
        story.url_fragment,
    );

    let user_id = login_user.id;
    let url_fragment = story.url_fragment.clone();
    let is_url_fragment_available = db_action(&data.pool, &persistent, move |conn| {
        actions::stories::is_author_story_url_available(user_id, &url_fragment, &conn)
    })
    .await?;
    if !is_url_fragment_available {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Story URL",
            message: format!("You already have a story with the URL {} and cannot be added as co-author to this story", &story.url_fragment),
        })
        .map_err_app(&persistent);
    }

    let author_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        let user = actions::users::find_user_by_id(author_id, &conn)?;
        match user {
            Some(user) => actions::access::modifications::set_creative_role_for_user(
                story,
                user,
                CreativeRole::Author,
                &conn,
            ),
            None => Err(IntertextualError::InternalServerError),
        }
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}

#[post("/@{user}/{story}/0/settings_authors_unset_self_as_public/")]
async fn handle_story_authors_unset_self_public_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await?;

    handle_story_authors_unset_self_public_shared(data, persistent, authors, story).await
}

#[post("/collaboration/{story}/0/settings_authors_unset_self_as_public/")]
async fn handle_story_authors_unset_self_public_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let url_fragment = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await?;

    handle_story_authors_unset_self_public_shared(data, persistent, authors, story).await
}

async fn handle_story_authors_unset_self_public_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_management_rights_for(story_id, user_id, &conn)
    })
    .await?;

    if story.is_collaboration && authors.len() == 2 {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Author List",
            message: String::from(
                "Each collaboration needs to have at least two authors, so you cannot remove yourself",
            ),
        })
        .map_err_app(&persistent);
    }

    if authors.len() == 1 {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Author List",
            message: String::from(
                "Each story needs to have at least one author, so you cannot remove yourself",
            ),
        })
        .map_err_app(&persistent);
    }

    // Note : We need an author path that doesn't include us, to not get redirected to an invalid URL
    let authors = authors
        .into_iter()
        .filter(|a| a.id != login_user.id)
        .collect::<Vec<models::users::User>>();
    let return_url = format!(
        "/{}/{}/0/settings_access/?confirm=authors#authors",
        story.author_path(&authors),
        story.url_fragment,
    );

    let author_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        let user = actions::users::find_user_by_id(author_id, &conn)?;
        match user {
            Some(user) => actions::access::modifications::set_creative_role_for_user(
                story,
                user,
                CreativeRole::UnconfirmedAuthor,
                &conn,
            ),
            None => Err(IntertextualError::InternalServerError),
        }
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}

#[post("/@{user}/{story}/0/settings_collaboration_set/")]
async fn handle_story_set_collab_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await?;

    handle_story_set_collab_shared(data, persistent, authors, story).await
}

#[post("/collaboration/{story}/0/settings_collaboration_set/")]
async fn handle_story_set_collab_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let url_fragment = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await?;

    handle_story_set_collab_shared(data, persistent, authors, story).await
}

async fn handle_story_set_collab_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    mut story: models::stories::Story,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_management_rights_for(story_id, user_id, &conn)
    })
    .await?;

    let url_fragment = story.url_fragment.clone();
    let is_url_fragment_available = db_action(&data.pool, &persistent, move |conn| {
        actions::stories::is_collaboration_story_url_available(&url_fragment, &conn)
    })
    .await?;
    if !is_url_fragment_available {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Story URL",
            message: format!(
                "The URL /collaboration/{} is already in use by another story",
                &story.url_fragment
            ),
        })
        .map_err_app(&persistent);
    }

    story.is_collaboration = true;

    let return_url = format!(
        "/{}/{}/0/settings_access/?confirm=collaboration#authors",
        story.author_path(&authors),
        story.url_fragment,
    );
    db_action(&data.pool, &persistent, move |conn| {
        modifications::update_story_collaboration(story, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}

#[post("/@{user}/{story}/0/settings_collaboration_unset/")]
async fn handle_story_unset_collab_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await?;

    handle_story_unset_collab_shared(data, persistent, authors, story).await
}

#[post("/collaboration/{story}/0/settings_collaboration_unset/")]
async fn handle_story_unset_collab_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let url_fragment = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await?;

    handle_story_unset_collab_shared(data, persistent, authors, story).await
}

async fn handle_story_unset_collab_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    mut story: models::stories::Story,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_management_rights_for(story_id, user_id, &conn)
    })
    .await?;

    story.is_collaboration = false;

    let return_url = format!(
        "/{}/{}/0/settings_access/?confirm=collaboration#authors",
        story.author_path(&authors),
        story.url_fragment,
    );
    db_action(&data.pool, &persistent, move |conn| {
        modifications::update_story_collaboration(story, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}

#[post("/@{user}/{story}/0/settings_interaction/")]
async fn handle_story_interaction_edit_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    params: web::Form<StoryInteractionEditParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await?;

    handle_story_interaction_edit_shared(data, persistent, authors, story, params).await
}

#[post("/collaboration/{story}/0/settings_interaction/")]
async fn handle_story_interaction_edit_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    params: web::Form<StoryInteractionEditParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let url_fragment = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await?;

    handle_story_interaction_edit_shared(data, persistent, authors, story, params).await
}

async fn handle_story_interaction_edit_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    mut story: models::stories::Story,
    params: web::Form<StoryInteractionEditParams>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_management_rights_for(story_id, user_id, &conn)
    })
    .await?;

    let params = params.into_inner();
    let comments_enabled = params.comment_mode.as_str() == "enable";
    let recommendation_mode =
        RecommendationsState::from_str(&params.recommendation_mode).map_err_app(&persistent)?;

    story.comments_enabled = comments_enabled;
    story.recommendations_enabled = recommendation_mode.into();

    let return_url = format!(
        "/{}/{}/0/settings_access/?confirm=interaction#interaction",
        story.author_path(&authors),
        story.url_fragment,
    );
    db_action(&data.pool, &persistent, move |conn| {
        modifications::update_story_interaction(story, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}
