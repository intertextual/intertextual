use actix_web::http::header::LOCATION;
use actix_web::{get, post, web, HttpRequest, HttpResponse};
use askama::Template;
use serde::{Deserialize, Serialize};

use intertextual::actions;
use intertextual::actions::stories::modifications;
use intertextual::data;
use intertextual::models;
use intertextual::models::filter::FilterMode;
use intertextual::models::stories::OngoingState;

use crate::app::validation::validate_story_description;
use crate::app::validation::validate_story_title;
use crate::prelude::*;

#[derive(Template)]
#[template(path = "story/settings.html")]
struct StoryEditTemplate {
    persistent: PersistentTemplate,
    authors: Vec<models::users::User>,
    author_path: String,
    story: models::stories::Story,
    chapters: Vec<models::stories::ChapterMetadata>,
    show_contents_confirm_box: bool,
    show_ongoing_story_confirm_box: bool,
    show_chapter_index_confirm_box: bool,
    show_chapter_order_confirm_box: bool,
}

#[derive(Serialize, Deserialize)]
pub struct StorySettingsQuery {
    pub confirm: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct StoryEditParams {
    title: String,
    description: RichTagline,
}

#[derive(Serialize, Deserialize)]
pub struct StoryOngoingEditParams {
    ongoing_status: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct StoryMultiChapterEditParams {
    is_multi_chapter: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct ChapterMoveParams {
    new_number: i32,
}

#[get("/@{user}/{story}/0/settings/")]
async fn main_page_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    url_params: web::Query<StorySettingsQuery>,
) -> Result<HttpResponse, AppError> {
    let params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await?;

    main_page_shared(data, persistent, authors, story, params.confirm).await
}

#[get("/collaboration/{story}/0/settings/")]
async fn main_page_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    url_params: web::Query<StorySettingsQuery>,
) -> Result<HttpResponse, AppError> {
    let params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let url_fragment = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await?;

    main_page_shared(data, persistent, authors, story, params.confirm).await
}

async fn main_page_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    confirm: Option<String>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_edition_rights_for(story_id, user_id, &conn)
    })
    .await?;

    let story_id = story.id;
    let filter_mode = FilterMode::from_login(login_user);
    let chapters = db_action(&data.pool, &persistent, move |conn| {
        actions::stories::find_chapters_metadata_by_story_id(story_id, &filter_mode, &conn)
    })
    .await?;

    let author_path = story.author_path(&authors);
    let s = StoryEditTemplate {
        persistent: PersistentTemplate::from(&persistent),
        authors,
        author_path,
        story,
        chapters,
        show_contents_confirm_box: matches!(&confirm, Some(v) if v == "contents"),
        show_ongoing_story_confirm_box: matches!(&confirm, Some(v) if v == "ongoing_story"),
        show_chapter_index_confirm_box: matches!(&confirm, Some(v) if v == "chapter_index"),
        show_chapter_order_confirm_box: matches!(&confirm, Some(v) if v == "chapter_order"),
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/@{user}/{story}/0/settings/")]
async fn handle_story_edit_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    params: web::Form<StoryEditParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await?;

    handle_story_edit_shared(data, persistent, authors, story, params).await
}

#[post("/collaboration/{story}/0/settings/")]
async fn handle_story_edit_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    params: web::Form<StoryEditParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let url_fragment = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await?;

    handle_story_edit_shared(data, persistent, authors, story, params).await
}

async fn handle_story_edit_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    mut story: models::stories::Story,
    params: web::Form<StoryEditParams>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_edition_rights_for(story_id, user_id, &conn)
    })
    .await?;

    let params = params.into_inner();
    let new_title = params.title;
    let new_description = params.description;

    validate_story_title(&new_title, "Story Title").map_err_app(&persistent)?;
    validate_story_description(&new_description, "Description").map_err_app(&persistent)?;

    story.title = String::from(new_title.trim());
    story.description = new_description;

    let return_url = format!(
        "/{}/{}/0/settings/?confirm=contents#contents",
        story.author_path(&authors),
        story.url_fragment,
    );
    db_action(&data.pool, &persistent, move |conn| {
        modifications::update_story_title_desc(story, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}

#[post("/@{user}/{story}/0/settings_ongoing_story/")]
async fn handle_story_ongoing_edit_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    params: web::Form<StoryOngoingEditParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await?;

    handle_story_ongoing_edit_shared(data, persistent, authors, story, params).await
}

#[post("/collaboration/{story}/0/settings_ongoing_story/")]
async fn handle_story_ongoing_edit_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    params: web::Form<StoryOngoingEditParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let url_fragment = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await?;

    handle_story_ongoing_edit_shared(data, persistent, authors, story, params).await
}

async fn handle_story_ongoing_edit_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    params: web::Form<StoryOngoingEditParams>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_edition_rights_for(story_id, user_id, &conn)
    })
    .await?;

    let params = params.into_inner();
    let always_ongoing = matches!(params.ongoing_status.as_deref(), Some("always_ongoing"));
    let ongoing_if_unpublished = matches!(
        params.ongoing_status.as_deref(),
        Some("ongoing_if_upublished")
    );
    let return_url = format!(
        "/{}/{}/0/settings/?confirm=ongoing_story#ongoing_story",
        story.author_path(&authors),
        story.url_fragment,
    );
    db_action(&data.pool, &persistent, move |conn| {
        if always_ongoing {
            modifications::update_story_ongoing_status(story.id, OngoingState::AlwaysOngoing, &conn)
        } else if ongoing_if_unpublished {
            modifications::update_story_ongoing_status(
                story.id,
                OngoingState::OngoingIfUnpublishedChapters,
                &conn,
            )
        } else {
            modifications::update_story_ongoing_status(story.id, OngoingState::NeverOngoing, &conn)
        }
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}

#[post("/@{user}/{story}/0/settings_multi_chapter/")]
async fn handle_story_multi_chapter_edit_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    params: web::Form<StoryMultiChapterEditParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await?;

    handle_story_multi_chapter_edit_shared(data, persistent, authors, story, params).await
}

#[post("/collaboration/{story}/0/settings_multi_chapter/")]
async fn handle_story_multi_chapter_edit_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    params: web::Form<StoryMultiChapterEditParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let url_fragment = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await?;

    handle_story_multi_chapter_edit_shared(data, persistent, authors, story, params).await
}

async fn handle_story_multi_chapter_edit_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    params: web::Form<StoryMultiChapterEditParams>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_edition_rights_for(story_id, user_id, &conn)
    })
    .await?;

    let params = params.into_inner();
    let is_multi_chapter = matches!(params.is_multi_chapter.as_deref(), Some("on"));
    let return_url = format!(
        "/{}/{}/0/settings/?confirm=chapter_index#chapter_index",
        story.author_path(&authors),
        story.url_fragment,
    );
    db_action(&data.pool, &persistent, move |conn| {
        modifications::update_story_multi_chapters(story, is_multi_chapter, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}

#[post("/@{user}/{story}/{chapter}/move/")]
async fn handle_chapter_move_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, i32)>,
    params: web::Form<ChapterMoveParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();

    let (username, url_fragment, chapter_number) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login_user);
    let (authors, story, chapter) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_chapter_by_url(
            username,
            url_fragment,
            chapter_number,
            &mode,
            &conn,
        )
    })
    .await?;

    handle_chapter_move_shared(data, persistent, authors, story, chapter, params).await
}

#[post("/collaboration/{story}/{chapter}/move/")]
async fn handle_chapter_move_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, i32)>,
    params: web::Form<ChapterMoveParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();

    let (url_fragment, chapter_number) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login_user);
    let (authors, story, chapter) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_chapter_by_url(url_fragment, chapter_number, &mode, &conn)
    })
    .await?;

    handle_chapter_move_shared(data, persistent, authors, story, chapter, params).await
}

async fn handle_chapter_move_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
    params: web::Form<ChapterMoveParams>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_edition_rights_for(story_id, user_id, &conn)
    })
    .await?;

    let params = params.into_inner();
    let new_number = params.new_number;

    let return_url = format!(
        "/{}/{}/0/settings/?confirm=chapter_order#chapter_order",
        story.author_path(&authors),
        story.url_fragment,
    );
    db_action(&data.pool, &persistent, move |conn| {
        modifications::move_chapter_to(chapter, new_number, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}
