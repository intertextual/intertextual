use std::ops::Sub;

use actix_web::http::header::LOCATION;
use actix_web::{get, post, web, HttpRequest, HttpResponse};
use askama::Template;
use futures::TryStreamExt;
use log::warn;

use intertextual::actions;
use intertextual::actions::stories::modifications;
use intertextual::data;
use intertextual::data::stories::{story_publication_availability, PublicationAvailability};
use intertextual::models;
use intertextual::models::filter::FilterMode;
use intertextual::models::stories::OngoingState;
use intertextual::utils::publication::get_publication_mode;
use intertextual::utils::publication::PublicationMode;
use intertextual::utils::word_count::count_words_from_html;

use crate::app::multipart;
use crate::app::validation::validate_story_title;
use crate::prelude::*;

#[derive(Template)]
#[template(path = "story/new_chapter.html")]
struct ChapterSubmitTemplate {
    persistent: PersistentTemplate,
    authors: Vec<models::users::User>,
    author_path: String,
    story: models::stories::Story,
    can_manage_publication: bool,
    default_publication_mode: DefaultPublicationMode,
    next_publication_date: String,
    publication_availability: PublicationAvailability,
    default_foreword: SanitizedHtml,
    default_afterword: SanitizedHtml,
}

#[derive(Eq, PartialEq)]
pub enum DefaultPublicationMode {
    Now,
    Later,
    DoNotPublish,
}

#[get("/@{author}/{story}/0/new_chapter/")]
async fn main_page_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let (username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login(login_user);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await?;

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_edition_rights_for(story_id, user_id, &conn)
    })
    .await?;

    let story_id = story.id;
    let user_id = login_user.id;

    let publication_availability = db_action(&data.pool, &persistent, move |conn| {
        story_publication_availability(story_id, &conn)
    })
    .await?;
    let can_manage_story = db_action(&data.pool, &persistent, move |conn| {
        actions::access::get_creative_role(story_id, user_id, &conn)
    })
    .await?
    .can_manage();

    let user_settings = login_user.get_settings();

    let default_publication_date =
        user_settings.get_next_publication_date(chrono::Utc::now().naive_utc());

    let author_path = story.author_path(&authors);
    let s = ChapterSubmitTemplate {
        persistent: PersistentTemplate::from(&persistent),
        authors,
        author_path,
        story,
        can_manage_publication: can_manage_story,
        default_publication_mode: match user_settings.preferred_publication_mode {
            models::users::PreferredPublicationMode::PublishNow => DefaultPublicationMode::Now,
            models::users::PreferredPublicationMode::PublishLater => DefaultPublicationMode::Later,
            models::users::PreferredPublicationMode::DoNotPublish => {
                DefaultPublicationMode::DoNotPublish
            }
        },
        next_publication_date: default_publication_date
            .format("%Y-%m-%d %H:%M")
            .to_string(),
        publication_availability,
        default_foreword: login_user.default_foreword.clone(),
        default_afterword: login_user.default_afterword.clone(),
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[get("/collaboration/{story}/0/new_chapter/")]
async fn main_page_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let url_fragment = path.into_inner();
    let mode = FilterMode::from_login(login_user);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await?;

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_edition_rights_for(story_id, user_id, &conn)
    })
    .await?;

    let story_id = story.id;
    let user_id = login_user.id;
    let publication_availability = db_action(&data.pool, &persistent, move |conn| {
        story_publication_availability(story_id, &conn)
    })
    .await?;
    let can_manage_story = db_action(&data.pool, &persistent, move |conn| {
        actions::access::get_creative_role(story_id, user_id, &conn)
    })
    .await?
    .can_manage();

    let user_settings = login_user.get_settings();

    let default_publication_date =
        user_settings.get_next_publication_date(chrono::Utc::now().naive_utc());

    let author_path = story.author_path(&authors);
    let s = ChapterSubmitTemplate {
        persistent: PersistentTemplate::from(&persistent),
        authors,
        author_path,
        story,
        can_manage_publication: can_manage_story,
        default_publication_mode: match user_settings.preferred_publication_mode {
            models::users::PreferredPublicationMode::PublishNow => DefaultPublicationMode::Now,
            models::users::PreferredPublicationMode::PublishLater => DefaultPublicationMode::Later,
            models::users::PreferredPublicationMode::DoNotPublish => {
                DefaultPublicationMode::DoNotPublish
            }
        },
        next_publication_date: default_publication_date
            .format("%Y-%m-%d %H:%M")
            .to_string(),
        publication_availability,
        default_foreword: SanitizedHtml::new(),
        default_afterword: SanitizedHtml::new(),
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/@{author}/{story}/0/new_chapter/")]
async fn handle_submit_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    payload: actix_multipart::Multipart,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await?;

    handle_submit_shared(data, persistent, authors, story, payload).await
}

#[post("/collaboration/{story}/0/new_chapter/")]
async fn handle_submit_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    payload: actix_multipart::Multipart,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let url_fragment = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await?;

    handle_submit_shared(data, persistent, authors, story, payload).await
}

async fn handle_submit_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    mut payload: actix_multipart::Multipart,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_edition_rights_for(story_id, user_id, &conn)
    })
    .await?;

    let story_id = story.id;
    let user_id = login_user.id;
    let can_manage_story = db_action(&data.pool, &persistent, move |conn| {
        actions::access::get_creative_role(story_id, user_id, &conn)
    })
    .await?
    .can_manage();

    let mut title: Option<String> = None;
    let mut foreword: SanitizedHtml = SanitizedHtml::new();
    let mut input_choice: Option<String> = None;
    let mut html_from_editor: SanitizedHtml = SanitizedHtml::new();
    let mut html_from_file: SanitizedHtml = SanitizedHtml::new();
    let mut afterword: SanitizedHtml = SanitizedHtml::new();
    let mut publication_mode: Option<String> = None;
    let mut publication_time: Option<String> = None;
    let mut is_last_chapter: Option<String> = None;
    while let Ok(Some(mut field)) = payload.try_next().await {
        let content_disposition = field.content_disposition().clone();
        if !content_disposition.is_form_data() {
            return Err(IntertextualError::FormFieldFormatError {
                form_field_name: "Unknown",
                message: "The data sent to the server is invalid".to_string(),
            }
            .into_app(&persistent));
        }

        let content_disposition_name = content_disposition
            .get_name()
            .ok_or_else(|| IntertextualError::FormFieldFormatError {
                form_field_name: "Unknown",
                message: "The data sent to the server is invalid".to_string(),
            })
            .map_err_app(&persistent)?;

        match content_disposition_name {
            "title" => {
                title = Some(
                    multipart::process_utf8_field_into_string(
                        &persistent,
                        &mut field,
                        multipart::FIELD_SIZE_LIMIT,
                    )
                    .await?,
                );
            }
            "foreword" => {
                foreword = multipart::process_utf8_field_into_sanitized_html(
                    &persistent,
                    &mut field,
                    multipart::CONTENT_SIZE_LIMIT,
                )
                .await?;
            }
            "input_choice" => {
                input_choice = Some(
                    multipart::process_utf8_field_into_string(
                        &persistent,
                        &mut field,
                        multipart::FIELD_SIZE_LIMIT,
                    )
                    .await?,
                );
            }
            "file" => {
                html_from_file = multipart::process_file_stream_into_sanitized_html(
                    &persistent,
                    &mut field,
                    content_disposition,
                    multipart::CONTENT_SIZE_LIMIT,
                )
                .await?;
            }
            "content" => {
                html_from_editor = multipart::process_utf8_field_into_sanitized_html(
                    &persistent,
                    &mut field,
                    multipart::CONTENT_SIZE_LIMIT,
                )
                .await?;
            }
            "afterword" => {
                afterword = multipart::process_utf8_field_into_sanitized_html(
                    &persistent,
                    &mut field,
                    multipart::CONTENT_SIZE_LIMIT,
                )
                .await?;
            }
            "publication_mode" => {
                publication_mode = Some(
                    multipart::process_utf8_field_into_string(
                        &persistent,
                        &mut field,
                        multipart::FIELD_SIZE_LIMIT,
                    )
                    .await?,
                );
            }
            "publication_time" => {
                publication_time = Some(
                    multipart::process_utf8_field_into_string(
                        &persistent,
                        &mut field,
                        multipart::FIELD_SIZE_LIMIT,
                    )
                    .await?,
                );
            }
            "is_last_chapter" => {
                is_last_chapter = Some(
                    multipart::process_utf8_field_into_string(
                        &persistent,
                        &mut field,
                        multipart::FIELD_SIZE_LIMIT,
                    )
                    .await?,
                );
            }
            _ => {
                warn!(
                    "Received unknown field {}. Ignorning.",
                    content_disposition_name
                );
            }
        }
    }

    let title = title
        .ok_or_else(|| IntertextualError::FormFieldFormatError {
            form_field_name: "Chapter Title",
            message: "This form field was missing from the input data".to_string(),
        })
        .map_err_app(&persistent)?;

    let content = match input_choice.as_deref() {
        Some("direct") => html_from_editor,
        Some("file") => html_from_file,
        _ => {
            return Err(IntertextualError::FormFieldFormatError {
                form_field_name: "Input mode",
                message: "An invalid input mode was selected".to_string(),
            }
            .into_app(&persistent));
        }
    };

    let story_id = story.id;
    let publication_availability = db_action(&data.pool, &persistent, move |conn| {
        story_publication_availability(story_id, &conn)
    })
    .await?;

    let publication = if can_manage_story
        && matches!(
            publication_availability,
            PublicationAvailability::CanBePublished
        ) {
        let publication_mode = publication_mode
            .ok_or_else(|| IntertextualError::FormFieldFormatError {
                form_field_name: "Publication mode",
                message: "This form field was missing from the input data".to_string(),
            })
            .map_err_app(&persistent)?;
        let publication_time = publication_time
            .ok_or_else(|| IntertextualError::FormFieldFormatError {
                form_field_name: "Publication time",
                message: "This form field was missing from the input data".to_string(),
            })
            .map_err_app(&persistent)?;
        get_publication_mode(&publication_mode, &publication_time).map_err_app(&persistent)?
    } else {
        PublicationMode::DoNotPublish
    };

    let word_count = count_words_from_html(&content);
    let new_chapter = models::stories::NewChapter {
        story_id: story.id,
        title: if title.trim().is_empty() {
            None
        } else {
            validate_story_title(&title, "title").map_err_app(&persistent)?;
            Some(String::from(title.trim()))
        },
        foreword,
        content,
        word_count,
        afterword,
    };

    let max_pub_date = chrono::Utc::now()
        .naive_utc()
        .sub(chrono::Duration::minutes(2));
    if let PublicationMode::PublishLater(publication_date) = publication {
        if max_pub_date > publication_date {
            return Err(IntertextualError::FormFieldFormatError {
                form_field_name: "Publication Date",
                message: format!(
                    "The publication date {} is in the past, which is not allowed.",
                    publication_date.format("%Y-%m-%d %H:%M")
                ),
            }
            .into_app(&persistent));
        }
    }
    let is_last_chapter = matches!(is_last_chapter.as_deref(), Some("on"));

    let story_id = story.id;
    let result = db_action(&data.pool, &persistent, move |conn| {
        if is_last_chapter {
            modifications::update_story_ongoing_status(
                story_id,
                OngoingState::OngoingIfUnpublishedChapters,
                &conn,
            )?;
        }
        modifications::create_new_chapter(new_chapter, &conn)
    })
    .await;
    if crate::app::is_unique_violation(&result) {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Story URL",
            message: "This story URL is already taken.".to_string(),
        }
        .into_app(&persistent));
    }
    let chapter = result?;
    let return_url = format!(
        "/{}/{}/{}/?confirm=new_chapter",
        story.author_path(&authors),
        story.url_fragment,
        chapter.chapter_number,
    );

    match publication {
        PublicationMode::PublishNow if can_manage_story => {
            let publication_date = chrono::Utc::now().naive_utc();
            db_action(&data.pool, &persistent, move |conn| {
                modifications::set_public_after(chapter, Some(publication_date), &conn)
            })
            .await?;
        }
        PublicationMode::PublishLater(publication_date) if can_manage_story => {
            db_action(&data.pool, &persistent, move |conn| {
                modifications::set_public_after(chapter, Some(publication_date), &conn)
            })
            .await?;
        }
        _ => {}
    }

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}
