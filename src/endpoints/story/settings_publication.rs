use actix_web::http::header::LOCATION;
use actix_web::{get, post, web, HttpRequest, HttpResponse};
use askama::Template;
use intertextual::data::stories::{story_publication_availability, PublicationAvailability};
use serde::{Deserialize, Serialize};

use intertextual::actions;
use intertextual::actions::stories::modifications;
use intertextual::data;
use intertextual::models;
use intertextual::models::error::IntertextualError;
use intertextual::models::filter::FilterMode;
use intertextual::models::shared::AppState;
use intertextual::utils::publication::get_publication_mode;
use intertextual::utils::publication::PublicationMode;

use crate::app::identity::Identity;
use crate::app::persistent::*;
use crate::error::*;
use crate::prelude::*;

#[derive(Template)]
#[template(path = "story/publication.html")]
struct PublicationTemplate {
    persistent: PersistentTemplate,
    authors: Vec<models::users::User>,
    author_path: String,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
    is_standalone: bool,
    default_publication_mode: DefaultPublicationMode,
    publication_availability: PublicationAvailability,
    next_publication_date: String,
    show_publication_confirm_box: bool,
    show_tag_edition_confirm_box: bool,
}

#[derive(Eq, PartialEq)]
pub enum DefaultPublicationMode {
    Now,
    Later,
    DoNotPublish,
}

#[derive(Serialize, Deserialize)]
pub struct PublicationSettingsQuery {
    pub confirm: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct PublicationParams {
    publication_mode: Option<String>,
    publication_time: Option<String>,
}

#[get("/@{user}/{story}/{chapter}/publication/")]
async fn main_page_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, i32)>,
    url_params: web::Query<PublicationSettingsQuery>,
) -> Result<HttpResponse, AppError> {
    let params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();

    let (username, url_fragment, chapter_number) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login_user);
    let (authors, story, chapter) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_chapter_by_url(
            username,
            url_fragment,
            chapter_number,
            &mode,
            &conn,
        )
    })
    .await?;

    main_page_shared(data, persistent, authors, story, chapter, params.confirm).await
}

#[get("/collaboration/{story}/{chapter}/publication/")]
async fn main_page_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, i32)>,
    url_params: web::Query<PublicationSettingsQuery>,
) -> Result<HttpResponse, AppError> {
    let params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();

    let (url_fragment, chapter_number) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login_user);
    let (authors, story, chapter) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_chapter_by_url(url_fragment, chapter_number, &mode, &conn)
    })
    .await?;

    main_page_shared(data, persistent, authors, story, chapter, params.confirm).await
}

async fn main_page_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
    confirm: Option<String>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_management_rights_for(story_id, user_id, &conn)
    })
    .await?;

    let story_id = story.id;
    let filter_mode = FilterMode::from_login(login_user);
    let (is_standalone, publication_availability) =
        db_action(&data.pool, &persistent, move |conn| {
            Ok((
                actions::stories::story_is_standalone(story_id, &filter_mode, &conn)?,
                story_publication_availability(story_id, &conn)?,
            ))
        })
        .await?;

    let user_settings = login_user.get_settings();
    let default_publication_mode = if chapter.is_published() {
        DefaultPublicationMode::Now
    } else {
        match user_settings.preferred_publication_mode {
            models::users::PreferredPublicationMode::PublishNow => DefaultPublicationMode::Now,
            models::users::PreferredPublicationMode::PublishLater => DefaultPublicationMode::Later,
            models::users::PreferredPublicationMode::DoNotPublish => {
                DefaultPublicationMode::DoNotPublish
            }
        }
    };
    let default_publication_date =
        user_settings.get_next_publication_date(chrono::Utc::now().naive_utc());

    let author_path = story.author_path(&authors);
    let s = PublicationTemplate {
        persistent: PersistentTemplate::from(&persistent),
        authors,
        author_path,
        story,
        chapter,
        is_standalone,
        default_publication_mode,
        publication_availability,
        next_publication_date: default_publication_date
            .format("%Y-%m-%d %H:%M")
            .to_string(),
        show_publication_confirm_box: matches!(&confirm, Some(v) if v == "publication"),
        show_tag_edition_confirm_box: matches!(&confirm, Some(v) if v == "tag_edition"),
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/@{user}/{story}/{chapter}/publication/")]
async fn handle_chapter_publication_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, i32)>,
    form: web::Form<PublicationParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();

    let (username, url_fragment, chapter_number) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login_user);
    let (authors, story, chapter) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_chapter_by_url(
            username,
            url_fragment,
            chapter_number,
            &mode,
            &conn,
        )
    })
    .await?;

    handle_chapter_publication_shared(data, persistent, authors, story, chapter, form).await
}

#[post("/collaboration/{story}/{chapter}/publication/")]
async fn handle_chapter_publication_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, i32)>,
    form: web::Form<PublicationParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();

    let (url_fragment, chapter_number) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login_user);
    let (authors, story, chapter) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_chapter_by_url(url_fragment, chapter_number, &mode, &conn)
    })
    .await?;

    handle_chapter_publication_shared(data, persistent, authors, story, chapter, form).await
}

async fn handle_chapter_publication_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
    form: web::Form<PublicationParams>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;
    let form = form.into_inner();

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_management_rights_for(story_id, user_id, &conn)
    })
    .await?;

    let publication_mode = form
        .publication_mode
        .ok_or_else(|| IntertextualError::FormFieldFormatError {
            form_field_name: "Publication mode",
            message: "This form field was missing from the input data".to_string(),
        })
        .map_err_app(&persistent)?;
    let publication_time = form
        .publication_time
        .ok_or_else(|| IntertextualError::FormFieldFormatError {
            form_field_name: "Publication time",
            message: "This form field was missing from the input data".to_string(),
        })
        .map_err_app(&persistent)?;

    let publication =
        get_publication_mode(&publication_mode, &publication_time).map_err_app(&persistent)?;

    let story_id = story.id;
    let publication_availability = db_action(&data.pool, &persistent, move |conn| {
        story_publication_availability(story_id, &conn)
    })
    .await?;

    match publication {
        PublicationMode::PublishNow => match publication_availability {
            PublicationAvailability::BlockedByTags(_) => {
                let return_url = format!(
                    "/{}/{}/{}/publication/",
                    story.author_path(&authors),
                    story.url_fragment,
                    chapter.chapter_number,
                );
                Ok(HttpResponse::SeeOther()
                    .insert_header((LOCATION, return_url))
                    .finish())
            }
            PublicationAvailability::CanBePublished => {
                let return_url = format!(
                    "/{}/{}/{}/?confirm=publication",
                    story.author_path(&authors),
                    story.url_fragment,
                    chapter.chapter_number,
                );
                let publication_date = chrono::Utc::now().naive_utc();
                db_action(&data.pool, &persistent, move |conn| {
                    modifications::set_public_after(chapter, Some(publication_date), &conn)
                })
                .await?;
                Ok(HttpResponse::SeeOther()
                    .insert_header((LOCATION, return_url))
                    .finish())
            }
        },
        PublicationMode::PublishLater(publication_date) => match publication_availability {
            PublicationAvailability::BlockedByTags(_) => {
                let return_url = format!(
                    "/{}/{}/{}/publication/",
                    story.author_path(&authors),
                    story.url_fragment,
                    chapter.chapter_number,
                );
                Ok(HttpResponse::SeeOther()
                    .insert_header((LOCATION, return_url))
                    .finish())
            }
            PublicationAvailability::CanBePublished => {
                let return_url = format!(
                    "/{}/{}/{}/?confirm=publication",
                    story.author_path(&authors),
                    story.url_fragment,
                    chapter.chapter_number,
                );
                db_action(&data.pool, &persistent, move |conn| {
                    modifications::set_public_after(chapter, Some(publication_date), &conn)
                })
                .await?;
                Ok(HttpResponse::SeeOther()
                    .insert_header((LOCATION, return_url))
                    .finish())
            }
        },
        PublicationMode::DoNotPublish => {
            let return_url = format!(
                "/{}/{}/{}/?confirm=publication",
                story.author_path(&authors),
                story.url_fragment,
                chapter.chapter_number,
            );
            db_action(&data.pool, &persistent, move |conn| {
                modifications::set_public_after(chapter, None, &conn)
            })
            .await?;
            Ok(HttpResponse::SeeOther()
                .insert_header((LOCATION, return_url))
                .finish())
        }
    }
}
