use actix_web::{get, web, HttpRequest, HttpResponse};
use askama::Template;
use serde::Deserialize;

use intertextual::actions;
use intertextual::models::filter::FilterMode;
use intertextual::models::stories::StoryEntry;
use intertextual::models::stories::StoryQueryExtraInfos;
use intertextual::utils::page_list::PageListing;

use crate::prelude::*;

const STORIES_PER_PAGE: i64 = 20;

#[derive(Template)]
#[template(path = "story/story_list.html")]
struct StoryListTemplate {
    persistent: PersistentTemplate,
    sort_mode: SortByMode,
    stories: Vec<StoryEntry>,
    excluded_stories: Vec<StoryEntry>,
    page_listing: PageListing,
}

#[derive(Deserialize)]
struct StoryListParams {
    start: Option<String>,
    sort_by: Option<String>,
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum SortByMode {
    TitleAsc,
    TitleDesc,
    UpdateAsc,
    UpdateDesc,
}

impl SortByMode {
    pub fn parse(sort_by: &Option<String>) -> SortByMode {
        match sort_by.as_deref() {
            Some(t) if t == "title_asc" => SortByMode::TitleAsc,
            Some(t) if t == "title_desc" => SortByMode::TitleDesc,
            Some(t) if t == "update_asc" => SortByMode::UpdateAsc,
            Some(t) if t == "update_desc" => SortByMode::UpdateDesc,
            _ => SortByMode::UpdateDesc,
        }
    }

    pub fn to_story_sort_mode(self) -> intertextual::models::stories::StorySortMode {
        use intertextual::models::stories::StorySortMode;
        match self {
            SortByMode::TitleAsc => StorySortMode::TitleAsc,
            SortByMode::TitleDesc => StorySortMode::TitleDesc,
            SortByMode::UpdateAsc => StorySortMode::LastUpdateAsc,
            SortByMode::UpdateDesc => StorySortMode::LastUpdateDesc,
        }
    }

    pub fn next_title_sort(&self) -> &'static str {
        match self {
            Self::TitleAsc => "title_desc",
            _ => "title_asc",
        }
    }

    pub fn title_symbol(&self) -> &'static str {
        match self {
            Self::TitleAsc => "↓",
            Self::TitleDesc => "↑",
            _ => "",
        }
    }

    pub fn next_update_sort(&self) -> &'static str {
        match self {
            Self::UpdateDesc => "update_asc",
            _ => "update_desc",
        }
    }

    pub fn update_symbol(&self) -> &'static str {
        match self {
            Self::UpdateAsc => "↑",
            Self::UpdateDesc => "↓",
            _ => "",
        }
    }

    pub fn current_amp(&self) -> &'static str {
        match self {
            SortByMode::TitleAsc => "",
            SortByMode::TitleDesc => "&sort_by=title_desc",
            SortByMode::UpdateAsc => "&sort_by=update_asc",
            SortByMode::UpdateDesc => "&sort_by=update_desc",
        }
    }
}

#[get("/stories/")]
async fn main_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<StoryListParams>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let filter_mode = FilterMode::from_login_opt(&login_user);

    let sort_mode = SortByMode::parse(&url_params.sort_by);
    let start_index = url_params
        .start
        .and_then(|s| s.parse::<i64>().ok())
        .unwrap_or(0);
    let user_info = StoryQueryExtraInfos::from_maybe_user(&data.site, &login_user);
    let story_sort_mode = sort_mode.to_story_sort_mode();
    let (stories, quantity) = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<(Vec<StoryEntry>, i64), IntertextualError> {
            let stories_quantity = actions::stories::find_stories_quantity(&filter_mode, &conn)?;
            let stories = actions::stories::find_stories_by_sort_mode(
                start_index,
                STORIES_PER_PAGE,
                story_sort_mode,
                &filter_mode,
                &conn,
            )?;
            let mut results = Vec::<StoryEntry>::new();
            results.reserve(stories.len());
            for story in stories {
                results.push(StoryEntry::from_database(story, &user_info, &conn)?);
            }
            Ok((results, stories_quantity))
        },
    )
    .await?;

    let page_listing = PageListing::get_from_count(quantity, start_index, STORIES_PER_PAGE);

    let (excluded_stories, stories) = stories.into_iter().partition(StoryEntry::is_excluded);

    let s = StoryListTemplate {
        persistent: PersistentTemplate::from(&persistent),
        sort_mode,
        stories,
        excluded_stories,
        page_listing,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}
