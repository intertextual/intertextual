use actix_web::http::header::LOCATION;
use actix_web::{get, post, web, HttpRequest, HttpResponse};
use askama::Template;
use futures::TryStreamExt;
use log::warn;

use intertextual::actions;
use intertextual::actions::stories::modifications;
use intertextual::data;
use intertextual::models;
use intertextual::models::filter::FilterMode;
use intertextual::utils::word_count::count_words_from_html;

use crate::app::multipart;
use crate::app::validation::validate_story_title;
use crate::prelude::*;

#[derive(Template)]
#[template(path = "story/chapter_edit.html")]
struct StoryEditTemplate {
    persistent: PersistentTemplate,
    authors: Vec<models::users::User>,
    author_path: String,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
    is_standalone: bool,
}

#[get("/@{user}/{story}/{chapter}/edit/")]
async fn main_page_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, i32)>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();

    let (username, url_fragment, chapter_number) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login_user);
    let (authors, story, chapter) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_chapter_by_url(
            username,
            url_fragment,
            chapter_number,
            &mode,
            &conn,
        )
    })
    .await?;

    main_page_shared(data, persistent, authors, story, chapter).await
}

#[get("/collaboration/{story}/{chapter}/edit/")]
async fn main_page_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, i32)>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();

    let (url_fragment, chapter_number) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login_user);
    let (authors, story, chapter) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_chapter_by_url(url_fragment, chapter_number, &mode, &conn)
    })
    .await?;

    main_page_shared(data, persistent, authors, story, chapter).await
}

async fn main_page_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_edition_rights_for(story_id, user_id, &conn)
    })
    .await?;

    let story_id = story.id;
    let mode = FilterMode::from_login(login_user);
    let is_standalone = db_action(&data.pool, &persistent, move |conn| {
        actions::stories::story_is_standalone(story_id, &mode, &conn)
    })
    .await?;

    let author_path = story.author_path(&authors);
    let s = StoryEditTemplate {
        persistent: PersistentTemplate::from(&persistent),
        authors,
        author_path,
        story,
        chapter,
        is_standalone,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/@{user}/{story}/{chapter}/edit/")]
async fn handle_chapter_edit_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, i32)>,
    payload: actix_multipart::Multipart,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();

    let (username, url_fragment, chapter_number) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login_user);
    let (authors, story, chapter) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_chapter_by_url(
            username,
            url_fragment,
            chapter_number,
            &mode,
            &conn,
        )
    })
    .await?;

    handle_chapter_edit_shared(data, persistent, authors, story, chapter, payload).await
}

#[post("/collaboration/{story}/{chapter}/edit/")]
async fn handle_chapter_edit_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, i32)>,
    payload: actix_multipart::Multipart,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();

    let (url_fragment, chapter_number) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login_user);
    let (authors, story, chapter) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_chapter_by_url(url_fragment, chapter_number, &mode, &conn)
    })
    .await?;

    handle_chapter_edit_shared(data, persistent, authors, story, chapter, payload).await
}

async fn handle_chapter_edit_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    mut chapter: models::stories::Chapter,
    mut payload: actix_multipart::Multipart,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_edition_rights_for(story_id, user_id, &conn)
    })
    .await?;

    let mut title: Option<String> = None;
    let mut input_choice: Option<String> = None;
    let mut do_not_notify_users: Option<String> = None;
    let mut new_foreword: SanitizedHtml = SanitizedHtml::new();
    let mut html_from_editor: SanitizedHtml = SanitizedHtml::new();
    let mut html_from_file: SanitizedHtml = SanitizedHtml::new();
    let mut new_afterword: SanitizedHtml = SanitizedHtml::new();
    while let Ok(Some(mut field)) = payload.try_next().await {
        let content_disposition = field.content_disposition().clone();
        if !content_disposition.is_form_data() {
            return Err(IntertextualError::FormFieldFormatError {
                form_field_name: "Unknown",
                message: "The data sent to the server is invalid".to_string(),
            }
            .into_app(&persistent));
        }

        let content_disposition_name = content_disposition
            .get_name()
            .ok_or_else(|| IntertextualError::FormFieldFormatError {
                form_field_name: "Unknown",
                message: "The data sent to the server is invalid".to_string(),
            })
            .map_err_app(&persistent)?;

        match content_disposition_name {
            "title" => {
                title = Some(
                    multipart::process_utf8_field_into_string(
                        &persistent,
                        &mut field,
                        multipart::FIELD_SIZE_LIMIT,
                    )
                    .await?,
                );
            }
            "input_choice" => {
                input_choice = Some(
                    multipart::process_utf8_field_into_string(
                        &persistent,
                        &mut field,
                        multipart::FIELD_SIZE_LIMIT,
                    )
                    .await?,
                );
            }
            "do_not_notify_users" => {
                do_not_notify_users = Some(
                    multipart::process_utf8_field_into_string(
                        &persistent,
                        &mut field,
                        multipart::FIELD_SIZE_LIMIT,
                    )
                    .await?,
                );
            }
            "foreword" => {
                new_foreword = multipart::process_utf8_field_into_sanitized_html(
                    &persistent,
                    &mut field,
                    multipart::CONTENT_SIZE_LIMIT,
                )
                .await?;
            }
            "file" => {
                html_from_file = multipart::process_file_stream_into_sanitized_html(
                    &persistent,
                    &mut field,
                    content_disposition,
                    multipart::CONTENT_SIZE_LIMIT,
                )
                .await?;
            }
            "content" => {
                html_from_editor = multipart::process_utf8_field_into_sanitized_html(
                    &persistent,
                    &mut field,
                    multipart::CONTENT_SIZE_LIMIT,
                )
                .await?;
            }
            "afterword" => {
                new_afterword = multipart::process_utf8_field_into_sanitized_html(
                    &persistent,
                    &mut field,
                    multipart::CONTENT_SIZE_LIMIT,
                )
                .await?;
            }
            _ => {
                warn!(
                    "Received unknown field {}. Ignorning.",
                    content_disposition_name
                );
            }
        }
    }

    let title = title
        .ok_or_else(|| IntertextualError::FormFieldFormatError {
            form_field_name: "Chapter Title",
            message: "This form field was missing from the input data".to_string(),
        })
        .map_err_app(&persistent)?;

    let new_content = match input_choice.as_deref() {
        Some("direct") => html_from_editor,
        Some("file") => html_from_file,
        _ => {
            return Err(IntertextualError::FormFieldFormatError {
                form_field_name: "Input mode",
                message: "An invalid input mode was selected".to_string(),
            }
            .into_app(&persistent));
        }
    };

    let do_not_notify_users = matches!(do_not_notify_users.as_deref(), Some("on"));

    let new_title = title.trim();
    let new_title = if new_title.is_empty() {
        None
    } else {
        validate_story_title(new_title, "title").map_err_app(&persistent)?;
        Some(String::from(new_title))
    };
    let new_word_count = count_words_from_html(&new_content);
    chapter.title = new_title;
    chapter.foreword = new_foreword;
    chapter.content = new_content;
    chapter.afterword = new_afterword;
    chapter.word_count = new_word_count;

    let return_url = format!(
        "/{}/{}/{}/?confirm=edit_chapter",
        story.author_path(&authors),
        story.url_fragment,
        chapter.chapter_number,
    );

    db_action(&data.pool, &persistent, move |conn| {
        modifications::update_chapter(chapter, do_not_notify_users, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}

#[post("/@{user}/{story}/{chapter}/delete/")]
async fn handle_chapter_delete_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, i32)>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();

    let (username, url_fragment, chapter_number) = path.into_inner();

    let mode = FilterMode::from_login_opt(&login_user);
    let (authors, story, chapter) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_chapter_by_url(
            username,
            url_fragment,
            chapter_number,
            &mode,
            &conn,
        )
    })
    .await?;

    handle_chapter_delete_shared(data, persistent, authors, story, chapter).await
}

#[post("/collaboration/{story}/{chapter}/delete/")]
async fn handle_chapter_delete_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, i32)>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let (url_fragment, chapter_number) = path.into_inner();

    let mode = FilterMode::from_login_opt(&login_user);
    let (authors, story, chapter) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_chapter_by_url(url_fragment, chapter_number, &mode, &conn)
    })
    .await?;

    handle_chapter_delete_shared(data, persistent, authors, story, chapter).await
}

async fn handle_chapter_delete_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let mode = FilterMode::from_login(login_user);
    let story_id = story.id;
    let chapter_list = db_action(&data.pool, &persistent, move |conn| {
        actions::stories::find_chapters_by_story_id(story_id, &mode, &conn)
    })
    .await?;

    let return_url = if chapter_list.len() > 2 {
        format!(
            "/{}/{}?confirm=delete_chapter",
            story.author_path(&authors),
            story.url_fragment,
        )
    } else {
        match authors.first() {
            Some(author) => format!("/@{}/?confirm=delete_chapter", author.username,),
            None => "/".to_string(),
        }
    };

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_edition_rights_for(story_id, user_id, &conn)
    })
    .await?;

    if chapter.is_published() {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Deletion",
            message: "You cannot delete this chapter, as it is currently published. You need to unpublish it first.".to_string(),
        })
        .map_err_app(&persistent);
    }

    let _ = db_action(&data.pool, &persistent, move |conn| {
        modifications::delete_chapter(chapter, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}
