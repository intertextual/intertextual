use actix_web::http::header::LOCATION;
use actix_web::{get, post, web, HttpRequest, HttpResponse};
use askama::Template;
use serde::{Deserialize, Serialize};

use intertextual::actions;
use intertextual::data;
use intertextual::models;
use intertextual::models::error::IntertextualError;
use intertextual::models::filter::FilterMode;
use intertextual::models::shared::AppState;
use intertextual::models::stories::Story;
use intertextual::models::tags;
use intertextual::models::users::User;

use crate::app::identity::Identity;
use crate::app::persistent::*;
use crate::app::validation::validate_tag_name;
use crate::error::*;
use crate::prelude::*;

const MAX_RECENT_TAGS: usize = 15;
const MAX_ALLOWED_CUSTOM_TAGS: usize = 250;

#[derive(Template)]
#[template(path = "story/settings_tags.html")]
struct StoryEditTemplate {
    persistent: PersistentTemplate,
    authors: Vec<User>,
    author_path: String,
    story: Story,
    recent_tags: Vec<CheckableTagEntry>,
    categories: Vec<TagCategoryEntry>,
    selected_tags: Vec<SelectedTagMode>,
    story_creation: bool,
    show_draft_creation_confirm_box: bool,
    show_tag_selection_confirm_box: bool,
    show_tag_edition_confirm_box: bool,
}

struct TagCategoryEntry {
    id: uuid::Uuid,
    name: String,
    description: String,
    checkable_tags: Vec<CheckableTagEntry>,
    allow_custom_tags: bool,
    other_tags: Vec<String>,
}

struct CheckableTagEntry {
    id: uuid::Uuid,
    name: String,
    description: String,
    checked: bool,
}

struct SelectedTagMode {
    internal_tag_id: uuid::Uuid,
    name: String,
    description: String,
    can_be_minor: bool,
    is_major: bool,
    is_minor: bool,
    is_spoiler: bool,
}

#[derive(Serialize, Deserialize)]
pub struct TagSettingsQuery {
    pub story_creation: Option<String>,
    pub confirm: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct TagSelectParams {
    #[serde(flatten)]
    all_fields: std::collections::BTreeMap<String, String>,
}

#[get("/@{user}/{story}/0/settings_tags/")]
async fn main_page_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    url_params: web::Query<TagSettingsQuery>,
) -> Result<HttpResponse, AppError> {
    let params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await?;

    let story_creation = matches!(params.story_creation, Some(v) if v == "true");
    main_page_shared(
        data,
        persistent,
        authors,
        story,
        story_creation,
        params.confirm,
    )
    .await
}

#[get("/collaboration/{story}/0/settings_tags/")]
async fn main_page_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    url_params: web::Query<TagSettingsQuery>,
) -> Result<HttpResponse, AppError> {
    let params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let url_fragment = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await?;

    let story_creation = matches!(params.story_creation, Some(v) if v == "true");
    main_page_shared(
        data,
        persistent,
        authors,
        story,
        story_creation,
        params.confirm,
    )
    .await
}

type DataQueryResult = (
    Vec<TagCategoryEntry>,
    Vec<CheckableTagEntry>,
    Vec<SelectedTagMode>,
);

async fn main_page_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    story_creation: bool,
    confirm: Option<String>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_edition_rights_for(story_id, user_id, &conn)
    })
    .await?;

    let login_user_id = login_user.id;
    let story_id = story.id;
    let filter_mode = FilterMode::from_login(login_user);
    let (categories, recent_tags, selected_tags) = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<DataQueryResult, IntertextualError> {
            let story_tags = actions::tags::find_internal_tags_by_story_id(story_id, &conn)?;
            let raw_categories = actions::tags::get_all_categories(&conn)?;
            let mut categories = Vec::<TagCategoryEntry>::with_capacity(raw_categories.len());
            for category in raw_categories {
                let category_tags = story_tags
                    .iter()
                    .filter(|(cat, _, _, _)| cat.id == category.id)
                    .collect::<Vec<&(
                        tags::TagCategory,
                        tags::CanonicalTag,
                        tags::InternalTagAssociation,
                        tags::StoryToInternalTagAssociation,
                    )>>();
                let raw_checkable_tags =
                    actions::tags::get_checkable_tags_by_category(category.id, &conn)?;
                let mut checkable_tags =
                    Vec::<CheckableTagEntry>::with_capacity(raw_checkable_tags.len());

                for tag in raw_checkable_tags {
                    let checked = category_tags.iter().any(|(_, t, _, _)| t.id == tag.id);
                    checkable_tags.push(CheckableTagEntry {
                        id: tag.id,
                        name: tag.display_name,
                        description: tag.description.unwrap_or_default(),
                        checked,
                    });
                }
                let mut other_tags = Vec::<String>::new();
                for (_, canonical, internal, _) in category_tags {
                    if checkable_tags.iter().any(|t| t.id == canonical.id) {
                        continue;
                    }
                    other_tags.push(internal.internal_name.clone());
                }

                categories.push(TagCategoryEntry {
                    id: category.id,
                    name: category.display_name,
                    description: category.description,
                    checkable_tags,
                    allow_custom_tags: category.allow_user_defined_tags,
                    other_tags,
                });
            }

            let mut recent_tags: Vec<(i32, chrono::NaiveDateTime, CheckableTagEntry)> = vec![];
            let stories_by_login_user =
                actions::stories::find_all_stories_by_author(login_user_id, &filter_mode, &conn)?;
            for story in stories_by_login_user {
                let tags = actions::tags::find_internal_tags_by_story_id(story.id, &conn)?;
                for (_, tag, _, _) in tags {
                    if let Some(tag_data) = recent_tags.iter_mut().find(|(_, _, t)| t.id == tag.id)
                    {
                        tag_data.0 += 1;
                        tag_data.1 = std::cmp::max(tag_data.1, story.story_internally_created_at);
                        continue;
                    }
                    let checked = story_tags.iter().any(|(_, t, _, _)| t.id == tag.id);
                    recent_tags.push((
                        1,
                        story.story_internally_created_at,
                        CheckableTagEntry {
                            id: tag.id,
                            name: tag.display_name,
                            description: tag.description.unwrap_or_default(),
                            checked,
                        },
                    ));
                }
            }

            let min_time = recent_tags.iter().map(|(_, time, _)| *time).min();
            let max_time = recent_tags.iter().map(|(_, time, _)| *time).max();

            recent_tags.sort_by_cached_key(|(stories, creation, _)| {
                let stories = *stories as f32;
                let min_time = min_time.unwrap_or(*creation);
                let max_time = max_time.unwrap_or(*creation);
                let quotient = (max_time - min_time).num_seconds();
                if quotient > 0 {
                    let creation_percent =
                        (*creation - min_time).num_seconds() as f32 / (quotient as f32);
                    -(100f32 * stories * (0.5f32 + 0.5f32 * creation_percent)) as i64
                } else {
                    -(100f32 * stories) as i64
                }
            });

            let mut recent_tags: Vec<CheckableTagEntry> = recent_tags
                .into_iter()
                .take(MAX_RECENT_TAGS)
                .map(|(_, _, t)| t)
                .collect();
            recent_tags.sort_by(|t1, t2| t1.name.to_lowercase().cmp(&t2.name.to_lowercase()));

            let mut selected_tag_mode: Vec<SelectedTagMode> = Vec::with_capacity(story_tags.len());
            for (category, canonical_tag, tag, association) in story_tags {
                selected_tag_mode.push(SelectedTagMode {
                    internal_tag_id: tag.internal_id,
                    name: if tag.internal_name != canonical_tag.display_name {
                        format!("{} (#{})", tag.internal_name, canonical_tag.display_name)
                    } else {
                        canonical_tag.display_name
                    },
                    description: canonical_tag.description.unwrap_or_default(),
                    can_be_minor: !category.is_always_displayed,
                    is_major: association.tag_type
                        == i32::from(models::tags::TagAssociationType::Major),
                    is_minor: association.tag_type
                        == i32::from(models::tags::TagAssociationType::Standard),
                    is_spoiler: association.tag_type
                        == i32::from(models::tags::TagAssociationType::Spoiler),
                })
            }

            selected_tag_mode.sort_by(|t1, t2| t1.name.to_lowercase().cmp(&t2.name.to_lowercase()));

            Ok((categories, recent_tags, selected_tag_mode))
        },
    )
    .await?;

    let author_path = story.author_path(&authors);
    let s = StoryEditTemplate {
        persistent: PersistentTemplate::from(&persistent),
        authors,
        author_path,
        story,
        recent_tags,
        categories,
        selected_tags,
        story_creation,
        show_draft_creation_confirm_box: matches!(&confirm, Some(v) if v == "draft"),
        show_tag_edition_confirm_box: matches!(&confirm, Some(v) if v == "tag_select"),
        show_tag_selection_confirm_box: matches!(&confirm, Some(v) if v == "tag_edition"),
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/@{user}/{story}/0/settings_tags/select/")]
async fn handle_tag_select_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    params: web::Form<TagSelectParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await?;

    handle_tag_select_shared(data, persistent, authors, story, params).await
}

#[post("/collaboration/{story}/0/settings_tags/select/")]
async fn handle_tag_select_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    params: web::Form<TagSelectParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let url_fragment = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await?;

    handle_tag_select_shared(data, persistent, authors, story, params).await
}

async fn handle_tag_select_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    params: web::Form<TagSelectParams>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_edition_rights_for(story_id, user_id, &conn)
    })
    .await?;

    let params = params.into_inner();

    let author_path = story.author_path(&authors);
    let url_fragment = story.url_fragment.clone();

    let story_id = story.id;
    let all_tag_fields = params.all_fields;
    let story_creation = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<bool, IntertextualError> {
            let existing_tags = actions::tags::find_internal_tags_by_story_id(story_id, &conn)?;
            let set_major_tags_automatically = existing_tags.is_empty();
            let mut tags_to_keep = vec![];
            let mut added_tag_ids = vec![];
            let mut story_creation = false;
            for (name, value) in all_tag_fields {
                if name == "story_creation" && value == "true" {
                    story_creation = true;
                    continue;
                }

                let (category_id, tag_id_or_none) = if let Some(tag_id) = name.strip_prefix("recent_tag_") {
                    match uuid::Uuid::parse_str(tag_id) {
                        Ok(uuid) => (uuid, Some(uuid)),
                        _ => continue,
                    }
                } else if let Some(remainder) = name.strip_prefix("category_") {
                    match remainder.strip_suffix("_others") {
                        Some(uuid) => match uuid::Uuid::parse_str(uuid) {
                            Ok(uuid) => (uuid, None),
                            _ => continue,
                        },
                        None => {
                            // Right. This is a checkbox, and actually a bit tricky to parse...
                            let mut split = remainder.split("_tag_");
                            match (split.next(), split.next()) {
                                (Some(category_uuid), Some(tag_uuid)) => match (uuid::Uuid::parse_str(category_uuid), uuid::Uuid::parse_str(tag_uuid)) {
                                    (Ok(category_uuid), Ok(tag_uuid)) => (category_uuid, Some(tag_uuid)),
                                    _ => continue,
                                },
                                _ => continue,
                            }
                        }
                    }
                } else {
                    continue
                };

                match tag_id_or_none {
                    Some(tag_uuid) => {
                        let tag = match actions::tags::find_canonical_tag_by_id(tag_uuid, &conn)? {
                            Some(tag) => tag,
                            None => continue,
                        };
                        let tag_exists = existing_tags
                                .iter()
                                .find(|(_, t, _, _)| t.id == tag_uuid);
                        match tag_exists {
                            Some((_, _, tag, _ )) => tags_to_keep.push(tag.internal_id),
                            None => {
                                let (internal_tag, _) =
                                actions::tags::modifications::get_or_insert_tag(tag.category_id, &tag.display_name, &conn)?;
                                added_tag_ids.push(internal_tag.internal_id);
                                /* Ignore the errors for this operation : these errors are generated when adding a tag several times to a story */
                                let _ = actions::tags::modifications::add_tag_to_story(
                                    story_id,
                                    internal_tag,
                                    models::tags::TagAssociationType::Standard,
                                    &conn,
                                );
                            }
                        }
                    },
                    None => {
                        // Check if the author is not trying to add too many tags to their story
                        if value.split(' ').nth(MAX_ALLOWED_CUSTOM_TAGS).is_some() {
                            return Err(IntertextualError::FormFieldFormatError {
                                form_field_name: "Custom tags",
                                message: format!("You cannot add more than {} custom tags to a single category.", MAX_ALLOWED_CUSTOM_TAGS),
                            });
                        }

                        let tags = value.split(' ');
                        for raw_tag in tags {
                            let tag_name = raw_tag.strip_prefix('#').unwrap_or(raw_tag).trim();
                            if tag_name.is_empty() {
                                continue;
                            }
                            validate_tag_name(tag_name)?;
                            let tag_exists = existing_tags
                                    .iter()
                                    .find(|(_, _, t, _)| t.internal_name == tag_name);
                            match tag_exists {
                                Some((_, _, tag, _ )) => tags_to_keep.push(tag.internal_id),
                                None => {
                                    let (internal_tag, _) =
                                        actions::tags::modifications::get_or_insert_tag(category_id, tag_name, &conn)?;
                                    added_tag_ids.push(internal_tag.internal_id);
                                    /* Ignore the errors for this operation : these errors are generated when adding a tag several times to a story */
                                    let _ = actions::tags::modifications::add_tag_to_story(
                                        story_id,
                                        internal_tag,
                                        models::tags::TagAssociationType::Standard,
                                        &conn,
                                    );
                                },
                            }
                        }
                    }
                }
            }

            for (_, _, internal_tag, _) in existing_tags {
                if !tags_to_keep
                    .iter()
                    .any(|&t| t == internal_tag.internal_id)
                {
                    // Remove the tags from this story.
                    actions::tags::modifications::remove_tag_from_story(story_id, internal_tag, &conn)?;
                }
            }

            // Set the major tags as needed
            if set_major_tags_automatically {
                let mut top_tags: Vec<(models::tags::InternalTagAssociation, i64)> = Vec::with_capacity(added_tag_ids.len());
                for tag_id in added_tag_ids {
                    if let Some((category, canonical, internal)) = actions::tags::find_internal_tag_hierarchy_by_id(tag_id, &conn)? {
                        if category.is_always_displayed {
                            continue;
                        }
                        let story_count = actions::tags::get_total_story_count(canonical.id, &conn)?;
                        top_tags.push((internal, story_count));
                    }
                }

                top_tags.sort_by_key(|(_t, c)| 1 - *c);

                for (internal_tag, _) in top_tags.into_iter().take(6) {
                    actions::tags::modifications::update_story_association_type(
                        story_id,
                        internal_tag.internal_id,
                        models::tags::TagAssociationType::Major,
                        &conn,
                    )?;
                }
            }

            Ok(story_creation)
        }
    ).await?;

    let return_url = if story_creation {
        format!(
            "/{}/{}/0/settings_tags/?story_creation=true&confirm=tag_select#edit_tag_modes",
            author_path, url_fragment,
        )
    } else {
        format!(
            "/{}/{}/0/settings_tags/?confirm=tag_select#edit_tag_modes",
            author_path, url_fragment,
        )
    };

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}

#[post("/@{user}/{story}/0/settings_tags/edit/")]
async fn handle_tag_edit_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    params: web::Form<TagSelectParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await?;

    handle_tag_edit_shared(data, persistent, authors, story, params).await
}

#[post("/collaboration/{story}/0/settings_tags/edit/")]
async fn handle_tag_edit_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    params: web::Form<TagSelectParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let url_fragment = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await?;

    handle_tag_edit_shared(data, persistent, authors, story, params).await
}

async fn handle_tag_edit_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    params: web::Form<TagSelectParams>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let story_id = story.id;
    let user_id = login_user.id;
    db_action(&data.pool, &persistent, move |conn| {
        actions::access::require_edition_rights_for(story_id, user_id, &conn)
    })
    .await?;

    let params = params.into_inner();

    let author_path = story.author_path(&authors);
    let url_fragment = story.url_fragment.clone();

    let story_id = story.id;
    let all_tag_fields = params.all_fields;
    let story_creation = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<bool, IntertextualError> {
            let mut story_creation = false;
            for (name, value) in all_tag_fields {
                if name == "story_creation" && value == "true" {
                    story_creation = true;
                    continue;
                }
                let internal_tag_id = match name.strip_prefix("tag_") {
                    Some(uuid) => match uuid::Uuid::parse_str(uuid) {
                        Ok(uuid) => uuid,
                        Err(_) => continue,
                    },
                    None => continue,
                };

                let mode = match value.as_str() {
                    "major" => models::tags::TagAssociationType::Major,
                    "minor" => models::tags::TagAssociationType::Standard,
                    "standard" => models::tags::TagAssociationType::Standard,
                    "spoiler" => models::tags::TagAssociationType::Spoiler,
                    "delete" => {
                        let tag = actions::tags::find_internal_tag_by_id(internal_tag_id, &conn)?;
                        if let Some(tag) = tag {
                            actions::tags::modifications::remove_tag_from_story(
                                story_id, tag, &conn,
                            )?;
                        }
                        continue;
                    }
                    _ => continue,
                };

                actions::tags::modifications::update_story_association_type(
                    story_id,
                    internal_tag_id,
                    mode,
                    &conn,
                )?;
            }

            Ok(story_creation)
        },
    )
    .await?;

    let return_url = if story_creation {
        format!(
            "/{}/{}/1/publication?confirm=tag_edition",
            author_path, url_fragment,
        )
    } else {
        format!("/{}/{}/?confirm=tag_edition", author_path, url_fragment,)
    };

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}
