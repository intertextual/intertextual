use actix_web::http::header::LOCATION;
use actix_web::{get, post, web, HttpRequest, HttpResponse};
use askama::Template;
use futures::TryStreamExt;

use intertextual::actions::announcements;
use intertextual::models::admin::NewModerationAction;
use intertextual::models::announcements::{FrontpageAnnouncement, NewFrontpageAnnouncement};

use crate::app::multipart;
use crate::prelude::*;

#[derive(Template)]
#[template(path = "admin/frontpage_announcement.html")]
struct StaticPageTemplate {
    persistent: PersistentTemplate,
    announcements: Vec<FrontpageAnnouncement>,
}

#[get("/admin/frontpage_announcement/")]
async fn main_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let announcements = db_action(&data.pool, &persistent, move |conn| {
        announcements::admin::get_frontpage_announcement_list(&conn)
    })
    .await?;

    let s = StaticPageTemplate {
        persistent: PersistentTemplate::from(&persistent),
        announcements,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/admin/frontpage_announcement/create/")]
async fn create_announcement(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    mut payload: actix_multipart::Multipart,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let mut new_publication_date: Option<chrono::NaiveDateTime> = None;
    let mut new_removal_date: Option<chrono::NaiveDateTime> = None;
    let mut new_content: SanitizedHtml = SanitizedHtml::new();
    while let Ok(Some(mut field)) = payload.try_next().await {
        let content_disposition = field.content_disposition();
        if !content_disposition.is_form_data() {
            return Err(IntertextualError::FormFieldFormatError {
                form_field_name: "Unknown",
                message: "The data sent to the server is invalid".to_string(),
            }
            .into_app(&persistent));
        }

        let content_disposition_name = content_disposition
            .get_name()
            .ok_or_else(|| IntertextualError::FormFieldFormatError {
                form_field_name: "Unknown",
                message: "The data sent to the server is invalid".to_string(),
            })
            .map_err_app(&persistent)?;

        match content_disposition_name {
            "publication_date" => {
                let data = multipart::process_utf8_field_into_string(
                    &persistent,
                    &mut field,
                    multipart::CONTENT_SIZE_LIMIT,
                )
                .await?;
                if !data.is_empty() {
                    new_publication_date =
                        Some(chrono::NaiveDateTime::parse_from_str(&data, "%Y-%m-%d %H:%M")
                            .map_err(|_| {
                                IntertextualError::FormFieldFormatError {
                                    form_field_name: "Publication Date",
                                    message:
                                        "The publication date is invalid. The expected format is : 2020-08-24 11:30"
                                            .to_string(),
                                }
                            })
                            .map_err_app(&persistent)?);
                }
            }
            "removal_date" => {
                let data = multipart::process_utf8_field_into_string(
                    &persistent,
                    &mut field,
                    multipart::CONTENT_SIZE_LIMIT,
                )
                .await?;
                if !data.is_empty() {
                    new_removal_date =
                        Some(chrono::NaiveDateTime::parse_from_str(&data, "%Y-%m-%d %H:%M")
                            .map_err(|_| {
                                IntertextualError::FormFieldFormatError {
                                    form_field_name: "Publication Date",
                                    message:
                                        "The publication date is invalid. The expected format is : 2020-08-24 11:30"
                                            .to_string(),
                                }
                            })
                            .map_err_app(&persistent)?);
                }
            }
            "content" => {
                new_content = multipart::process_utf8_field_into_sanitized_html(
                    &persistent,
                    &mut field,
                    multipart::CONTENT_SIZE_LIMIT,
                )
                .await?;
            }
            _ => {
                log::warn!(
                    "Received unknown field {}. Ignorning.",
                    content_disposition_name
                );
            }
        }
    }

    let new_announcement = NewFrontpageAnnouncement {
        content: new_content,
        remove_after_date: new_removal_date,
        show_publicly_after_date: new_publication_date,
    };

    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: "Created new frontpage announcement".to_string(),
        message: "No message required".to_string(),
    };

    db_action(&data.pool, &persistent, move |conn| {
        announcements::admin::create_frontpage_announcement(new_announcement, action, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, "/admin/frontpage_announcement/"))
        .finish())
}

#[post("/admin/frontpage_announcement/{announcement_id}/edit/")]
async fn update_announcement(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<uuid::Uuid>,
    mut payload: actix_multipart::Multipart,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let notification_id = path.into_inner();

    let mut new_publication_date: Option<chrono::NaiveDateTime> = None;
    let mut new_removal_date: Option<chrono::NaiveDateTime> = None;
    let mut new_content: SanitizedHtml = SanitizedHtml::new();
    while let Ok(Some(mut field)) = payload.try_next().await {
        let content_disposition = field.content_disposition();
        if !content_disposition.is_form_data() {
            return Err(IntertextualError::FormFieldFormatError {
                form_field_name: "Unknown",
                message: "The data sent to the server is invalid".to_string(),
            }
            .into_app(&persistent));
        }

        let content_disposition_name = content_disposition
            .get_name()
            .ok_or_else(|| IntertextualError::FormFieldFormatError {
                form_field_name: "Unknown",
                message: "The data sent to the server is invalid".to_string(),
            })
            .map_err_app(&persistent)?;

        match content_disposition_name {
            "publication_date" => {
                let data = multipart::process_utf8_field_into_string(
                    &persistent,
                    &mut field,
                    multipart::CONTENT_SIZE_LIMIT,
                )
                .await?;
                if !data.is_empty() {
                    new_publication_date =
                        Some(chrono::NaiveDateTime::parse_from_str(&data, "%Y-%m-%d %H:%M")
                            .map_err(|_| {
                                IntertextualError::FormFieldFormatError {
                                    form_field_name: "Publication Date",
                                    message:
                                        "The publication date is invalid. The expected format is : 2020-08-24 11:30"
                                            .to_string(),
                                }
                            })
                            .map_err_app(&persistent)?);
                }
            }
            "removal_date" => {
                let data = multipart::process_utf8_field_into_string(
                    &persistent,
                    &mut field,
                    multipart::CONTENT_SIZE_LIMIT,
                )
                .await?;
                if !data.is_empty() {
                    new_removal_date =
                        Some(chrono::NaiveDateTime::parse_from_str(&data, "%Y-%m-%d %H:%M")
                            .map_err(|_| {
                                IntertextualError::FormFieldFormatError {
                                    form_field_name: "Publication Date",
                                    message:
                                        "The publication date is invalid. The expected format is : 2020-08-24 11:30"
                                            .to_string(),
                                }
                            })
                            .map_err_app(&persistent)?);
                }
            }
            "content" => {
                new_content = multipart::process_utf8_field_into_sanitized_html(
                    &persistent,
                    &mut field,
                    multipart::CONTENT_SIZE_LIMIT,
                )
                .await?;
            }
            _ => {
                log::warn!(
                    "Received unknown field {}. Ignorning.",
                    content_disposition_name
                );
            }
        }
    }

    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!("Edited frontpage announcement {}", notification_id),
        message: "No message required".to_string(),
    };

    db_action(&data.pool, &persistent, move |conn| {
        announcements::admin::update_frontpage_announcement(
            notification_id,
            new_publication_date,
            new_removal_date,
            &new_content,
            action,
            &conn,
        )
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, "/admin/frontpage_announcement/"))
        .finish())
}

#[post("/admin/frontpage_announcement/{announcement_id}/delete/")]
async fn delete_announcement(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<uuid::Uuid>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let notification_id = path.into_inner();

    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!("Removed frontpage announcement {}", notification_id),
        message: "No message required".to_string(),
    };

    db_action(&data.pool, &persistent, move |conn| {
        announcements::admin::delete_frontpage_announcement(notification_id, action, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, "/admin/frontpage_announcement/"))
        .finish())
}
