use actix_web::http::header::LOCATION;
use actix_web::{get, post, web, HttpRequest, HttpResponse};
use askama::Template;
use serde::{Deserialize, Serialize};

use intertextual::actions;
use intertextual::models;
use intertextual::models::admin::NewModerationAction;
use intertextual::models::error::IntertextualError;
use intertextual::models::filter::FilterMode;
use intertextual::models::stories::StoryEntry;
use intertextual::models::stories::StoryQueryExtraInfos;
use intertextual::utils::page_list::PageListing;

use crate::app::validation::validate_admin_message;
use crate::prelude::*;

const COMMENTS_PER_PAGE: i64 = 20;

#[derive(Template)]
#[template(path = "admin/comment_list.html")]
struct AdminCommentListTemplate {
    persistent: PersistentTemplate,
    sort_mode: SortByMode,
    comments: Vec<CommentEntry>,
    page_listing: PageListing,
}

struct CommentEntry {
    pub comment: models::comments::Comment,
    pub comment_author: models::users::User,
    pub story: models::stories::StoryEntry,
    pub chapter: models::stories::Chapter,
}

#[derive(Template)]
#[template(path = "admin/delete_comment.html")]
struct DeleteCommentTemplate {
    persistent: PersistentTemplate,
    authors: Vec<models::users::User>,
    author_path: String,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
    comment_id: uuid::Uuid,
    comment_time: String,
    comment_user_username: String,
    comment_user_display_name: String,
    formatted_comment: SanitizedHtml,
}

#[derive(Template)]
#[template(path = "admin/comment_report.html")]
struct CommentReportTemplate {
    persistent: PersistentTemplate,
    authors: Vec<models::users::User>,
    author_path: String,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
    comment_id: uuid::Uuid,
    comment_time: String,
    comment_user_username: String,
    comment_user_display_name: String,
    formatted_comment: SanitizedHtml,
    report_time: String,
    reporter_username: String,
    reporter_display_name: String,
    report: String,
}

#[derive(Serialize, Deserialize)]
pub struct AdminActionParams {
    message: String,
}

#[derive(Deserialize)]
pub struct CommentsListQuery {
    pub start: Option<String>,
    pub sort_by: Option<String>,
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum SortByMode {
    CreationAsc,
    CreationDesc,
}

impl SortByMode {
    pub fn parse(sort_by: &Option<String>) -> SortByMode {
        match sort_by.as_deref() {
            Some(t) if t == "creation_asc" => SortByMode::CreationAsc,
            Some(t) if t == "creation_desc" => SortByMode::CreationDesc,
            _ => SortByMode::CreationDesc,
        }
    }

    pub fn to_comment_sort_mode(self) -> actions::comments::admin::CommentSortMode {
        use actions::comments::admin::CommentSortMode;
        match self {
            SortByMode::CreationAsc => CommentSortMode::CreationAsc,
            SortByMode::CreationDesc => CommentSortMode::CreationDesc,
        }
    }

    pub fn next_creation_sort(&self) -> &'static str {
        match self {
            Self::CreationDesc => "creation_asc",
            _ => "creation_desc",
        }
    }

    pub fn creation_symbol(&self) -> &'static str {
        match self {
            Self::CreationAsc => "↑",
            Self::CreationDesc => "↓",
        }
    }

    pub fn current_amp(&self) -> &'static str {
        match self {
            SortByMode::CreationAsc => "&sort_by=creation_asc",
            SortByMode::CreationDesc => "",
        }
    }
}

#[get("/admin/comments/")]
async fn admin_comments_list(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<CommentsListQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let start = url_params
        .start
        .and_then(|s| s.parse::<i64>().ok())
        .unwrap_or(0);
    let sort_mode = SortByMode::parse(&url_params.sort_by);

    let comment_sort_mode = sort_mode.to_comment_sort_mode();
    let user_info = StoryQueryExtraInfos::from_user(&data.site, login_user);
    let (comments, comment_quantity) = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<(Vec<CommentEntry>, i64), IntertextualError> {
            let comments = actions::comments::admin::find_comments_by_sort_mode(
                start,
                COMMENTS_PER_PAGE,
                comment_sort_mode,
                &conn,
            )?;
            let mut result: Vec<CommentEntry> = Vec::with_capacity(comments.len());
            for comment in comments {
                if let Some((chapter, story)) =
                    actions::stories::find_chapter_by_id(comment.chapter_id, &conn)?
                {
                    if let Some(comment_author) =
                        actions::users::find_user_by_id(comment.commenter_id, &conn)?
                    {
                        let story = StoryEntry::from_database(story, &user_info, &conn)?;
                        result.push(CommentEntry {
                            comment,
                            comment_author,
                            story,
                            chapter,
                        });
                    }
                }
            }
            Ok((
                result,
                actions::comments::admin::find_total_comments_count(&conn)?,
            ))
        },
    )
    .await?;

    let s = AdminCommentListTemplate {
        persistent: PersistentTemplate::from(&persistent),
        comments,
        page_listing: PageListing::get_from_count(comment_quantity, start, COMMENTS_PER_PAGE),
        sort_mode,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[get("/admin/comments/{id}/")]
async fn main_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<uuid::Uuid>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let comment_id = path.into_inner();

    let filter_mode = FilterMode::from_login(login_user);
    let (authors, story, chapter, user, comment) =
        db_action(&data.pool, &persistent, move |conn| {
            intertextual::data::comments::get_comment(comment_id, &filter_mode, &conn)
        })
        .await?;

    let author_path = story.author_path(&authors);
    let formatted_comment = richblock_to_html(&comment.message, &data.pool, &persistent).await?;
    let s = DeleteCommentTemplate {
        persistent: PersistentTemplate::from(&persistent),
        story,
        chapter,
        authors,
        author_path,
        comment_id,
        comment_time: comment
            .created
            .format("%Y-%m-%d at %H:%M (UTC+00)")
            .to_string(),
        comment_user_username: user.username,
        comment_user_display_name: user.display_name,
        formatted_comment,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[get("/admin/comments_report/{id}/")]
async fn report_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<uuid::Uuid>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let comment_id = path.into_inner();

    let intertextual::data::reports::ReportedComment {
        authors,
        story,
        chapter,
        comment_author,
        comment,
        reporter,
        report,
    } = db_action(&data.pool, &persistent, move |conn| {
        intertextual::data::reports::get_reported_comment(comment_id, &conn)
    })
    .await?;

    let author_path = story.author_path(&authors);
    let formatted_comment = richblock_to_html(&comment.message, &data.pool, &persistent).await?;
    let s = CommentReportTemplate {
        persistent: PersistentTemplate::from(&persistent),
        authors,
        author_path,
        story,
        chapter,
        comment_id,
        comment_time: comment
            .created
            .format("%Y-%m-%d at %H:%M (UTC+00)")
            .to_string(),
        comment_user_username: comment_author.username,
        comment_user_display_name: comment_author.display_name,
        formatted_comment,
        report_time: report
            .report_date
            .format("%Y-%m-%d at %H:%M (UTC+00)")
            .to_string(),
        reporter_username: reporter.username,
        reporter_display_name: reporter.display_name,
        report: report.reason,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/admin/comments/delete/{id}/")]
async fn handle_delete(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<uuid::Uuid>,
    form: web::Form<AdminActionParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let comment_id = path.into_inner();

    let (comment, _, _, _) = db_action(&data.pool, &persistent, move |conn| {
        actions::comments::find_comment(comment_id, &conn)
    })
    .await?
    .ok_or(IntertextualError::CommentNotFound { comment_id })
    .map_err_app(&persistent)?;

    let message = form.into_inner().message;
    validate_admin_message(&message, "Justification").map_err_app(&persistent)?;

    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!(
            "Deleted comment id={} by user id={} for chapter id={}",
            comment.id, comment.commenter_id, comment.chapter_id,
        ),
        message,
    };

    let moderator =
        models::users::ModeratorUserWrapper::from_user(login_user).map_err_app(&persistent)?;
    db_action(&data.pool, &persistent, move |conn| {
        actions::comments::admin::delete_comment(moderator, comment, action, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, "/admin/"))
        .finish())
}
