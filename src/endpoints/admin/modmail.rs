use actix_web::http::header::LOCATION;
use actix_web::{get, post, web, HttpRequest, HttpResponse};
use askama::Template;
use diesel::PgConnection;
use serde::{Deserialize, Serialize};

use intertextual::actions;
use intertextual::models;
use intertextual::models::admin::NewModerationAction;
use intertextual::models::error::IntertextualError;
use intertextual::models::formats::SanitizedHtml;
use intertextual::models::shared::AppState;
use intertextual::utils::page_list::PageListing;

use crate::app::identity::Identity;
use crate::app::persistent::*;
use crate::app::validation::validate_rich_modmail;
use crate::endpoints::filters;
use crate::error::*;
use crate::prelude::*;

const MESSAGES_PER_PAGE: i64 = 50;

#[derive(Template)]
#[template(path = "admin/modmail_list.html")]
struct ModmailListTemplate {
    persistent: PersistentTemplate,
    page_title: String,
    current_fragment: String,
    modmail: Vec<ModmailEntry>,
    page_listing: PageListing,
}

#[derive(Template)]
#[template(path = "admin/modmail_user.html")]
struct ModmailUserTemplate {
    persistent: PersistentTemplate,
    user: models::users::User,
    modmail: Vec<ModmailEntry>,
    page_listing: PageListing,
}

struct ModmailEntry {
    id: uuid::Uuid,
    user: models::users::User,
    message: SanitizedHtml,
    message_date: chrono::NaiveDateTime,
    is_user_to_mods: bool,
    moderator_who_handled_report: Option<models::users::User>,
}

impl ModmailEntry {
    pub fn from_database(
        mail: models::modmail::Modmail,
        conn: &PgConnection,
    ) -> Result<ModmailEntry, IntertextualError> {
        let user =
            actions::users::admin::find_user_by_id_including_deactivated(mail.user_id, conn)?;
        let user = user.ok_or(IntertextualError::ModmailNotFound {
            modmail_id: mail.id,
        })?;
        let moderator_who_handled_report = match mail.handled_by_moderator_id {
            Some(id) => actions::users::admin::find_user_by_id_including_deactivated(id, conn)?,
            None => None,
        };
        let message = mail.message.html(conn);
        Ok(ModmailEntry {
            id: mail.id,
            user,
            message,
            message_date: mail.message_date,
            is_user_to_mods: mail.is_user_to_mods,
            moderator_who_handled_report,
        })
    }
}

#[derive(Serialize, Deserialize)]
pub struct ModmailQuery {
    start: Option<i64>,
}

#[derive(Serialize, Deserialize)]
pub struct ModmailParams {
    message: models::formats::RichBlock,
}

#[get("/admin/modmail/new/")]
async fn get_new(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<ModmailQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let start = url_params.start.unwrap_or(0);
    let (modmail, total_modmail_count) = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<(Vec<ModmailEntry>, i64), IntertextualError> {
            let raw_modmail =
                actions::modmail::admins::get_unhandled_modmails(start, MESSAGES_PER_PAGE, &conn)?;
            let mut modmail = Vec::with_capacity(raw_modmail.len());
            for mail in raw_modmail {
                modmail.push(ModmailEntry::from_database(mail, &conn)?);
            }
            Ok((
                modmail,
                actions::modmail::admins::get_unhandled_modmail_count(&conn)?,
            ))
        },
    )
    .await?;

    let page_listing = PageListing::get_from_count(total_modmail_count, start, MESSAGES_PER_PAGE);
    let s = ModmailListTemplate {
        persistent: PersistentTemplate::from(&persistent),
        page_title: "Unhandled modmails".to_string(),
        current_fragment: "new".to_string(),
        modmail,
        page_listing,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[get("/admin/modmail/all/")]
async fn get_all(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<ModmailQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let start = url_params.start.unwrap_or(0);
    let (modmail, total_modmail_count) = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<(Vec<ModmailEntry>, i64), IntertextualError> {
            let raw_modmail =
                actions::modmail::admins::get_all_modmails(start, MESSAGES_PER_PAGE, &conn)?;
            let mut modmail = Vec::with_capacity(raw_modmail.len());
            for mail in raw_modmail {
                modmail.push(ModmailEntry::from_database(mail, &conn)?);
            }
            Ok((
                modmail,
                actions::modmail::admins::get_all_modmail_count(&conn)?,
            ))
        },
    )
    .await?;

    let page_listing = PageListing::get_from_count(total_modmail_count, start, MESSAGES_PER_PAGE);
    let s = ModmailListTemplate {
        persistent: PersistentTemplate::from(&persistent),
        page_title: "All modmails".to_string(),
        current_fragment: "all".to_string(),
        modmail,
        page_listing,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[get("/admin/modmail/user/@{username}/")]
async fn get_user(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    url_params: web::Query<ModmailQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let username = path.into_inner();

    let username_copy = username.clone();
    let user = db_action(&data.pool, &persistent, move |conn| {
        actions::users::admin::find_user_by_username_including_deactivated(&username_copy, &conn)
    })
    .await?
    .ok_or(IntertextualError::UserNotFound { username })
    .map_err_app(&persistent)?;

    let start = url_params.start.unwrap_or(0);
    let user_id = user.id;
    let (modmail, total_modmail_count) = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<(Vec<ModmailEntry>, i64), IntertextualError> {
            let raw_modmail =
                actions::modmail::get_modmail_for_user(user_id, start, MESSAGES_PER_PAGE, &conn)?;
            let mut modmail = Vec::with_capacity(raw_modmail.len());
            for mail in raw_modmail {
                modmail.push(ModmailEntry::from_database(mail, &conn)?);
            }
            Ok((
                modmail,
                actions::modmail::get_modmail_count_for_user(user_id, &conn)?,
            ))
        },
    )
    .await?;

    let page_listing = PageListing::get_from_count(total_modmail_count, start, MESSAGES_PER_PAGE);
    let s = ModmailUserTemplate {
        persistent: PersistentTemplate::from(&persistent),
        user,
        modmail,
        page_listing,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/admin/modmail/user/@{username}/send/")]
async fn handle_send(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    params: web::Form<ModmailParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let username = path.into_inner();

    let username_copy = username.clone();
    let user = db_action(&data.pool, &persistent, move |conn| {
        actions::users::admin::find_user_by_username_including_deactivated(&username_copy, &conn)
    })
    .await?
    .ok_or(IntertextualError::UserNotFound { username })
    .map_err_app(&persistent)?;

    let params = params.into_inner();
    let message = params.message;
    validate_rich_modmail(&message, "Moderation Message").map_err_app(&persistent)?;
    let new_modmail = models::modmail::NewModmail {
        user_id: user.id,
        message,
    };
    let moderator =
        models::users::ModeratorUserWrapper::from_user(login_user).map_err_app(&persistent)?;
    let _ = db_action(&data.pool, &persistent, move |conn| {
        actions::modmail::admins::send_new_modmail(moderator, new_modmail, &conn)
    })
    .await?;
    Ok(HttpResponse::SeeOther()
        .insert_header((
            LOCATION,
            format!("/admin/modmail/user/@{}/", user.username,),
        ))
        .finish())
}

#[post("/admin/modmail/unhandled/{id}/")]
async fn set_unhandled(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<uuid::Uuid>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let modmail_id = path.into_inner();
    let modmail = db_action(&data.pool, &persistent, move |conn| {
        actions::modmail::admins::get_modmail_by_id(modmail_id, &conn)
    })
    .await?
    .ok_or(IntertextualError::ModmailNotFound { modmail_id })
    .map_err_app(&persistent)?;

    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!("Set modmail from user id={} as unhandled", modmail.user_id),
        message: "No message required".to_string(),
    };
    let _ = db_action(&data.pool, &persistent, move |conn| {
        actions::modmail::admins::set_modmail_as_unhandled(modmail, action, &conn)
    })
    .await?;
    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, "/admin/modmail/new/"))
        .finish())
}

#[post("/admin/modmail/handled/{id}/")]
async fn set_handled(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<uuid::Uuid>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let modmail_id = path.into_inner();
    let modmail = db_action(&data.pool, &persistent, move |conn| {
        actions::modmail::admins::get_modmail_by_id(modmail_id, &conn)
    })
    .await?
    .ok_or(IntertextualError::ModmailNotFound { modmail_id })
    .map_err_app(&persistent)?;

    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!("Set modmail from user id={} as handled", modmail.user_id),
        message: "No message required".to_string(),
    };
    let moderator =
        models::users::ModeratorUserWrapper::from_user(login_user).map_err_app(&persistent)?;
    let _ = db_action(&data.pool, &persistent, move |conn| {
        actions::modmail::admins::set_modmail_as_handled(moderator, modmail, action, &conn)
    })
    .await?;
    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, "/admin/modmail/new/"))
        .finish())
}
