use actix_web::http::header::LOCATION;
use actix_web::{get, post, web, HttpRequest, HttpResponse};
use argonautica::Hasher;
use askama::Template;
use serde::{Deserialize, Serialize};

use intertextual::actions;
use intertextual::models;
use intertextual::models::admin::NewModerationAction;
use intertextual::models::error::IntertextualError;
use intertextual::models::shared::AppState;
use intertextual::utils::page_list::PageListing;

use crate::app::identity::Identity;
use crate::app::persistent::*;
use crate::app::validation::validate_admin_message;
use crate::error::*;
use crate::prelude::*;

// Warn the user for 30 days.
pub const DEFAULT_WARNING_DURATION_IN_DAYS: i64 = 30;
// Mute / Ban the user for a thousand years.
pub const DEFAULT_MUTE_DURATION_IN_DAYS: i64 = 365 * 1_000;
pub const DEFAULT_BAN_DURATION_IN_DAYS: i64 = 365 * 1_000;

const USERS_PER_PAGE: i64 = 50;

#[derive(Template)]
#[template(path = "admin/user_list.html")]
struct AdminUserListTemplate {
    persistent: PersistentTemplate,
    sort_mode: SortByMode,
    users: Vec<models::users::User>,
    page_listing: PageListing,
}

#[derive(Template)]
#[template(path = "admin/user_details.html")]
struct AdminUserDetailsTemplate {
    persistent: PersistentTemplate,
    user: models::users::User,
    account_deletion_request: Option<(chrono::NaiveDateTime, bool)>,
    administrator_promotion_enabled: bool,
}

#[derive(Deserialize)]
pub struct AdminUsersQuery {
    start: Option<String>,
    sort_by: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct AdminActionParams {
    message: String,
}

#[derive(Serialize, Deserialize)]
pub struct AdminTimedActionParams {
    message: String,
    duration: String,
}

#[derive(Serialize, Deserialize)]
pub struct ResetPasswordAdminActionParams {
    message: String,
    new_password: String,
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum SortByMode {
    UsernameAsc,
    UsernameDesc,
    RegistrationDateAsc,
    RegistrationDateDesc,
}

impl SortByMode {
    pub fn parse(sort_by: &Option<String>) -> SortByMode {
        match sort_by.as_deref() {
            Some(t) if t == "username_asc" => SortByMode::UsernameAsc,
            Some(t) if t == "username_desc" => SortByMode::UsernameDesc,
            Some(t) if t == "registration_asc" => SortByMode::RegistrationDateAsc,
            Some(t) if t == "registration_desc" => SortByMode::RegistrationDateDesc,
            _ => SortByMode::UsernameAsc,
        }
    }

    pub fn to_user_sort_mode(self) -> actions::users::admin::UsersSortMode {
        use actions::users::admin::UsersSortMode;
        match self {
            SortByMode::UsernameAsc => UsersSortMode::UsernameAsc,
            SortByMode::UsernameDesc => UsersSortMode::UsernameDesc,
            SortByMode::RegistrationDateAsc => UsersSortMode::RegistrationDateAsc,
            SortByMode::RegistrationDateDesc => UsersSortMode::RegistrationDateDesc,
        }
    }

    pub fn next_username_sort(&self) -> &'static str {
        match self {
            Self::UsernameAsc => "username_desc",
            _ => "username_asc",
        }
    }

    pub fn username_symbol(&self) -> &'static str {
        match self {
            Self::UsernameAsc => "↓",
            Self::UsernameDesc => "↑",
            _ => "",
        }
    }

    pub fn next_registration_sort(&self) -> &'static str {
        match self {
            Self::RegistrationDateDesc => "registration_asc",
            _ => "registration_desc",
        }
    }

    pub fn registration_symbol(&self) -> &'static str {
        match self {
            Self::RegistrationDateAsc => "↑",
            Self::RegistrationDateDesc => "↓",
            _ => "",
        }
    }

    pub fn current_amp(&self) -> &'static str {
        match self {
            SortByMode::UsernameAsc => "",
            SortByMode::UsernameDesc => "&sort_by=username_desc",
            SortByMode::RegistrationDateAsc => "&sort_by=registration_asc",
            SortByMode::RegistrationDateDesc => "&sort_by=registration_desc",
        }
    }
}

#[get("/admin/users/")]
async fn admin_page_users(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<AdminUsersQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let start = url_params
        .start
        .and_then(|s| s.parse::<i64>().ok())
        .unwrap_or(0);
    let sort_mode = SortByMode::parse(&url_params.sort_by);

    let user_sort_mode = sort_mode.to_user_sort_mode();
    let (total_users_count, users) = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<(i64, Vec<models::users::User>), IntertextualError> {
            Ok((
                actions::users::admin::find_all_users_count(&conn)?,
                actions::users::admin::find_all_users_in_range(
                    start,
                    USERS_PER_PAGE,
                    user_sort_mode,
                    &conn,
                )?,
            ))
        },
    )
    .await?;

    let s = AdminUserListTemplate {
        persistent: PersistentTemplate::from(&persistent),
        sort_mode,
        users,
        page_listing: PageListing::get_from_count(total_users_count, start, USERS_PER_PAGE),
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[get("/admin/users/@{username}/")]
async fn admin_page_user_details(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let username = path.into_inner();
    let username_copy = username.clone();
    let user = db_action(&data.pool, &persistent, move |conn| {
        actions::users::admin::find_user_by_username_including_deactivated(&username_copy, &conn)
    })
    .await?
    .ok_or(IntertextualError::UserNotFound { username })
    .map_err_app(&persistent)?;

    let user_id = user.id;
    let account_deletion_request = db_action(&data.pool, &persistent, move |conn| {
        actions::users::get_earliest_active_deletion_request(user_id, &conn)
    })
    .await?;

    let days_after_request = i64::from(data.site.account_deletion_request_duration_days);
    let account_deletion_request =
        account_deletion_request.map(|(request_date, can_be_cancelled)| {
            (
                request_date + chrono::Duration::days(days_after_request),
                can_be_cancelled,
            )
        });

    let administrator_promotion_enabled = login_user.administrator;
    let s = AdminUserDetailsTemplate {
        persistent: PersistentTemplate::from(&persistent),
        user,
        account_deletion_request,
        administrator_promotion_enabled,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/admin/users/@{username}/reset_password/")]
async fn account_reset_password(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    params: web::Path<String>,
    form: web::Form<ResetPasswordAdminActionParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let username = params.into_inner();
    let username_copy = username.clone();
    let user = db_action(&data.pool, &persistent, move |conn| {
        actions::users::admin::find_user_by_username_including_deactivated(&username_copy, &conn)
    })
    .await?
    .ok_or(IntertextualError::UserNotFound { username })
    .map_err_app(&persistent)?;

    let form = form.into_inner();
    let message = form.message;
    let new_password = form.new_password;
    validate_admin_message(&message, "Justification").map_err_app(&persistent)?;

    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!("Reset password of user id={}", user.id),
        message,
    };

    let pw_hash = Hasher::default()
        .with_password(new_password)
        .with_secret_key(data.password_secret_key.as_str())
        .hash()
        .unwrap();

    let username = user.username.clone();
    let moderator =
        models::users::ModeratorUserWrapper::from_user(login_user).map_err_app(&persistent)?;
    let _ = db_action(&data.pool, &persistent, move |conn| {
        actions::users::admin::reset_user_password(moderator, user, pw_hash, action, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, format!("/admin/users/@{}/", &username,)))
        .finish())
}

#[post("/admin/users/@{username}/promote_administrator/")]
async fn account_promote(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    params: web::Path<String>,
    message: web::Form<AdminActionParams>,
) -> Result<HttpResponse, AppError> {
    change_user_state(
        request,
        id,
        data,
        params,
        message,
        |user_id| format!("Added user id={} to administrators", user_id),
        |moderator, user, log_entry, conn| {
            actions::users::admin::promote_user_to_administrator(moderator, user, log_entry, conn)
        },
    )
    .await
}

#[post("/admin/users/@{username}/demote_administrator/")]
async fn account_demote(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    params: web::Path<String>,
    message: web::Form<AdminActionParams>,
) -> Result<HttpResponse, AppError> {
    change_user_state(
        request,
        id,
        data,
        params,
        message,
        |user_id| format!("Removed user id={} from administrators", user_id),
        |moderator, user, log_entry, conn| {
            actions::users::admin::demote_user_from_administrator(moderator, user, log_entry, conn)
        },
    )
    .await
}

#[post("/admin/users/@{username}/unwarn/")]
async fn account_unwarn(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    params: web::Path<String>,
    message: web::Form<AdminActionParams>,
) -> Result<HttpResponse, AppError> {
    change_user_state(
        request,
        id,
        data,
        params,
        message,
        |user_id| format!("Removed warning from account of user id={}", user_id),
        |moderator, user, log_entry, conn| {
            actions::users::admin::unwarn_user(moderator, user, log_entry, conn)
        },
    )
    .await
}

#[post("/admin/users/@{username}/warn/")]
async fn account_warn(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    params: web::Path<String>,
    message: web::Form<AdminTimedActionParams>,
) -> Result<HttpResponse, AppError> {
    Ok(change_user_state_with_duration(
        request,
        id,
        data,
        params,
        message,
        |user_id| format!("Warned account of user id={}", user_id),
        |moderator, user, duration, log_entry, conn| {
            let mute_duration = duration
                .unwrap_or_else(|| chrono::Duration::days(DEFAULT_WARNING_DURATION_IN_DAYS));
            let mute_end_date = chrono::Utc::now()
                .checked_add_signed(mute_duration)
                .ok_or(IntertextualError::InternalServerError)?
                .naive_utc();
            actions::users::admin::warn_user_until(moderator, user, mute_end_date, log_entry, conn)
        },
    )
    .await?)
}

#[post("/admin/users/@{username}/unmute/")]
async fn account_unmute(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    params: web::Path<String>,
    message: web::Form<AdminActionParams>,
) -> Result<HttpResponse, AppError> {
    change_user_state(
        request,
        id,
        data,
        params,
        message,
        |user_id| format!("Unmuted account of user id={}", user_id),
        |moderator, user, log_entry, conn| {
            actions::users::admin::unmute_user(moderator, user, log_entry, conn)
        },
    )
    .await
}

#[post("/admin/users/@{username}/mute/")]
async fn account_mute(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    params: web::Path<String>,
    message: web::Form<AdminTimedActionParams>,
) -> Result<HttpResponse, AppError> {
    Ok(change_user_state_with_duration(
        request,
        id,
        data,
        params,
        message,
        |user_id| format!("Muted account of user id={}", user_id),
        |moderator, user, duration, log_entry, conn| {
            let mute_duration =
                duration.unwrap_or_else(|| chrono::Duration::days(DEFAULT_MUTE_DURATION_IN_DAYS));
            let mute_end_date = chrono::Utc::now()
                .checked_add_signed(mute_duration)
                .ok_or(IntertextualError::InternalServerError)?
                .naive_utc();
            actions::users::admin::mute_user_until(moderator, user, mute_end_date, log_entry, conn)
        },
    )
    .await?)
}

#[post("/admin/users/@{username}/unban/")]
async fn account_unban(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    params: web::Path<String>,
    message: web::Form<AdminActionParams>,
) -> Result<HttpResponse, AppError> {
    change_user_state(
        request,
        id,
        data,
        params,
        message,
        |user_id| format!("Unbanned account of user id={}", user_id),
        |moderator, user, log_entry, conn| {
            actions::users::admin::unban_user(moderator, user, log_entry, conn)
        },
    )
    .await
}

#[post("/admin/users/@{username}/ban/")]
async fn account_ban(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    params: web::Path<String>,
    message: web::Form<AdminTimedActionParams>,
) -> Result<HttpResponse, AppError> {
    Ok(change_user_state_with_duration(
        request,
        id,
        data,
        params,
        message,
        |user_id| format!("Banned account of user id={}", user_id),
        |moderator, user, duration, log_entry, conn| {
            let ban_duration =
                duration.unwrap_or_else(|| chrono::Duration::days(DEFAULT_BAN_DURATION_IN_DAYS));
            let ban_end_date = chrono::Utc::now()
                .checked_add_signed(ban_duration)
                .ok_or(IntertextualError::InternalServerError)?
                .naive_utc();
            actions::users::admin::ban_user_until(moderator, user, ban_end_date, log_entry, conn)
        },
    )
    .await?)
}

#[post("/admin/users/@{username}/cancel_delete/")]
async fn account_cancel_delete(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    params: web::Path<String>,
    message: web::Form<AdminActionParams>,
) -> Result<HttpResponse, AppError> {
    change_user_state(
        request,
        id,
        data,
        params,
        message,
        |user_id| format!("Canceled deletion of account of user id={}", user_id),
        |moderator, user, log_entry, conn| {
            actions::users::admin::cancel_user_deletion_request(moderator, user, log_entry, conn)
        },
    )
    .await
}

#[post("/admin/users/@{username}/delete/")]
async fn account_delete(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    params: web::Path<String>,
    message: web::Form<AdminActionParams>,
) -> Result<HttpResponse, AppError> {
    Ok(change_user_state(
        request,
        id,
        data,
        params,
        message,
        |user_id| format!("Requested deletion of account of user id={}", user_id),
        |moderator, user, log_entry, conn| {
            actions::users::admin::request_user_deletion(moderator, user, log_entry, conn)
        },
    )
    .await?)
}

async fn change_user_state<ActionNameFormatter, UserAction>(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    form: web::Form<AdminActionParams>,

    action_name: ActionNameFormatter,
    user_action: UserAction,
) -> Result<HttpResponse, AppError>
where
    ActionNameFormatter: FnOnce(uuid::Uuid) -> String,
    UserAction: FnOnce(
            models::users::ModeratorUserWrapper,
            models::users::User,
            NewModerationAction,
            &diesel::PgConnection,
        )
            -> Result<(models::users::User, models::admin::ModerationAction), IntertextualError>
        + Sync
        + Send
        + 'static,
{
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let username = path.into_inner();
    let username_copy = username.clone();
    let user = db_action(&data.pool, &persistent, move |conn| {
        actions::users::admin::find_user_by_username_including_deactivated(&username_copy, &conn)
    })
    .await?
    .ok_or(IntertextualError::UserNotFound { username })
    .map_err_app(&persistent)?;

    if user.id == login_user.id {
        return Ok(crate::app::common_pages::not_accessible(
            PersistentTemplate::from(&persistent),
            Some("You cannot edit your own informations for this action".to_string()),
        ));
    }

    let message = form.into_inner().message;
    validate_admin_message(&message, "Justification").map_err_app(&persistent)?;

    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: action_name(user.id),
        message,
    };

    let username = user.username.clone();
    let moderator =
        models::users::ModeratorUserWrapper::from_user(login_user).map_err_app(&persistent)?;
    let _ = db_action(&data.pool, &persistent, move |conn| {
        user_action(moderator, user, action, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, format!("/admin/users/@{}/", &username,)))
        .finish())
}

async fn change_user_state_with_duration<ActionNameFormatter, UserAction>(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    form: web::Form<AdminTimedActionParams>,

    action_name: ActionNameFormatter,
    user_action: UserAction,
) -> Result<HttpResponse, AppError>
where
    ActionNameFormatter: FnOnce(uuid::Uuid) -> String,
    UserAction: FnOnce(
            models::users::ModeratorUserWrapper,
            models::users::User,
            Option<chrono::Duration>,
            NewModerationAction,
            &diesel::PgConnection,
        )
            -> Result<(models::users::User, models::admin::ModerationAction), IntertextualError>
        + Sync
        + Send
        + 'static,
{
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let username = path.into_inner();
    let username_copy = username.clone();
    let user = db_action(&data.pool, &persistent, move |conn| {
        actions::users::admin::find_user_by_username_including_deactivated(&username_copy, &conn)
    })
    .await?
    .ok_or(IntertextualError::UserNotFound { username })
    .map_err_app(&persistent)?;

    if user.id == login_user.id {
        return Ok(crate::app::common_pages::not_accessible(
            PersistentTemplate::from(&persistent),
            Some("You cannot edit your own informations for this action".to_string()),
        ));
    }

    let AdminTimedActionParams { message, duration } = form.into_inner();
    validate_admin_message(&message, "Justification").map_err_app(&persistent)?;
    let duration = duration.parse::<i64>().map(chrono::Duration::days).ok();

    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: action_name(user.id),
        message,
    };

    let username = user.username.clone();
    let moderator =
        models::users::ModeratorUserWrapper::from_user(login_user).map_err_app(&persistent)?;
    let _ = db_action(&data.pool, &persistent, move |conn| {
        user_action(moderator, user, duration, action, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, format!("/admin/users/@{}/", &username,)))
        .finish())
}
