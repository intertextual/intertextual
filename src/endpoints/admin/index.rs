use actix_web::{get, web, HttpRequest, HttpResponse};
use askama::Template;

use intertextual::actions;

use crate::prelude::*;

#[derive(Template)]
#[template(path = "admin/index.html")]
struct AdminPageTemplate {
    persistent: PersistentTemplate,
    unhandled_reports: i64,
    unhandled_modmails: i64,
}

#[get("/admin/")]
async fn admin_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let (unhandled_reports, unhandled_modmails) = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<(i64, i64), IntertextualError> {
            Ok((
                actions::reports::admin::get_unhandled_report_count(&conn)?,
                actions::modmail::admins::get_unhandled_modmail_count(&conn)?,
            ))
        },
    )
    .await?;

    let s = AdminPageTemplate {
        persistent: PersistentTemplate::from(&persistent),
        unhandled_reports,
        unhandled_modmails,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}
