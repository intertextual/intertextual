use actix_web::http::header::LOCATION;
use actix_web::{get, post, web, HttpRequest, HttpResponse};
use askama::Template;
use futures::TryStreamExt;

use intertextual::actions::static_pages;
use intertextual::models::admin::NewModerationAction;

use crate::app::multipart;
use crate::prelude::*;

#[derive(Template)]
#[template(path = "admin/static_page.html")]
struct StaticPageTemplate {
    persistent: PersistentTemplate,
    title: String,
    page_name: String,
    content: SanitizedHtml,
}

#[get("/admin/static_pages/{name}/")]
async fn get_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let kind = static_pages::StaticPageKind::from_page_url(&path.into_inner())
        .ok_or(IntertextualError::PageDoesNotExist)
        .map_err_app(&persistent)?;
    let (title, page_name) = (kind.title().to_string(), kind.page_url().to_string());
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let content = db_action(&data.pool, &persistent, move |conn| {
        static_pages::get_static_page(kind, &conn)
    })
    .await?;

    let s = StaticPageTemplate {
        persistent: PersistentTemplate::from(&persistent),
        title,
        page_name,
        content,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/admin/static_pages/{name}/")]
async fn update_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    mut payload: actix_multipart::Multipart,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let kind = static_pages::StaticPageKind::from_page_url(&path.into_inner())
        .ok_or(IntertextualError::PageDoesNotExist)
        .map_err_app(&persistent)?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let mut new_content: SanitizedHtml = SanitizedHtml::new();
    while let Ok(Some(mut field)) = payload.try_next().await {
        let content_disposition = field.content_disposition();
        if !content_disposition.is_form_data() {
            return Err(IntertextualError::FormFieldFormatError {
                form_field_name: "Unknown",
                message: "The data sent to the server is invalid".to_string(),
            }
            .into_app(&persistent));
        }

        let content_disposition_name = content_disposition
            .get_name()
            .ok_or_else(|| IntertextualError::FormFieldFormatError {
                form_field_name: "Unknown",
                message: "The data sent to the server is invalid".to_string(),
            })
            .map_err_app(&persistent)?;

        match content_disposition_name {
            "content" => {
                new_content = multipart::process_utf8_field_into_sanitized_html(
                    &persistent,
                    &mut field,
                    multipart::CONTENT_SIZE_LIMIT,
                )
                .await?;
            }
            _ => {
                log::warn!(
                    "Received unknown field {}. Ignorning.",
                    content_disposition_name
                );
            }
        }
    }

    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!("Edited page {}", kind.page_url()),
        message: "No message required".to_string(),
    };

    db_action(&data.pool, &persistent, move |conn| {
        static_pages::admin::set_static_page(kind, &new_content, action, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((
            LOCATION,
            format!("/admin/static_pages/{}/", kind.page_url(),),
        ))
        .finish())
}
