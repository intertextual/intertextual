use actix_web::http::header::LOCATION;
use actix_web::{get, post, web, HttpRequest, HttpResponse};
use askama::Template;
use serde::Deserialize;

use intertextual::actions;
use intertextual::models::admin::NewModerationAction;
use intertextual::models::error::IntertextualError;
use intertextual::models::shared::AppState;
use intertextual::models::tags::TagHighlightRule;
use intertextual::models::tags::TagHighlightRuleColor;

use crate::prelude::*;

#[derive(Template)]
#[template(path = "admin/tag_highlight_rules_templates.html")]
struct AdminTagHighlightRulesTemplate {
    persistent: PersistentTemplate,
    templates: Vec<TagHighlightRulesTemplate>,
    show_new_template_confirmation: bool,
    show_template_update_confirmation: bool,
    show_template_delete_confirmation: bool,
}

struct TagHighlightRulesTemplate {
    index: String,
    name: String,
    description: String,
    rules: Vec<(usize, TagHighlightRule)>,
    is_visible_by_default: bool,
}

#[derive(Deserialize)]
pub struct AdminTagHighlightRulesTemplatePageQuery {
    pub confirm: Option<String>,
    pub edited_template: Option<String>,
}

#[get("/admin/tag_highlight_rules_templates/")]
async fn main_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<AdminTagHighlightRulesTemplatePageQuery>,
) -> Result<HttpResponse, AppError> {
    let params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let confirm = params.confirm;
    let edited_template = params.edited_template;

    let templates = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<Vec<TagHighlightRulesTemplate>, IntertextualError> {
            let all_templates = actions::tags::get_tag_highlight_rule_templates_list(&conn)?;
            let mut result = Vec::with_capacity(all_templates.len());
            for (index, (template_name, template_description)) in
                all_templates.into_iter().enumerate()
            {
                let template_content =
                    actions::tags::get_tag_highlight_rule_template_by_name(&template_name, &conn)?;
                let is_visible_by_default =
                    matches!(&edited_template, Some(v) if v == &template_name);

                result.push(TagHighlightRulesTemplate {
                    index: index.to_string(),
                    name: template_name,
                    description: template_description,
                    rules: template_content.into_iter().enumerate().collect(),
                    is_visible_by_default,
                })
            }

            Ok(result)
        },
    )
    .await?;

    let s = AdminTagHighlightRulesTemplate {
        persistent: PersistentTemplate::from(&persistent),
        templates,
        show_new_template_confirmation: matches!(&confirm, Some(v) if v == "create"),
        show_template_update_confirmation: matches!(&confirm, Some(v) if v == "update"),
        show_template_delete_confirmation: matches!(&confirm, Some(v) if v == "delete"),
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[derive(Deserialize)]
struct AddNewRuleParams {
    pub template_name: String,
    pub tags: String,
    pub use_custom_color: Option<String>,
    pub custom_color: String,
    pub select_color: String,
    pub symbol: String,
}

#[post("/admin/tag_highlight_rules_templates_add_rule/")]
async fn handle_add_new_rule(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,

    form: web::Form<AddNewRuleParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let form = form.into_inner();
    let template_name = form.template_name;
    let template_name_copy = template_name.clone();
    let current_rules = db_action(&data.pool, &persistent, move |conn| {
        actions::tags::get_tag_highlight_rule_template_by_name(&template_name_copy, &conn)
    })
    .await?;

    let tags = form
        .tags
        .split(' ')
        .map(|t| t.trim())
        .map(|t| t.strip_prefix('#').unwrap_or(t))
        .filter(|t| !t.is_empty())
        .map(|t| t.to_string())
        .collect::<Vec<String>>();
    let use_custom_color = matches!(form.use_custom_color, Some(value) if &value == "on");
    let story_color = if use_custom_color {
        TagHighlightRuleColor::parse_from_str(&form.custom_color).map_err_app(&persistent)?
    } else {
        TagHighlightRuleColor::parse_from_str(&form.select_color).map_err_app(&persistent)?
    };
    let story_symbol = if form.symbol.trim().is_empty() {
        None
    } else {
        Some(form.symbol.trim().to_string())
    };

    let mut new_rules = current_rules;

    new_rules.push(TagHighlightRule {
        tags,
        story_color,
        story_symbol,
    });

    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!(
            "Added a rule to highlight rules template '{}'",
            &template_name
        ),
        message: "<No message required for this action>".to_string(),
    };

    let template_name_copy = template_name.clone();
    db_action(&data.pool, &persistent, move |conn| {
        actions::tags::admin::update_tag_highlight_rule_template_content(
            template_name_copy,
            new_rules,
            action,
            &conn,
        )
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((
            LOCATION,
            format!(
                "/admin/tag_highlight_rules_templates/?confirm=update&edited_template={}#rules",
                template_name,
            ),
        ))
        .finish())
}

#[derive(Deserialize)]
struct DeleteRuleParams {
    pub template_name: String,
    pub rule_index: usize,
}

#[post("/admin/tag_highlight_rules_templates_delete_rule/")]
async fn handle_delete_rule(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,

    form: web::Form<DeleteRuleParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let form = form.into_inner();
    let template_name = form.template_name;
    let template_name_copy = template_name.clone();
    let current_rules = db_action(&data.pool, &persistent, move |conn| {
        actions::tags::get_tag_highlight_rule_template_by_name(&template_name_copy, &conn)
    })
    .await?;

    let current_index = form.rule_index;
    if current_index >= current_rules.len() {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Edited rule",
            message: format!(
                "The edited rule does not exist (no rule with index {})",
                current_index
            ),
        })
        .map_err_app(&persistent);
    }

    let mut new_rules = current_rules;
    new_rules.remove(current_index);

    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!(
            "Deleted a rule in highlight rules template '{}'",
            &template_name
        ),
        message: "<No message required for this action>".to_string(),
    };

    let template_name_copy = template_name.clone();
    db_action(&data.pool, &persistent, move |conn| {
        actions::tags::admin::update_tag_highlight_rule_template_content(
            template_name_copy,
            new_rules,
            action,
            &conn,
        )
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((
            LOCATION,
            format!(
                "/admin/tag_highlight_rules_templates/?confirm=update&edited_template={}#rules",
                template_name,
            ),
        ))
        .finish())
}

#[derive(Deserialize)]
struct EditRuleParams {
    pub template_name: String,
    pub rule_index: usize,
    pub position: String,
    pub tags: String,
    pub use_custom_color: Option<String>,
    pub custom_color: String,
    pub select_color: String,
    pub symbol: String,
}

#[post("/admin/tag_highlight_rules_templates_edit_rule/")]
async fn handle_edit_rule(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,

    form: web::Form<EditRuleParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let form = form.into_inner();
    let template_name = form.template_name;
    let template_name_copy = template_name.clone();
    let current_rules = db_action(&data.pool, &persistent, move |conn| {
        actions::tags::get_tag_highlight_rule_template_by_name(&template_name_copy, &conn)
    })
    .await?;

    let current_index = form.rule_index;
    let target_index = form
        .position
        .parse::<usize>()
        .map_err(|_| IntertextualError::FormFieldFormatError {
            form_field_name: "Position",
            message: "The position must be a number greater or equal to 1".to_string(),
        })
        .map_err_app(&persistent)?;
    if target_index == 0 {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Position",
            message: "The position must be a number greater or equal to 1".to_string(),
        })
        .map_err_app(&persistent);
    }

    // Note: In the UI, the target index starts at 1. We remove it here.
    let target_index = target_index - 1;

    if current_index >= current_rules.len() {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Edited rule",
            message: format!(
                "The edited rule does not exist (no rule with index {})",
                current_index
            ),
        })
        .map_err_app(&persistent);
    }
    if target_index >= current_rules.len() {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Position",
            message: format!(
                "The target position must be less or equal to {} (the current number of rules)",
                current_rules.len()
            ),
        })
        .map_err_app(&persistent);
    }
    let tags = form
        .tags
        .split(' ')
        .map(|t| t.trim())
        .map(|t| t.strip_prefix('#').unwrap_or(t))
        .filter(|t| !t.is_empty())
        .map(|t| t.to_string())
        .collect::<Vec<String>>();
    let use_custom_color = matches!(form.use_custom_color, Some(value) if &value == "on");
    let story_color = if use_custom_color {
        TagHighlightRuleColor::parse_from_str(&form.custom_color).map_err_app(&persistent)?
    } else {
        TagHighlightRuleColor::parse_from_str(&form.select_color).map_err_app(&persistent)?
    };
    let story_symbol = if form.symbol.trim().is_empty() {
        None
    } else {
        Some(form.symbol.trim().to_string())
    };

    let mut new_rules = current_rules;

    // Reorder if needed
    if current_index != target_index {
        let element = new_rules.remove(current_index);
        new_rules.insert(target_index, element);
    };

    // Note: The item has now been moved to the target_index position
    // Make sure that current_index isn't pointing to an obsolete position
    // to apply the changes there if needed.
    let current_index = target_index;

    // Change data if needed
    let item = new_rules
        .get_mut(current_index)
        .ok_or_else(|| IntertextualError::FormFieldFormatError {
            form_field_name: "Edited rule",
            message: format!(
                "The edited rule does not exist (no rule with index {})",
                current_index
            ),
        })
        .map_err_app(&persistent)?;
    item.tags = tags;
    item.story_color = story_color;
    item.story_symbol = story_symbol;

    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!(
            "Edited rule in highlight rules template '{}'",
            &template_name
        ),
        message: "<No message required for this action>".to_string(),
    };

    let template_name_copy = template_name.clone();
    db_action(&data.pool, &persistent, move |conn| {
        actions::tags::admin::update_tag_highlight_rule_template_content(
            template_name_copy,
            new_rules,
            action,
            &conn,
        )
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((
            LOCATION,
            format!(
                "/admin/tag_highlight_rules_templates/?confirm=update&edited_template={}#rules",
                template_name,
            ),
        ))
        .finish())
}

#[derive(Deserialize)]
struct CreateTemplateParams {
    pub new_template_name: String,
    pub new_template_description: String,
}

#[post("/admin/tag_highlight_rules_templates_create/")]
async fn handle_create_template(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,

    form: web::Form<CreateTemplateParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let form = form.into_inner();
    let new_template_name = form.new_template_name;
    if new_template_name.trim().is_empty() {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Template name",
            message: "The field name cannot be be empty or made of whitespace".to_string(),
        })
        .map_err_app(&persistent);
    }
    let new_template_description = form.new_template_description;

    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!(
            "Created new highlight rules template '{}'",
            &new_template_name
        ),
        message: "<No message required for this action>".to_string(),
    };

    let new_template_name_copy = new_template_name.clone();
    db_action(&data.pool, &persistent, move |conn| {
        actions::tags::admin::create_tag_highlight_rule_template(
            new_template_name_copy,
            new_template_description,
            vec![],
            action,
            &conn,
        )
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((
            LOCATION,
            format!(
                "/admin/tag_highlight_rules_templates/?confirm=create&edited_template={}#rules",
                new_template_name,
            ),
        ))
        .finish())
}

#[derive(Deserialize)]
struct UpdateTemplateParams {
    pub template_name: String,
    pub new_template_name: String,
    pub new_template_description: String,
}

#[post("/admin/tag_highlight_rules_templates_update/")]
async fn handle_update_template(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,

    form: web::Form<UpdateTemplateParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let form = form.into_inner();
    let template_name = form.template_name;
    let new_template_name = form.new_template_name;
    if new_template_name.trim().is_empty() {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Template name",
            message: "The field name cannot be be empty or made of whitespace".to_string(),
        })
        .map_err_app(&persistent);
    }

    let new_template_description = form.new_template_description;
    let action = if template_name == new_template_name {
        NewModerationAction {
            username: login_user.username.clone(),
            action_type: format!(
                "Updated description of highlight rules template '{}'",
                &template_name
            ),
            message: "<No message required for this action>".to_string(),
        }
    } else {
        NewModerationAction {
            username: login_user.username.clone(),
            action_type: format!(
                "Renamed highlight rules template '{}' => '{}'",
                &template_name, &new_template_name,
            ),
            message: "<No message required for this action>".to_string(),
        }
    };

    let new_template_name_copy = new_template_name.clone();
    db_action(&data.pool, &persistent, move |conn| {
        actions::tags::admin::update_tag_highlight_rule_template_metadata(
            template_name,
            new_template_name_copy,
            new_template_description,
            action,
            &conn,
        )
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((
            LOCATION,
            format!(
                "/admin/tag_highlight_rules_templates/?confirm=update&edited_template={}#rules",
                new_template_name,
            ),
        ))
        .finish())
}

#[derive(Deserialize)]
struct DeleteTemplateParams {
    pub template_name: String,
}

#[post("/admin/tag_highlight_rules_templates_delete/")]
async fn handle_delete_template(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,

    form: web::Form<DeleteTemplateParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let form = form.into_inner();
    let template_name = form.template_name;

    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!("Deleted highlight rules template '{}'", &template_name),
        message: "<No message required for this action>".to_string(),
    };

    db_action(&data.pool, &persistent, move |conn| {
        actions::tags::admin::delete_tag_highlight_rule_template(template_name, action, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((
            LOCATION,
            "/admin/tag_highlight_rules_templates/?confirm=delete#rules",
        ))
        .finish())
}
