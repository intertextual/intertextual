use actix_web::http::header::LOCATION;
use actix_web::{get, post, web, HttpRequest, HttpResponse};
use askama::Template;
use futures::TryStreamExt;
use serde::{Deserialize, Serialize};

use intertextual::actions::announcements;
use intertextual::models::admin::NewModerationAction;
use intertextual::models::announcements::{Announcement, NewAnnouncement};
use intertextual::utils::page_list::PageListing;

use crate::app::multipart;
use crate::prelude::*;

const MESSAGES_PER_PAGE: i64 = 20;

#[derive(Template)]
#[template(path = "admin/announcement_list.html")]
struct AnnouncementListTemplate {
    persistent: PersistentTemplate,
    announcements: Vec<AnnouncementEntry>,
    page_listing: PageListing,
}

struct AnnouncementEntry {
    url_fragment: String,
    title: Option<String>,
    publication_date: Option<chrono::NaiveDateTime>,
}

#[derive(Template)]
#[template(path = "admin/announcement_create.html")]
struct AnnouncementCreateTemplate {
    persistent: PersistentTemplate,
}

#[derive(Template)]
#[template(path = "admin/announcement.html")]
struct AnnouncementTemplate {
    persistent: PersistentTemplate,
    announcement: Announcement,
}

#[derive(Serialize, Deserialize)]
pub struct AnnouncementListQuery {
    start: Option<i64>,
}

#[get("/admin/announcements/")]
async fn main_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<AnnouncementListQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let start = url_params.start.unwrap_or(0);
    let (announcements, total_announcement_count) = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<(Vec<AnnouncementEntry>, i64), IntertextualError> {
            let raw_announcements =
                announcements::admin::get_all_announcements(start, MESSAGES_PER_PAGE, &conn)?;
            let mut announcements = Vec::with_capacity(raw_announcements.len());
            for announcement in raw_announcements {
                announcements.push(AnnouncementEntry {
                    url_fragment: announcement.url_fragment,
                    publication_date: announcement.show_publicly_after_date,
                    title: announcement.title,
                });
            }
            Ok((
                announcements,
                announcements::admin::get_all_announcements_count(&conn)?,
            ))
        },
    )
    .await?;

    let page_listing =
        PageListing::get_from_count(total_announcement_count, start, MESSAGES_PER_PAGE);
    let s = AnnouncementListTemplate {
        persistent: PersistentTemplate::from(&persistent),
        announcements,
        page_listing,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[get("/admin/announcements/create/")]
async fn create_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let s = AnnouncementCreateTemplate {
        persistent: PersistentTemplate::from(&persistent),
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[get("/admin/announcements/{url_fragment}/edit/")]
async fn specific_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let url_fragment = path.into_inner();

    let announcement = db_action(&data.pool, &persistent, move |conn| {
        announcements::admin::get_announcement_by_url(&url_fragment, &conn)
    })
    .await?
    .ok_or(IntertextualError::PageDoesNotExist)
    .map_err_app(&persistent)?;

    let s = AnnouncementTemplate {
        persistent: PersistentTemplate::from(&persistent),
        announcement,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/admin/announcements/create/")]
async fn create_announcement(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    mut payload: actix_multipart::Multipart,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let mut new_url_fragment: String = String::new();
    let mut new_title: Option<String> = None;
    let mut new_publication_date: Option<chrono::NaiveDateTime> = None;
    let mut new_content: SanitizedHtml = SanitizedHtml::new();
    while let Ok(Some(mut field)) = payload.try_next().await {
        let content_disposition = field.content_disposition();
        if !content_disposition.is_form_data() {
            return Err(IntertextualError::FormFieldFormatError {
                form_field_name: "Unknown",
                message: "The data sent to the server is invalid".to_string(),
            }
            .into_app(&persistent));
        }

        let content_disposition_name = content_disposition
            .get_name()
            .ok_or_else(|| IntertextualError::FormFieldFormatError {
                form_field_name: "Unknown",
                message: "The data sent to the server is invalid".to_string(),
            })
            .map_err_app(&persistent)?;

        match content_disposition_name {
            "url_fragment" => {
                new_url_fragment = multipart::process_utf8_field_into_string(
                    &persistent,
                    &mut field,
                    multipart::CONTENT_SIZE_LIMIT,
                )
                .await?;
            }
            "title" => {
                let title = multipart::process_utf8_field_into_string(
                    &persistent,
                    &mut field,
                    multipart::CONTENT_SIZE_LIMIT,
                )
                .await?;
                if !title.is_empty() {
                    new_title = Some(title);
                }
            }
            "publication_date" => {
                let data = multipart::process_utf8_field_into_string(
                    &persistent,
                    &mut field,
                    multipart::CONTENT_SIZE_LIMIT,
                )
                .await?;
                if !data.is_empty() {
                    new_publication_date =
                        Some(chrono::NaiveDateTime::parse_from_str(&data, "%Y-%m-%d %H:%M")
                            .map_err(|_| {
                                IntertextualError::FormFieldFormatError {
                                    form_field_name: "Publication Date",
                                    message:
                                        "The publication date is invalid. The expected format is : 2020-08-24 11:30"
                                            .to_string(),
                                }
                            })
                            .map_err_app(&persistent)?);
                }
            }
            "content" => {
                new_content = multipart::process_utf8_field_into_sanitized_html(
                    &persistent,
                    &mut field,
                    multipart::CONTENT_SIZE_LIMIT,
                )
                .await?;
            }
            _ => {
                log::warn!(
                    "Received unknown field {}. Ignorning.",
                    content_disposition_name
                );
            }
        }
    }

    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!("Created new announcement {}", new_url_fragment),
        message: "No message required".to_string(),
    };

    let location = format!("/admin/announcements/{}/edit/", new_url_fragment);
    let new_announcement = NewAnnouncement {
        title: new_title,
        url_fragment: new_url_fragment,
        content: new_content,
        show_publicly_after_date: new_publication_date,
    };

    db_action(&data.pool, &persistent, move |conn| {
        announcements::admin::create_announcement(new_announcement, action, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, location))
        .finish())
}

#[post("/admin/announcements/{url_fragment}/edit/")]
async fn update_announcement(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    mut payload: actix_multipart::Multipart,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let url_fragment = path.into_inner();

    let mut new_title: Option<String> = None;
    let mut new_publication_date: Option<chrono::NaiveDateTime> = None;
    let mut new_content: SanitizedHtml = SanitizedHtml::new();
    while let Ok(Some(mut field)) = payload.try_next().await {
        let content_disposition = field.content_disposition();
        if !content_disposition.is_form_data() {
            return Err(IntertextualError::FormFieldFormatError {
                form_field_name: "Unknown",
                message: "The data sent to the server is invalid".to_string(),
            }
            .into_app(&persistent));
        }

        let content_disposition_name = content_disposition
            .get_name()
            .ok_or_else(|| IntertextualError::FormFieldFormatError {
                form_field_name: "Unknown",
                message: "The data sent to the server is invalid".to_string(),
            })
            .map_err_app(&persistent)?;

        match content_disposition_name {
            "title" => {
                let data = multipart::process_utf8_field_into_string(
                    &persistent,
                    &mut field,
                    multipart::CONTENT_SIZE_LIMIT,
                )
                .await?;
                if !data.is_empty() {
                    new_title = Some(data);
                }
            }
            "publication_date" => {
                let data = multipart::process_utf8_field_into_string(
                    &persistent,
                    &mut field,
                    multipart::CONTENT_SIZE_LIMIT,
                )
                .await?;
                if !data.is_empty() {
                    new_publication_date =
                        Some(chrono::NaiveDateTime::parse_from_str(&data, "%Y-%m-%d %H:%M")
                            .map_err(|_| {
                                IntertextualError::FormFieldFormatError {
                                    form_field_name: "Publication Date",
                                    message:
                                        "The publication date is invalid. The expected format is : 2020-08-24 11:30"
                                            .to_string(),
                                }
                            })
                            .map_err_app(&persistent)?);
                }
            }
            "content" => {
                new_content = multipart::process_utf8_field_into_sanitized_html(
                    &persistent,
                    &mut field,
                    multipart::CONTENT_SIZE_LIMIT,
                )
                .await?;
            }
            _ => {
                log::warn!(
                    "Received unknown field {}. Ignorning.",
                    content_disposition_name
                );
            }
        }
    }

    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!("Edited announcement {}", url_fragment),
        message: "No message required".to_string(),
    };

    let location = format!("/admin/announcements/{}/edit/", url_fragment);
    db_action(&data.pool, &persistent, move |conn| {
        announcements::admin::update_announcement(
            &url_fragment,
            new_publication_date,
            new_title.as_deref(),
            &new_content,
            action,
            &conn,
        )
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, location))
        .finish())
}

#[post("/admin/announcements/{url_fragment}/delete/")]
async fn delete_announcement(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let url_fragment = path.into_inner();

    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!("Removed announcement {}", url_fragment),
        message: "No message required".to_string(),
    };

    db_action(&data.pool, &persistent, move |conn| {
        announcements::admin::delete_announcement(&url_fragment, action, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, "/admin/announcements/"))
        .finish())
}
