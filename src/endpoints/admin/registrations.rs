use actix_web::http::header::LOCATION;
use actix_web::{get, post, web, HttpRequest, HttpResponse};
use askama::Template;
use serde::Deserialize;

use intertextual::actions::users::admin::registration_limit;
use intertextual::models::admin::NewModerationAction;
use intertextual::models::error::IntertextualError;
use intertextual::models::shared::AppState;

use crate::app::identity::Identity;
use crate::app::persistent::*;
use crate::app::validation::validate_admin_message;
use crate::error::*;
use crate::prelude::*;

#[derive(Template)]
#[template(path = "admin/registrations.html")]
struct RegistrationsPageTemplate {
    persistent: PersistentTemplate,
    registration_count: i64,
    registration_manually_disabled: bool,
    registration_range: String,
    registration_limit: Option<RegistrationLimitData>,
}

struct RegistrationLimitData {
    limit: i64,
    percent: i64,
}

#[derive(Deserialize)]
pub struct AdminActionParams {
    message: String,
}

#[derive(Deserialize)]
pub struct ChangeLimitParams {
    enable_limit: Option<String>,
    registration_limit: Option<String>,
    message: String,
}

#[get("/admin/registrations/")]
async fn main_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let registration_limit_parameters = db_action(&data.pool, &persistent, move |conn| {
        registration_limit::get_registration_limit(&conn)
    })
    .await?;

    let registration_manually_disabled = match registration_limit_parameters {
        registration_limit::RegistrationLimit::RegistrationsDisabled => true,
        registration_limit::RegistrationLimit::RegistrationsEnabledWithoutLimit => false,
        registration_limit::RegistrationLimit::RegistrationsEnabledWithLimit {
            current_registrations: _,
            hours: _,
            max_registrations: _,
        } => false,
    };
    let registration_count = match registration_limit_parameters {
        registration_limit::RegistrationLimit::RegistrationsDisabled => 0,
        registration_limit::RegistrationLimit::RegistrationsEnabledWithoutLimit => 0,
        registration_limit::RegistrationLimit::RegistrationsEnabledWithLimit {
            current_registrations,
            hours: _,
            max_registrations: _,
        } => current_registrations,
    };
    let registration_limit = match registration_limit_parameters {
        registration_limit::RegistrationLimit::RegistrationsDisabled => None,
        registration_limit::RegistrationLimit::RegistrationsEnabledWithoutLimit => None,
        registration_limit::RegistrationLimit::RegistrationsEnabledWithLimit {
            current_registrations: _,
            hours: _,
            max_registrations,
        } => Some(max_registrations),
    };
    let registration_range = match registration_limit_parameters {
        registration_limit::RegistrationLimit::RegistrationsDisabled => chrono::Duration::hours(0),
        registration_limit::RegistrationLimit::RegistrationsEnabledWithoutLimit => {
            chrono::Duration::hours(0)
        }
        registration_limit::RegistrationLimit::RegistrationsEnabledWithLimit {
            current_registrations: _,
            hours,
            max_registrations: _,
        } => chrono::Duration::hours(hours),
    };

    let registration_range = format!("{} hours", registration_range.num_hours());

    let registration_limit = registration_limit.map(|limit| RegistrationLimitData {
        limit,
        percent: (100 * registration_count) / limit,
    });

    let s = RegistrationsPageTemplate {
        persistent: PersistentTemplate::from(&persistent),
        registration_range,
        registration_count,
        registration_manually_disabled,
        registration_limit,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/admin/registrations/enable/")]
async fn handle_enable(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    form: web::Form<AdminActionParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let form = form.into_inner();
    let message = form.message;
    validate_admin_message(&message, "Justification").map_err_app(&persistent)?;
    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: "Manually re-enabled registrations".to_string(),
        message,
    };

    let _ = db_action(&data.pool, &persistent, move |conn| {
        registration_limit::enable_registrations(action, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, "/admin/registrations/"))
        .finish())
}

#[post("/admin/registrations/disable/")]
async fn handle_disable(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    form: web::Form<AdminActionParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let form = form.into_inner();
    let message = form.message;
    validate_admin_message(&message, "Justification").map_err_app(&persistent)?;
    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: "Manually disabled registrations".to_string(),
        message,
    };

    let _ = db_action(&data.pool, &persistent, move |conn| {
        registration_limit::set_registration_limit(
            registration_limit::NewRegistrationLimit::RegistrationsDisabled,
            action,
            &conn,
        )
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, "/admin/registrations/"))
        .finish())
}

#[post("/admin/registrations/limit/")]
async fn handle_limit(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    form: web::Form<ChangeLimitParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let form = form.into_inner();
    let has_limit = matches!(&form.enable_limit, Some(value) if value == "on");
    let limit = if has_limit {
        match form.registration_limit.map(|limit| limit.parse::<i64>()) {
            Some(Ok(limit)) => Some(limit),
            _ => {
                return Err(IntertextualError::FormFieldFormatError {
                    form_field_name: "Registration Limit",
                    message: "The registration limit was not a valid number".to_string(),
                }
                .into_app(&persistent));
            }
        }
    } else {
        None
    };

    let message = form.message;
    validate_admin_message(&message, "Justification").map_err_app(&persistent)?;
    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: match limit {
            Some(limit) => format!("Set the registration limit to {}", limit),
            None => "Removed the registration limit".to_string(),
        },
        message,
    };

    let _ = db_action(&data.pool, &persistent, move |conn| match limit {
        Some(limit) => registration_limit::set_registration_limit(
            registration_limit::NewRegistrationLimit::RegistrationsEnabledWithLimit {
                hours: 24,
                max_registrations: limit,
            },
            action,
            &conn,
        ),
        None => registration_limit::set_registration_limit(
            registration_limit::NewRegistrationLimit::RegistrationsEnabledWithoutLimit,
            action,
            &conn,
        ),
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, "/admin/registrations/"))
        .finish())
}
