use actix_web::{get, web, HttpRequest, HttpResponse};
use askama::Template;
use serde::Deserialize;

use intertextual::actions;
use intertextual::models::admin::ModerationAction;
use intertextual::utils::page_list::PageListing;

use crate::prelude::*;

const ENTRIES_PER_PAGE: i64 = 50;

#[derive(Template)]
#[template(path = "admin/moderation_log.html")]
struct ModerationLogTemplate {
    persistent: PersistentTemplate,
    action_log: Vec<ModerationAction>,
    page_listing: PageListing,
}

#[derive(Deserialize)]
pub struct ModerationLogQuery {
    start: Option<i64>,
}

#[get("/admin/moderation_log/")]
async fn main_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<ModerationLogQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let start = url_params.start.unwrap_or(0);
    let (action_log, log_size) = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<(Vec<ModerationAction>, i64), IntertextualError> {
            let log = actions::utils::get_moderation_log(start, ENTRIES_PER_PAGE, &conn)?;
            let log_size = actions::utils::get_moderation_log_entry_count(&conn)?;
            Ok((log, log_size))
        },
    )
    .await?;

    let page_listing = PageListing::get_from_count(log_size, start, ENTRIES_PER_PAGE);

    let s = ModerationLogTemplate {
        persistent: PersistentTemplate::from(&persistent),
        action_log,
        page_listing,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}
