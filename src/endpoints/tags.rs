use actix_web::{get, web, HttpRequest, HttpResponse};
use askama::Template;

use intertextual::actions;
use intertextual::models::error::IntertextualError;
use intertextual::models::shared::AppState;

use crate::app::identity::Identity;
use crate::app::persistent::*;
use crate::error::*;
use crate::prelude::*;

#[derive(Template)]
#[template(path = "tag_cloud.html")]
struct TagsCloudTemplate {
    persistent: PersistentTemplate,
    tags: Vec<TagEntry>,
}

#[derive(Template)]
#[template(path = "tag_listing.html")]
struct TagsListingTemplate {
    persistent: PersistentTemplate,
    tag_categories: Vec<SplitTagCategoryEntry>,
}

struct SplitTagCategoryEntry {
    name: String,
    description: String,
    checkable_tags: Vec<TagEntry>,
    custom_tags: Vec<TagEntry>,
}

struct TagEntry {
    name: String,
    description: String,
    checkable: bool,
    tagged_stories: i64,
    em_size: f64,
}

#[get("/tags/")]
async fn tag_list_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;

    let tag_categories = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<Vec<SplitTagCategoryEntry>, IntertextualError> {
            let categories = actions::tags::get_all_categories(&conn)?;
            let mut result = Vec::with_capacity(categories.len());
            for category in categories {
                let raw_tags = actions::tags::get_tags_by_category(category.id, &conn)?;
                let mut tags = Vec::with_capacity(raw_tags.len());
                for tag in raw_tags {
                    let tagged_stories = actions::tags::get_total_story_count(tag.id, &conn)?;
                    tags.push(TagEntry {
                        name: tag.display_name,
                        description: tag.description.unwrap_or_default(),
                        checkable: tag.is_checkable,
                        tagged_stories,
                        em_size: 0.0,
                    });
                }

                // Sort the tags alphabetically.
                tags.sort_by(|t, u| t.name.to_lowercase().cmp(&u.name.to_lowercase()));

                let (mut checkable_tags, custom_tags): (Vec<TagEntry>, Vec<TagEntry>) =
                    tags.into_iter().partition(|t| t.checkable);

                // Remove zero-hit stories from the custom tags
                let mut custom_tags: Vec<TagEntry> = custom_tags
                    .into_iter()
                    .filter(|t| t.tagged_stories > 0)
                    .collect();
                compute_em_sizes(&mut checkable_tags);
                compute_em_sizes(&mut custom_tags);
                result.push(SplitTagCategoryEntry {
                    name: category.display_name,
                    description: category.description,
                    checkable_tags,
                    custom_tags,
                });
            }
            Ok(result)
        },
    )
    .await?;

    let s = TagsListingTemplate {
        persistent: PersistentTemplate::from(&persistent),
        tag_categories,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[get("/tags/popularity/")]
async fn popularity_tags_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;

    let tags = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<Vec<TagEntry>, IntertextualError> {
            let raw_tags = actions::tags::get_all_tags(&conn)?;
            let mut tags = Vec::with_capacity(raw_tags.len());
            for tag in raw_tags {
                let tagged_stories = actions::tags::get_total_story_count(tag.id, &conn)?;

                if !tag.is_checkable && tagged_stories == 0 {
                    // Remove zero-hit stories from the custom tags
                    continue;
                }

                if !tag.show_in_popularity_listing {
                    // Do not show explicitly excluded tags
                    continue;
                }

                tags.push(TagEntry {
                    name: tag.display_name,
                    description: tag.description.unwrap_or_default(),
                    checkable: tag.is_checkable,
                    tagged_stories,
                    em_size: 0.0,
                });
            }

            // Sort the tags alphabetically.
            tags.sort_by(|t, u| t.name.to_lowercase().cmp(&u.name.to_lowercase()));

            compute_em_sizes(&mut tags);
            Ok(tags)
        },
    )
    .await?;

    let s = TagsCloudTemplate {
        persistent: PersistentTemplate::from(&persistent),
        tags,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

const MIN_FONT_SIZE: f64 = 1.0;
const MAX_FONT_SIZE: f64 = 2.0;

fn compute_em_sizes(tags: &mut Vec<TagEntry>) {
    let min_value = tags.iter().map(|t| t.tagged_stories).min().unwrap_or(0) as f64;
    let max_value = tags.iter().map(|t| t.tagged_stories).max().unwrap_or(0) as f64;
    let ratio = if (max_value - min_value).abs() < f64::EPSILON {
        // If this is the case, we don't really care - the distance value will always be basically 0, and the result
        // will always be MIN_FONT_SIZE, which is what we want.
        // However, allowing 0 as a denominator returns NaN, which will propagate to the em size. To handle this
        // cleanly, we manually set the denominator to 1.0 instead.
        1.0f64
    } else {
        max_value - min_value
    };
    for tag in tags.iter_mut() {
        // Compute the value [0..1] between min_value and max_value
        let distance_value = ((tag.tagged_stories as f64) - min_value) / ratio;

        // Transform the [0..1] value into a [MIN..MAX] value
        let computed_em_size = MIN_FONT_SIZE + distance_value * (MAX_FONT_SIZE - MIN_FONT_SIZE);

        // Round the size to 2 decimal places
        tag.em_size = (computed_em_size * 100.0).round() / 100.0;
    }
}
