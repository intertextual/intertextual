use actix_web::{get, post, web, HttpRequest, HttpResponse};
use askama::Template;
use serde::{Deserialize, Serialize};

use intertextual::actions;
use intertextual::models::filter::FilterMode;
use intertextual::models::stories::StoryEntry;
use intertextual::models::stories::StoryQueryExtraInfos;
use intertextual::models::stories::StorySearchResult;
use intertextual::models::stories::StorySortMode;
use intertextual::utils::page_list::PageListing;

use crate::prelude::*;

const STORIES_PER_PAGE: i64 = 20;

#[derive(Template)]
#[template(path = "search.html")]
struct SearchTemplate {
    persistent: PersistentTemplate,
    query: String,
    sort_mode: SortByMode,
    stories: Vec<(StoryEntry, SearchResult)>,
    excluded_stories: Vec<(StoryEntry, SearchResult)>,
    solutions: Vec<(String, usize)>,
    page_listing: PageListing,
    open_advanced_by_default: bool,
    included_categories: Vec<CheckableTagCategoryEntry>,
    excluded_categories: Vec<CheckableTagCategoryEntry>,
    default_title: String,
    default_author_name: String,
    default_description: String,
    default_included_tags: String,
    default_excluded_tags: String,
    tag_details: Option<TagDetails>,
}

struct TagDetails {
    name: String,
    category: String,
    equivalent_tags: Vec<String>,
    description: String,
}

struct SearchResult {
    percent_match: usize,
}

struct CheckableTagCategoryEntry {
    category_name: String,
    tags: Vec<CheckableTagEntry>,
}

struct CheckableTagEntry {
    tag_name: String,
    checked: bool,
}

#[derive(Deserialize)]
struct SearchParams {
    advanced: Option<String>,
    q: Option<String>,
    start: Option<String>,
    sort_by: Option<String>,
}

#[derive(Serialize, Deserialize)]
struct AdvancedSearchParams {
    title: Option<String>,
    author_name: Option<String>,
    description: Option<String>,
    add_tag: Option<String>,
    included_tags: Option<String>,
    remove_tag: Option<String>,
    excluded_tags: Option<String>,
    #[serde(flatten)]
    others: std::collections::BTreeMap<String, String>,
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum SortByMode {
    TitleAsc,
    TitleDesc,
    UpdateAsc,
    UpdateDesc,
}

impl SortByMode {
    pub fn parse(sort_by: &Option<String>) -> SortByMode {
        match sort_by.as_deref() {
            Some(t) if t == "title_asc" => SortByMode::TitleAsc,
            Some(t) if t == "title_desc" => SortByMode::TitleDesc,
            Some(t) if t == "update_asc" => SortByMode::UpdateAsc,
            Some(t) if t == "update_desc" => SortByMode::UpdateDesc,
            _ => SortByMode::UpdateDesc,
        }
    }

    pub fn to_story_sort_mode(self) -> StorySortMode {
        match self {
            SortByMode::TitleAsc => StorySortMode::TitleAsc,
            SortByMode::TitleDesc => StorySortMode::TitleDesc,
            SortByMode::UpdateAsc => StorySortMode::LastUpdateAsc,
            SortByMode::UpdateDesc => StorySortMode::LastUpdateDesc,
        }
    }

    pub fn next_title_sort(&self) -> &'static str {
        match self {
            Self::TitleAsc => "title_desc",
            _ => "title_asc",
        }
    }

    pub fn title_symbol(&self) -> &'static str {
        match self {
            Self::TitleAsc => "↓",
            Self::TitleDesc => "↑",
            _ => "",
        }
    }

    pub fn next_update_sort(&self) -> &'static str {
        match self {
            Self::UpdateDesc => "update_asc",
            _ => "update_desc",
        }
    }

    pub fn update_symbol(&self) -> &'static str {
        match self {
            Self::UpdateAsc => "↑",
            Self::UpdateDesc => "↓",
            _ => "",
        }
    }

    pub fn current_amp(&self) -> &'static str {
        match self {
            SortByMode::TitleAsc => "&sort_by=title_asc",
            SortByMode::TitleDesc => "&sort_by=title_desc",
            SortByMode::UpdateAsc => "&sort_by=update_asc",
            SortByMode::UpdateDesc => "",
        }
    }
}

#[get("/search/")]
async fn search_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<SearchParams>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();
    let filter_mode = FilterMode::from_login_opt(&login);

    let query = url_params.q;
    let sort_mode = SortByMode::parse(&url_params.sort_by);

    let (solutions, parsed_query, query_result) =
        match ambiguous_query_disambiguation(query.clone()) {
            Some(solutions) => {
                let mut solution_informations: Vec<(String, usize)> = Vec::new();
                let mut first_parsed_query = SearchQuery {
                    author: None,
                    description: None,
                    excluded_tags: vec![],
                    formatted_query: String::new(),
                    tags: vec![],
                    title: None,
                };
                let mut first_query_result = Vec::new();
                for possible_query in solutions {
                    let parsed_query = parse_search_query(Some(possible_query.clone()));
                    let parsed_query_copy = parsed_query.clone();
                    let filter_mode = filter_mode.clone();
                    let sort_mode = sort_mode.to_story_sort_mode();
                    let query_result = db_action(&data.pool, &persistent, move |conn| {
                        execute_search(parsed_query_copy, sort_mode, filter_mode, &conn)
                    })
                    .await?;
                    if !query_result.is_empty() {
                        solution_informations.push((
                            parsed_query.formatted_query.trim().to_string(),
                            query_result.len(),
                        ));
                    }
                    if query_result.len() > first_query_result.len() {
                        first_parsed_query = parsed_query;
                        first_query_result = query_result;
                    }
                }

                if first_query_result.is_empty() {
                    // We have no good match. Just fall back to the normal query.
                    let parsed_query = parse_search_query(query);
                    let parsed_query_copy = parsed_query.clone();
                    let filter_mode = filter_mode.clone();
                    let sort_mode = sort_mode.to_story_sort_mode();
                    let query_result = db_action(&data.pool, &persistent, move |conn| {
                        execute_search(parsed_query_copy, sort_mode, filter_mode, &conn)
                    })
                    .await?;
                    (vec![], parsed_query, query_result)
                } else {
                    (
                        if solution_informations.len() > 1 {
                            solution_informations
                        } else {
                            vec![]
                        },
                        first_parsed_query,
                        first_query_result,
                    )
                }
            }
            None => {
                let parsed_query = parse_search_query(query);
                let parsed_query_copy = parsed_query.clone();
                let filter_mode = filter_mode.clone();
                let sort_mode = sort_mode.to_story_sort_mode();
                let query_result = db_action(&data.pool, &persistent, move |conn| {
                    execute_search(parsed_query_copy, sort_mode, filter_mode, &conn)
                })
                .await?;
                (vec![], parsed_query, query_result)
            }
        };

    let start_index = url_params
        .start
        .and_then(|s| s.parse::<i64>().ok())
        .unwrap_or(0);

    let page_listing =
        PageListing::get_from_count(query_result.len() as i64, start_index, STORIES_PER_PAGE);

    let mut search_results: Vec<(StoryEntry, SearchResult)> =
        Vec::with_capacity(std::cmp::min(query_result.len(), STORIES_PER_PAGE as usize));
    let user_info = StoryQueryExtraInfos::from_maybe_user(&data.site, &login);
    for StorySearchResult {
        story_id,
        found_tags,
        requested_tags,
    } in query_result
        .into_iter()
        .skip(start_index as usize)
        .take(STORIES_PER_PAGE as usize)
    {
        let user_info = user_info.clone();
        let story_entry = db_action(
            &data.pool,
            &persistent,
            move |conn| -> Result<StoryEntry, IntertextualError> {
                let story = actions::stories::find_story_by_id(story_id, &conn)?;
                match story {
                    Some(story) => Ok(StoryEntry::from_database(story, &user_info, &conn)?),
                    None => Err(IntertextualError::InternalServerError),
                }
            },
        )
        .await?;
        let percent_match = if requested_tags == 0 {
            100
        } else {
            (found_tags * 100) / requested_tags
        };
        search_results.push((story_entry, SearchResult { percent_match }));
    }

    let tag_details = if parsed_query.tags.len() == 1
        && parsed_query.excluded_tags.is_empty()
        && parsed_query.title.is_none()
        && parsed_query.author.is_none()
        && parsed_query.description.is_none()
    {
        match parsed_query.tags.first() {
            Some(name) => {
                let name_copy = name.clone();
                let tag = db_action(&data.pool, &persistent, move |conn| {
                    actions::tags::find_tag_by_name(&name_copy, &conn)
                })
                .await?;
                match tag {
                    None => None,
                    Some((category, tag)) => {
                        let name = tag.display_name.clone();
                        let description = tag.description.clone();
                        let equivalent_tags = db_action(&data.pool, &persistent, move |conn| {
                            actions::tags::find_internal_tags_equivalent_to(&tag, &conn)
                        })
                        .await?
                        .into_iter()
                        .map(|t| t.internal_name)
                        .filter(|t| t != &name)
                        .collect();
                        Some(TagDetails {
                            name,
                            category: category.display_name,
                            description: description.unwrap_or_default(),
                            equivalent_tags,
                        })
                    }
                }
            }
            None => None,
        }
    } else {
        None
    };

    let tags_stringified = parsed_query
        .tags
        .iter()
        .map(|t| format!("#{}", t))
        .collect::<Vec<String>>()
        .join(" ");
    let excluded_tags_stringified = parsed_query
        .excluded_tags
        .iter()
        .map(|t| format!("-#{}", t))
        .collect::<Vec<String>>()
        .join(" ");
    let tags = parsed_query.tags.to_vec();
    let included_categories = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<Vec<CheckableTagCategoryEntry>, IntertextualError> {
            let raw_categories =
                actions::tags::get_quick_select_for_included_tag_search_categories(&conn)?;
            let mut categories = Vec::with_capacity(raw_categories.len());
            for category in raw_categories {
                let raw_tags = actions::tags::get_checkable_tags_by_category(category.id, &conn)?;
                categories.push(CheckableTagCategoryEntry {
                    category_name: category.display_name,
                    tags: raw_tags
                        .into_iter()
                        .map(|t| {
                            let checked = tags.iter().any(|name| name == &t.display_name);
                            CheckableTagEntry {
                                tag_name: t.display_name,
                                checked,
                            }
                        })
                        .collect(),
                });
            }
            Ok(categories)
        },
    )
    .await?;
    let tags = parsed_query.excluded_tags.to_vec();
    let excluded_categories = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<Vec<CheckableTagCategoryEntry>, IntertextualError> {
            let raw_categories =
                actions::tags::get_quick_select_for_excluded_tag_search_categories(&conn)?;
            let mut categories = Vec::with_capacity(raw_categories.len());
            for category in raw_categories {
                let raw_tags = actions::tags::get_checkable_tags_by_category(category.id, &conn)?;
                categories.push(CheckableTagCategoryEntry {
                    category_name: category.display_name,
                    tags: raw_tags
                        .into_iter()
                        .map(|t| {
                            let checked = tags.iter().any(|name| name == &t.display_name);
                            CheckableTagEntry {
                                tag_name: t.display_name,
                                checked,
                            }
                        })
                        .collect(),
                });
            }
            Ok(categories)
        },
    )
    .await?;

    let (excluded_stories, stories) = search_results.into_iter().partition(|(s, _)| {
        // If all excluded authors are in the query string, we want to show this story in the "normal" list. Otherwise, only show it in the "excluded results"
        let has_excluded_authors = s.excluded_authors.iter().any(|u| {
            let author_username_matches_query = matches!(&parsed_query.author, Some(val) if u.username.to_lowercase() == val.to_string().to_lowercase());
            let author_display_name_matches_query = matches!(&parsed_query.author, Some(val) if u.display_name.to_lowercase() == val.to_string().to_lowercase());
            // Author is excluded if neither username nor display_name matches the query. Otherwise, the excluded author is ignored.
            !author_username_matches_query && !author_display_name_matches_query
        });

        // If all excluded tags are in the query string, we want to show this story in the "normal" list. Otherwise, only show it in the "excluded results"
        let has_excluded_tags = s
            .excluded_tags
            .iter()
            .any(|(_, t)| !parsed_query.tags.contains(&t.display_name));

        // The story is excluded if it has either an excluded author or excluded tags
        has_excluded_authors || has_excluded_tags
    });

    let s = SearchTemplate {
        persistent: PersistentTemplate::from(&persistent),
        query: parsed_query.formatted_query.trim().to_string(),
        sort_mode,
        stories,
        excluded_stories,
        solutions,
        page_listing,
        open_advanced_by_default: matches!(url_params.advanced, Some(v) if v.as_str() == "true"),
        included_categories,
        excluded_categories,
        default_title: parsed_query.title.unwrap_or_default(),
        default_author_name: parsed_query.author.unwrap_or_default(),
        default_description: parsed_query.description.unwrap_or_default(),
        default_included_tags: tags_stringified,
        default_excluded_tags: excluded_tags_stringified,
        tag_details,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

fn execute_search(
    parsed_query: SearchQuery,
    sort_mode: StorySortMode,
    filter_mode: FilterMode,
    conn: &diesel::pg::PgConnection,
) -> Result<Vec<StorySearchResult>, IntertextualError> {
    let mut tags = vec![];
    tags.reserve(parsed_query.tags.len());
    for tag_name in parsed_query.tags {
        let tag = actions::tags::find_tag_by_name(&tag_name, conn)?;
        if let Some((_, tag)) = tag {
            tags.push(tag);
        }
    }

    let mut excluded_tags = vec![];
    excluded_tags.reserve(parsed_query.excluded_tags.len());
    for tag_name in parsed_query.excluded_tags {
        let tag = actions::tags::find_tag_by_name(&tag_name, conn)?;
        if let Some((_, tag)) = tag {
            excluded_tags.push(tag);
        }
    }

    actions::search::execute_search(
        actions::search::SearchInformation {
            tags,
            excluded_tags,
            title: parsed_query.title,
            author: parsed_query.author,
            description: parsed_query.description,
        },
        sort_mode,
        &filter_mode,
        conn,
    )
}

#[post("/search/advanced/")]
async fn advanced_search_page(
    form: web::Form<AdvancedSearchParams>,
) -> Result<HttpResponse, AppError> {
    let form = form.into_inner();

    let mut result_query = String::new();
    let mut tags: Vec<String> = vec![];
    let mut excluded_tags: Vec<String> = vec![];
    match form.title {
        Some(title) if !title.is_empty() => {
            result_query.push_str("title:");
            if title.contains(' ') {
                result_query.push('\"');
                result_query.push_str(&title);
                result_query.push('\"');
            } else {
                result_query.push_str(&title);
            }
            result_query.push(' ');
        }
        _ => {}
    }
    match form.author_name {
        Some(author_name) if !author_name.is_empty() => {
            result_query.push_str("author:");
            if author_name.contains(' ') {
                result_query.push('\"');
                result_query.push_str(&author_name);
                result_query.push('\"');
            } else {
                result_query.push_str(&author_name);
            }
            result_query.push(' ');
        }
        _ => {}
    }
    match form.description {
        Some(description) if !description.is_empty() => {
            result_query.push_str("description:");
            if description.contains(' ') {
                result_query.push('\"');
                result_query.push_str(&description);
                result_query.push('\"');
            } else {
                result_query.push_str(&description);
            }
            result_query.push(' ');
        }
        _ => {}
    }
    match form.add_tag {
        Some(add_tag) if !add_tag.is_empty() => {
            let clean_tag = add_tag.strip_prefix('#').unwrap_or(&add_tag).to_string();
            if !clean_tag.is_empty() {
                tags.push(clean_tag);
            }
        }
        _ => {}
    }
    if let Some(included_tags) = form.included_tags {
        for entry in included_tags.trim().split(' ') {
            let clean_tag = entry.strip_prefix('#').unwrap_or(entry).to_string();
            if !clean_tag.is_empty() {
                tags.push(clean_tag);
            }
        }
    }
    match form.remove_tag {
        Some(remove_tag) if !remove_tag.is_empty() => {
            let clean_tag = remove_tag
                .strip_prefix('#')
                .unwrap_or(&remove_tag)
                .to_string();
            if !clean_tag.is_empty() {
                excluded_tags.push(clean_tag);
            }
        }
        _ => {}
    }
    if let Some(excluded_tag_list) = form.excluded_tags {
        for entry in excluded_tag_list.trim().split(' ') {
            if let Some(matched_tag) = entry.strip_prefix('-') {
                let clean_tag = matched_tag
                    .strip_prefix('#')
                    .unwrap_or(matched_tag)
                    .to_string();
                if !clean_tag.is_empty() {
                    excluded_tags.push(clean_tag);
                }
            }
        }
    }
    for (maybe_tag, state) in form.others {
        if state != "on" {
            continue;
        }

        if let Some(tag) = maybe_tag.strip_prefix("include_tag_") {
            tags.push(tag.to_string());
        }

        if let Some(tag) = maybe_tag.strip_prefix("exclude_tag_") {
            excluded_tags.push(tag.to_string());
        }
    }

    tags.sort_unstable();
    tags.dedup();
    for tag in tags {
        result_query.push_str("%23");
        result_query.push_str(&tag);
        result_query.push(' ');
    }

    excluded_tags.sort_unstable();
    excluded_tags.dedup();
    for tag in excluded_tags {
        result_query.push_str("-%23");
        result_query.push_str(&tag);
        result_query.push(' ');
    }

    Ok(HttpResponse::SeeOther()
        .insert_header((
            actix_web::http::header::LOCATION,
            format!("/search/?advanced=true&q={}", result_query.trim(),),
        ))
        .finish())
}

fn ambiguous_query_disambiguation(query: Option<String>) -> Option<Vec<String>> {
    match query {
        None => None,
        Some(query) => {
            if query.starts_with('#') || query.contains(" #") {
                return None;
            }

            if query.starts_with("-#") || query.contains(" -#") {
                return None;
            }

            if query.starts_with("title:") || query.contains(" title:") {
                return None;
            }

            if query.starts_with("author:") || query.contains(" author:") {
                return None;
            }

            if query.starts_with("description:") || query.contains(" description:") {
                return None;
            }

            Some(vec![
                // Author
                format!("author:\"{}\"", query),
                // Story
                format!("title:\"{}\"", query),
                // Tags
                query
                    .split(' ')
                    .map(|q| format!("#{}", q))
                    .collect::<Vec<String>>()
                    .join(" "),
            ])
        }
    }
}

#[derive(Clone)]
struct SearchQuery {
    formatted_query: String,
    title: Option<String>,
    author: Option<String>,
    description: Option<String>,
    tags: Vec<String>,
    excluded_tags: Vec<String>,
}

enum InQuoteType {
    None,
    Title,
    Author,
    Description,
}

fn parse_search_query(query: Option<String>) -> SearchQuery {
    let mut result = SearchQuery {
        formatted_query: String::new(),
        title: None,
        author: None,
        description: None,
        tags: vec![],
        excluded_tags: vec![],
    };

    if query.is_none() {
        return result;
    }

    let query = query.unwrap();

    let mut in_quote_type = InQuoteType::None;
    let mut quote_agglomerated: Option<String> = None;

    for entry in query.trim().split(' ') {
        if !matches!(in_quote_type, InQuoteType::None) {
            if entry.ends_with('\"') && !entry.ends_with("\\\"") {
                // This is the end of the quoted part.

                // Append the remainder of the un-quoted string into the agglomerator.
                quote_agglomerated = match quote_agglomerated {
                    Some(mut s) => {
                        s.push(' ');
                        Some(s)
                    }
                    None => None,
                };
                quote_agglomerated = match quote_agglomerated {
                    Some(mut s) => {
                        s.push_str(entry.strip_suffix('\"').unwrap_or(entry));
                        Some(s)
                    }
                    None => None,
                };

                // Move the agglomerator into the right result field.
                match in_quote_type {
                    InQuoteType::None => panic!("We should never match in_quote_type == none. The matches! statement before should have filtered it out."),
                    InQuoteType::Title => {
                        let contents = quote_agglomerated.take();
                        result.formatted_query.push_str("title:\"");
                        result
                            .formatted_query
                            .push_str(contents.as_deref().unwrap_or(""));
                        result.formatted_query.push('\"');
                        result.title = contents
                    }
                    InQuoteType::Author => {
                        let contents = quote_agglomerated.take();
                        result.formatted_query.push_str("author:\"");
                        result
                            .formatted_query
                            .push_str(contents.as_deref().unwrap_or(""));
                        result.formatted_query.push('\"');
                        result.author = contents
                    }
                    InQuoteType::Description => {
                        let contents = quote_agglomerated.take();
                        result.formatted_query.push_str("description:\"");
                        result
                            .formatted_query
                            .push_str(contents.as_deref().unwrap_or(""));
                        result.formatted_query.push('\"');
                        result.description = contents
                    }
                }

                // Exit the "In Quote" state
                in_quote_type = InQuoteType::None;
                continue;
            }

            quote_agglomerated = match quote_agglomerated {
                Some(mut s) => {
                    s.push(' ');
                    Some(s)
                }
                None => None,
            };
            quote_agglomerated = match quote_agglomerated {
                Some(mut s) => {
                    s.push_str(entry);
                    Some(s)
                }
                None => None,
            };
            continue;
        }

        if let Some(matched_title) = entry.strip_prefix("title:") {
            if matched_title.starts_with('\"')
                && matched_title.ends_with('\"')
                && !matched_title.ends_with("\\\"")
            {
                // Start and end are unescaped quotes
                let slice = &matched_title[1..matched_title.len() - 1];
                result.title = Some(slice.to_string());
                result.formatted_query.push_str("title:");
                result.formatted_query.push_str(slice);
                result.formatted_query.push(' ');
                continue;
            }

            if let Some(quote_content) = matched_title.strip_prefix('\"') {
                // Start is a quote, end isn't a quote
                quote_agglomerated = Some(quote_content.to_string());
                in_quote_type = InQuoteType::Title;
                continue;
            }

            result.title = Some(matched_title.to_string());
            result.formatted_query.push_str(entry);
            result.formatted_query.push(' ');
            continue;
        }

        if let Some(matched_author) = entry.strip_prefix("author:") {
            if matched_author.starts_with('\"')
                && matched_author.ends_with('\"')
                && !matched_author.ends_with("\\\"")
            {
                // Start and end are unescaped quotes
                let slice = &matched_author[1..matched_author.len() - 1];
                result.author = Some(slice.to_string());
                result.formatted_query.push_str("author:");
                result.formatted_query.push_str(slice);
                result.formatted_query.push(' ');
                continue;
            }

            if let Some(quote_content) = matched_author.strip_prefix('\"') {
                // Start is a quote, end isn't a quote
                quote_agglomerated = Some(quote_content.to_string());
                in_quote_type = InQuoteType::Author;
                continue;
            }

            result.author = Some(matched_author.to_string());
            result.formatted_query.push_str(entry);
            result.formatted_query.push(' ');
            continue;
        }

        if let Some(matched_description) = entry.strip_prefix("description:") {
            if matched_description.starts_with('\"')
                && matched_description.ends_with('\"')
                && !matched_description.ends_with("\\\"")
            {
                // Start and end are unescaped quotes
                let slice = &matched_description[1..matched_description.len() - 1];
                result.description = Some(slice.to_string());
                result.formatted_query.push_str("description:");
                result.formatted_query.push_str(slice);
                result.formatted_query.push(' ');
                continue;
            }

            if let Some(quote_content) = matched_description.strip_prefix('\"') {
                // Start is a quote, end isn't a quote
                quote_agglomerated = Some(quote_content.to_string());
                in_quote_type = InQuoteType::Description;
                continue;
            }

            result.description = Some(matched_description.to_string());
            result.formatted_query.push_str(entry);
            result.formatted_query.push(' ');
            continue;
        }

        if let Some(matched_tag) = entry.strip_prefix('-') {
            let clean_tag = matched_tag.strip_prefix('#').unwrap_or(matched_tag);
            if !clean_tag.is_empty() {
                result.formatted_query.push_str("-#");
                result.formatted_query.push_str(clean_tag);
                result.formatted_query.push(' ');
                result.excluded_tags.push(clean_tag.to_string());
            }
            continue;
        }

        let clean_tag = entry.strip_prefix('#').unwrap_or(entry);
        if !clean_tag.is_empty() {
            result.formatted_query.push('#');
            result.formatted_query.push_str(clean_tag);
            result.formatted_query.push(' ');
            result.tags.push(clean_tag.to_string());
        }
    }

    // In case the result was quote-mismatched, move the agglomerator into the right result field.
    match in_quote_type {
        InQuoteType::None => {}
        InQuoteType::Title => {
            let contents = quote_agglomerated.take();
            result.formatted_query.push_str("title:\"");
            result
                .formatted_query
                .push_str(contents.as_deref().unwrap_or(""));
            result.formatted_query.push('\"');
            result.title = contents
        }
        InQuoteType::Author => {
            let contents = quote_agglomerated.take();
            result.formatted_query.push_str("author:\"");
            result
                .formatted_query
                .push_str(contents.as_deref().unwrap_or(""));
            result.formatted_query.push('\"');
            result.author = contents
        }
        InQuoteType::Description => {
            let contents = quote_agglomerated.take();
            result.formatted_query.push_str("description:\"");
            result
                .formatted_query
                .push_str(contents.as_deref().unwrap_or(""));
            result.formatted_query.push('\"');
            result.description = contents
        }
    }

    result
}
