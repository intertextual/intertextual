use actix_web::http::header::LOCATION;
use actix_web::{post, web, HttpRequest, HttpResponse};

use intertextual::actions;
use intertextual::constants::RECOMMENDATION_URL;
use intertextual::models;
use intertextual::models::error::IntertextualError;
use intertextual::models::shared::AppState;

use crate::app::identity::Identity;
use crate::app::persistent::*;
use crate::error::*;
use crate::prelude::*;

#[post("/follow/@{user}/")]
async fn handle_follow_author_recommendations(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let author_username = path.into_inner();
    let author_username_copy = author_username.clone();
    let mode = models::filter::FilterMode::from_login(login_user);
    let author = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&author_username_copy, &mode, &conn)
    })
    .await?
    .ok_or(IntertextualError::UserNotFound {
        username: author_username,
    })
    .map_err_app(&persistent)?;

    let user_id = login_user.id;
    let author_id = author.id;
    let _ignore_errors = db_action(&data.pool, &persistent, move |conn| {
        actions::follows::modifications::follow_author_recommendations(user_id, author_id, &conn)
    })
    .await;

    let return_url = format!("/{}/@{}/#follow", RECOMMENDATION_URL, author.username,);

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}

#[post("/unfollow/@{user}/")]
async fn handle_unfollow_author_recommendations(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let author_username = path.into_inner();
    let username = author_username.clone();
    let mode = models::filter::FilterMode::from_login(login_user);
    let author = db_action(&data.pool, &persistent, move |conn| {
        actions::users::find_user_by_username(&username, &mode, &conn)
    })
    .await?
    .ok_or(IntertextualError::UserNotFound {
        username: author_username,
    })
    .map_err_app(&persistent)?;

    let user_id = login_user.id;
    let author_id = author.id;
    let _ignore_errors = db_action(&data.pool, &persistent, move |conn| {
        actions::follows::modifications::unfollow_author_recommendations(user_id, author_id, &conn)
    })
    .await;

    let return_url = format!("/{}/@{}/#follow", RECOMMENDATION_URL, author.username,);

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}
