use actix_web::http::header::LOCATION;
use actix_web::{get, post, web, HttpRequest, HttpResponse};
use askama::Template;
use serde::{Deserialize, Serialize};

use intertextual::actions;
use intertextual::constants::RECOMMENDATION_URL;
use intertextual::models;
use intertextual::models::filter::FilterMode;

use crate::prelude::*;

#[derive(Template)]
#[template(path = "recommendation/report.html")]
struct ReportRecommendationTemplate {
    persistent: PersistentTemplate,
    user: models::users::User,
    recommendation: RecommendationEntry,
}

pub struct RecommendationEntry {
    authors: Vec<models::users::ShortUserEntry>,
    author_path: String,
    story_url_fragment: String,
    story_title: String,
    formatted_recommendation: SanitizedHtml,
}

#[derive(Serialize, Deserialize)]
pub struct CommentReportFormParams {
    reason: Option<String>,
    other_content: Option<String>,
}

#[get("/@{user}/@{author}/{story}/report/")]
async fn get_report_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, String)>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let (recommender_username, author_username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (recommender, author, story, recommendation) =
        db_action(&data.pool, &persistent, move |conn| {
            intertextual::data::recommendations::find_author_recommendation_by_url(
                recommender_username,
                author_username,
                url_fragment,
                &mode,
                &conn,
            )
        })
        .await?;

    get_report_shared(
        data,
        persistent,
        recommender,
        recommendation,
        vec![author],
        story,
    )
    .await
}

#[get("/@{user}/collaboration/{story}/report/")]
async fn get_report_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let (recommender_username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (recommender, authors, story, recommendation) =
        db_action(&data.pool, &persistent, move |conn| {
            intertextual::data::recommendations::find_collaboration_recommendation_by_url(
                recommender_username,
                url_fragment,
                &mode,
                &conn,
            )
        })
        .await?;

    get_report_shared(
        data,
        persistent,
        recommender,
        recommendation,
        authors,
        story,
    )
    .await
}

async fn get_report_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    recommender: models::users::User,
    recommendation: models::recommendations::Recommendation,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;
    login_user
        .require_report_recommendation_rights_for(&recommender, &authors, &story)
        .map_err_app(&persistent)?;

    let formatted_content =
        richblock_to_html(&recommendation.description, &data.pool, &persistent).await?;
    let recommendation = RecommendationEntry {
        authors: authors
            .iter()
            .map(models::users::ShortUserEntry::from)
            .collect(),
        author_path: story.author_path(&authors),
        story_title: story.title,
        story_url_fragment: story.url_fragment,
        formatted_recommendation: formatted_content,
    };
    let s = ReportRecommendationTemplate {
        persistent: PersistentTemplate::from(&persistent),
        user: recommender,
        recommendation,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/@{user}/@{author}/{story}/report/")]
async fn post_report_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, String)>,
    form: web::Form<CommentReportFormParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let (recommender_username, author_username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (recommender, author, story, recommendation) =
        db_action(&data.pool, &persistent, move |conn| {
            intertextual::data::recommendations::find_author_recommendation_by_url(
                recommender_username,
                author_username,
                url_fragment,
                &mode,
                &conn,
            )
        })
        .await?;

    post_report_shared(
        data,
        persistent,
        recommender,
        recommendation,
        vec![author],
        story,
        form,
    )
    .await
}

#[post("/@{user}/collaboration/{story}/report/")]
async fn post_report_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    form: web::Form<CommentReportFormParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let (recommender_username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (recommender, authors, story, recommendation) =
        db_action(&data.pool, &persistent, move |conn| {
            intertextual::data::recommendations::find_collaboration_recommendation_by_url(
                recommender_username,
                url_fragment,
                &mode,
                &conn,
            )
        })
        .await?;

    post_report_shared(
        data,
        persistent,
        recommender,
        recommendation,
        authors,
        story,
        form,
    )
    .await
}

async fn post_report_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    recommender: models::users::User,
    _recommendation: models::recommendations::Recommendation,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    form: web::Form<CommentReportFormParams>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    login_user
        .require_report_recommendation_rights_for(&recommender, &authors, &story)
        .map_err_app(&persistent)?;

    let form = form.into_inner();
    let message = match form.reason {
        Some(m) if m.as_str() == "spam" => "Reported for spam".to_string(),
        Some(m) if m.as_str() == "tos" => "Reported for TOS violation".to_string(),
        Some(m) if m.as_str() == "offensive" => "Reported for offensive language".to_string(),
        Some(m) if m.as_str() == "harassment" => "Reported for harassment".to_string(),
        _ => form.other_content.unwrap_or_default(),
    };

    crate::app::validation::validate_comment_report(&message, "Report message")
        .map_err_app(&persistent)?;

    let login_id = login_user.id;
    let recommender_id = recommender.id;
    let story_id = story.id;
    let report_result = db_action(&data.pool, &persistent, move |conn| {
        actions::reports::modifications::report_recommendation(
            login_id,
            recommender_id,
            story_id,
            message,
            &conn,
        )
    })
    .await;
    if crate::app::is_unique_violation(&report_result) {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Report",
            message: "You can only report a chapter once".to_string(),
        }
        .into_app(&persistent));
    }
    let return_url = format!(
        "/{}/@{}/{}/{}/",
        RECOMMENDATION_URL,
        recommender.username,
        story.author_path(&authors),
        story.url_fragment,
    );

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}

#[post("/@{user}/@{author}/{story}/unreport/")]
async fn post_unreport_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, String)>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let (recommender_username, author_username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (recommender, author, story, recommendation) =
        db_action(&data.pool, &persistent, move |conn| {
            intertextual::data::recommendations::find_author_recommendation_by_url(
                recommender_username,
                author_username,
                url_fragment,
                &mode,
                &conn,
            )
        })
        .await?;

    post_unreport_shared(
        data,
        persistent,
        recommender,
        recommendation,
        vec![author],
        story,
    )
    .await
}

#[post("/@{user}/collaboration/{story}/unreport/")]
async fn post_unreport_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login = persistent.login();

    let (recommender_username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login_opt(&login);
    let (recommender, authors, story, recommendation) =
        db_action(&data.pool, &persistent, move |conn| {
            intertextual::data::recommendations::find_collaboration_recommendation_by_url(
                recommender_username,
                url_fragment,
                &mode,
                &conn,
            )
        })
        .await?;

    post_unreport_shared(
        data,
        persistent,
        recommender,
        recommendation,
        authors,
        story,
    )
    .await
}

async fn post_unreport_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    recommender: models::users::User,
    _recommendation: models::recommendations::Recommendation,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    login_user
        .require_report_recommendation_rights_for(&recommender, &authors, &story)
        .map_err_app(&persistent)?;

    let login_id = login_user.id;
    let recommender_id = recommender.id;
    let story_id = story.id;
    let _unreported_recommendation = db_action(&data.pool, &persistent, move |conn| {
        actions::reports::modifications::unreport_recommendation(
            login_id,
            recommender_id,
            story_id,
            &conn,
        )
    })
    .await?;
    let return_url = format!(
        "/{}/@{}/{}/{}/",
        RECOMMENDATION_URL,
        recommender.username,
        story.author_path(&authors),
        story.url_fragment,
    );

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}
