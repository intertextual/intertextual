use actix_web::{get, web, HttpRequest, HttpResponse};
use askama::Template;

use intertextual::actions;
use intertextual::models;
use intertextual::models::filter::FilterMode;
use intertextual::models::stories::StoryQueryExtraInfos;

use crate::prelude::*;

#[derive(Template)]
#[template(path = "recommendation/index.html")]
struct UserSingleRecommendationTemplate {
    persistent: PersistentTemplate,
    user: models::users::User,
    recommendation: RecommendationEntry,
    is_self: bool,
    is_administrator: bool,
}

pub struct RecommendationEntry {
    story: models::stories::StoryEntry,
    formatted_recommendation: SanitizedHtml,
    can_report: bool,
    is_already_reported: bool,
}

#[get("/@{user}/@{author}/{story}/")]
async fn single_page_author(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, String)>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let filter_mode = FilterMode::from_login_opt(&login_user);

    let (recommender_username, author_username, story_url_fragment) = path.into_inner();
    let (recommender, author, story, recommendation) =
        db_action(&data.pool, &persistent, move |conn| {
            intertextual::data::recommendations::find_author_recommendation_by_url(
                recommender_username,
                author_username,
                story_url_fragment,
                &filter_mode,
                &conn,
            )
        })
        .await?;

    let can_report = match login_user {
        Some(login_user) => login_user
            .require_report_recommendation_rights_for(
                &recommender,
                std::slice::from_ref(&author),
                &story,
            )
            .is_ok(),
        _ => false,
    };
    let is_already_reported = match login_user {
        Some(login_user) => {
            let login_user_id = login_user.id;
            let recommender_id = recommender.id;
            let story_id = story.id;
            db_action(&data.pool, &persistent, move |conn| {
                actions::reports::user_has_reported_recommendation(
                    login_user_id,
                    recommender_id,
                    story_id,
                    &conn,
                )
            })
            .await?
        }
        _ => false,
    };

    let user_info = StoryQueryExtraInfos::from_maybe_user(&data.site, &login_user);
    let story_entry = db_action(&data.pool, &persistent, move |conn| {
        models::stories::StoryEntry::from_database(story, &user_info, &conn)
    })
    .await?;
    let recommendation = RecommendationEntry {
        story: story_entry,
        formatted_recommendation: richblock_to_html(
            &recommendation.description,
            &data.pool,
            &persistent,
        )
        .await?,
        can_report,
        is_already_reported,
    };

    let is_self = login_user.iter().any(|u| u.id == recommender.id);
    let is_administrator = login_user.iter().any(|u| u.administrator);
    let s = UserSingleRecommendationTemplate {
        persistent: PersistentTemplate::from(&persistent),
        user: recommender,
        recommendation,
        is_self,
        is_administrator,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[get("/@{user}/collaboration/{story}/")]
async fn single_page_collaboration(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let filter_mode = FilterMode::from_login_opt(&login_user);

    let (recommender_username, story_url_fragment) = path.into_inner();
    let filter_mode = filter_mode.clone();
    let (recommender, authors, story, recommendation) =
        db_action(&data.pool, &persistent, move |conn| {
            intertextual::data::recommendations::find_collaboration_recommendation_by_url(
                recommender_username,
                story_url_fragment,
                &filter_mode,
                &conn,
            )
        })
        .await?;

    let can_report = match login_user {
        Some(login_user) => login_user
            .require_report_recommendation_rights_for(&recommender, &authors, &story)
            .is_ok(),
        _ => false,
    };
    let is_already_reported = match login_user {
        Some(login_user) => {
            let login_user_id = login_user.id;
            let recommender_id = recommender.id;
            let story_id = story.id;
            db_action(&data.pool, &persistent, move |conn| {
                actions::reports::user_has_reported_recommendation(
                    login_user_id,
                    recommender_id,
                    story_id,
                    &conn,
                )
            })
            .await?
        }
        _ => false,
    };

    let user_info = StoryQueryExtraInfos::from_maybe_user(&data.site, &login_user);
    let story_entry = db_action(&data.pool, &persistent, move |conn| {
        models::stories::StoryEntry::from_database(story, &user_info, &conn)
    })
    .await?;
    let recommendation = RecommendationEntry {
        story: story_entry,
        formatted_recommendation: richblock_to_html(
            &recommendation.description,
            &data.pool,
            &persistent,
        )
        .await?,
        can_report,
        is_already_reported,
    };

    let is_self = login_user.iter().any(|u| u.id == recommender.id);
    let is_administrator = login_user.iter().any(|u| u.administrator);
    let s = UserSingleRecommendationTemplate {
        persistent: PersistentTemplate::from(&persistent),
        user: recommender,
        recommendation,
        is_self,
        is_administrator,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}
