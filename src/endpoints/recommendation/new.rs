use actix_web::http::header::LOCATION;
use actix_web::{get, post, web, HttpRequest, HttpResponse};
use askama::Template;
use serde::{Deserialize, Serialize};

use intertextual::actions;
use intertextual::actions::recommendations::modifications;
use intertextual::constants::RECOMMENDATION_URL;
use intertextual::data;
use intertextual::models;
use intertextual::models::filter::FilterMode;

use crate::prelude::*;

#[derive(Template)]
#[template(path = "recommendation/new.html")]
struct NewRecommendationTemplate {
    persistent: PersistentTemplate,
    authors: Vec<models::users::ShortUserEntry>,
    author_path: String,
    story: models::stories::Story,
}

#[derive(Serialize, Deserialize)]
pub struct RecommendationParams {
    description: RichBlock,
    feature_recommendation: Option<String>,
}

#[get("/add/@{author}/{story}/")]
async fn main_author_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let (username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login(login_user);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await?;

    login_user
        .require_recommendation_write_rights_for(&authors, &story)
        .map_err_app(&persistent)?;

    let author_path = story.author_path(&authors);
    let s = NewRecommendationTemplate {
        persistent: PersistentTemplate::from(&persistent),
        authors: authors
            .iter()
            .map(models::users::ShortUserEntry::from)
            .collect(),
        author_path,
        story,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[get("/add/collaboration/{story}/")]
async fn main_collaboration_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let url_fragment = path.into_inner();
    let mode = FilterMode::from_login(login_user);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await?;
    login_user
        .require_recommendation_write_rights_for(&authors, &story)
        .map_err_app(&persistent)?;

    let s = NewRecommendationTemplate {
        persistent: PersistentTemplate::from(&persistent),
        authors: authors
            .iter()
            .map(models::users::ShortUserEntry::from)
            .collect(),
        author_path: "collaboration".to_string(),
        story,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/add/@{author}/{story}/")]
async fn handle_author_submit(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    form: web::Form<RecommendationParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let (username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login(login_user);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await?;

    login_user
        .require_recommendation_write_rights_for(&authors, &story)
        .map_err_app(&persistent)?;

    let form = form.into_inner();

    // TODO : validate description of the recommendation.

    let featured_by_user = matches!(form.feature_recommendation, Some(value) if &value == "on");
    let new_recommendation = models::recommendations::NewRecommendation {
        user_id: login_user.id,
        story_id: story.id,
        description: form.description,
        featured_by_user,
    };
    let result = db_action(&data.pool, &persistent, move |conn| {
        modifications::create_recommendation(new_recommendation, &conn)
    })
    .await;
    if crate::app::is_unique_violation(&result) {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Suggestion",
            message: "You already wrote a suggestion for this story.".to_string(),
        }
        .into_app(&persistent));
    }

    let user_id = login_user.id;
    let story_id = story.id;
    db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<(), IntertextualError> {
            if !actions::user_approvals::has_given_user_approval(user_id, story_id, &conn)? {
                actions::user_approvals::modifications::add_user_approval(
                    user_id, story_id, &conn,
                )?;
            }
            Ok(())
        },
    )
    .await?;

    let return_url = format!("/{}/@{}/", RECOMMENDATION_URL, login_user.username,);

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}

#[post("/add/collaboration/{story}/")]
async fn handle_collaboration_submit(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    form: web::Form<RecommendationParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let url_fragment = path.into_inner();
    let mode = FilterMode::from_login(login_user);
    let (authors, story) = db_action(&data.pool, &persistent, move |conn| {
        data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await?;

    login_user
        .require_recommendation_write_rights_for(&authors, &story)
        .map_err_app(&persistent)?;

    let form = form.into_inner();

    // TODO : validate description of the recommendation.

    let featured_by_user = matches!(form.feature_recommendation, Some(value) if &value == "on");
    let new_recommendation = models::recommendations::NewRecommendation {
        user_id: login_user.id,
        story_id: story.id,
        description: form.description,
        featured_by_user,
    };
    let result = db_action(&data.pool, &persistent, move |conn| {
        modifications::create_recommendation(new_recommendation, &conn)
    })
    .await;
    if crate::app::is_unique_violation(&result) {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Suggestion",
            message: "You already wrote a suggestion for this story.".to_string(),
        }
        .into_app(&persistent));
    }

    let user_id = login_user.id;
    let story_id = story.id;
    db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<(), IntertextualError> {
            if !actions::user_approvals::has_given_user_approval(user_id, story_id, &conn)? {
                actions::user_approvals::modifications::add_user_approval(
                    user_id, story_id, &conn,
                )?;
            }
            Ok(())
        },
    )
    .await?;

    let return_url = format!("/{}/@{}/", RECOMMENDATION_URL, login_user.username,);

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, return_url))
        .finish())
}
