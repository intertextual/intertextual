use actix_web::{get, web, HttpRequest, HttpResponse};
use askama::Template;
use serde::Deserialize;

use intertextual::actions;
use intertextual::models;
use intertextual::models::stories::StoryEntry;
use intertextual::models::stories::StoryQueryExtraInfos;

use crate::prelude::*;

#[derive(Template)]
#[template(path = "recommendation/user_list.html")]
struct UserRecommendationsTemplate {
    persistent: PersistentTemplate,
    author: models::users::User,
    recommendations: Vec<RecommendationEntry>,
    excluded_recommendations: Vec<RecommendationEntry>,
    is_self: bool,
    is_administrator: bool,
    can_follow_recommendations: bool,
    user_is_following_author_recommendations: bool,
    show_recommendation_edit_confirmation: bool,
    show_recommendation_delete_confirmation: bool,
}

pub struct RecommendationEntry {
    story: StoryEntry,
    formatted_recommendation: SanitizedHtml,
    can_report: bool,
    is_already_reported: bool,
}

#[derive(Deserialize)]
struct RecommendationParams {
    start: Option<String>,
    confirm: Option<String>,
}

#[get("/")]
async fn main_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<RecommendationParams>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let username = {
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequired)
            .map_err_app(&persistent)?;
        login_user.username.clone()
    };
    shared_page(data, persistent, username, url_params).await
}

#[get("/@{user}/")]
async fn specific_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    url_params: web::Query<RecommendationParams>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let username = path.into_inner();
    shared_page(data, persistent, username, url_params).await
}

async fn shared_page(
    data: web::Data<AppState>,
    persistent: PersistentData,
    username: String,
    url_params: RecommendationParams,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let _start_index = url_params
        .start
        .and_then(|s| s.parse::<i64>().ok())
        .unwrap_or(0);

    let user_info = StoryQueryExtraInfos::from_maybe_user(&data.site, &login_user);
    let (author, recommendations) = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<(models::users::User, Vec<RecommendationEntry>), IntertextualError> {
            let login_user = match &user_info.maybe_user_id {
                Some(login_user_id) => actions::users::find_user_by_id(*login_user_id, &conn)?,
                None => None,
            };

            let filter_mode = user_info.filter_mode.clone();
            let user = actions::users::find_user_by_username(&username, &filter_mode, &conn)?;
            let user = user.ok_or(IntertextualError::UserNotFound { username })?;

            let raw_recommendations =
                actions::recommendations::find_recommendations_by_user(user.id, &conn)?;
            let mut recommendations = Vec::with_capacity(raw_recommendations.len());
            for (story, recommendation) in raw_recommendations {
                let authors =
                    actions::stories::find_authors_by_story(story.id, &filter_mode, &conn)?;
                let can_report = match &login_user {
                    Some(login_user) => login_user
                        .require_report_recommendation_rights_for(&user, &authors, &story)
                        .is_ok(),
                    _ => false,
                };
                let is_already_reported = match &login_user {
                    Some(login_user) => actions::reports::user_has_reported_recommendation(
                        login_user.id,
                        user.id,
                        story.id,
                        &conn,
                    )?,
                    _ => false,
                };
                let story_entry = StoryEntry::from_database(story, &user_info, &conn)?;
                let entry = RecommendationEntry {
                    story: story_entry,
                    formatted_recommendation: recommendation.description.html(&conn),
                    can_report,
                    is_already_reported,
                };
                recommendations.push(entry);
            }
            Ok((user, recommendations))
        },
    )
    .await?;

    let (can_follow_recommendations, user_is_following_author_recommendations) = match &login_user {
        Some(login_user) => {
            let login_user_id = login_user.id;
            let author_id = author.id;
            (
                login_user_id != author_id,
                db_action(&data.pool, &persistent, move |conn| {
                    actions::follows::is_following_author_recommendations(
                        login_user_id,
                        author_id,
                        &conn,
                    )
                })
                .await?,
            )
        }
        None => (false, false),
    };
    let is_self = login_user.iter().any(|u| u.id == author.id);
    let is_administrator = login_user.iter().any(|u| u.administrator);

    let (excluded_recommendations, recommendations) = recommendations
        .into_iter()
        .partition(|s| s.story.is_excluded());

    let s = UserRecommendationsTemplate {
        persistent: PersistentTemplate::from(&persistent),
        author,
        recommendations,
        excluded_recommendations,
        is_self,
        is_administrator,
        can_follow_recommendations,
        user_is_following_author_recommendations,
        show_recommendation_delete_confirmation: matches!(&url_params.confirm, Some(v) if v == "recommendation_delete"),
        show_recommendation_edit_confirmation: matches!(&url_params.confirm, Some(v) if v == "recommendation_edit"),
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}
