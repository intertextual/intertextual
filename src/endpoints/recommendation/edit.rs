use actix_web::http::header::LOCATION;
use actix_web::{get, post, web, HttpRequest, HttpResponse};
use askama::Template;
use serde::{Deserialize, Serialize};

use intertextual::actions;
use intertextual::constants::RECOMMENDATION_URL;
use intertextual::models;

use crate::prelude::*;

#[derive(Template)]
#[template(path = "recommendation/edit.html")]
struct EditUserRecommendationTemplate {
    persistent: PersistentTemplate,
    user: models::users::User,
    authors: Vec<models::users::ShortUserEntry>,
    author_path: String,
    story: models::stories::Story,
    recommendation_description: RichBlock,
}

#[derive(Serialize, Deserialize)]
pub struct EditRecommendationParams {
    content: String,
    feature_recommendation: Option<String>,
}

#[get("/@{user}/@{author}/{story}/edit/")]
async fn main_author_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, String)>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let (recommender_username, author_username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login(login_user);
    let (recommender, author, story, recommendation) =
        db_action(&data.pool, &persistent, move |conn| {
            intertextual::data::recommendations::find_author_recommendation_by_url(
                recommender_username,
                author_username,
                url_fragment,
                &mode,
                &conn,
            )
        })
        .await?;

    login_user
        .require_recommendation_edit_rights_for(&recommender)
        .map_err_app(&persistent)?;

    let s = EditUserRecommendationTemplate {
        persistent: PersistentTemplate::from(&persistent),
        user: recommender,
        authors: vec![models::users::ShortUserEntry::from(&author)],
        author_path: format!("@{}", author.username),
        story,
        recommendation_description: recommendation.description,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[get("/@{user}/collaboration/{story}/edit/")]
async fn main_collaboration_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let (recommender_username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login(login_user);
    let (recommender, authors, story, recommendation) =
        db_action(&data.pool, &persistent, move |conn| {
            intertextual::data::recommendations::find_collaboration_recommendation_by_url(
                recommender_username,
                url_fragment,
                &mode,
                &conn,
            )
        })
        .await?;

    login_user
        .require_recommendation_edit_rights_for(&recommender)
        .map_err_app(&persistent)?;

    let s = EditUserRecommendationTemplate {
        persistent: PersistentTemplate::from(&persistent),
        user: recommender,
        authors: authors
            .iter()
            .map(models::users::ShortUserEntry::from)
            .collect(),
        author_path: "collaboration".to_string(),
        story,
        recommendation_description: recommendation.description,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/@{user}/@{author}/{story}/edit/")]
async fn handle_author_edit(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, String)>,

    form: web::Form<EditRecommendationParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let (recommender_username, author_username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login(login_user);
    let (recommender, _author, _story, recommendation) =
        db_action(&data.pool, &persistent, move |conn| {
            intertextual::data::recommendations::find_author_recommendation_by_url(
                recommender_username,
                author_username,
                url_fragment,
                &mode,
                &conn,
            )
        })
        .await?;

    login_user
        .require_recommendation_edit_rights_for(&recommender)
        .map_err_app(&persistent)?;

    let form = form.into_inner();
    let featured_by_user = form.feature_recommendation.filter(|o| o == "on").is_some();
    let description = String::from(form.content.trim());

    let recommendation = db_action(&data.pool, &persistent, move |conn| {
        actions::recommendations::modifications::update_recommendation_description(
            recommendation,
            description,
            &conn,
        )
    })
    .await?;
    db_action(&data.pool, &persistent, move |conn| {
        actions::recommendations::modifications::update_recommendation_featured_by_user(
            recommendation,
            featured_by_user,
            &conn,
        )
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((
            LOCATION,
            format!(
                "/{}/@{}/?confirm=recommendation_edit",
                RECOMMENDATION_URL, recommender.username,
            ),
        ))
        .finish())
}

#[post("/@{user}/collaboration/{story}/edit/")]
async fn handle_collaboration_edit(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,

    form: web::Form<EditRecommendationParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let (recommender_username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login(login_user);
    let (recommender, _authors, _story, recommendation) =
        db_action(&data.pool, &persistent, move |conn| {
            intertextual::data::recommendations::find_collaboration_recommendation_by_url(
                recommender_username,
                url_fragment,
                &mode,
                &conn,
            )
        })
        .await?;

    login_user
        .require_recommendation_edit_rights_for(&recommender)
        .map_err_app(&persistent)?;

    let form = form.into_inner();
    let featured_by_user = form.feature_recommendation.filter(|o| o == "on").is_some();
    let description = String::from(form.content.trim());

    let recommendation = db_action(&data.pool, &persistent, move |conn| {
        actions::recommendations::modifications::update_recommendation_description(
            recommendation,
            description,
            &conn,
        )
    })
    .await?;
    db_action(&data.pool, &persistent, move |conn| {
        actions::recommendations::modifications::update_recommendation_featured_by_user(
            recommendation,
            featured_by_user,
            &conn,
        )
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((
            LOCATION,
            format!(
                "/{}/@{}/?confirm=recommendation_edit",
                RECOMMENDATION_URL, recommender.username,
            ),
        ))
        .finish())
}

#[post("/@{recommender_username}/@{author}/{story}/delete/")]
async fn handle_author_delete(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, String)>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let (recommender_username, author_username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login(login_user);
    let (recommender, _author, _story, recommendation) =
        db_action(&data.pool, &persistent, move |conn| {
            intertextual::data::recommendations::find_author_recommendation_by_url(
                recommender_username,
                author_username,
                url_fragment,
                &mode,
                &conn,
            )
        })
        .await?;

    login_user
        .require_recommendation_edit_rights_for(&recommender)
        .map_err_app(&persistent)?;

    db_action(&data.pool, &persistent, move |conn| {
        actions::recommendations::modifications::delete_recommendation(recommendation, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((
            LOCATION,
            format!(
                "/{}/@{}/?confirm=recommendation_delete",
                RECOMMENDATION_URL, recommender.username,
            ),
        ))
        .finish())
}

#[post("/@{recommender_username}/collaboration/{story}/delete/")]
async fn handle_collaboration_delete(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let (recommender_username, url_fragment) = path.into_inner();
    let mode = FilterMode::from_login(login_user);
    let (recommender, _authors, _story, recommendation) =
        db_action(&data.pool, &persistent, move |conn| {
            intertextual::data::recommendations::find_collaboration_recommendation_by_url(
                recommender_username,
                url_fragment,
                &mode,
                &conn,
            )
        })
        .await?;

    login_user
        .require_recommendation_edit_rights_for(&recommender)
        .map_err_app(&persistent)?;

    db_action(&data.pool, &persistent, move |conn| {
        actions::recommendations::modifications::delete_recommendation(recommendation, &conn)
    })
    .await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((
            LOCATION,
            format!(
                "/{}/@{}/?confirm=recommendation_delete",
                RECOMMENDATION_URL, recommender.username,
            ),
        ))
        .finish())
}
