use actix_web::{cookie::*, post, web, HttpRequest, HttpResponse};
use serde::Deserialize;

use intertextual::actions;
use intertextual::models::shared::AppState;
use intertextual::models::style::Style;

use crate::app::identity::Identity;
use crate::app::persistent::*;
use crate::error::*;
use crate::prelude::*;

#[derive(Deserialize)]
struct StyleFormExtraData {
    current_path: String,
}

#[post("/style/")]
async fn style_form(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    // Can't transparently unpack form data into a single struct with #[serde(flatten)] due to https://github.com/serde-rs/serde/issues/1183
    body: String,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;

    let style_prefs: Style = serde_urlencoded::from_str(&body).unwrap_or_default();

    let current_path: String = serde_urlencoded::from_str::<StyleFormExtraData>(&body)
        .map(|d| d.current_path)
        .unwrap_or_else(|_| String::from("/"));

    let mut resp = HttpResponse::SeeOther();
    resp.insert_header((actix_web::http::header::LOCATION, current_path));

    let user_id = persistent.login().map(|u| u.id);

    match user_id {
        Some(u) => {
            db_action(&data.pool, &persistent, move |conn| {
                actions::users::modifications::update_user_style(u, Some(style_prefs), &conn)
            })
            .await?;
        }
        None => {
            let cookie = Cookie::build("style", serde_json::to_string(&style_prefs).unwrap())
                .permanent()
                .secure(data.site.secure_cookies)
                .same_site(SameSite::Strict)
                .finish();
            resp.insert_header((actix_web::http::header::SET_COOKIE, cookie.to_string()));
        }
    }

    Ok(resp.finish())
}
