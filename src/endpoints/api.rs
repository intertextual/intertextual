use actix_web::{get, web};
use serde::{Deserialize, Serialize};

use intertextual::actions;
use intertextual::models::error::IntertextualError;
use intertextual::models::filter::FilterMode;
use intertextual::models::shared::AppState;

use crate::app::persistent::PersistentData;
use crate::error::*;
use crate::prelude::*;

const MAX_RESULTS: usize = 50;

#[derive(Serialize)]
struct TagResponseEntry {
    pub tag_name: String,
    pub tag_category: String,
    pub story_count: i64,
}

#[derive(Serialize)]
struct UserResponseEntry {
    pub username: String,
    pub display_name: String,
}

#[derive(Serialize)]
struct AuthorResponseEntry {
    pub username: String,
    pub display_name: String,
    pub story_count: i64,
}

#[derive(Deserialize)]
struct SearchQuery {
    q: Option<String>,
}

#[derive(Deserialize)]
struct UsernameAvailableQuery {
    username: Option<String>,
}

#[derive(Deserialize)]
struct StoryUrlAvailableQuery {
    username: Option<String>,
    is_collaboration: Option<String>,
    url_fragment: Option<String>,
}

#[get("/api/tags/")]
async fn api_tags(
    data: web::Data<AppState>,
    url_params: web::Query<SearchQuery>,
) -> Result<web::Json<Vec<TagResponseEntry>>, AppError> {
    let persistent = PersistentData::from_no_id(&data);
    let url_params = url_params.into_inner();
    let query = url_params.q.unwrap_or_default();
    let response = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<Vec<TagResponseEntry>, IntertextualError> {
            let possible_tags = actions::tags::search_from_query(query, &conn)?;
            let mut response: Vec<TagResponseEntry> =
                Vec::with_capacity(std::cmp::min(possible_tags.len(), MAX_RESULTS));
            for (category, tag, _) in possible_tags.into_iter().take(MAX_RESULTS) {
                let tag_name = tag.display_name;
                if response.iter().any(|v| v.tag_name == tag_name) {
                    continue;
                }
                let tag_category = category.display_name;
                let story_count = actions::tags::get_total_story_count(tag.id, &conn)?;
                if story_count == 0 && !tag.is_checkable {
                    // Do not propose non-checkable, zero-count tags.
                    continue;
                }
                response.push(TagResponseEntry {
                    tag_name,
                    tag_category,
                    story_count,
                })
            }

            Ok(response)
        },
    )
    .await?;

    Ok(web::Json(response))
}

#[get("/api/users/")]
async fn api_users(
    data: web::Data<AppState>,
    url_params: web::Query<SearchQuery>,
) -> Result<web::Json<Vec<UserResponseEntry>>, AppError> {
    let persistent = PersistentData::from_no_id(&data);
    let url_params = url_params.into_inner();
    let query = url_params.q.unwrap_or_default();
    let response = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<Vec<UserResponseEntry>, IntertextualError> {
            let possible_users = actions::users::search_from_query(query, &conn)?;
            let mut response: Vec<UserResponseEntry> =
                Vec::with_capacity(std::cmp::min(possible_users.len(), MAX_RESULTS));
            for author in possible_users.into_iter().take(MAX_RESULTS) {
                response.push(UserResponseEntry {
                    username: author.username,
                    display_name: author.display_name,
                })
            }

            Ok(response)
        },
    )
    .await?;

    Ok(web::Json(response))
}

#[get("/api/authors/")]
async fn api_authors(
    data: web::Data<AppState>,
    url_params: web::Query<SearchQuery>,
) -> Result<web::Json<Vec<AuthorResponseEntry>>, AppError> {
    let persistent = PersistentData::from_no_id(&data);
    let url_params = url_params.into_inner();
    let query = url_params.q.unwrap_or_default();
    let response = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<Vec<AuthorResponseEntry>, IntertextualError> {
            let possible_authors = actions::users::search_from_query(query, &conn)?;
            let mut response: Vec<AuthorResponseEntry> =
                Vec::with_capacity(std::cmp::min(possible_authors.len(), MAX_RESULTS));
            for author in possible_authors.into_iter().take(MAX_RESULTS) {
                let story_count = actions::stories::find_story_count_by_author(
                    author.id,
                    &FilterMode::Standard,
                    &conn,
                )?;
                if story_count == 0 {
                    continue;
                }
                response.push(AuthorResponseEntry {
                    username: author.username,
                    display_name: author.display_name,
                    story_count,
                })
            }

            Ok(response)
        },
    )
    .await?;

    Ok(web::Json(response))
}

#[get("/api/username_available/")]
async fn api_username_available(
    data: web::Data<AppState>,
    url_params: web::Query<UsernameAvailableQuery>,
) -> Result<web::Json<bool>, AppError> {
    let persistent = PersistentData::from_no_id(&data);
    let url_params = url_params.into_inner();
    let username = url_params.username.unwrap_or_default();
    let response = db_action(&data.pool, &persistent, move |conn| {
        actions::users::is_username_available(&username, &conn)
    })
    .await?;

    Ok(web::Json(response))
}

#[get("/api/story_url_available/")]
async fn api_story_url_available(
    data: web::Data<AppState>,
    url_params: web::Query<StoryUrlAvailableQuery>,
) -> Result<web::Json<bool>, AppError> {
    let persistent = PersistentData::from_no_id(&data);
    let url_params = url_params.into_inner();
    let is_collaboration = url_params.is_collaboration != None;
    let username = url_params.username.unwrap_or_default();
    let url_fragment = url_params.url_fragment.unwrap_or_default();
    let response = db_action(
        &data.pool,
        &persistent,
        move |conn| -> Result<bool, IntertextualError> {
            if is_collaboration {
                actions::stories::is_collaboration_story_url_available(&url_fragment, &conn)
            } else {
                let user = actions::users::find_user_by_username_for_login(&username, &conn)?;
                match user {
                    Some(user) => actions::stories::is_author_story_url_available(
                        user.id,
                        &url_fragment,
                        &conn,
                    ),
                    None => Ok(false),
                }
            }
        },
    )
    .await?;

    Ok(web::Json(response))
}
