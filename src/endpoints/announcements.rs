use actix_web::{get, web, HttpRequest, HttpResponse};
use askama::Template;

use intertextual::actions::announcements;

use crate::prelude::*;

#[derive(Template)]
#[template(path = "announcement.html")]
struct AnnouncementTemplate {
    persistent: PersistentTemplate,
    title: String,
    content: SanitizedHtml,
}

#[get("/announcements/{url_fragment}/")]
async fn main_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_request_cookie(&data, id, &request).await?;
    let login_user = persistent.login();

    let url_fragment = path.into_inner();

    let is_administrator = login_user.map(|l| l.administrator).unwrap_or(false);
    let announcement = db_action(&data.pool, &persistent, move |conn| {
        if is_administrator {
            announcements::admin::get_announcement_by_url(&url_fragment, &conn)
        } else {
            announcements::get_announcement_by_url(&url_fragment, &conn)
        }
    })
    .await?
    .ok_or(IntertextualError::PageDoesNotExist)
    .map_err_app(&persistent)?;

    let s = AnnouncementTemplate {
        persistent: PersistentTemplate::from(&persistent),
        title: announcement
            .title
            .unwrap_or_else(|| "Announcement".to_string()),
        content: announcement.content,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}
