use std::io::Write;
use std::str::FromStr;

use actix_multipart::Field;
use actix_web::http::header::ContentDisposition;
use futures::StreamExt;
use log::info;

use intertextual::models::error::IntertextualError;
use intertextual::models::formats::SanitizedHtml;
use intertextual::utils::htmlize::*;

use crate::app::blocking::blocking_action;
use crate::app::persistent::PersistentData;
use crate::app::persistent::PersistentTemplate;
use crate::error::*;

pub const CONTENT_SIZE_LIMIT: usize = 1024 * 1024; // Content fields : 1 MiB
pub const FIELD_SIZE_LIMIT: usize = 5 * 1024; // Normal, non-content fields (such as title, description, etc...) : 5 KiB

/// Process a multipart form field into a string.
///
/// # Arguments
///
/// * `field` - The multipart form field to process.
/// * `max_size_in_bytes` - The maximum size of the field accepted. If the field is longer, then this method will error out.
///
pub async fn process_utf8_field_into_string(
    persistent: &PersistentData,
    field: &mut Field,
    max_size_in_bytes: usize,
) -> Result<String, AppError> {
    let mut result = Vec::new();
    let mut current_size_in_bytes = 0;
    while let Some(chunk) = field.next().await {
        let data = chunk.map_err_app(persistent)?;
        current_size_in_bytes += data.len();
        if current_size_in_bytes > max_size_in_bytes {
            return Err(AppError::MultipartFormFieldTooLong {
                persistent: PersistentTemplate::from(persistent),
            });
        }
        result
            .write_all(&data)
            .map_err(IntertextualError::from)
            .map_err_app(persistent)?;
    }

    let result = String::from_utf8(result)
        .map_err(IntertextualError::from)
        .map_err_app(persistent)?;
    Ok(result)
}

pub async fn process_file_stream_into_sanitized_html(
    persistent: &PersistentData,
    field: &mut Field,
    content_disposition: ContentDisposition,
    max_size_in_bytes: usize,
) -> Result<SanitizedHtml, AppError> {
    let filename = content_disposition
        .get_filename()
        .ok_or_else(|| IntertextualError::FormFieldFormatError {
            form_field_name: "File",
            message: "The file had no associated filename".to_string(),
        })
        .map_err_app(persistent)?;

    if filename.is_empty() {
        return Ok(SanitizedHtml::new());
    }

    info!("Retrieving filename from form field");
    let base_filename = std::path::PathBuf::from_str(filename).unwrap();
    let file_extension = base_filename.extension().and_then(std::ffi::OsStr::to_str);

    let suffix = match file_extension {
        Some(ext)
            if !ext.contains(|c: char| !c.is_ascii_alphanumeric())
                && !ext.is_empty()
                && ext.len() <= 8 =>
        {
            ".".to_owned() + ext
        }
        _ => {
            return Err(IntertextualError::FormFieldFormatError {
                form_field_name: "File",
                message: "The file extension was invalid".to_string(),
            })
            .map_err_app(persistent);
        }
    };

    let mut file = blocking_action(persistent, move || {
        tempfile::Builder::new()
            .suffix(&suffix)
            .tempfile_in("./tmp/")
    })
    .await?;
    info!("Reading stream into temporary file {:?}", file.path());
    let mut current_size_in_bytes: usize = 0;
    while let Some(chunk) = field.next().await {
        let data = chunk.map_err_app(persistent)?;
        current_size_in_bytes += data.len();
        if current_size_in_bytes > max_size_in_bytes {
            return Err(AppError::MultipartFormFieldTooLong {
                persistent: PersistentTemplate::from(persistent),
            });
        }
        file = blocking_action(persistent, move || file.write_all(&data).map(|_| file)).await?;
    }

    blocking_action(persistent, move || {
        file_to_sanitized_html(file.into_temp_path())
    })
    .await
}

pub async fn process_utf8_field_into_sanitized_html(
    persistent: &PersistentData,
    field: &mut Field,
    max_size_in_bytes: usize,
) -> Result<SanitizedHtml, AppError> {
    let raw_content = process_utf8_field_into_string(persistent, field, max_size_in_bytes).await?;
    let sanitized_html = SanitizedHtml::from(&raw_content);
    let trimmed_html = remove_leading_whitespaces_from_paragraphs_in_html(sanitized_html);
    Ok(trimmed_html)
}
