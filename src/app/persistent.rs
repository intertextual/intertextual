use std::{convert::TryFrom, sync::Arc};

use actix_web::HttpRequest;

use intertextual::models::error::IntertextualError;
use intertextual::models::notifications::NotificationCategory;
use intertextual::models::shared::AppState;
use intertextual::models::shared::SharedSiteData;
use intertextual::models::style::*;
use intertextual::models::users::User;

use crate::app::identity::Identity;
use crate::app::identity::IdentityData;
use crate::error::*;
use crate::prelude::*;

#[derive(Debug)]
pub struct LoginUserData {
    pub user: User,
    pub unread_notifications_count: i64,
    pub has_unread_events: bool,
    pub marked_for_later: i64,
    pub admin_notifications: i64,
}

#[derive(Debug)]
pub struct PersistentData {
    site: Arc<SharedSiteData>,
    login: Option<LoginUserData>,
    style: Style,
    current_path: String,
}

#[derive(Debug, Clone)]
pub struct LoginUserTemplate {
    pub username: String,
    pub display_name: String,
    pub unread_notifications_count: i64,
    pub has_unread_events: bool,
    pub marked_for_later: i64,
    pub admin_notifications: i64,
}

#[derive(Debug, Clone)]
pub struct PersistentTemplate {
    pub site: Arc<SharedSiteData>,
    pub login: Option<LoginUserTemplate>,
    pub style: Style,
    pub current_path: String,
}

impl PersistentData {
    pub fn from_no_id(data: &AppState) -> PersistentData {
        PersistentData {
            site: data.site.clone(),
            login: None,
            style: Default::default(),
            current_path: String::from("/"),
        }
    }

    pub async fn from_request_cookie(
        data: &AppState,
        id: Identity,
        request: &HttpRequest,
    ) -> Result<PersistentData, AppError> {
        let login = identity_user(id, data).await?;

        let style: Style = match &login {
            Some(user) => user
                .style_prefs
                .as_ref()
                .and_then(|v| serde_json::from_value(v.clone()).ok())
                .unwrap_or_default(),
            None => request
                .cookie("style")
                .and_then(|c| serde_json::from_str(c.value()).ok())
                .unwrap_or_default(),
        };

        // Get the 'unread notifications count' and the 'has unread events' query, if needed
        let (unread_notifications_count, has_unread_events) = match &login {
            Some(user) => {
                // TODO : retrieve this from the database
                let user_id = user.id;
                let unread_notification_categories = db_action(
                    &data.pool,
                    &PersistentData::from_no_id(data),
                    move |conn| {
                        intertextual::actions::notifications::find_all_unread_notifications_by_user_id(
                            user_id, &conn,
                        )
                    },
                ).await?
                .into_iter()
                .map(|e| {
                    NotificationCategory::try_from(e.category)
                        .unwrap_or(NotificationCategory::Unknown)
                })
                .collect::<Vec<NotificationCategory>>();
                let notifications = unread_notification_categories
                    .iter()
                    .filter(|n| user.should_show_red_notify_for(**n))
                    .count();
                let has_events = unread_notification_categories
                    .iter()
                    .any(|n| user.should_show_event_for(*n));
                (notifications as i64, has_events)
            }
            None => (0i64, false),
        };
        // Get the 'marked for later' story count if needed
        let marked_for_later = match &login {
            Some(user) => {
                let user_id = user.id;
                db_action(
                    &data.pool,
                    &PersistentData::from_no_id(data),
                    move |conn| {
                        intertextual::actions::marked_for_later::total_marked_for_later_count_given_by_user(
                            user_id, &conn,
                        )
                    }
                ).await?
            }
            None => 0i64,
        };
        let admin_notifications = match &login {
            Some(user) if user.administrator => {
                db_action(
                    &data.pool,
                    &PersistentData::from_no_id(data),
                    |conn| -> Result<i64, IntertextualError> {
                        Ok(
                            intertextual::actions::modmail::admins::get_unhandled_modmail_count(
                                &conn,
                            )? + intertextual::actions::reports::admin::get_unhandled_report_count(
                                &conn,
                            )?,
                        )
                    },
                )
                .await?
            }
            _ => 0i64,
        };

        // Create the right persistent data
        Ok(PersistentData {
            site: data.site.clone(),
            login: login.map(|user| LoginUserData {
                user,
                unread_notifications_count,
                has_unread_events,
                marked_for_later,
                admin_notifications,
            }),
            style,
            current_path: request
                .uri()
                .path_and_query()
                .map(|p| p.as_str())
                .unwrap_or("/")
                .to_string(),
        })
    }

    pub fn login(&self) -> Option<&User> {
        self.login.as_ref().map(|l| &l.user)
    }

    pub fn style(&self) -> &Style {
        &self.style
    }
}

impl From<PersistentData> for PersistentTemplate {
    fn from(data: PersistentData) -> PersistentTemplate {
        PersistentTemplate {
            site: data.site,
            login: data.login.map(|l| LoginUserTemplate {
                username: l.user.username,
                display_name: l.user.display_name,
                has_unread_events: l.has_unread_events,
                unread_notifications_count: l.unread_notifications_count,
                marked_for_later: l.marked_for_later,
                admin_notifications: l.admin_notifications,
            }),
            style: data.style,
            current_path: data.current_path,
        }
    }
}

impl From<&PersistentData> for PersistentTemplate {
    fn from(data: &PersistentData) -> PersistentTemplate {
        PersistentTemplate {
            site: data.site.clone(),
            login: data.login.as_ref().map(|l| LoginUserTemplate {
                username: l.user.username.clone(),
                display_name: l.user.display_name.clone(),
                has_unread_events: l.has_unread_events,
                unread_notifications_count: l.unread_notifications_count,
                marked_for_later: l.marked_for_later,
                admin_notifications: l.admin_notifications,
            }),
            style: data.style.clone(),
            current_path: data.current_path.clone(),
        }
    }
}

impl PersistentTemplate {
    pub fn get_classes(&self) -> String {
        let mut classes = vec![];
        match self.style.color_scheme {
            ColorScheme::Light => classes.push("theme-light"),
            ColorScheme::Dark => classes.push("theme-dark"),
            ColorScheme::Green => classes.push("theme-green"),
            ColorScheme::BrowserDefault => {}
        };
        match self.style.font_family {
            FontFamily::Serif => {}
            FontFamily::SansSerif => classes.push("font-sans-serif"),
        };
        match self.style.text_alignment {
            TextAlignment::Left => {}
            TextAlignment::Justify => classes.push("text-justify"),
        };
        match self.style.content_width {
            ContentWidth::Wide => {}
            ContentWidth::Narrow => classes.push("content-narrow"),
        };
        match self.style.paragraph_layout {
            ParagraphLayout::Classic => {}
            ParagraphLayout::Modern => classes.push("paragraph-style-modern"),
        };
        format!(
            "class=\"{}\" style=\"font-size: {}px\"",
            classes.join(" "),
            self.style.font_size,
        )
    }
}

async fn identity_user(id: Identity, data: &AppState) -> Result<Option<User>, AppError> {
    match id.identity() {
        None => Ok(None),
        Some(IdentityData { user_id, token_id }) => {
            let user = db_action(&data.pool, &PersistentData::from_no_id(data), move |conn| {
                intertextual::actions::users::find_user_for_token_and_update_time_if_needed(
                    user_id, token_id, &conn,
                )
            })
            .await?;
            if user.is_none() {
                log::info!(
                    "Identity cookie contains invalid token pair (no match in database): {} / {}",
                    user_id,
                    token_id
                );
                id.forget();
            }
            Ok(user)
        }
    }
}
