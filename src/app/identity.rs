//! # Identity Service
//!
//! This code is based on the [`actix-identity`](https://github.com/actix/actix-extras/tree/master/actix-identity) crate.
//!
//! Handles the identity of a logged-in user via a cookie.

use std::cell::RefCell;
use std::future::Future;
use std::rc::Rc;
use std::task::{Context, Poll};

use actix_service::{Service, Transform};
use futures::future::{ok, FutureExt, LocalBoxFuture, Ready};
use serde::{Deserialize, Serialize};

use actix_web::cookie::{Cookie, CookieJar, Key, SameSite};
use actix_web::dev::{Extensions, Payload, ServiceRequest, ServiceResponse};
use actix_web::error::{Error, Result};
use actix_web::http::header::{self, HeaderValue};
use actix_web::{FromRequest, HttpMessage, HttpRequest};

#[derive(Clone)]
pub struct Identity(HttpRequest);

#[derive(Clone, Copy)]
pub enum IdentityRememberMode {
    Session,
    MaxAge { duration: core::time::Duration },
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct IdentityData {
    pub user_id: uuid::Uuid,
    pub token_id: uuid::Uuid,
}

impl Identity {
    /// Return the claimed identity of the user associated request or
    /// ``None`` if no identity can be found associated with the request.
    pub fn identity(&self) -> Option<IdentityData> {
        Identity::get_identity(&self.0.extensions())
    }

    /// Remember identity.
    pub fn remember(&self, identity: IdentityData, remember_mode: IdentityRememberMode) {
        if let Some(id) = self
            .0
            .extensions_mut()
            .get_mut::<IntertextualIdentityItem>()
        {
            id.id = Some(identity);
            id.remember_mode = remember_mode;
            id.changed = true;
        }
    }

    /// This method is used to 'forget' the current identity on subsequent
    /// requests.
    pub fn forget(&self) {
        if let Some(id) = self
            .0
            .extensions_mut()
            .get_mut::<IntertextualIdentityItem>()
        {
            id.id = None;
            id.changed = true;
        }
    }

    fn get_identity(extensions: &Extensions) -> Option<IdentityData> {
        if let Some(id) = extensions.get::<IntertextualIdentityItem>() {
            id.id.clone()
        } else {
            None
        }
    }
}

struct IntertextualIdentityItem {
    id: Option<IdentityData>,
    remember_mode: IdentityRememberMode,
    changed: bool,
}

pub trait RequestIntertextualIdentity {
    fn get_identity(&self) -> Option<IdentityData>;
}

impl<T> RequestIntertextualIdentity for T
where
    T: HttpMessage,
{
    fn get_identity(&self) -> Option<IdentityData> {
        Identity::get_identity(&self.extensions())
    }
}

impl FromRequest for Identity {
    type Error = Error;
    type Future = Ready<Result<Identity, Error>>;

    #[inline]
    fn from_request(req: &HttpRequest, _: &mut Payload) -> Self::Future {
        ok(Identity(req.clone()))
    }
}

pub trait IntertextualIdentityPolicy: Sized + 'static {
    type Future: Future<Output = Result<Option<IdentityData>, Error>>;
    type ResponseFuture: Future<Output = Result<(), Error>>;
    fn from_request(&self, request: &mut ServiceRequest) -> Self::Future;
    fn to_response(
        &self,
        identity: Option<IdentityData>,
        remember_mode: IdentityRememberMode,
        changed: bool,
        response: &mut ServiceResponse,
    ) -> Self::ResponseFuture;
}

pub struct IntertextualIdentityService<T> {
    backend: Rc<T>,
}

impl<T> IntertextualIdentityService<T> {
    /// Create new identity service with specified backend.
    pub fn new(backend: T) -> Self {
        IntertextualIdentityService {
            backend: Rc::new(backend),
        }
    }
}

impl<S, T> Transform<S, ServiceRequest> for IntertextualIdentityService<T>
where
    S: Service<ServiceRequest, Response = ServiceResponse, Error = Error> + 'static,
    S::Future: 'static,
    T: IntertextualIdentityPolicy,
{
    type Response = ServiceResponse;
    type Error = Error;
    type InitError = ();
    type Transform = IntertextualIdentityServiceMiddleware<S, T>;
    type Future = Ready<Result<Self::Transform, Self::InitError>>;

    fn new_transform(&self, service: S) -> Self::Future {
        ok(IntertextualIdentityServiceMiddleware {
            backend: self.backend.clone(),
            service: Rc::new(RefCell::new(service)),
        })
    }
}

#[doc(hidden)]
pub struct IntertextualIdentityServiceMiddleware<S, T> {
    backend: Rc<T>,
    service: Rc<RefCell<S>>,
}

impl<S, T> Clone for IntertextualIdentityServiceMiddleware<S, T> {
    fn clone(&self) -> Self {
        Self {
            backend: self.backend.clone(),
            service: self.service.clone(),
        }
    }
}

impl<S, T> Service<ServiceRequest> for IntertextualIdentityServiceMiddleware<S, T>
where
    S: Service<ServiceRequest, Response = ServiceResponse, Error = Error> + 'static,
    S::Future: 'static,
    T: IntertextualIdentityPolicy,
{
    type Response = ServiceResponse;
    type Error = Error;
    type Future = LocalBoxFuture<'static, Result<Self::Response, Self::Error>>;

    fn poll_ready(&self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.service.poll_ready(cx)
    }

    fn call(&self, mut req: ServiceRequest) -> Self::Future {
        let srv = self.service.clone();
        let backend = self.backend.clone();
        let fut = self.backend.from_request(&mut req);

        async move {
            match fut.await {
                Ok(id) => {
                    req.extensions_mut().insert(IntertextualIdentityItem {
                        id,
                        remember_mode: IdentityRememberMode::Session,
                        changed: false,
                    });

                    // https://github.com/actix/actix-web/issues/1263
                    let fut = srv.call(req);
                    let mut res = fut.await?;
                    let id = res
                        .request()
                        .extensions_mut()
                        .remove::<IntertextualIdentityItem>();

                    if let Some(id) = id {
                        match backend
                            .to_response(id.id, id.remember_mode, id.changed, &mut res)
                            .await
                        {
                            Ok(_) => Ok(res),
                            Err(e) => Ok(res.error_response(e)),
                        }
                    } else {
                        Ok(res)
                    }
                }
                Err(err) => Ok(req.error_response(err)),
            }
        }
        .boxed_local()
    }
}

struct IntertextualIdentityCookieInner {
    key_v2: Key,
    name: String,
    path: String,
    secure: bool,
    http_only: Option<bool>,
    same_site: Option<SameSite>,
}

#[derive(Deserialize, Serialize, Debug)]
struct IntertextualIdentityCookieValue {
    user_id: uuid::Uuid,
    token_id: uuid::Uuid,
}

impl IntertextualIdentityCookieInner {
    fn new(key: &[u8]) -> IntertextualIdentityCookieInner {
        let key_v2: Vec<u8> = key.iter().chain([1, 0, 0, 0].iter()).cloned().collect();
        IntertextualIdentityCookieInner {
            key_v2: Key::derive_from(&key_v2),
            name: "intertextual-identity".to_owned(),
            path: "/".to_owned(),
            secure: true,
            http_only: None,
            same_site: None,
        }
    }

    fn set_cookie(
        &self,
        resp: &mut ServiceResponse,
        value: Option<IntertextualIdentityCookieValue>,
        remember_mode: IdentityRememberMode,
    ) -> Result<()> {
        let add_cookie = value.is_some();
        let val = value.as_ref().map(serde_json::to_string);
        let mut cookie = Cookie::new(self.name.clone(), val.unwrap_or_else(|| Ok(String::new()))?);
        cookie.set_path(self.path.clone());
        cookie.set_secure(self.secure);
        cookie.set_http_only(true);

        match remember_mode {
            IdentityRememberMode::Session => cookie.set_max_age(None),
            IdentityRememberMode::MaxAge { duration } => {
                use std::convert::TryInto;
                cookie.set_max_age(duration.try_into().ok())
            }
        }

        if let Some(http_only) = self.http_only {
            cookie.set_http_only(http_only);
        }

        if let Some(same_site) = self.same_site {
            cookie.set_same_site(same_site);
        }

        let mut jar = CookieJar::new();
        if add_cookie {
            jar.private_mut(&self.key_v2).add(cookie);
        } else {
            jar.add_original(cookie.clone());
            jar.private_mut(&self.key_v2).remove(cookie);
        }
        for cookie in jar.delta() {
            let val = HeaderValue::from_str(&cookie.to_string())?;
            resp.headers_mut().append(header::SET_COOKIE, val);
        }
        Ok(())
    }

    fn load(&self, req: &ServiceRequest) -> Option<IntertextualIdentityCookieValue> {
        let cookie = req.cookie(&self.name)?;
        let mut jar = CookieJar::new();
        jar.add_original(cookie.clone());
        let contents = jar.private(&self.key_v2).get(&self.name)?;
        let cookie: IntertextualIdentityCookieValue =
            serde_json::from_str(contents.value()).ok()?;
        Some(cookie)
    }
}

pub struct IntertextualIdentityCookiePolicy(Rc<IntertextualIdentityCookieInner>);

impl IntertextualIdentityCookiePolicy {
    /// Construct new `IntertextualIdentityCookiePolicy` instance.
    ///
    /// Panics if key length is less than 32 bytes.
    pub fn new(key: &[u8]) -> IntertextualIdentityCookiePolicy {
        IntertextualIdentityCookiePolicy(Rc::new(IntertextualIdentityCookieInner::new(key)))
    }

    pub fn name<S: Into<String>>(mut self, value: S) -> IntertextualIdentityCookiePolicy {
        Rc::get_mut(&mut self.0).unwrap().name = value.into();
        self
    }

    pub fn secure(mut self, value: bool) -> IntertextualIdentityCookiePolicy {
        Rc::get_mut(&mut self.0).unwrap().secure = value;
        self
    }

    pub fn http_only(mut self, http_only: bool) -> Self {
        Rc::get_mut(&mut self.0).unwrap().http_only = Some(http_only);
        self
    }

    pub fn same_site(mut self, same_site: SameSite) -> Self {
        Rc::get_mut(&mut self.0).unwrap().same_site = Some(same_site);
        self
    }
}

impl IntertextualIdentityPolicy for IntertextualIdentityCookiePolicy {
    type Future = Ready<Result<Option<IdentityData>, Error>>;
    type ResponseFuture = Ready<Result<(), Error>>;

    fn from_request(&self, req: &mut ServiceRequest) -> Self::Future {
        ok(self
            .0
            .load(req)
            .map(
                |IntertextualIdentityCookieValue { user_id, token_id }| IdentityData {
                    user_id,
                    token_id,
                },
            ))
    }

    fn to_response(
        &self,
        identity: Option<IdentityData>,
        remember_mode: IdentityRememberMode,
        changed: bool,
        res: &mut ServiceResponse,
    ) -> Self::ResponseFuture {
        let _ = if changed {
            self.0.set_cookie(
                res,
                identity.map(|IdentityData { user_id, token_id }| {
                    IntertextualIdentityCookieValue { user_id, token_id }
                }),
                remember_mode,
            )
        } else {
            Ok(())
        };
        ok(())
    }
}
