use askama::Template;
use log::error;

use crate::app::persistent::PersistentTemplate;

#[derive(Template)]
#[template(path = "common/400_bad_request.html")]
struct BadRequestTemplate {
    persistent: PersistentTemplate,
    reason: bool,
    reason_text: String,
}

pub fn bad_request(
    persistent: PersistentTemplate,
    reason: Option<String>,
) -> actix_web::HttpResponse {
    let s = BadRequestTemplate {
        persistent: persistent.clone(),
        reason: reason.is_some(),
        reason_text: reason.unwrap_or_default(),
    }
    .render();
    match s {
        Ok(s) => actix_web::HttpResponse::BadRequest().body(s),
        Err(e) => {
            error!("400 reply : internal server error : {:?}", e);
            internal_server_error(persistent)
        }
    }
}

#[derive(Template)]
#[template(path = "common/403_not_accessible.html")]
struct NotAccessibleTemplate {
    persistent: PersistentTemplate,
    reason: bool,
    reason_text: String,
}

pub fn not_accessible(
    persistent: PersistentTemplate,
    reason: Option<String>,
) -> actix_web::HttpResponse {
    let s = NotAccessibleTemplate {
        persistent: persistent.clone(),
        reason: reason.is_some(),
        reason_text: reason.unwrap_or_default(),
    }
    .render();
    match s {
        Ok(s) => actix_web::HttpResponse::Forbidden().body(s),
        Err(e) => {
            error!("403 reply : internal server error : {:?}", e);
            internal_server_error(persistent)
        }
    }
}

#[derive(Template)]
#[template(path = "common/404_not_found.html")]
struct NotFoundTemplate {
    persistent: PersistentTemplate,
    reason: bool,
    reason_text: String,
}

pub fn not_found(
    persistent: PersistentTemplate,
    reason: Option<String>,
) -> actix_web::HttpResponse {
    let s = NotFoundTemplate {
        persistent: persistent.clone(),
        reason: reason.is_some(),
        reason_text: reason.unwrap_or_default(),
    }
    .render();
    match s {
        Ok(s) => actix_web::HttpResponse::NotFound().body(s),
        Err(e) => {
            error!("404 reply : internal server error : {:?}", e);
            internal_server_error(persistent)
        }
    }
}

#[derive(Template)]
#[template(path = "common/500_internal_server_error.html")]
struct InternalServerErrorTemplate {
    persistent: PersistentTemplate,
}

pub fn internal_server_error(persistent: PersistentTemplate) -> actix_web::HttpResponse {
    let s = InternalServerErrorTemplate { persistent }.render();
    match s {
        Ok(s) => actix_web::HttpResponse::InternalServerError().body(s),
        Err(e) => {
            error!("500 reply : internal server error : {:?}", e);
            actix_web::HttpResponse::InternalServerError().finish()
        }
    }
}
