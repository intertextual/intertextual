use actix_web::web;
use diesel::{
    r2d2::{ConnectionManager, PooledConnection},
    PgConnection,
};
use intertextual::{models::shared::DbPool, prelude::IntertextualError};

use crate::prelude::{AppError, IntoApp, MapApp, PersistentData};

pub async fn blocking_action<T, F, E>(persistent: &PersistentData, f: F) -> Result<T, AppError>
where
    T: Send + 'static,
    F: FnOnce() -> Result<T, E> + Send + 'static,
    E: IntoApp + std::fmt::Debug + Send + 'static,
{
    web::block(f)
        .await
        .map_err_app(persistent)?
        .map_err_app(persistent)
}

pub async fn db_action<T, F>(
    pool: &DbPool,
    persistent: &PersistentData,
    f: F,
) -> Result<T, AppError>
where
    T: Send + 'static,
    F: FnOnce(PooledConnection<ConnectionManager<PgConnection>>) -> Result<T, IntertextualError>
        + Send
        + 'static,
{
    let pool = pool.clone();
    blocking_action(persistent, move || {
        let conn = pool.get()?;
        f(conn)
    })
    .await
}
