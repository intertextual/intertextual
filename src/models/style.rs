use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
#[serde(rename_all = "snake_case")]
pub enum ColorScheme {
    Light,
    Dark,
    Green,
    BrowserDefault,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
#[serde(rename_all = "snake_case")]
pub enum FontFamily {
    Serif,
    SansSerif,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
#[serde(rename_all = "snake_case")]
pub enum TextAlignment {
    Left,
    Justify,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
#[serde(rename_all = "snake_case")]
pub enum ContentWidth {
    Wide,
    Narrow,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
#[serde(rename_all = "snake_case")]
pub enum ParagraphLayout {
    Classic,
    Modern,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
#[serde(default, rename_all = "snake_case")]
pub struct Style {
    pub color_scheme: ColorScheme,
    pub font_family: FontFamily,
    pub text_alignment: TextAlignment,
    pub content_width: ContentWidth,
    pub paragraph_layout: ParagraphLayout,
    pub font_size: u32,
}

impl Default for Style {
    fn default() -> Self {
        Style {
            color_scheme: ColorScheme::BrowserDefault,
            font_family: FontFamily::Serif,
            text_alignment: TextAlignment::Left,
            content_width: ContentWidth::Wide,
            paragraph_layout: ParagraphLayout::Classic,
            font_size: 18,
        }
    }
}
