//! # Models : Tags
//!
//! This module contains models related to story tags and tag associations

use num_enum::{IntoPrimitive, TryFromPrimitive};
use serde::{Deserialize, Serialize};

use crate::models::stories::Story;
use crate::prelude::*;
use crate::schema::*;

/// Represents how a given tag is linked to a story
#[derive(Clone, Copy, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(i32)]
pub enum TagAssociationType {
    /// Standard link : if the tag is "always visible", it will be always shown. If it isn't, it will be collapsed by default
    Standard,
    /// Major link : the tag is never collapsed
    Major,
    /// Spoiler link : the tag is collapsed under the explicit "spoilers" section
    Spoiler,
}

/// Represents a tag category stored in the database
#[derive(Identifiable, Queryable, Clone)]
#[table_name = "tag_categories"]
pub struct TagCategory {
    /// The unique identifier of this category.
    ///
    /// This should not be shown directly to the user, but might be used in form names, form values, or URLs.
    pub id: uuid::Uuid,

    /// The name of this category, as shown to the user. Must be unique amongst all canonical tags.
    pub display_name: String,

    /// Description of the category
    pub description: String,

    /// Index of the category. Used to display the categories in a fixed order.
    pub order_index: i32,

    ///Indicates whether tags from this category are always displayed as "major".
    ///
    ///Note : If a tag from an "is_always_displayed" category is marked as spoiler, then it will be
    ///hidden regardless of this value.
    pub is_always_displayed: bool,

    /// Whether users can add their own tags to this category.
    pub allow_user_defined_tags: bool,

    /// Whether checkable tags from this category should be shown in the "included" part of the advanced search
    pub quick_select_for_included_tag_search: bool,

    /// Whether checkable tags from this category should be shown in the "excluded" part of the advanced search
    pub quick_select_for_excluded_tag_search: bool,
}

/// Represents a canonical tag stored in the database
#[derive(Identifiable, Queryable, Associations, Clone)]
#[belongs_to(TagCategory, foreign_key = "category_id")]
#[table_name = "canonical_tags"]
pub struct CanonicalTag {
    /// The unique identifier of this tag.
    ///
    /// This should not be shown directly to the user, but might be used in form names, form values, or URLs.
    pub id: uuid::Uuid,

    /// The canonical name of this tag and associated tags, as shown to the user. Must be unique amongst all canonical tags.
    pub display_name: String,

    /// The category that this tag belongs to.
    pub category_id: uuid::Uuid,

    /// Whether this tag is checkable (i.e. will be displayed in a "checkable" section when selecting story tags)
    pub is_checkable: bool,

    /// The optional description for this tag. Provides additional information.
    ///
    /// It is recommended for checkable tags to have a description, in order for authors to be able to
    // easily distingush whether this tag applies to them.
    pub description: Option<String>,

    /// Whether this tag should be included in the tag popularity listing
    pub show_in_popularity_listing: bool,

    /// Whether this tag should block stories and chapters from being published.
    pub block_publication: bool,

    /// The optional block publication message for this tag. Provides additional information.
    ///
    /// It is recommended for blocked tags to have a descfription, in order for authors to be able to
    /// know why their stories cannot be published.
    pub block_publication_message: Option<RichBlock>,
}

/// Represents an internal tag stored in the database
#[derive(Queryable, Associations)]
#[belongs_to(CanonicalTag, foreign_key = "canonical_id")]
#[table_name = "internal_to_canonical_tag_associations"]
pub struct InternalTagAssociation {
    /// The raw internal ID for this tag. Must not be shown to the user.
    pub internal_id: uuid::Uuid,
    /// The canonical ID for this tag. This should be used ot get the user-facing text.
    pub canonical_id: uuid::Uuid,
    /// The name of the internal tag. This should generally not be displayed.
    ///
    /// When displaying the tags of a story, use the [CanonicalTag::display_name] field instead.
    ///
    /// The internal name can be displayed when editing a story's tags, or when showing the details
    /// of any specific canonical tag.
    pub internal_name: String,
    /// The number of stories this tag is associated to. This should be auto-updated as needed.
    pub tagged_stories: i32,
}

/// Represents a link between a story and an internal tag, stored in the database
#[derive(Serialize, Deserialize, Queryable, Insertable, Associations)]
#[belongs_to(Story, foreign_key = "story_id")]
#[belongs_to(InternalTagAssociation, foreign_key = "internal_tag_id")]
#[table_name = "story_to_internal_tag_associations"]
pub struct StoryToInternalTagAssociation {
    /// The story ID that this association points towards.
    pub story_id: uuid::Uuid,
    /// The internal tag that this association points towards.
    pub internal_tag_id: uuid::Uuid,
    /// Note : this should be casted to the [TagAssociationType] enum.
    pub tag_type: i32,
}

/// Represents a single tag highlight rule.
///
/// A tag highlight rule is made of a set of tags to match exactly and of data to use when the
/// match is fullfilled.
/// These rules are usually given as a vec, and are applied in order until one of them matches.
///
/// Note : this information is serialized as JSONb inside the database.
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct TagHighlightRule {
    /// The list of tags. All of these tags must match in order for the rule to match.
    pub tags: Vec<String>,
    /// The color to apply to the card title. If left empty, no color will be applied to the card.
    pub story_color: Option<TagHighlightRuleColor>,
    /// The text or symbol to show next to the card title. If left empty, no text or symbol will be shown in the card.
    pub story_symbol: Option<String>,
}

impl TagHighlightRule {
    /// Checks if the given rule matches a tag list.
    pub fn matches(&self, tag_list: &[&str]) -> bool {
        for tag in &self.tags {
            if !tag_list.contains(&tag.as_ref()) {
                return false;
            }
        }
        true
    }
}

/// Represents the raw components of a CSS color.
#[derive(Copy, Clone, Debug, Deserialize, Serialize)]
pub struct TagHighlightRuleColor {
    /// The red component, must be between 0 and 255
    pub r: i32,
    /// The green component, must be between 0 and 255
    pub g: i32,
    /// The blue component, must be between 0 and 255
    pub b: i32,
    /// The alpha component, must be between 0 and 1
    pub a: f32,
}

impl TagHighlightRuleColor {
    /// Converts a color into a CSS RGBA-ready color
    pub fn to_color(&self) -> String {
        format!("rgba({}, {}, {}, {})", self.r, self.g, self.b, self.a)
    }

    /// Parses a color from a raw string representation.
    ///
    /// Note: The only available string representation for now is
    /// `rgba(255, 255, 255, 1.0)` or equivalent.
    pub fn parse_from_str(value: &str) -> Result<Option<TagHighlightRuleColor>, IntertextualError> {
        let value = value.trim();
        if value.is_empty() {
            // The user entered an empty-equivalent string.
            return Ok(None);
        }
        let value =
            value
                .strip_prefix("rgba(")
                .ok_or_else(|| IntertextualError::FormFieldFormatError {
                    form_field_name: "Color",
                    message: "The supported color format is rgba(255, 255, 255, 1.0)".to_string(),
                })?;
        let value =
            value
                .strip_suffix(')')
                .ok_or_else(|| IntertextualError::FormFieldFormatError {
                    form_field_name: "Color",
                    message: "The supported color format is rgba(255, 255, 255, 1.0)".to_string(),
                })?;
        let chars: Vec<&str> = value.split(',').collect();
        match chars.as_slice() {
            [r_str, g_str, b_str, a_str] => {
                let r = r_str.trim().parse::<i32>().map_err(|e| {
                    IntertextualError::FormFieldFormatError {
                        form_field_name: "Color",
                        message: format!("Could not parse red component in {} : {}", value, e),
                    }
                })?;
                if !(0..=255).contains(&r) {
                    return Err(IntertextualError::FormFieldFormatError {
                        form_field_name: "Color",
                        message: format!(
                            "Could not parse red component in {} : value must be between 0 and 255",
                            value
                        ),
                    });
                }
                let g = g_str.trim().parse::<i32>().map_err(|e| {
                    IntertextualError::FormFieldFormatError {
                        form_field_name: "Color",
                        message: format!("Could not parse green component in {} : {}", value, e),
                    }
                })?;
                if !(0..=255).contains(&g) {
                    return Err(IntertextualError::FormFieldFormatError {
                        form_field_name: "Color",
                        message: format!(
                            "Could not parse green component in {} : value must be between 0 and 255",
                            value
                        ),
                    });
                }
                let b = b_str.trim().parse::<i32>().map_err(|e| {
                    IntertextualError::FormFieldFormatError {
                        form_field_name: "Color",
                        message: format!("Could not parse blue component in {} : {}", value, e),
                    }
                })?;
                if !(0..=255).contains(&b) {
                    return Err(IntertextualError::FormFieldFormatError {
                        form_field_name: "Color",
                        message: format!(
                            "Could not parse blue component in {} : value must be between 0 and 255",
                            value
                        ),
                    });
                }
                let a = a_str.trim().parse::<f32>().map_err(|e| {
                    IntertextualError::FormFieldFormatError {
                        form_field_name: "Color",
                        message: format!("Could not parse alpha component in {} : {}", value, e),
                    }
                })?;
                if !(0.0f32..=1.0f32).contains(&a) {
                    return Err(IntertextualError::FormFieldFormatError {
                        form_field_name: "Color",
                        message: format!(
                            "Could not parse alpha component in {} : value must be between 0.0 and 1.0",
                            value
                        ),
                    });
                }
                Ok(Some(TagHighlightRuleColor { r, g, b, a }))
            }
            _ => Err(IntertextualError::FormFieldFormatError {
                form_field_name: "Color",
                message: "The supported color format is rgba(255, 255, 255, 1.0)".to_string(),
            }),
        }
    }
}
