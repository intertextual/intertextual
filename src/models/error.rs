//! # Models : Error
//!
//! This module contains the error model used by this library

use thiserror::Error;

/// Represents an error in the library
#[derive(Error, Debug)]
pub enum IntertextualError {
    /// This error was caused by a communication issue with the database on the [`diesel`] level
    #[error(transparent)]
    DBError(#[from] diesel::result::Error),

    /// This error was caused by a communication issue with the database pool on the [`r2d2`] level
    #[error(transparent)]
    DBPoolError(#[from] diesel::r2d2::PoolError),

    /// This error was caused by formatting issues of an UTF-8 string
    #[error(transparent)]
    FromUtf8Error(#[from] std::string::FromUtf8Error),

    /// This error was caused by a generic IO error
    #[error(transparent)]
    IOError(#[from] std::io::Error),

    /// This error was caused by an error in the [`pandoc`] converter
    #[error(transparent)]
    PandocError(#[from] pandoc::PandocError),

    /// An user tried to retrieve an unknown username
    #[error("Could not find user")]
    UserNotFound {
        /// The username that could not be retrieved
        username: String,
    },

    /// An user tried to retrieve an unknown username or an unknown story fragment
    #[error("Could not find story")]
    AuthorStoryNotFound {
        /// The username that could not be retrieved
        author_username: String,
        /// The story url fragment that could not be retrieved
        url_fragment: String,
    },

    /// An user tried to retrieve an unknown story fragment for a collaboration story
    #[error("Could not find collaborative story")]
    CollaborationStoryNotFound {
        /// The story url fragment that could not be retrieved
        url_fragment: String,
    },

    /// An user tried to retrieve an unknown username, story fragment or chapter number
    #[error("Could not find chapter")]
    AuthorChapterNotFound {
        /// The username that could not be retrieved
        author_username: String,
        /// The story url fragment that could not be retrieved
        url_fragment: String,
        /// The chapter index that could not be retrieved
        chapter_number: i32,
    },

    /// An user tried to retrieve an unknown story fragment or chapter number for a collaborative story
    #[error("Could not find chapter")]
    CollaborationChapterNotFound {
        /// The story url fragment that could not be retrieved
        url_fragment: String,
        /// The chapter index that could not be retrieved
        chapter_number: i32,
    },

    /// An user tried to retrieve an unknown recommender, username, or story fragment for a recommendation
    #[error("Could not find recommendation")]
    AuthorRecommendationNotFound {
        /// The recommender username that could not be retrieved
        recommender_username: String,
        /// The username that could not be retrieved
        author_username: String,
        /// The story url fragment that could not be retrieved
        url_fragment: String,
    },

    /// An user tried to retrieve an unknown recommender or story fragment for a recommendation on a collaborative story
    #[error("Could not find recommendation")]
    CollaborationRecommendationNotFound {
        /// The recommender username that could not be retrieved
        recommender_username: String,
        /// The story url fragment that could not be retrieved
        url_fragment: String,
    },

    /// An user tried to retrieve an unknown comment ID
    #[error("Could not find comment")]
    CommentNotFound {
        /// The unknown comment ID
        comment_id: uuid::Uuid,
    },

    /// An user tried to retrieve an unknown notification ID
    #[error("Could not find notification")]
    NotificationNotFound {
        /// The unknown notification ID
        notification_id: uuid::Uuid,
    },

    /// An user tried to retrieve an unknown modmail ID
    #[error("Could not find moderation message")]
    ModmailNotFound {
        /// The unknown modmail ID
        modmail_id: uuid::Uuid,
    },

    /// An user tried to send a form field that did not respect the expected format
    #[error("A form field was not in the expected format")]
    FormFieldFormatError {
        /// Name of the field that didn't respect the format
        form_field_name: &'static str,
        /// Reason why the format was not respected
        message: String,
    },

    /// An user tried to access a page without logging in
    #[error("Log in is required for this page")]
    LoginRequired,

    /// An user tried to access a page without the required priviledges
    #[error("Higher access rights are required for this page")]
    AccessRightsRequired,

    /// An user tried to access a page without logging in, but we don't want to tell them (see [`IntertextualError::PageDoesNotExist`])
    #[error("Log in is required for this page, but it will be displayed as 404")]
    LoginRequiredObfuscated,

    /// An user tried to access a page without the required priviledges, but we don't want to tell them (see [`IntertextualError::PageDoesNotExist`])
    #[error("Higher access rights for this page, but it will be displayed as 404")]
    AccessRightsRequiredObfuscated,

    /// An user tried to log in without the right information
    #[error("The provided password was invalid")]
    InvalidPassword,

    /// An user tried to access a page that does not exist
    #[error("This page does not exist")]
    PageDoesNotExist,

    /// An action was not handled properly by the server
    #[error("Generic internal error")]
    InternalServerError,
}
