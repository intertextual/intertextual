//! # Models : Shared Date
//!
//! This module contains models related to the website data shared between the requests

use std::sync::Arc;

use diesel::prelude::*;
use diesel::r2d2::{ConnectionManager, Pool};

/// Represents the connection pool (shorthand)
pub type DbPool = Pool<ConnectionManager<PgConnection>>;

/// Represents the website data shared between the requests
#[derive(Debug)]
pub struct SharedSiteData {
    /// Whether the website uses the age verification mechanism
    pub is_adult: bool,
    /// Whether cookies should have the Secure attribute set
    pub secure_cookies: bool,
    /// The list of files in the theme folder should be made available under the `/theme` endpoint.
    pub allowed_theme_files: Vec<String>,
    /// The base url of the website
    pub base_url: String,
    /// The name of the website, displayed in the title and on the front page
    pub name: String,
    /// The description of the website, used in meta tags
    pub description: String,
    /// The number of days before accounts are deleted on the website
    pub account_deletion_request_duration_days: u16,
    /// The tags of the website, used in meta tags
    pub tags: Vec<String>,
    /// The default tag highlight rules to apply on the whole website for non logged-in users
    /// and logged-in users without custom rules.
    pub default_tag_highlight_rules: Arc<Vec<crate::models::tags::TagHighlightRule>>,
    /// The default colors to show in the tag highlight color picker.
    pub default_colors: Arc<Vec<DefaultColor>>,
    /// Query string to force cache reloading
    pub cachebuster: String,
    /// The tags to hide on the front page
    pub hidden_canonical_tags_on_front_page: Arc<Vec<uuid::Uuid>>,
}

/// Represents a default color
#[derive(Debug)]
pub struct DefaultColor {
    /// The color name, as shown to the user
    pub name: &'static str,
    /// The color's RGBA value. Must exactly match the format `rgba(255, 255, 255, 1.0)`
    pub rgba: &'static str,
}

/// Represents the application state, used in a single request
pub struct AppState {
    /// The shared website data
    pub site: Arc<SharedSiteData>,
    /// The shared pool information
    pub pool: DbPool,
    /// The publication date
    pub default_publication_date: crate::utils::publication::PublicationDate,
    /// The secret key used in password encryption
    pub password_secret_key: String,
}
