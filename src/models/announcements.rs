//! # Models : Announcements
//!
//! This module contains models related to the frontpage announcement and the overall announcements

use chrono::NaiveDateTime;
use std::cmp::{Eq, PartialEq};

use crate::{prelude::SanitizedHtml, schema::*};

/// Represents a comment before its insertion in the database
#[derive(Insertable)]
#[table_name = "frontpage_announcement"]
pub struct NewFrontpageAnnouncement {
    /// Timestamp indicating whether the announcement should be shown publicly.
    ///
    /// If this timestamp is set to [None], then the announcement MUST NOT be shown to a user other than moderators.
    /// If this timestamp is set to a date in the future, then the announcement MUST NOT be shown to a user other than moderators.
    ///
    /// Note : All timestamps stored in the database are UTC
    pub show_publicly_after_date: Option<NaiveDateTime>,

    /// Timestamp indicating whether the announcement should be shown publicly.
    ///
    /// If this timestamp is set to [None], then the announcement MUST NOT be shown to a user other than moderators.
    /// If this timestamp is set to a date in the future, then the announcement MUST NOT be shown to a user other than moderators.
    ///
    /// Note : All timestamps stored in the database are UTC
    pub remove_after_date: Option<NaiveDateTime>,

    /// Content of the announcement, formatted as trusted HTML.
    pub content: SanitizedHtml,
}

/// Represents a single comment
#[derive(Debug, Identifiable, Queryable, Associations, PartialEq, Eq)]
#[table_name = "frontpage_announcement"]
pub struct FrontpageAnnouncement {
    /// The internal ID of the frontpage announcement
    pub id: uuid::Uuid,

    /// This is only used for internal bookkeeping. Timestamp indicating when the announcement was internally created by the administratos.
    ///
    /// Note : All timestamps stored in the database are UTC
    pub announcement_internally_created_at: chrono::NaiveDateTime,

    /// Timestamp indicating whether the announcement should be shown publicly.
    ///
    /// If this timestamp is set to [None], then the announcement MUST NOT be shown to a user other than moderators.
    /// If this timestamp is set to a date in the future, then the announcement MUST NOT be shown to a user other than moderators.
    ///
    /// Note : All timestamps stored in the database are UTC
    pub show_publicly_after_date: Option<NaiveDateTime>,

    /// Timestamp indicating whether the announcement should be shown publicly.
    ///
    /// If this timestamp is set to [None], then the announcement MUST NOT be shown to a user other than moderators.
    /// If this timestamp is set to a date in the future, then the announcement MUST NOT be shown to a user other than moderators.
    ///
    /// Note : All timestamps stored in the database are UTC
    pub remove_after_date: Option<NaiveDateTime>,

    /// Content of the announcement, formatted as trusted HTML.
    pub content: SanitizedHtml,
}

/// Represents a comment before its insertion in the database
#[derive(Insertable)]
#[table_name = "announcements"]
pub struct NewAnnouncement {
    /// The URL fragment of the announcement
    pub url_fragment: String,

    /// The ID of the chapter this comment is linked to
    pub title: Option<String>,

    /// Timestamp indicating whether the announcement should be shown publicly.
    ///
    /// If this timestamp is set to [None], then the announcement MUST NOT be shown to a user other than moderators.
    /// If this timestamp is set to a date in the future, then the announcement MUST NOT be shown to a user other than moderators.
    ///
    /// Note : All timestamps stored in the database are UTC
    pub show_publicly_after_date: Option<NaiveDateTime>,

    /// Content of the announcement, formatted as trusted HTML.
    pub content: SanitizedHtml,
}

/// Represents an announcement
#[derive(Debug, Identifiable, Queryable, Associations, PartialEq, Eq)]
#[table_name = "announcements"]
pub struct Announcement {
    /// The internal ID of the announcement
    pub id: uuid::Uuid,

    /// The URL fragment of the announcement
    pub url_fragment: String,

    /// The ID of the chapter this comment is linked to
    pub title: Option<String>,

    /// This is only used for internal bookkeeping. Timestamp indicating when the announcement was internally created by the administratos.
    ///
    /// Note : All timestamps stored in the database are UTC
    pub announcement_internally_created_at: chrono::NaiveDateTime,

    /// Timestamp indicating whether the announcement should be shown publicly.
    ///
    /// If this timestamp is set to [None], then the announcement MUST NOT be shown to a user other than moderators.
    /// If this timestamp is set to a date in the future, then the announcement MUST NOT be shown to a user other than moderators.
    ///
    /// Note : All timestamps stored in the database are UTC
    pub show_publicly_after_date: Option<NaiveDateTime>,

    /// Content of the announcement, formatted as trusted HTML.
    pub content: SanitizedHtml,
}
