//! # Intertextual

#[macro_use]
extern crate diesel;

#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate maplit;

pub mod actions;
pub mod constants;
pub mod data;
pub mod models;
pub mod utils;

/// Database stuff
pub mod schema;

/// Contains all re-exported classes that are often used by other modules
pub mod prelude {
    pub use crate::models::error::IntertextualError;
    pub use crate::models::filter::FilterMode;
    pub use crate::models::formats::*;
    pub use crate::models::shared::AppState;
    pub use crate::models::shared::DbPool;
}
