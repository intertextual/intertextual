//! # Follows
//!
//! This module contains methods related to following stories and authors, or to notify the user when
//! a followed story or author has changed
use diesel::prelude::*;

use crate::models::error::IntertextualError;
use crate::schema::*;

/// Checks whether the user `user_id` is following the stories of the author `author_id`
pub fn is_following_author(
    user_id: uuid::Uuid,
    author_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<bool, IntertextualError> {
    let follow_result = user_author_follows::table
        .filter(user_author_follows::user_id.eq(user_id))
        .filter(user_author_follows::author_id.eq(author_id))
        .select(user_author_follows::user_id)
        .first::<uuid::Uuid>(conn)
        .optional()?;
    Ok(follow_result.is_some())
}

/// Checks whether the user `user_id` is following the recommendations of the author `author_id`
pub fn is_following_author_recommendations(
    user_id: uuid::Uuid,
    author_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<bool, IntertextualError> {
    let follow_result = user_author_recommendation_follows::table
        .filter(user_author_recommendation_follows::user_id.eq(user_id))
        .filter(user_author_recommendation_follows::author_id.eq(author_id))
        .select(user_author_recommendation_follows::user_id)
        .first::<uuid::Uuid>(conn)
        .optional()?;
    Ok(follow_result.is_some())
}

/// Checks whether the user `user_id` is following the story `story_id` specifically (not by following an author)
pub fn is_following_story(
    user_id: uuid::Uuid,
    story_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<bool, IntertextualError> {
    let follow_result = user_story_follows::table
        .filter(user_story_follows::user_id.eq(user_id))
        .filter(user_story_follows::story_id.eq(story_id))
        .select(user_story_follows::user_id)
        .first::<uuid::Uuid>(conn)
        .optional()?;
    Ok(follow_result.is_some())
}

/// This section contains commands that should only be accessed by the user themselves
pub mod private {
    use diesel::prelude::*;

    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::schema::*;

    /// Gets the list of all authors followed by the user `user_id`, as well as the follow date
    pub fn get_user_author_follows(
        user_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<Vec<(models::users::User, chrono::NaiveDateTime)>, IntertextualError> {
        let follow_result = user_author_follows::table
            .filter(user_author_follows::user_id.eq(user_id))
            .inner_join(users::table.on(user_author_follows::author_id.eq(users::id)))
            .select((users::all_columns, user_author_follows::follow_date))
            .load::<(models::users::User, chrono::NaiveDateTime)>(conn)?;
        Ok(follow_result)
    }

    /// Gets the list of all stories followed by the user `user_id`, as well as the follow date
    pub fn get_user_story_follows(
        user_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<Vec<(models::stories::Story, chrono::NaiveDateTime)>, IntertextualError> {
        let follow_result = user_story_follows::table
            .filter(user_story_follows::user_id.eq(user_id))
            .inner_join(stories::table)
            .select((stories::all_columns, user_story_follows::follow_date))
            .load::<(models::stories::Story, chrono::NaiveDateTime)>(conn)?;
        Ok(follow_result)
    }

    /// Gets the list of all authors whose recommendations are followed by the user `user_id`, as well as the follow date
    pub fn get_user_recommender_follows(
        user_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<Vec<(models::users::User, chrono::NaiveDateTime)>, IntertextualError> {
        let follow_result = user_author_recommendation_follows::table
            .filter(user_author_recommendation_follows::user_id.eq(user_id))
            .inner_join(
                users::table.on(user_author_recommendation_follows::author_id.eq(users::id)),
            )
            .select((
                users::all_columns,
                user_author_recommendation_follows::follow_date,
            ))
            .load::<(models::users::User, chrono::NaiveDateTime)>(conn)?;
        Ok(follow_result)
    }
}

/// This section contains commands that should only be accessed by an author checking their statistics
pub mod author_private {
    use diesel::prelude::*;

    use crate::models::error::IntertextualError;
    use crate::models::stories::CreativeRole;
    use crate::schema::*;

    /// Gets the number of followers of the author `author_id`
    pub fn get_author_follow_count(
        author_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<i64, IntertextualError> {
        let follow_result = user_author_follows::table
            .filter(user_author_follows::author_id.eq(author_id))
            .count()
            .get_result::<i64>(conn)?;
        Ok(follow_result)
    }

    /// Gets the number of followers who follow the recommendations of the author `author_id`
    pub fn get_author_recommender_follow_count(
        author_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<i64, IntertextualError> {
        let follow_result = user_author_recommendation_follows::table
            .filter(user_author_recommendation_follows::author_id.eq(author_id))
            .count()
            .get_result::<i64>(conn)?;
        Ok(follow_result)
    }

    /// Gets the number of followers who follow the story `story_id` or an author of this story
    pub fn get_combined_follow_count_by_story(
        story_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<i64, IntertextualError> {
        conn.transaction::<i64, IntertextualError, _>(|| {
            let author_ids = user_to_story_associations::table
                .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
                .filter(user_to_story_associations::story_id.eq(story_id))
                .select(user_to_story_associations::user_id)
                .load::<uuid::Uuid>(conn)?;
            let author_follow_result = user_author_follows::table
                .filter(user_author_follows::author_id.eq_any(&author_ids))
                .select(user_author_follows::user_id)
                .distinct()
                .count()
                .get_result::<i64>(conn)?;
            let follow_result = user_story_follows::table
                .filter(user_story_follows::story_id.eq(story_id))
                .count()
                .get_result::<i64>(conn)?;
            let cross_follow_result = user_author_follows::table
                .inner_join(
                    user_story_follows::table
                        .on(user_story_follows::user_id.eq(user_author_follows::user_id)),
                )
                .filter(user_story_follows::story_id.eq(story_id))
                .filter(user_author_follows::author_id.eq_any(&author_ids))
                .select(user_author_follows::user_id)
                .distinct()
                .count()
                .get_result::<i64>(conn)?;
            Ok(author_follow_result + follow_result - cross_follow_result)
        })
    }

    /// Gets the number of followers who follow the story `story_id` specifically (without following one of the authors)
    pub fn get_unique_follow_count_by_story(
        story_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<i64, IntertextualError> {
        conn.transaction::<i64, IntertextualError, _>(|| {
            let author_ids = user_to_story_associations::table
                .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
                .filter(user_to_story_associations::story_id.eq(story_id))
                .select(user_to_story_associations::user_id)
                .load::<uuid::Uuid>(conn)?;
            let follow_result = user_story_follows::table
                .filter(user_story_follows::story_id.eq(story_id))
                .count()
                .get_result::<i64>(conn)?;
            let cross_follow_result = user_author_follows::table
                .filter(user_author_follows::author_id.eq(story_id))
                .inner_join(
                    user_story_follows::table
                        .on(user_story_follows::user_id.eq(user_author_follows::user_id)),
                )
                .filter(user_author_follows::author_id.eq_any(&author_ids))
                .select(user_author_follows::user_id)
                .distinct()
                .count()
                .get_result::<i64>(conn)?;
            Ok(follow_result - cross_follow_result)
        })
    }
}

/// This module contains methond related to modifying the follow status of a story, e.g. following or unfollowing something.
pub mod modifications {
    use diesel::prelude::*;

    use crate::constants::RECOMMENDATION_URL;
    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::prelude::*;
    use crate::schema::*;

    type FollowType = (uuid::Uuid, uuid::Uuid, chrono::NaiveDateTime);

    /// Sets the user `user_id` as folloiwng the author `author_id`
    ///
    /// Note : This will automatically generate pending notifications for all stories currently planned for publication
    pub fn follow_author(
        user_id: uuid::Uuid,
        author_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<bool, IntertextualError> {
        conn.transaction::<bool, IntertextualError, _>(|| {
            let follow_result = diesel::insert_into(user_author_follows::table)
                .values((
                    user_author_follows::user_id.eq(user_id),
                    user_author_follows::author_id.eq(author_id),
                ))
                .get_result::<FollowType>(conn)
                .optional()?;

            // Generate the notifications for any planned chapter from this author.
            let stories = crate::actions::stories::find_all_stories_by_author(
                author_id,
                &FilterMode::BypassFilters,
                conn,
            )?;
            for story in stories {
                let author_list = crate::actions::stories::find_authors_by_story(
                    story.id,
                    &FilterMode::BypassFilters,
                    conn,
                )?;
                let chapters = chapters::table
                    .filter(chapters::story_id.eq(story.id))
                    .load::<models::stories::Chapter>(conn)?;
                for chapter in chapters {
                    match &chapter.show_publicly_after_date {
                        Some(first_publication_date)
                            if *first_publication_date > chrono::Utc::now().naive_utc() =>
                        {
                            let new_notification =
                                super::notifications::helper_new_chapter_notification(
                                    *first_publication_date,
                                    user_id,
                                    &author_list,
                                    &story,
                                    &chapter,
                                );
                            let _ = crate::actions::notifications::modifications::add_notification(
                                new_notification,
                                conn,
                            );
                        }
                        _ => {}
                    }
                }
            }

            // Notify the author that they have been followed
            let new_notification = models::notifications::NewNotification {
                target_user_id: author_id,
                notification_time: chrono::Utc::now().naive_utc(),
                internal_description: format!("u{}a{}follow", user_id, author_id),
                category: models::notifications::NotificationCategory::UserFollowedYou.into(),
                message: "A user followed you !".to_string(),
                link: "/notifications/".to_string(),
            };
            let _ = crate::actions::notifications::modifications::add_notification(
                new_notification,
                conn,
            );

            Ok(follow_result.is_some())
        })
    }

    /// Sets the user `user_id` as folloiwng the recommendations of the author `author_id`
    pub fn follow_author_recommendations(
        user_id: uuid::Uuid,
        author_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<bool, IntertextualError> {
        conn.transaction::<bool, IntertextualError, _>(|| {
            let follow_result = diesel::insert_into(user_author_recommendation_follows::table)
                .values((
                    user_author_recommendation_follows::user_id.eq(user_id),
                    user_author_recommendation_follows::author_id.eq(author_id),
                ))
                .get_result::<FollowType>(conn)
                .optional()?;

            // Notify the author that they have been followed
            let author: models::users::User = users::table.find(author_id).first(conn)?;
            let new_notification = models::notifications::NewNotification {
                target_user_id: author_id,
                notification_time: chrono::Utc::now().naive_utc(),
                internal_description: format!("u{}a{}recfollow", user_id, author_id),
                category:
                    models::notifications::NotificationCategory::UserFollowedYourRecommendations
                        .into(),
                message: "A user followed your story suggestions !".to_string(),
                link: format!("/{}/@{}/", RECOMMENDATION_URL, author.username),
            };
            let _ = crate::actions::notifications::modifications::add_notification(
                new_notification,
                conn,
            );

            Ok(follow_result.is_some())
        })
    }

    /// Sets the user `user_id` as unfollowing the author `author_id` but following the story `story_id`
    ///
    /// Note : This will automatically remove pending notifications for all stories currently planned for publication that aren't followed anymore
    pub fn unfollow_author_if_needed_and_follow_story(
        user_id: uuid::Uuid,
        story_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<bool, IntertextualError> {
        conn.transaction::<bool, IntertextualError, _>(|| {
            // Follow the story
            let follow_result = diesel::insert_into(user_story_follows::table)
                .values((
                    user_story_follows::user_id.eq(user_id),
                    user_story_follows::story_id.eq(story_id),
                ))
                .get_result::<FollowType>(conn)
                .optional()?;

            let story: models::stories::Story = stories::table.find(story_id).first(conn)?;
            let author_list = crate::actions::stories::find_authors_by_story(story.id, &FilterMode::BypassFilters, conn)?;
            for author in &author_list {
                // Unfollow the author if needed
                diesel::delete(
                    user_author_follows::table
                        .filter(user_author_follows::user_id.eq(user_id))
                        .filter(user_author_follows::author_id.eq(author.id)),
                )
                .get_result::<FollowType>(conn)
                .optional()?;

                // Remove the "a user followed you" notification
                let internal_description = format!("u{}a{}follow", user_id, author.id);
                let _ = crate::actions::notifications::modifications::clean_unread_notifications_with_internal_description_for_user(&internal_description, author.id, conn);

                // Notify the author that their story has been followed
                let new_notification = models::notifications::NewNotification {
                    target_user_id: author.id,
                    notification_time: chrono::Utc::now().naive_utc(),
                    internal_description: format!("u{}a{}s{}follow", user_id, author.id, story_id),
                    category: models::notifications::NotificationCategory::UserFollowedYourStory.into(),
                    message: format!("A user followed your story {} !", story.title),
                    link: format!("/@{}/", author.username),
                };
                let _ = crate::actions::notifications::modifications::add_notification(
                    new_notification,
                    conn,
                );
            }

            // Generate the notifications for any planned chapter from this story.
            let chapters = chapters::table
                .filter(chapters::story_id.eq(story.id))
                .load::<models::stories::Chapter>(conn)?;
            for chapter in chapters {
                match &chapter.show_publicly_after_date {
                    Some(first_publication_date)
                        if *first_publication_date > chrono::Utc::now().naive_utc() =>
                    {
                        let new_notification = super::notifications::helper_new_chapter_notification(
                            *first_publication_date,
                            user_id,
                            &author_list,
                            &story,
                            &chapter,
                        );
                        let _ = crate::actions::notifications::modifications::add_notification(
                            new_notification,
                            conn,
                        );
                    }
                    _ => {}
                }
            }

            Ok(follow_result.is_some())
        })
    }

    /// Sets the user `user_id` as unfollowing the author `author_id`
    ///
    /// Note : This will automatically remove pending notifications for all stories currently planned for publication that aren't followed anymore
    pub fn unfollow_author(
        user_id: uuid::Uuid,
        author_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<bool, IntertextualError> {
        conn.transaction::<bool, IntertextualError, _>(|| {
            let follow_result = diesel::delete(
                user_author_follows::table
                    .filter(user_author_follows::user_id.eq(user_id))
                    .filter(user_author_follows::author_id.eq(author_id)),
            )
            .get_result::<FollowType>(conn)
            .optional()?;

            let stories = crate::actions::stories::find_all_stories_by_author(
                author_id,
                &crate::models::filter::FilterMode::BypassFilters,
                conn,
            )?;

            for story in stories {
                let story_id = story.id;
                // If we don't follow the story explicitely, remove all notifications for this story
                let story_is_followed = super::is_following_story(user_id, story_id, conn)?;
                if !story_is_followed {
                    let chapters = chapters::table
                        .filter(chapters::story_id.eq(story_id))
                        .load::<models::stories::Chapter>(conn)?;
                    for chapter in chapters {
                        let internal_description = format!(
                            "a{}s{}c{}created",
                            author_id, story_id, chapter.chapter_number
                        );
                        let _ = crate::actions::notifications::modifications::clean_unread_notifications_with_internal_description_for_user(
                            &internal_description,
                            user_id,
                            conn,
                        );
                    }
                }
            }

            // Remove the "a user followed you" notification
            let internal_description = format!("u{}a{}follow", user_id, author_id);
            let _ = crate::actions::notifications::modifications::clean_unread_notifications_with_internal_description_for_user(&internal_description, author_id, conn);

            Ok(follow_result.is_some())
        })
    }

    /// Sets the user `user_id` as unfollowing the recommendations of author `author_id`
    pub fn unfollow_author_recommendations(
        user_id: uuid::Uuid,
        author_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<bool, IntertextualError> {
        conn.transaction::<bool, IntertextualError, _>(|| {
            let follow_result = diesel::delete(
                user_author_recommendation_follows::table
                    .filter(user_author_recommendation_follows::user_id.eq(user_id))
                    .filter(user_author_recommendation_follows::author_id.eq(author_id)),
            )
            .get_result::<FollowType>(conn)
            .optional()?;

            // Remove the "a user followed your recommendations" notification
            let internal_description = format!("u{}a{}recfollow", user_id, author_id);
            let _ = crate::actions::notifications::modifications::clean_unread_notifications_with_internal_description_for_user(&internal_description, author_id, conn);

            Ok(follow_result.is_some())
        })
    }

    /// Sets the user `user_id` as unfollowing the story `sotry_id`
    ///
    /// Note : This will automatically remove pending notifications for all chapters currently planned for publication that aren't followed anymore
    pub fn unfollow_story(
        user_id: uuid::Uuid,
        story_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<bool, IntertextualError> {
        conn.transaction::<bool, IntertextualError, _>(|| {
            let follow_result = diesel::delete(
                user_story_follows::table
                    .filter(user_story_follows::user_id.eq(user_id))
                    .filter(user_story_follows::story_id.eq(story_id)),
            )
            .get_result::<FollowType>(conn)
            .optional()?;

            let story: models::stories::Story = stories::table.find(story_id).first(conn)?;

            // If we don't follow the author, remove all notifications for this story
            let author_list = crate::actions::stories::find_authors_by_story(story.id, &FilterMode::BypassFilters, conn)?;
            match author_list.as_slice() {
                [author] if !story.is_collaboration => {
                    // Standalone story
                    let author_is_followed = super::is_following_author(user_id, author.id, conn)?;
                    if !author_is_followed {
                        let chapters = chapters::table
                            .filter(chapters::story_id.eq(story_id))
                            .load::<models::stories::Chapter>(conn)?;
                        for chapter in chapters {
                            let internal_description = format!(
                                "a{}s{}c{}created",
                                author.id, story_id, chapter.chapter_number
                            );
                            let _ = crate::actions::notifications::modifications::clean_unread_notifications_with_internal_description_for_user(
                                &internal_description,
                                user_id,
                                conn,
                            );
                        }
                    }
                }
                _ => {
                    // Collaboration story
                    let an_author_is_followed = author_list
                        .iter()
                        .any(|a| super::is_following_author(user_id, a.id, conn).unwrap_or(false));

                    if !an_author_is_followed {
                        let chapters = chapters::table
                            .filter(chapters::story_id.eq(story_id))
                            .load::<models::stories::Chapter>(conn)?;
                        for chapter in chapters {
                            let internal_description =
                                format!("collabs{}c{}created", story_id, chapter.chapter_number);
                            let _ = crate::actions::notifications::modifications::clean_unread_notifications_with_internal_description_for_user(
                                &internal_description,
                                user_id,
                                conn,
                            );
                        }
                    }
                }
            }

            // Remove the "a user followed your story X" notification
            for author in &author_list {
                let internal_description = format!("u{}a{}s{}follow", user_id, author.id, story_id);
                let _ = crate::actions::notifications::modifications::clean_unread_notifications_with_internal_description_for_user(&internal_description, author.id, conn);
            }

            Ok(follow_result.is_some())
        })
    }
}

/// This module handles the notification system related to following or unfollowing stories
pub mod notifications {
    use diesel::prelude::*;

    use crate::constants::RECOMMENDATION_URL;
    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::prelude::*;
    use crate::schema::*;

    /// Sends a notification for a chapter creation or publication update.
    ///
    /// Note : When using this method within another action, ensure that they are wrapped together into a
    /// transaction using [`diesel::connection::Connection::transaction`]
    ///
    /// Note : if the `first_publication_date` field is `None`, then the chapter is considered immediately unpublished and pending notifications are cleared
    pub fn notify_chapter_publication_change(
        first_publication_date: &Option<chrono::NaiveDateTime>,
        chapter: &models::stories::Chapter,
        conn: &PgConnection,
    ) -> Result<bool, IntertextualError> {
        let story: models::stories::Story = stories::table.find(chapter.story_id).first(conn)?;
        let author_list = crate::actions::stories::find_authors_by_story(
            story.id,
            &FilterMode::BypassFilters,
            conn,
        )?;
        match first_publication_date {
            None => {
                let internal_description = match author_list.as_slice() {
                    [author] if !story.is_collaboration => format!(
                        "a{}s{}c{}created",
                        author.id, story.id, chapter.chapter_number
                    ),
                    _ => format!("collabs{}c{}created", story.id, chapter.chapter_number),
                };
                crate::actions::notifications::modifications::clean_all_unread_notifications_with_internal_description(
                    &internal_description,
                    conn,
                )?;
            }
            Some(first_publication_date) => {
                let followers_of_story = user_story_follows::table
                    .filter(user_story_follows::story_id.eq(story.id))
                    .select(user_story_follows::user_id)
                    .load::<uuid::Uuid>(conn)?;
                let followers_of_user = author_list
                    .iter()
                    .filter_map(|author| {
                        user_author_follows::table
                            .filter(user_author_follows::author_id.eq(author.id))
                            .select(user_author_follows::user_id)
                            .load::<uuid::Uuid>(conn)
                            .ok()
                    })
                    .flatten();
                let followers: std::collections::HashSet<uuid::Uuid> = followers_of_story
                    .into_iter()
                    .chain(followers_of_user)
                    .collect();
                for user_id in followers {
                    let new_notification = helper_new_chapter_notification(
                        *first_publication_date,
                        user_id,
                        &author_list,
                        &story,
                        chapter,
                    );
                    let _ = crate::actions::notifications::modifications::add_notification(
                        new_notification,
                        conn,
                    );
                }
            }
        };
        Ok(true)
    }

    /// Sends a notification that a story or chapter has been updated.
    ///
    /// Note : When using this method within another action, ensure that they are wrapped together into a
    /// transaction using [`diesel::connection::Connection::transaction`]
    pub fn notify_chapter_updated(
        chapter: &models::stories::Chapter,
        conn: &PgConnection,
    ) -> Result<bool, IntertextualError> {
        let story: models::stories::Story = stories::table.find(chapter.story_id).first(conn)?;
        let author_list = crate::actions::stories::find_authors_by_story(
            story.id,
            &FilterMode::BypassFilters,
            conn,
        )?;
        let followers_of_story = user_story_follows::table
            .filter(user_story_follows::story_id.eq(story.id))
            .select(user_story_follows::user_id)
            .load::<uuid::Uuid>(conn)?;
        let followers_of_user = author_list
            .iter()
            .filter_map(|author| {
                user_author_follows::table
                    .filter(user_author_follows::author_id.eq(author.id))
                    .select(user_author_follows::user_id)
                    .load::<uuid::Uuid>(conn)
                    .ok()
            })
            .flatten();
        let followers: std::collections::HashSet<uuid::Uuid> = followers_of_story
            .into_iter()
            .chain(followers_of_user)
            .collect();
        for user_id in followers {
            let new_notification = match author_list.as_slice() {
                [author] if !story.is_collaboration => models::notifications::NewNotification {
                    target_user_id: user_id,
                    notification_time: chrono::Utc::now().naive_utc(),
                    internal_description: format!(
                        "a{}s{}c{}updated",
                        author.id, story.id, chapter.chapter_number
                    ),
                    category: models::notifications::NotificationCategory::FollowUpdated.into(),
                    message: if chapter.chapter_number > 1 {
                        format!(
                            "{} updated chapter {} of {}",
                            author.display_name, chapter.chapter_number, story.title
                        )
                    } else {
                        format!("{} updated {}", author.display_name, story.title)
                    },
                    link: format!(
                        "/@{}/{}/{}/",
                        author.username, story.url_fragment, chapter.chapter_number
                    ),
                },
                _ => models::notifications::NewNotification {
                    target_user_id: user_id,
                    notification_time: chrono::Utc::now().naive_utc(),
                    internal_description: format!(
                        "collabs{}c{}updated",
                        story.id, chapter.chapter_number
                    ),
                    category: models::notifications::NotificationCategory::FollowUpdated.into(),
                    message: if chapter.chapter_number > 1 {
                        format!(
                            "{} updated chapter {} of {}",
                            get_combined_author_names(&author_list),
                            chapter.chapter_number,
                            story.title
                        )
                    } else {
                        format!(
                            "{} updated {}",
                            get_combined_author_names(&author_list),
                            story.title
                        )
                    },
                    link: format!(
                        "/collaboration/{}/{}/",
                        story.url_fragment, chapter.chapter_number
                    ),
                },
            };
            let _ = crate::actions::notifications::modifications::add_notification(
                new_notification,
                conn,
            );
        }
        Ok(true)
    }

    /// Sends a notification that a new recommendation has been published
    ///
    /// Note : When using this method within another action, ensure that they are wrapped together into a
    /// transaction using [`diesel::connection::Connection::transaction`]
    pub fn notify_new_recommendation(
        recommendation: &models::recommendations::Recommendation,
        conn: &PgConnection,
    ) -> Result<bool, IntertextualError> {
        let story: models::stories::Story =
            stories::table.find(recommendation.story_id).first(conn)?;
        let author_list = crate::actions::stories::find_authors_by_story(
            story.id,
            &FilterMode::BypassFilters,
            conn,
        )?;
        let recommender: models::users::User =
            users::table.find(recommendation.user_id).first(conn)?;
        let followers: std::collections::HashSet<uuid::Uuid> =
            user_author_recommendation_follows::table
                .filter(user_author_recommendation_follows::author_id.eq(recommender.id))
                .select(user_author_recommendation_follows::user_id)
                .load::<uuid::Uuid>(conn)?
                .into_iter()
                .collect();
        for user_id in followers {
            let new_notification = match author_list.as_slice() {
                [author] if !story.is_collaboration => models::notifications::NewNotification {
                    target_user_id: user_id,
                    notification_time: chrono::Utc::now().naive_utc(),
                    internal_description: format!(
                        "u{}a{}s{}recommended",
                        recommender.id, author.id, story.id
                    ),
                    category: models::notifications::NotificationCategory::FollowNewRecommendation
                        .into(),
                    message: format!(
                        "{} suggested the story : {} from {}",
                        recommender.display_name, story.title, author.display_name
                    ),
                    link: format!(
                        "/{}/@{}/@{}/{}",
                        RECOMMENDATION_URL,
                        recommender.username,
                        author.username,
                        story.url_fragment
                    ),
                },
                _ => models::notifications::NewNotification {
                    target_user_id: user_id,
                    notification_time: chrono::Utc::now().naive_utc(),
                    internal_description: format!(
                        "u{}collabs{}recommended",
                        recommender.id, story.id
                    ),
                    category: models::notifications::NotificationCategory::FollowNewRecommendation
                        .into(),
                    message: format!(
                        "{} suggested the story : {} by {}",
                        recommender.display_name,
                        story.title,
                        get_combined_author_names(&author_list)
                    ),
                    link: format!(
                        "/{}/@{}/collaboration/{}",
                        RECOMMENDATION_URL, recommender.username, story.url_fragment
                    ),
                },
            };
            let _ = crate::actions::notifications::modifications::add_notification(
                new_notification,
                conn,
            );
        }
        Ok(true)
    }

    fn get_combined_author_names(author_list: &[models::users::User]) -> String {
        let mut collaboration = String::new();
        let author_count = author_list.len();
        for (index, author) in author_list.iter().enumerate() {
            collaboration.push_str(author.display_name.as_str());
            if index >= 1 && index - 1 == author_count {
                // This is the last author, do nothing.
            } else if index >= 2 && index - 2 == author_count {
                // This is the second-to-last author.
                collaboration.push_str(" and ");
            } else {
                // There is more than 2 authors remaining
                collaboration.push_str(", ");
            }
        }
        collaboration
    }

    /// Creates a notification that a new story or chapter has been created at some point. This does not modify the database
    pub fn helper_new_chapter_notification(
        notification_time: chrono::NaiveDateTime,
        target_user_id: uuid::Uuid,
        author_list: &[models::users::User],
        story: &models::stories::Story,
        chapter: &models::stories::Chapter,
    ) -> models::notifications::NewNotification {
        match author_list {
            [author] if !story.is_collaboration => models::notifications::NewNotification {
                target_user_id,
                notification_time,
                internal_description: format!(
                    "a{}s{}c{}created",
                    author.id, story.id, chapter.chapter_number
                ),
                category: if chapter.chapter_number > 1 {
                    models::notifications::NotificationCategory::FollowNewChapter
                } else {
                    models::notifications::NotificationCategory::FollowNewStory
                }
                .into(),
                message: if chapter.chapter_number > 1 {
                    format!(
                        "{} published chapter {} of {}",
                        author.display_name, chapter.chapter_number, story.title
                    )
                } else {
                    format!(
                        "{} published a new story {}",
                        author.display_name, story.title
                    )
                },
                link: format!(
                    "/@{}/{}/{}/",
                    author.username, story.url_fragment, chapter.chapter_number
                ),
            },
            _ => models::notifications::NewNotification {
                target_user_id,
                notification_time,
                internal_description: format!(
                    "collabs{}c{}created",
                    story.id, chapter.chapter_number
                ),
                category: if chapter.chapter_number > 1 {
                    models::notifications::NotificationCategory::FollowNewChapter
                } else {
                    models::notifications::NotificationCategory::FollowNewStory
                }
                .into(),
                message: if chapter.chapter_number > 1 {
                    format!(
                        "{} published chapter {} of {}",
                        get_combined_author_names(author_list),
                        chapter.chapter_number,
                        story.title
                    )
                } else {
                    format!(
                        "{} published a new story {}",
                        get_combined_author_names(author_list),
                        story.title
                    )
                },
                link: format!(
                    "/collaboration/{}/{}/",
                    story.url_fragment, chapter.chapter_number
                ),
            },
        }
    }
}
