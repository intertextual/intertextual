use diesel::prelude::*;

use crate::models::announcements::Announcement;
use crate::models::announcements::FrontpageAnnouncement;
use crate::models::error::IntertextualError;
use crate::schema::*;

/// Finds the number of hits for the chapter `chapter_id`
pub fn get_frontpage_announcement(
    conn: &PgConnection,
) -> Result<Option<FrontpageAnnouncement>, IntertextualError> {
    let result = frontpage_announcement::table
        .filter(frontpage_announcement::show_publicly_after_date.lt(chrono::Utc::now().naive_utc()))
        .filter(frontpage_announcement::remove_after_date.gt(chrono::Utc::now().naive_utc()))
        .get_result::<FrontpageAnnouncement>(conn)
        .optional()?;
    Ok(result)
}

/// Finds the announcement matching an URL fragment.
pub fn get_announcement_by_url(
    url: &str,
    conn: &PgConnection,
) -> Result<Option<Announcement>, IntertextualError> {
    let result = announcements::table
        .filter(announcements::url_fragment.eq(url))
        .filter(announcements::show_publicly_after_date.lt(chrono::Utc::now().naive_utc()))
        .get_result::<Announcement>(conn)
        .optional()?;
    Ok(result)
}

/// These methods should be called automatically by a background thread or a script, but not manually by an user or moderator action
pub mod automated_actions {
    use diesel::prelude::*;

    use crate::models::error::IntertextualError;
    use crate::schema::*;

    /// Create a new frontpage announcement.
    pub fn delete_past_frontpage_announcements(
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        diesel::delete(frontpage_announcement::table)
            .filter(frontpage_announcement::remove_after_date.lt(chrono::Utc::now().naive_utc()))
            .execute(conn)?;
        Ok(())
    }
}

/// Methods used to administrate stories
pub mod admin {
    use crate::prelude::*;
    use diesel::prelude::*;

    use crate::actions::utils::log_mod_action;
    use crate::models;
    use crate::models::announcements::Announcement;
    use crate::models::announcements::FrontpageAnnouncement;
    use crate::models::announcements::NewAnnouncement;
    use crate::models::announcements::NewFrontpageAnnouncement;
    use crate::models::error::IntertextualError;
    use crate::schema::*;

    /// Get the `limit` next announcements, skipping the `start` first announcements
    pub fn get_all_announcements(
        start: i64,
        limit: i64,
        conn: &PgConnection,
    ) -> Result<Vec<Announcement>, IntertextualError> {
        let result = announcements::table
            .order_by(announcements::announcement_internally_created_at.desc())
            .offset(start)
            .limit(limit)
            .load(conn)?;
        Ok(result)
    }

    /// Get the total count of announcements
    pub fn get_all_announcements_count(conn: &PgConnection) -> Result<i64, IntertextualError> {
        let result = announcements::table.count().get_result::<i64>(conn)?;
        Ok(result)
    }

    /// Note : This method is admin-only because only administration panels should always be able to see announcements.
    /// Other screens should use [super::get_announcement_by_url]
    pub fn get_announcement_by_url(
        url: &str,
        conn: &PgConnection,
    ) -> Result<Option<Announcement>, IntertextualError> {
        let result = announcements::table
            .filter(announcements::url_fragment.eq(url))
            .get_result::<Announcement>(conn)
            .optional()?;
        Ok(result)
    }

    /// Note : This method is admin-only because only administration panels should able to see all frontpage announcements.
    /// Other screens should use [super::get_frontpage_announcement]
    pub fn get_frontpage_announcement_list(
        conn: &PgConnection,
    ) -> Result<Vec<FrontpageAnnouncement>, IntertextualError> {
        let result = frontpage_announcement::table.load::<FrontpageAnnouncement>(conn)?;
        Ok(result)
    }

    /// Create a new frontpage announcement.
    pub fn create_frontpage_announcement(
        new_announcement: NewFrontpageAnnouncement,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            diesel::insert_into(frontpage_announcement::table)
                .values(new_announcement)
                .execute(conn)?;
            let result = log_mod_action(log_entry, conn)?;
            Ok(result)
        })
    }

    /// Change the content of a given frontpage announcement.
    pub fn update_frontpage_announcement(
        id: uuid::Uuid,
        publication_date: Option<chrono::NaiveDateTime>,
        removal_date: Option<chrono::NaiveDateTime>,
        content: &SanitizedHtml,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        let new_content = content.clone();
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            diesel::update(frontpage_announcement::table.find(id))
                .set((
                    frontpage_announcement::show_publicly_after_date.eq(publication_date),
                    frontpage_announcement::remove_after_date.eq(removal_date),
                    frontpage_announcement::content.eq(new_content),
                ))
                .execute(conn)?;
            let result = log_mod_action(log_entry, conn)?;
            Ok(result)
        })
    }

    /// Create a new frontpage announcement.
    pub fn delete_frontpage_announcement(
        id: uuid::Uuid,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            diesel::delete(frontpage_announcement::table)
                .filter(frontpage_announcement::id.eq(id))
                .execute(conn)?;
            let action = log_mod_action(log_entry, conn)?;
            Ok(action)
        })
    }

    /// Create a new announcement.
    pub fn create_announcement(
        new_announcement: NewAnnouncement,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            diesel::insert_into(announcements::table)
                .values(new_announcement)
                .execute(conn)?;
            let result = log_mod_action(log_entry, conn)?;
            Ok(result)
        })
    }

    /// Change the content of a given announcement.
    pub fn update_announcement(
        url: &str,
        publication_date: Option<chrono::NaiveDateTime>,
        title: Option<&str>,
        content: &SanitizedHtml,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        let new_title = title.map(String::from);
        let new_content = content.clone();
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            diesel::update(announcements::table.filter(announcements::url_fragment.eq(url)))
                .set((
                    announcements::title.eq(new_title),
                    announcements::content.eq(new_content),
                    announcements::show_publicly_after_date.eq(publication_date),
                ))
                .execute(conn)?;
            let result = log_mod_action(log_entry, conn)?;
            Ok(result)
        })
    }

    /// Create a new frontpage announcement.
    pub fn delete_announcement(
        url: &str,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            diesel::delete(announcements::table)
                .filter(announcements::url_fragment.eq(url))
                .execute(conn)?;
            let action = log_mod_action(log_entry, conn)?;
            Ok(action)
        })
    }
}
