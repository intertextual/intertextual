//! # Users
//!
//! This module contains methods used to create, edit, modify and administrate user accounts
use diesel::prelude::*;

use crate::models;
use crate::models::error::IntertextualError;
use crate::models::filter::FilterMode;
use crate::models::stories::CreativeRole;
use crate::schema::*;

/// Searches the user database for any user whose username or display name matches the given query.
pub fn search_from_query(
    query: String,
    conn: &PgConnection,
) -> Result<Vec<models::users::User>, IntertextualError> {
    let trimmed_query = query.trim();
    if trimmed_query.is_empty() {
        return Ok(vec![]);
    }
    let mut result = users::table
        .filter(users::deactivated_by_user.eq(false))
        .filter(
            users::banned_until
                .lt(chrono::Utc::now().naive_utc())
                .or(users::banned_until.is_null()),
        )
        .filter(
            users::username
                .ilike(format!("%{}%", trimmed_query))
                .or(users::display_name.ilike(format!("%{}%", trimmed_query))),
        )
        .order(users::display_name.asc())
        .load::<models::users::User>(conn)?;
    result.sort_by_cached_key(|k| {
        // Get the 'most exact" first while maintaining the overall alphabetical order
        if k.username == query || k.display_name == query {
            "1".to_string()
        } else if k.username.starts_with(&query) || k.display_name.starts_with(&query) {
            format!("2{}", k.display_name)
        } else {
            format!("3{}", k.display_name)
        }
    });
    Ok(result)
}

pub fn find_total_author_count(
    filter: &FilterMode,
    conn: &PgConnection,
) -> Result<i64, IntertextualError> {
    let result = match filter {
        FilterMode::BypassFilters => {
            let query = user_to_story_associations::table
                .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
                .inner_join(users::table.on(users::id.eq(user_to_story_associations::user_id)))
                .select(users::id)
                .distinct();
            query.load::<uuid::Uuid>(conn)?.len() as i64
        }
        FilterMode::IncludeDirectAccess { own_user_id } => {
            // Note: This is not quite correct as editors won't see the right list of authors if the author
            //       has no published stories. However, this seems like an acceptable limitation.
            let query = user_to_story_associations::table
                .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
                .inner_join(users::table.on(users::id.eq(user_to_story_associations::user_id)))
                .filter(
                    users::deactivated_by_user
                        .eq(false)
                        .or(users::id.eq(own_user_id)),
                )
                .filter(
                    users::banned_until
                        .lt(chrono::Utc::now().naive_utc())
                        .or(users::banned_until.is_null())
                        .or(users::id.eq(own_user_id)),
                )
                .inner_join(stories::table.on(stories::id.eq(user_to_story_associations::story_id)))
                .inner_join(chapters::table.on(chapters::story_id.eq(stories::id)))
                .filter(
                    chapters::show_publicly_after_date
                        .lt(chrono::Utc::now().naive_utc())
                        .or(users::id.eq(own_user_id)),
                )
                .filter(
                    chapters::moderator_locked
                        .eq(false)
                        .or(users::id.eq(own_user_id)),
                )
                .select(users::id)
                .distinct();
            query.load::<uuid::Uuid>(conn)?.len() as i64
        }
        FilterMode::Standard => {
            let query = user_to_story_associations::table
                .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
                .inner_join(users::table.on(users::id.eq(user_to_story_associations::user_id)))
                .filter(users::deactivated_by_user.eq(false))
                .filter(
                    users::banned_until
                        .lt(chrono::Utc::now().naive_utc())
                        .or(users::banned_until.is_null()),
                )
                .inner_join(stories::table.on(stories::id.eq(user_to_story_associations::story_id)))
                .inner_join(chapters::table.on(chapters::story_id.eq(stories::id)))
                .filter(chapters::show_publicly_after_date.lt(chrono::Utc::now().naive_utc()))
                .filter(chapters::moderator_locked.eq(false))
                .select(users::id)
                .distinct();
            query.load::<uuid::Uuid>(conn)?.len() as i64
        }
    };
    Ok(result)
}

pub fn find_authors_in_range(
    start: i64,
    limit: i64,
    filter: &FilterMode,
    conn: &PgConnection,
) -> Result<Vec<models::users::User>, IntertextualError> {
    let result = match filter {
        FilterMode::BypassFilters => {
            let query = user_to_story_associations::table
                .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
                .inner_join(users::table.on(users::id.eq(user_to_story_associations::user_id)))
                .select(users::all_columns)
                .order(users::display_name.asc())
                .distinct()
                .offset(start)
                .limit(limit);
            query.load::<models::users::User>(conn)?
        }
        FilterMode::IncludeDirectAccess { own_user_id } => {
            // Note: This is not quite correct as editors won't see the right list of authors if the author
            //       has no published stories. However, this seems like an acceptable limitation.
            let query = user_to_story_associations::table
                .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
                .inner_join(users::table.on(users::id.eq(user_to_story_associations::user_id)))
                .filter(
                    users::deactivated_by_user
                        .eq(false)
                        .or(users::id.eq(own_user_id)),
                )
                .filter(
                    users::banned_until
                        .lt(chrono::Utc::now().naive_utc())
                        .or(users::banned_until.is_null())
                        .or(users::id.eq(own_user_id)),
                )
                .inner_join(stories::table.on(stories::id.eq(user_to_story_associations::story_id)))
                .inner_join(chapters::table.on(chapters::story_id.eq(stories::id)))
                .filter(
                    chapters::show_publicly_after_date
                        .lt(chrono::Utc::now().naive_utc())
                        .or(users::id.eq(own_user_id)),
                )
                .filter(
                    chapters::moderator_locked
                        .eq(false)
                        .or(users::id.eq(own_user_id)),
                )
                .select(users::all_columns)
                .order(users::display_name.asc())
                .distinct()
                .offset(start)
                .limit(limit);
            query.load::<models::users::User>(conn)?
        }
        FilterMode::Standard => {
            let query = user_to_story_associations::table
                .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
                .inner_join(users::table.on(users::id.eq(user_to_story_associations::user_id)))
                .filter(users::deactivated_by_user.eq(false))
                .filter(
                    users::banned_until
                        .lt(chrono::Utc::now().naive_utc())
                        .or(users::banned_until.is_null()),
                )
                .inner_join(stories::table.on(stories::id.eq(user_to_story_associations::story_id)))
                .inner_join(chapters::table.on(chapters::story_id.eq(stories::id)))
                .filter(chapters::show_publicly_after_date.lt(chrono::Utc::now().naive_utc()))
                .filter(chapters::moderator_locked.eq(false))
                .select(users::all_columns)
                .order(users::display_name.asc())
                .distinct()
                .offset(start)
                .limit(limit);
            query.load::<models::users::User>(conn)?
        }
    };
    Ok(result)
}

/// Finds a list of all users in the database, respecting the given `filter`
pub fn find_all_users(
    filter: &FilterMode,
    conn: &PgConnection,
) -> Result<Vec<models::users::User>, IntertextualError> {
    let result = match filter {
        FilterMode::Standard => users::table
            .filter(users::deactivated_by_user.eq(false))
            .filter(
                users::banned_until
                    .lt(chrono::Utc::now().naive_utc())
                    .or(users::banned_until.is_null()),
            )
            .order(users::display_name.asc())
            .load::<models::users::User>(conn)?,
        FilterMode::IncludeDirectAccess { own_user_id } => users::table
            .filter(
                users::deactivated_by_user
                    .eq(false)
                    .or(users::id.eq(own_user_id)),
            )
            .filter(
                users::banned_until
                    .lt(chrono::Utc::now().naive_utc())
                    .or(users::banned_until.is_null())
                    .or(users::id.eq(own_user_id)),
            )
            .order(users::display_name.asc())
            .load::<models::users::User>(conn)?,
        FilterMode::BypassFilters => users::table
            .order(users::display_name.asc())
            .load::<models::users::User>(conn)?,
    };
    Ok(result)
}

/// Finds the user with the given `username`, respecting the given `filter`
pub fn find_user_by_username(
    username: &str,
    filter: &FilterMode,
    conn: &PgConnection,
) -> Result<Option<models::users::User>, IntertextualError> {
    let result = match filter {
        FilterMode::Standard => users::table
            .filter(users::deactivated_by_user.eq(false))
            .filter(
                users::banned_until
                    .lt(chrono::Utc::now().naive_utc())
                    .or(users::banned_until.is_null()),
            )
            .filter(users::username.ilike(username))
            .first::<models::users::User>(conn)
            .optional()?,
        FilterMode::IncludeDirectAccess { own_user_id } => users::table
            .filter(
                users::deactivated_by_user
                    .eq(false)
                    .or(users::id.eq(own_user_id)),
            )
            .filter(
                users::banned_until
                    .lt(chrono::Utc::now().naive_utc())
                    .or(users::banned_until.is_null())
                    .or(users::id.eq(own_user_id)),
            )
            .filter(users::username.ilike(username))
            .first::<models::users::User>(conn)
            .optional()?,
        FilterMode::BypassFilters => users::table
            .filter(users::username.ilike(username))
            .first::<models::users::User>(conn)
            .optional()?,
    };
    Ok(result)
}

/// Check if the given `username` can be used to create a new account
pub fn is_username_available(
    username: &str,
    conn: &PgConnection,
) -> Result<bool, IntertextualError> {
    let matched_count = users::table
        .filter(users::username.ilike(username))
        .count()
        .first::<i64>(conn)?;
    Ok(matched_count == 0)
}

/// Finds the user with the given `username` for login purposes
pub fn find_user_by_username_for_login(
    username: &str,
    conn: &PgConnection,
) -> Result<Option<models::users::User>, IntertextualError> {
    Ok(users::table
        .filter(users::username.ilike(username))
        .first::<models::users::User>(conn)
        .optional()?)
}

/// Finds the user with the given `id`
pub fn find_user_by_id(
    id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<Option<models::users::User>, IntertextualError> {
    Ok(users::table
        .filter(users::id.eq(id))
        .first::<models::users::User>(conn)
        .optional()?)
}

/// Use a token to find the user and update the token use time if needed (for cookie handling purposes)
pub fn find_user_for_token_and_update_time_if_needed(
    user_id: uuid::Uuid,
    token_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<Option<models::users::User>, IntertextualError> {
    conn.transaction::<Option<models::users::User>, IntertextualError, _>(|| {
        // Get the user if it matches the token data
        let maybe_user = auth_cookies_tokens::table
            .find((user_id, token_id))
            .filter(auth_cookies_tokens::valid_until.ge(chrono::Utc::now().naive_utc()))
            .inner_join(users::table)
            .select(users::all_columns)
            .first::<models::users::User>(conn)
            .optional()?;

        if maybe_user.is_some() {
            // Update the last visit time.
            diesel::update(auth_cookies_tokens::table.find((user_id, token_id)))
                .set(auth_cookies_tokens::last_visit.eq(chrono::Utc::now().naive_utc()))
                .execute(conn)?;
        }
        Ok(maybe_user)
    })
}

/// Find all the active tokens for a given user ID
pub fn get_tokens_for_user(
    user_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<Vec<(String, chrono::NaiveDateTime)>, IntertextualError> {
    Ok(auth_cookies_tokens::table
        .filter(auth_cookies_tokens::user_id.eq(user_id))
        .filter(auth_cookies_tokens::valid_until.ge(chrono::Utc::now().naive_utc()))
        .select((
            auth_cookies_tokens::token_name,
            auth_cookies_tokens::last_visit,
        ))
        .load::<(String, chrono::NaiveDateTime)>(conn)?)
}

/// Get information about this account's deletion request, if it exists
pub fn get_earliest_active_deletion_request(
    user_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<Option<(chrono::NaiveDateTime, bool)>, IntertextualError> {
    let result = account_deletion_log::table
        .filter(account_deletion_log::account_id.eq(user_id))
        .filter(account_deletion_log::is_active_deletion_request.eq(true))
        .load::<models::users::AccountDeletionLog>(conn)?;
    let can_be_cancelled_by_user = result.iter().any(|a| a.can_be_cancelled_by_user);
    let earliest_request = result.into_iter().min_by_key(|a| a.request_date);
    Ok(earliest_request.map(|e| (e.request_date, can_be_cancelled_by_user)))
}

/// Gets whether new accounts can be registered at all
pub fn get_registrations_enabled(conn: &PgConnection) -> Result<bool, IntertextualError> {
    conn.transaction::<bool, IntertextualError, _>(|| {
        let (registrations_enabled, limit_enabled, limit_hours, limit_max_registrations) =
            registration_parameters::table
                .select((
                    registration_parameters::registrations_enabled,
                    registration_parameters::limit_enabled,
                    registration_parameters::limit_hours,
                    registration_parameters::limit_max_registrations,
                ))
                .get_result::<(bool, bool, i32, i32)>(conn)?;
        let limit_hours = i64::from(limit_hours);
        let limit_max_registrations = i64::from(limit_max_registrations);
        if !registrations_enabled {
            return Ok(false);
        }
        if !limit_enabled {
            return Ok(true);
        }
        let earliest_date = chrono::Utc::now().naive_utc() - chrono::Duration::hours(limit_hours);
        let created_accounts = users::table
            .filter(users::account_created_at.gt(earliest_date))
            .count()
            .get_result::<i64>(conn)?;
        Ok(created_accounts < limit_max_registrations)
    })
}

/// Methods to modify an user account's settings
pub mod modifications {
    use diesel::prelude::*;

    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::models::formats::SanitizedHtml;
    use crate::models::style::Style;
    use crate::models::tags::TagHighlightRule;
    use crate::schema::*;

    /// Represents a new user that can be inserted in the database.
    #[derive(Insertable)]
    #[table_name = "users"]
    pub struct NewUser {
        /// The username of the new user.
        ///
        /// Note : This username is guaranteed to be globally unique. If this username is not unique,
        /// the operation to add the user will fail.
        pub username: String,

        /// The display name of the new user. Can be different from the username, and can be shared with other users.
        pub display_name: String,

        /// The hash of the password of the new user.
        pub pw_hash: String,
    }

    /// Insets the given new user in the database
    /// * [username] : The username of the new user.
    ///     Note : This username is guaranteed to be globally unique. If this username is not unique,
    ///     the operation to add the user will fail.
    /// * [display_name] : The display name of the new user. Can be different from the username, and can be shared with other users.
    /// * [password] : The password of the new user. This will be hashed before insertion in the DB
    /// * [hash_secret_key] : The secret key used to salt the password hash
    pub fn insert_user(
        username: String,
        display_name: String,
        password: String,
        hash_secret_key: String,
        conn: &PgConnection,
    ) -> Result<models::users::User, IntertextualError> {
        conn.transaction::<models::users::User, IntertextualError, _>(|| {
            // Check if the username is not already in use
            let existing_user = users::table
                .filter(users::username.ilike(username.as_str()))
                .first::<models::users::User>(conn)
                .optional()?;
            if existing_user.is_some() {
                // The user exists, return a dummy duplicate error
                // TODO : this is hackish. Don't do this ?
                return Err(IntertextualError::DBError(
                    diesel::result::Error::DatabaseError(
                        diesel::result::DatabaseErrorKind::UniqueViolation,
                        Box::new("".to_string()),
                    ),
                ));
            }
            let pw_hash = argonautica::Hasher::default()
                .with_password(password.as_str())
                .with_secret_key(hash_secret_key.as_str())
                .hash()
                .unwrap();
            let new_user = diesel::insert_into(users::table)
                .values(NewUser {
                    username,
                    display_name,
                    pw_hash,
                })
                .get_result::<models::users::User>(conn)?;
            Ok(new_user)
        })
    }

    /// Update the information for the given user
    pub fn update_user_information(
        user: models::users::User,
        new_display_name: String,
        new_email: Option<String>,
        new_details: Option<SanitizedHtml>,
        conn: &PgConnection,
    ) -> Result<models::users::User, IntertextualError> {
        Ok(diesel::update(users::table.find(user.id))
            .set((
                users::display_name.eq(new_display_name),
                users::email.eq(new_email),
                users::details.eq(new_details),
            ))
            .get_result::<models::users::User>(conn)?)
    }

    /// Update the settings for the given user
    pub fn update_user_settings(
        user_id: uuid::Uuid,
        new_settings: Option<serde_json::Value>,
        conn: &PgConnection,
    ) -> Result<models::users::User, IntertextualError> {
        Ok(diesel::update(users::table.find(user_id))
            .set(users::settings.eq(new_settings))
            .get_result::<models::users::User>(conn)?)
    }

    /// Update the tag highlight rules for the given user
    pub fn update_user_tag_highlight_rules(
        user_id: uuid::Uuid,
        new_tag_highlight_rules: Option<Vec<TagHighlightRule>>,
        conn: &PgConnection,
    ) -> Result<models::users::User, IntertextualError> {
        let rules = serde_json::to_value(new_tag_highlight_rules)
            .map_err(|_| IntertextualError::InternalServerError)?;
        Ok(diesel::update(users::table.find(user_id))
            .set(users::tag_highlight_rules.eq(rules))
            .get_result::<models::users::User>(conn)?)
    }

    /// Update the website style settings
    pub fn update_user_style(
        user_id: uuid::Uuid,
        new_style_prefs: Option<Style>,
        conn: &PgConnection,
    ) -> Result<models::users::User, IntertextualError> {
        let style_prefs = serde_json::to_value(new_style_prefs)
            .map_err(|_| IntertextualError::InternalServerError)?;
        Ok(diesel::update(users::table.find(user_id))
            .set(users::style_prefs.eq(style_prefs))
            .get_result::<models::users::User>(conn)?)
    }

    /// Update the default foreword/afterword for the given user
    pub fn update_user_default_notes(
        user_id: uuid::Uuid,
        new_default_foreword: SanitizedHtml,
        new_default_afterword: SanitizedHtml,
        conn: &PgConnection,
    ) -> Result<models::users::User, IntertextualError> {
        Ok(diesel::update(users::table.find(user_id))
            .set((
                users::default_foreword.eq(new_default_foreword),
                users::default_afterword.eq(new_default_afterword),
            ))
            .get_result::<models::users::User>(conn)?)
    }

    /// Used by the user when changing their password
    ///
    /// Note : this must only be used by the user
    pub fn update_user_password(
        user: models::users::User,
        new_password_hash: String,
        conn: &PgConnection,
    ) -> Result<models::users::User, IntertextualError> {
        Ok(diesel::update(users::table.find(user.id))
            .set((
                users::pw_hash.eq(new_password_hash),
                users::password_reset_required.eq(false),
            ))
            .get_result::<models::users::User>(conn)?)
    }

    /// Update the user activation status
    pub fn update_user_activation(
        user: models::users::User,
        new_activation_status: bool,
        conn: &PgConnection,
    ) -> Result<models::users::User, IntertextualError> {
        Ok(diesel::update(users::table.find(user.id))
            .set(users::deactivated_by_user.eq(!new_activation_status))
            .get_result::<models::users::User>(conn)?)
    }

    /// Request deletion of the user's account
    pub fn request_user_deletion(
        user: models::users::User,
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        diesel::insert_into(account_deletion_log::table)
            .values(models::users::NewAccountDeletionLog {
                account_id: user.id,
                can_be_cancelled_by_user: true,
                is_active_deletion_request: true,
                request_information: "action requested by user".to_string(),
            })
            .get_result::<models::users::AccountDeletionLog>(conn)?;
        Ok(())
    }

    /// Cancel any pending deletion request of the user's account
    pub fn cancel_user_deletion_request(
        user: models::users::User,
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        conn.transaction::<(), IntertextualError, _>(|| {
            diesel::update(account_deletion_log::table)
                .filter(
                    account_deletion_log::account_id
                        .eq(user.id)
                        .and(account_deletion_log::can_be_cancelled_by_user.eq(true)),
                )
                .set(account_deletion_log::is_active_deletion_request.eq(false))
                .get_result::<models::users::AccountDeletionLog>(conn)?;
            diesel::insert_into(account_deletion_log::table)
                .values(models::users::NewAccountDeletionLog {
                    account_id: user.id,
                    can_be_cancelled_by_user: true,
                    is_active_deletion_request: false,
                    request_information: "action cancelled by user".to_string(),
                })
                .get_result::<models::users::AccountDeletionLog>(conn)?;
            Ok(())
        })
    }

    /// Adds a new token for a given user ID
    pub fn add_auth_token_for_user(
        user_id: uuid::Uuid,
        token_name: String,
        valid_until: chrono::NaiveDateTime,
        conn: &PgConnection,
    ) -> Result<uuid::Uuid, IntertextualError> {
        let token = diesel::insert_into(auth_cookies_tokens::table)
            .values((
                auth_cookies_tokens::user_id.eq(user_id),
                auth_cookies_tokens::token_name.eq(token_name),
                auth_cookies_tokens::valid_until.eq(valid_until),
            ))
            .get_result::<(
                uuid::Uuid,            // User id
                uuid::Uuid,            // Token id
                String,                // Token name
                chrono::NaiveDateTime, // Last visit (initialized to now)
                chrono::NaiveDateTime, // Valid until
            )>(conn)?;
        Ok(token.1)
    }

    pub fn remove_auth_token_from_id(
        user_id: uuid::Uuid,
        token_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        diesel::delete(auth_cookies_tokens::table.find((user_id, token_id))).execute(conn)?;
        Ok(())
    }

    pub fn remove_auth_token_from_name(
        user_id: uuid::Uuid,
        token_name: String,
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        diesel::delete(
            auth_cookies_tokens::table
                .filter(auth_cookies_tokens::user_id.eq(user_id))
                .filter(auth_cookies_tokens::token_name.eq(token_name)),
        )
        .execute(conn)?;
        Ok(())
    }
}

/// These methods should be called automatically by a background thread or a script, but not manually by an user or moderator action
pub mod automated_actions {
    use diesel::prelude::*;
    use log::info;

    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::prelude::*;
    use crate::schema::*;

    /// Delete any account whose pending deletion has passed its limit date
    pub fn delete_pending_accounts(
        account_deletion_request_duration_days: u16,
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        let duration = chrono::Duration::days(account_deletion_request_duration_days as i64);
        let now = chrono::Utc::now().naive_utc();
        let pending_requests = get_accounts_with_active_requests(conn)?;
        for (user, request) in pending_requests {
            if request.request_date + duration > now {
                continue;
            }
            info!("Deleting user : {} (time limit reached)", user.id);
            delete_user(user, conn)?;
        }
        Ok(())
    }

    /// Remove all the expired auth tokens from the database
    pub fn clean_expired_auth_tokens(conn: &PgConnection) -> Result<(), IntertextualError> {
        diesel::delete(
            auth_cookies_tokens::table
                .filter(auth_cookies_tokens::valid_until.lt(chrono::Utc::now().naive_utc())),
        )
        .execute(conn)?;
        Ok(())
    }

    fn get_accounts_with_active_requests(
        conn: &PgConnection,
    ) -> Result<Vec<(models::users::User, models::users::AccountDeletionLog)>, IntertextualError>
    {
        let result = users::table
            .inner_join(
                account_deletion_log::table.on(account_deletion_log::account_id.eq(users::id)),
            )
            .filter(account_deletion_log::is_active_deletion_request.eq(true))
            .distinct_on(users::id)
            .load::<(models::users::User, models::users::AccountDeletionLog)>(conn)?;
        Ok(result)
    }

    /// Immediately delete the given user account.
    ///
    /// WARNING : This should nearly never be called explicitly. Instead, use [`crate::actions::users::modifications::request_user_deletion`] or
    /// [`crate::actions::users::admins::request_user_deletion`] to planify the request, and this method will be called as
    /// needed by [`delete_pending_accounts`]
    pub fn delete_user(
        user: models::users::User,
        conn: &PgConnection,
    ) -> Result<models::users::User, IntertextualError> {
        // Delete all standalone stories from this user
        conn.transaction::<models::users::User, IntertextualError, _>(|| {
            let stories = crate::actions::stories::find_all_stories_by_author(
                user.id,
                &crate::models::filter::FilterMode::IncludeDirectAccess {
                    own_user_id: user.id,
                },
                conn,
            )?;
            for story in stories {
                if story.is_collaboration {
                    let author_list = crate::actions::stories::find_authors_by_story(
                        story.id,
                        &FilterMode::BypassFilters,
                        conn,
                    )?;
                    let has_other_author = author_list.iter().any(|a| a.id != user.id);
                    if !has_other_author {
                        // Do not delete collaborations if there is still other authors other than yourself.
                        continue;
                    }
                }

                let chapters = crate::actions::stories::find_chapters_by_story_id(
                    story.id,
                    &crate::models::filter::FilterMode::IncludeDirectAccess {
                        own_user_id: user.id,
                    },
                    conn,
                )?;
                for chapter in chapters {
                    crate::actions::stories::modifications::delete_chapter(chapter, conn)?;
                }
            }

            diesel::update(account_deletion_log::table)
                .filter(account_deletion_log::account_id.eq(user.id))
                .set(account_deletion_log::is_active_deletion_request.eq(false))
                .get_result::<models::users::AccountDeletionLog>(conn)
                .optional()?;

            Ok(diesel::delete(users::table.find(user.id))
                .get_result::<models::users::User>(conn)?)
        })
    }
}

/// These methods are used by administrators to edit account characteristics
pub mod admin {
    use diesel::prelude::*;

    use crate::actions::utils::log_mod_action;
    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::models::users::ModeratorUserWrapper;
    use crate::schema::*;

    /// These methods allow checking and modifying the current registration limits
    pub mod registration_limit {
        use diesel::prelude::*;
        use std::convert::TryFrom;

        use crate::actions::utils::log_mod_action;
        use crate::models;
        use crate::models::error::IntertextualError;
        use crate::schema::*;

        /// Gets the information about the current registration status
        pub enum RegistrationLimit {
            /// Registrations are disabled
            RegistrationsDisabled,
            /// Registrations are enabled without any limit
            RegistrationsEnabledWithoutLimit,
            /// Registrations are enabled with the given limit, and the given current number of registrations are considered by the limit
            RegistrationsEnabledWithLimit {
                hours: i64,
                max_registrations: i64,
                current_registrations: i64,
            },
        }

        /// Gets the information about the new registration status to use
        pub enum NewRegistrationLimit {
            /// Disable registrations
            RegistrationsDisabled,
            /// Enable registrations without any limit
            RegistrationsEnabledWithoutLimit,
            /// Enable registrations with a limit
            RegistrationsEnabledWithLimit {
                /// Number of hours until the account is not considered anymore (rolling window)
                hours: i64,
                /// Max number of registrations before registrations are disabled
                max_registrations: i64,
            },
        }

        /// Get informations about the current registration limit
        pub fn get_registration_limit(
            conn: &PgConnection,
        ) -> Result<RegistrationLimit, IntertextualError> {
            conn.transaction::<RegistrationLimit, IntertextualError, _>(|| {
                let (enabled, limit_enabled, limit_hours, limit_max_registrations) =
                    registration_parameters::table
                        .select((
                            registration_parameters::registrations_enabled,
                            registration_parameters::limit_enabled,
                            registration_parameters::limit_hours,
                            registration_parameters::limit_max_registrations,
                        ))
                        .get_result::<(bool, bool, i32, i32)>(conn)?;
                let limit_hours = i64::from(limit_hours);
                let limit_max_registrations = i64::from(limit_max_registrations);
                if !enabled {
                    return Ok(RegistrationLimit::RegistrationsDisabled);
                }
                if !limit_enabled {
                    return Ok(RegistrationLimit::RegistrationsEnabledWithoutLimit);
                }
                let earliest_date =
                    chrono::Utc::now().naive_utc() - chrono::Duration::hours(limit_hours);
                let created_accounts = users::table
                    .filter(users::account_created_at.gt(earliest_date))
                    .count()
                    .get_result::<i64>(conn)?;
                Ok(RegistrationLimit::RegistrationsEnabledWithLimit {
                    current_registrations: created_accounts,
                    hours: limit_hours,
                    max_registrations: limit_max_registrations,
                })
            })
        }

        type EntryType = (uuid::Uuid, bool, bool, i32, i32);

        /// Simply re-enables the registrations, without modifying any existing values
        pub fn enable_registrations(
            log_entry: models::admin::NewModerationAction,
            conn: &PgConnection,
        ) -> Result<models::admin::ModerationAction, IntertextualError> {
            conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
                diesel::update(registration_parameters::table)
                    .set(registration_parameters::registrations_enabled.eq(true))
                    .get_result::<EntryType>(conn)?;

                // Store the action in the moderation log
                let mod_action = log_mod_action(log_entry, conn)?;

                Ok(mod_action)
            })
        }

        /// Updates the registration limit to the given value
        pub fn set_registration_limit(
            new_limit: NewRegistrationLimit,
            log_entry: models::admin::NewModerationAction,
            conn: &PgConnection,
        ) -> Result<models::admin::ModerationAction, IntertextualError> {
            conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
                match new_limit {
                    NewRegistrationLimit::RegistrationsDisabled => {
                        diesel::update(registration_parameters::table)
                            .set(registration_parameters::registrations_enabled.eq(false))
                            .get_result::<EntryType>(conn)?
                    }
                    NewRegistrationLimit::RegistrationsEnabledWithoutLimit => {
                        diesel::update(registration_parameters::table)
                            .set((
                                registration_parameters::registrations_enabled.eq(true),
                                registration_parameters::limit_enabled.eq(false),
                            ))
                            .get_result::<EntryType>(conn)?
                    }
                    NewRegistrationLimit::RegistrationsEnabledWithLimit {
                        hours,
                        max_registrations,
                    } => diesel::update(registration_parameters::table)
                        .set((
                            registration_parameters::registrations_enabled.eq(true),
                            registration_parameters::limit_enabled.eq(true),
                            registration_parameters::limit_hours
                                .eq(i32::try_from(hours).unwrap_or(1)),
                            registration_parameters::limit_max_registrations
                                .eq(i32::try_from(max_registrations).unwrap_or(0)),
                        ))
                        .get_result::<EntryType>(conn)?,
                };

                // Store the action in the moderation log
                let mod_action = log_mod_action(log_entry, conn)?;

                Ok(mod_action)
            })
        }
    }

    /// List of sorting modes used while exploring the global user list.
    pub enum UsersSortMode {
        /// Sort by username (A first)
        UsernameAsc,
        /// Sort by username (Z first)
        UsernameDesc,
        /// Sort by registration date (oldest first)
        RegistrationDateAsc,
        /// Sort by registration date (newest first)
        RegistrationDateDesc,
    }

    /// Finds the quantity of users in the database
    ///
    /// Note : this method should only be used in the administration panels
    pub fn find_all_users_count(conn: &PgConnection) -> Result<i64, IntertextualError> {
        Ok(users::table.count().get_result::<i64>(conn)?)
    }

    /// Finds the quantity of visible users in the database
    ///
    /// Note : this method should only be used in the administration panels
    pub fn find_all_visible_users_count(conn: &PgConnection) -> Result<i64, IntertextualError> {
        let result = users::table
            .filter(users::deactivated_by_user.eq(false))
            .filter(
                users::banned_until
                    .lt(chrono::Utc::now().naive_utc())
                    .or(users::banned_until.is_null()),
            )
            .count()
            .get_result::<i64>(conn)?;
        Ok(result)
    }

    /// Gets the user account registration times between the given start and end dates
    pub fn find_all_user_registration_times_between(
        start: chrono::NaiveDateTime,
        end: chrono::NaiveDateTime,
        conn: &PgConnection,
    ) -> Result<Vec<chrono::NaiveDateTime>, IntertextualError> {
        let result = users::table
            .filter(users::account_created_at.gt(start))
            .filter(users::account_created_at.le(end))
            .select(users::account_created_at)
            .load::<chrono::NaiveDateTime>(conn)?;
        Ok(result)
    }

    /// Finds the next `limit` users in the database, skipping the first `start` and using the given `sort_mode`
    ///
    /// Note : this method should only be used in the administration panels
    pub fn find_all_users_in_range(
        start: i64,
        limit: i64,
        sort_mode: UsersSortMode,
        conn: &PgConnection,
    ) -> Result<Vec<models::users::User>, IntertextualError> {
        Ok(match sort_mode {
            UsersSortMode::UsernameAsc => users::table
                .offset(start)
                .limit(limit)
                .order(users::username.asc())
                .load::<models::users::User>(conn)?,
            UsersSortMode::UsernameDesc => users::table
                .offset(start)
                .limit(limit)
                .order(users::username.desc())
                .load::<models::users::User>(conn)?,
            UsersSortMode::RegistrationDateAsc => users::table
                .offset(start)
                .limit(limit)
                .order(users::account_created_at.asc())
                .load::<models::users::User>(conn)?,
            UsersSortMode::RegistrationDateDesc => users::table
                .offset(start)
                .limit(limit)
                .order(users::account_created_at.desc())
                .load::<models::users::User>(conn)?,
        })
    }

    /// Gets a user information from the database based on the username
    ///
    /// Note : this method should only be used in the administration panels
    pub fn find_user_by_username_including_deactivated(
        username: &str,
        conn: &PgConnection,
    ) -> Result<Option<models::users::User>, IntertextualError> {
        let result = users::table
            .filter(users::username.ilike(username))
            .first::<models::users::User>(conn)
            .optional()?;
        Ok(result)
    }

    /// Gets a user information from the database based on the user ID
    ///
    /// Note : this method should only be used in the administration panels.
    pub fn find_user_by_id_including_deactivated(
        id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<Option<models::users::User>, IntertextualError> {
        let result = users::table
            .filter(users::id.eq(id))
            .first::<models::users::User>(conn)
            .optional()?;
        Ok(result)
    }

    /// Resets an user password to the given password.
    /// This will also flag the user as
    ///
    /// Note : this method should only be used in the administration panels.
    pub fn reset_user_password(
        moderator: ModeratorUserWrapper,
        user: models::users::User,
        new_password_hash: String,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<(models::users::User, models::admin::ModerationAction), IntertextualError> {
        conn.transaction::<(models::users::User, models::admin::ModerationAction), IntertextualError, _>(|| {
            let response = diesel::update(users::table.find(user.id))
                .set((
                    users::pw_hash.eq(new_password_hash),
                    users::password_reset_required.eq(true),
                ))
                .get_result::<models::users::User>(conn)?;

            // Show the action as modmail
            crate::actions::modmail::admins::send_new_system_modmail(
                moderator,
                user.id,
                format!(
                "System message. A moderator has reset your password for the following reason : {}",
                log_entry.message
            ),
                conn,
            )?;

            let mod_action = log_mod_action(log_entry, conn)?;

            Ok((response, mod_action))
        })
    }

    /// Mutes the user until a given end date
    pub fn mute_user_until(
        moderator: ModeratorUserWrapper,
        user: models::users::User,
        mute_end_date: chrono::NaiveDateTime,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<(models::users::User, models::admin::ModerationAction), IntertextualError> {
        conn.transaction::<(models::users::User, models::admin::ModerationAction), IntertextualError, _>(|| {
            let response = diesel::update(users::table.find(user.id))
                .set(users::muted_until.eq(mute_end_date))
                .get_result::<models::users::User>(conn)?;

            // Show the action as modmail
            crate::actions::modmail::admins::send_new_system_modmail(
                moderator,
                user.id,
                format!(
                    "System message. A moderator has muted your account for the following reason : {}",
                    log_entry.message
                ),
                conn,
            )?;

            // Store the action in the moderation log
            let mod_action = log_mod_action(log_entry, conn)?;

            // Notify the user
            let notification = models::notifications::NewNotification {
                target_user_id: user.id,
                notification_time: chrono::Utc::now().naive_utc(),
                internal_description: format!("administration_mute_u{}", user.id),
                message: "A moderator muted your account. You are now unable to review or comment on stories until the mute is lifted.".to_string(),
                link: "/moderation_contact/".to_string(),
                category: models::notifications::NotificationCategory::SystemMessage.into(),
            };
            crate::actions::notifications::modifications::add_notification(notification, conn)?;

            Ok((response, mod_action))
        })
    }

    /// Unmutes the given user
    pub fn unmute_user(
        moderator: ModeratorUserWrapper,
        user: models::users::User,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<(models::users::User, models::admin::ModerationAction), IntertextualError> {
        conn.transaction::<(models::users::User, models::admin::ModerationAction), IntertextualError, _>(|| {
            let response = diesel::update(users::table.find(user.id))
                .set(users::muted_until.eq(Option::<chrono::NaiveDateTime>::None))
                .get_result::<models::users::User>(conn)?;

            // Show the action as modmail
            crate::actions::modmail::admins::send_new_system_modmail(
                moderator,
                user.id,
                format!(
                    "System message. A moderator has unmuted your account for the following reason : {}",
                    log_entry.message
                ),
                conn,
            )?;

            // Store the action in the moderation log
            let mod_action = log_mod_action(log_entry, conn)?;

            // Notify the user
            let notification = models::notifications::NewNotification {
                target_user_id: user.id,
                notification_time: chrono::Utc::now().naive_utc(),
                internal_description: format!("administration_unmute_u{}", user.id),
                message: "A moderator un-muted your account".to_string(),
                link: "/moderation_contact/".to_string(),
                category: models::notifications::NotificationCategory::SystemMessage.into(),
            };
            crate::actions::notifications::modifications::add_notification(notification, conn)?;

            Ok((response, mod_action))
        })
    }

    /// Set a warning on the user's account until a given end date
    pub fn warn_user_until(
        moderator: ModeratorUserWrapper,
        user: models::users::User,
        warning_end_date: chrono::NaiveDateTime,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<(models::users::User, models::admin::ModerationAction), IntertextualError> {
        conn.transaction::<(models::users::User, models::admin::ModerationAction), IntertextualError, _>(|| {
            let response = diesel::update(users::table.find(user.id))
                .set(users::warned_until.eq(warning_end_date))
                .get_result::<models::users::User>(conn)?;
            diesel::insert_into(user_warnings::table)
                .values(models::admin::NewUserWarning {
                    moderator_id: moderator.id(),
                    user_id: user.id,
                    reason: log_entry.message.clone(),
                })
                .get_result::<models::admin::UserWarning>(conn)?;

            // Show the action as modmail
            crate::actions::modmail::admins::send_new_system_modmail(
                moderator,
                user.id,
                format!(
                    "System message. A moderator has warned your account for the following reason : {}",
                    log_entry.message
                ),
                conn,
            )?;

            // Store the action in the moderation log
            let mod_action = log_mod_action(log_entry, conn)?;

            // Notify the user
            let notification = models::notifications::NewNotification {
                target_user_id: user.id,
                notification_time: chrono::Utc::now().naive_utc(),
                internal_description: format!("administration_warn_u{}", user.id),
                message: "Your account has received a warning".to_string(),
                link: "/moderation_contact/".to_string(),
                category: models::notifications::NotificationCategory::SystemMessage.into(),
            };
            crate::actions::notifications::modifications::add_notification(notification, conn)?;

            Ok((response, mod_action))
        })
    }

    /// Remove any currently active warnings on the user's account
    pub fn unwarn_user(
        moderator: ModeratorUserWrapper,
        user: models::users::User,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<(models::users::User, models::admin::ModerationAction), IntertextualError> {
        conn.transaction::<(models::users::User, models::admin::ModerationAction), IntertextualError, _>(|| {
            let response = diesel::update(users::table.find(user.id))
                .set(users::warned_until.eq(Option::<chrono::NaiveDateTime>::None))
                .get_result::<models::users::User>(conn)?;

            // Show the action as modmail
            crate::actions::modmail::admins::send_new_system_modmail(
                moderator,
                user.id,
                format!(
                    "System message. A moderator has removed the current warning on your account for the following reason : {}",
                    log_entry.message
                ),
                conn,
            )?;

            // Store the action in the moderation log
            let mod_action = log_mod_action(log_entry, conn)?;

            // Note : we do not notify the user when a warning has been lifted

            Ok((response, mod_action))
        })
    }

    /// Ban the user's account until a given end date
    pub fn ban_user_until(
        moderator: ModeratorUserWrapper,
        user: models::users::User,
        ban_end_date: chrono::NaiveDateTime,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<(models::users::User, models::admin::ModerationAction), IntertextualError> {
        conn.transaction::<(models::users::User, models::admin::ModerationAction), IntertextualError, _>(|| {
            let response = diesel::update(users::table.find(user.id))
                .set(users::banned_until.eq(ban_end_date))
                .get_result::<models::users::User>(conn)?;

            // Show the action as modmail
            crate::actions::modmail::admins::send_new_system_modmail(
                moderator,
                user.id,
                format!(
                    "System message. Your account has been banned for the following reason : {}",
                    log_entry.message
                ),
                conn,
            )?;

            // Store the action in the moderation log
            let mod_action = log_mod_action(log_entry, conn)?;

            // Notify the user
            let notification = models::notifications::NewNotification {
                target_user_id: user.id,
                notification_time: chrono::Utc::now().naive_utc(),
                internal_description: format!("administration_ban_u{}", user.id),
                message: "Your account has been banned.".to_string(),
                link: "/moderation_contact/".to_string(),
                category: models::notifications::NotificationCategory::SystemMessage.into(),
            };
            crate::actions::notifications::modifications::add_notification(notification, conn)?;

            Ok((response, mod_action))
        })
    }

    /// Remove any existing ban on the user's account
    pub fn unban_user(
        moderator: ModeratorUserWrapper,
        user: models::users::User,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<(models::users::User, models::admin::ModerationAction), IntertextualError> {
        conn.transaction::<(models::users::User, models::admin::ModerationAction), IntertextualError, _>(|| {
            let response = diesel::update(users::table.find(user.id))
                .set(users::banned_until.eq(Option::<chrono::NaiveDateTime>::None))
                .get_result::<models::users::User>(conn)?;

            // Show the action as modmail
            crate::actions::modmail::admins::send_new_system_modmail(
                moderator,
                user.id,
                format!(
                    "System message. The ban on your account has been lifted for the following reason : {}",
                    log_entry.message
                ),
                conn,
            )?;

            // Store the action in the moderation log
            let mod_action = log_mod_action(log_entry, conn)?;

            // Notify the user
            let notification = models::notifications::NewNotification {
                target_user_id: user.id,
                notification_time: chrono::Utc::now().naive_utc(),
                internal_description: format!("administration_unban_u{}", user.id),
                message: "The ban of your account has been lifted.".to_string(),
                link: "/moderation_contact/".to_string(),
                category: models::notifications::NotificationCategory::SystemMessage.into(),
            };
            crate::actions::notifications::modifications::add_notification(notification, conn)?;

            Ok((response, mod_action))
        })
    }

    /// Change the user's access to administrator
    pub fn promote_user_to_administrator(
        moderator: ModeratorUserWrapper,
        user: models::users::User,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<(models::users::User, models::admin::ModerationAction), IntertextualError> {
        conn.transaction::<(models::users::User, models::admin::ModerationAction), IntertextualError, _>(|| {
            let response = diesel::update(users::table.find(user.id))
                .set(users::administrator.eq(true))
                .get_result::<models::users::User>(conn)?;

            // Show the action as modmail
            crate::actions::modmail::admins::send_new_system_modmail(
                moderator,
                user.id,
                format!(
                    "System message. Your account has been promoted to administrator for the following reason : {}",
                    log_entry.message
                ),
                conn,
            )?;

            // Store the action in the moderation log
            let mod_action = log_mod_action(log_entry, conn)?;

            // Notify the user
            let notification = models::notifications::NewNotification {
                target_user_id: user.id,
                notification_time: chrono::Utc::now().naive_utc(),
                internal_description: format!("administration_promote_u{}", user.id),
                message: "Your account has been promoted to administrator".to_string(),
                link: "/moderation_contact/".to_string(),
                category: models::notifications::NotificationCategory::SystemMessage.into(),
            };
            crate::actions::notifications::modifications::add_notification(notification, conn)?;

            Ok((response, mod_action))
        })
    }

    /// Change the user's access to normal user
    pub fn demote_user_from_administrator(
        moderator: ModeratorUserWrapper,
        user: models::users::User,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<(models::users::User, models::admin::ModerationAction), IntertextualError> {
        conn.transaction::<(models::users::User, models::admin::ModerationAction), IntertextualError, _>(|| {
            let response = diesel::update(users::table.find(user.id))
                .set(users::administrator.eq(false))
                .get_result::<models::users::User>(conn)?;

            // Show the action as modmail
            crate::actions::modmail::admins::send_new_system_modmail(
                moderator,
                user.id,
                format!(
                    "System message. Your account has been changed back to normal user status for the following reason : {}",
                    log_entry.message
                ),
                conn,
            )?;

            // Store the action in the moderation log
            let mod_action = log_mod_action(log_entry, conn)?;

            // Notify the user
            let notification = models::notifications::NewNotification {
                target_user_id: user.id,
                notification_time: chrono::Utc::now().naive_utc(),
                internal_description: format!("administration_demote_u{}", user.id),
                message: "Your account has been changed back to normal user status".to_string(),
                link: "/moderation_contact/".to_string(),
                category: models::notifications::NotificationCategory::SystemMessage.into(),
            };
            crate::actions::notifications::modifications::add_notification(notification, conn)?;

            Ok((response, mod_action))
        })
    }

    /// Request the deletion of an user's account
    pub fn request_user_deletion(
        moderator: ModeratorUserWrapper,
        user: models::users::User,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<(models::users::User, models::admin::ModerationAction), IntertextualError> {
        conn.transaction::<(models::users::User, models::admin::ModerationAction), IntertextualError, _>(|| {
            diesel::insert_into(account_deletion_log::table)
                .values(models::users::NewAccountDeletionLog {
                    account_id: user.id,
                    can_be_cancelled_by_user: false,
                    is_active_deletion_request: true,
                    request_information: "action requested by moderator".to_string(),
                })
                .get_result::<models::users::AccountDeletionLog>(conn)?;

            // Show the action as modmail
            crate::actions::modmail::admins::send_new_system_modmail(
                moderator,
                user.id,
                format!(
                    "System message. Your account has been marked for deletion for the following reason : {}",
                    log_entry.message
                ),
                conn,
            )?;

            // Store the action in the moderation log
            let mod_action = log_mod_action(log_entry, conn)?;

            // Notify the user
            let notification = models::notifications::NewNotification {
                target_user_id: user.id,
                notification_time: chrono::Utc::now().naive_utc(),
                internal_description: format!("administration_account_delete_u{}", user.id),
                message: "Your account was marked for deletion by a moderator".to_string(),
                link: "/moderation_contact/".to_string(),
                category: models::notifications::NotificationCategory::SystemMessage.into(),
            };
            crate::actions::notifications::modifications::add_notification(notification, conn)?;

            Ok((user, mod_action))
        })
    }

    /// Cancel any pending deletion request on this user's account
    pub fn cancel_user_deletion_request(
        moderator: ModeratorUserWrapper,
        user: models::users::User,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<(models::users::User, models::admin::ModerationAction), IntertextualError> {
        conn.transaction::<(models::users::User, models::admin::ModerationAction), IntertextualError, _>(|| {
            diesel::update(account_deletion_log::table)
                .filter(account_deletion_log::account_id.eq(user.id))
                .set(account_deletion_log::is_active_deletion_request.eq(false))
                .get_result::<models::users::AccountDeletionLog>(conn)?;
            diesel::insert_into(account_deletion_log::table)
                .values(models::users::NewAccountDeletionLog {
                    account_id: user.id,
                    can_be_cancelled_by_user: false,
                    is_active_deletion_request: false,
                    request_information: "action cancelled by moderator".to_string(),
                })
                .get_result::<models::users::AccountDeletionLog>(conn)?;

            // Show the action as modmail
            crate::actions::modmail::admins::send_new_system_modmail(
                moderator,
                user.id,
                format!(
                    "System message. Your account deletion has been cancelled for the following reason : {}",
                    log_entry.message
                ),
                conn,
            )?;

            // Store the action in the moderation log
            let mod_action = log_mod_action(log_entry, conn)?;

            // Notify the user
            let notification = models::notifications::NewNotification {
                target_user_id: user.id,
                notification_time: chrono::Utc::now().naive_utc(),
                internal_description: format!("administration_account_delete_cancel_u{}", user.id),
                message: "Your account deletion has been cancelled by a moderator".to_string(),
                link: "/moderation_contact/".to_string(),
                category: models::notifications::NotificationCategory::SystemMessage.into(),
            };
            crate::actions::notifications::modifications::add_notification(notification, conn)?;

            Ok((user, mod_action))
        })
    }
}
