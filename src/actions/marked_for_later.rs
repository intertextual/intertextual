//! # Marked for later
//!
//! This module contains methods used for the "mark for later" feature
use diesel::prelude::*;

use crate::models;
use crate::models::error::IntertextualError;
use crate::schema::*;

type UserApprovalType = (uuid::Uuid, uuid::Uuid, chrono::NaiveDateTime);

/// Gets the total number of stories "marked for later" by `user_id`
pub fn total_marked_for_later_count_given_by_user(
    user_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<i64, IntertextualError> {
    let approval_count = user_story_for_later::table
        .filter(user_story_for_later::user_id.eq(user_id))
        .count()
        .get_result::<i64>(conn)?;
    Ok(approval_count)
}

#[derive(Clone, Copy)]
/// Describes how to sort the marked for later stories
pub enum MarkedForLaterSortMode {
    /// Sort by title with A first and Z last
    StoryTitleAsc,
    /// Sort by title with Z first and A last
    StoryTitleDesc,
    /// Sort by most recently added to the list
    NewestFirst,
    /// Sort by least recently added to the list
    OldestFirst,
}

impl std::fmt::Display for MarkedForLaterSortMode {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                MarkedForLaterSortMode::NewestFirst => "recent",
                MarkedForLaterSortMode::OldestFirst => "oldest",
                MarkedForLaterSortMode::StoryTitleAsc => "title_asc",
                MarkedForLaterSortMode::StoryTitleDesc => "title_desc",
            }
        )
    }
}

impl MarkedForLaterSortMode {
    /// Get the sort mode from an URL string
    pub fn parse_url(url_param: Option<&str>) -> Option<MarkedForLaterSortMode> {
        match url_param {
            Some(s) if s == "recent" => Some(MarkedForLaterSortMode::NewestFirst),
            Some(s) if s == "oldest" => Some(MarkedForLaterSortMode::OldestFirst),
            Some(s) if s == "title_asc" => Some(MarkedForLaterSortMode::StoryTitleAsc),
            Some(s) if s == "title_desc" => Some(MarkedForLaterSortMode::StoryTitleDesc),
            _ => None,
        }
    }
}

/// Gets the given range of stories "marked for later" by `user_id`
pub fn marked_for_later_by_user(
    user_id: uuid::Uuid,
    start: i64,
    limit: i64,
    sort_mode: MarkedForLaterSortMode,
    conn: &PgConnection,
) -> Result<Vec<(models::stories::Story, chrono::NaiveDateTime)>, IntertextualError> {
    Ok(match sort_mode {
        MarkedForLaterSortMode::NewestFirst => user_story_for_later::table
            .filter(user_story_for_later::user_id.eq(user_id))
            .inner_join(stories::table)
            .select((stories::all_columns, user_story_for_later::approval_date))
            .order_by(user_story_for_later::approval_date.desc())
            .offset(start)
            .limit(limit)
            .load::<(models::stories::Story, chrono::NaiveDateTime)>(conn)?,
        MarkedForLaterSortMode::OldestFirst => user_story_for_later::table
            .filter(user_story_for_later::user_id.eq(user_id))
            .inner_join(stories::table)
            .select((stories::all_columns, user_story_for_later::approval_date))
            .order_by(user_story_for_later::approval_date.asc())
            .offset(start)
            .limit(limit)
            .load::<(models::stories::Story, chrono::NaiveDateTime)>(conn)?,
        MarkedForLaterSortMode::StoryTitleAsc => user_story_for_later::table
            .filter(user_story_for_later::user_id.eq(user_id))
            .inner_join(stories::table)
            .select((stories::all_columns, user_story_for_later::approval_date))
            .order_by(stories::title.asc())
            .offset(start)
            .limit(limit)
            .load::<(models::stories::Story, chrono::NaiveDateTime)>(conn)?,
        MarkedForLaterSortMode::StoryTitleDesc => user_story_for_later::table
            .filter(user_story_for_later::user_id.eq(user_id))
            .inner_join(stories::table)
            .select((stories::all_columns, user_story_for_later::approval_date))
            .order_by(stories::title.desc())
            .offset(start)
            .limit(limit)
            .load::<(models::stories::Story, chrono::NaiveDateTime)>(conn)?,
    })
}

/// Checks whether the given `user_id` has maked the `story_id` for later
pub fn has_marked_for_later(
    user_id: uuid::Uuid,
    story_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<bool, IntertextualError> {
    let kudo = user_story_for_later::table
        .filter(user_story_for_later::user_id.eq(user_id))
        .filter(user_story_for_later::story_id.eq(story_id))
        .first::<UserApprovalType>(conn)
        .optional()?;
    Ok(kudo.is_some())
}

/// Gets the raw data related to the "marked for later" feature
pub mod gdpr {
    use diesel::prelude::*;

    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::schema::*;

    /// Gets all the stories which the `user_id` marked for later
    pub fn all_user_story_for_later_from_user(
        user_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<Vec<(models::stories::Story, chrono::NaiveDateTime)>, IntertextualError> {
        let all_approvals = user_story_for_later::table
            .filter(user_story_for_later::user_id.eq(user_id))
            .inner_join(stories::table)
            .select((stories::all_columns, user_story_for_later::approval_date))
            .load::<(models::stories::Story, chrono::NaiveDateTime)>(conn)?;
        Ok(all_approvals)
    }
}

/// Methods to add or remove stories from your "marked for later" list
pub mod modifications {
    use diesel::prelude::*;

    use crate::models::error::IntertextualError;
    use crate::schema::*;

    type MarkedForLaterType = (uuid::Uuid, uuid::Uuid, chrono::NaiveDateTime);

    /// Mark the `story_id` for later by the user `user_id`
    pub fn mark_for_later(
        user_id: uuid::Uuid,
        story_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        diesel::insert_into(user_story_for_later::table)
            .values((
                user_story_for_later::user_id.eq(user_id),
                user_story_for_later::story_id.eq(story_id),
            ))
            .get_result::<MarkedForLaterType>(conn)?;
        Ok(())
    }

    /// Unmark the `story_id` for later by the user `user_id`
    pub fn unmark_for_later(
        user_id: uuid::Uuid,
        story_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        diesel::delete(
            user_story_for_later::table
                .filter(user_story_for_later::user_id.eq(user_id))
                .filter(user_story_for_later::story_id.eq(story_id)),
        )
        .get_result::<MarkedForLaterType>(conn)?;

        Ok(())
    }
}
