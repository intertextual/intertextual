//! # Notifications
//!
//! This module contains methods related to the user notification system
use diesel::{dsl::count, prelude::*};

use crate::models;
use crate::models::error::IntertextualError;
use crate::schema::*;

/// Finds the notification `notification_id`, if and only if it is targeted at the user `user_id`
pub fn find_notification_by_id(
    user_id: uuid::Uuid,
    notification_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<Option<models::notifications::Notification>, IntertextualError> {
    // Note : while the author ID is not strictly necessary for this, we ABSOLUTELY DO NOT want to show the notifications of another user.
    // Ensuring that we request both a user and a notification should prevent us from displaying notifications mistakenly.
    let result = notifications::table
        .filter(notifications::target_user_id.eq(user_id))
        .filter(notifications::notification_time.lt(chrono::Utc::now().naive_utc()))
        .filter(notifications::id.eq(notification_id))
        .first::<models::notifications::Notification>(conn)
        .optional()?;
    Ok(result)
}

/// Finds the unread notification count for the user `user_id`
pub fn get_unread_notification_count_by_user_id(
    user_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<i64, IntertextualError> {
    let result = notifications::table
        .filter(notifications::target_user_id.eq(user_id))
        .filter(notifications::read.eq(false))
        .filter(notifications::notification_time.lt(chrono::Utc::now().naive_utc()))
        .count()
        .get_result::<i64>(conn)?;
    Ok(result)
}

/// Finds all the unread notifications for the user `user_id`
pub fn find_all_unread_notifications_by_user_id(
    user_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<Vec<models::notifications::Notification>, IntertextualError> {
    let result = notifications::table
        .filter(notifications::target_user_id.eq(user_id))
        .filter(notifications::read.eq(false))
        .filter(notifications::notification_time.lt(chrono::Utc::now().naive_utc()))
        .order(notifications::notification_time.desc())
        .load::<models::notifications::Notification>(conn)?;
    Ok(result)
}

/// Finds the latest unread notifications for the user `user_id` with a limit.
pub fn find_latest_unread_notifications_by_user_id(
    user_id: uuid::Uuid,
    limit: i64,
    conn: &PgConnection,
) -> Result<Vec<models::notifications::Notification>, IntertextualError> {
    let result = notifications::table
        .filter(notifications::target_user_id.eq(user_id))
        .filter(notifications::read.eq(false))
        .filter(notifications::notification_time.lt(chrono::Utc::now().naive_utc()))
        .order(notifications::notification_time.desc())
        .limit(limit)
        .load::<models::notifications::Notification>(conn)?;
    Ok(result)
}

/// Finds the total notification count for the user `user_id`
pub fn get_all_notifications_count_by_user_id(
    user_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<i64, IntertextualError> {
    let result = notifications::table
        .filter(notifications::target_user_id.eq(user_id))
        .filter(notifications::notification_time.lt(chrono::Utc::now().naive_utc()))
        .select(count(notifications::id))
        .get_result(conn)?;
    Ok(result)
}

/// Finds the next `limit` notifications for the user `user_id`, skipping the first `start` notifications
pub fn find_all_notifications_by_user_id(
    user_id: uuid::Uuid,
    start: i64,
    limit: i64,
    conn: &PgConnection,
) -> Result<Vec<models::notifications::Notification>, IntertextualError> {
    let result = notifications::table
        .filter(notifications::target_user_id.eq(user_id))
        .filter(notifications::notification_time.lt(chrono::Utc::now().naive_utc()))
        .order(notifications::notification_time.desc())
        .offset(start)
        .limit(limit)
        .load::<models::notifications::Notification>(conn)?;
    Ok(result)
}

/// Modifies the notifications (usually by creating one or marking one as handled)
pub mod modifications {
    use diesel::prelude::*;

    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::schema::*;

    /// Adds the given story publication notification to the database
    pub fn add_chapter_publication_notification(
        story_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        diesel::insert_into(pending_notification_generation_requests::table)
            .values((
                pending_notification_generation_requests::request_type.eq("chapter_created"),
                pending_notification_generation_requests::target_id.eq(story_id),
            ))
            .execute(conn)?;
        Ok(())
    }

    /// Adds the given chapter publication notification to the database
    pub fn add_chapter_update_notification(
        chapter_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        diesel::insert_into(pending_notification_generation_requests::table)
            .values((
                pending_notification_generation_requests::request_type.eq("chapter_updated"),
                pending_notification_generation_requests::target_id.eq(chapter_id),
            ))
            .execute(conn)?;
        Ok(())
    }

    /// Adds the given chapter publication notification to the database
    pub fn new_recommendation_notification(
        chapter_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        diesel::insert_into(pending_notification_generation_requests::table)
            .values((
                pending_notification_generation_requests::request_type.eq("recommendation_created"),
                pending_notification_generation_requests::target_id.eq(chapter_id),
            ))
            .execute(conn)?;
        Ok(())
    }

    /// Adds the given `new_notification` to the database, but only if a notification with this internal
    /// description wasn't already sent to the user.
    ///
    /// Important : if the message is different, then the message will NOT be updated, and the first
    /// notification's message will be used. Ensure that you use different internal descriptions for
    /// notifications with different messages when this should be taken into account.
    ///
    /// Note : When using this method within another action, ensure that they are wrapped together into a
    /// transaction using [`diesel::connection::Connection::transaction`]
    pub fn add_notification_if_not_already_sent(
        new_notification: models::notifications::NewNotification,
        conn: &PgConnection,
    ) -> Result<models::notifications::Notification, IntertextualError> {
        // Check if this notification already exists
        let existing_notification = notifications::table
            .filter(notifications::internal_description.eq(&new_notification.internal_description))
            .filter(notifications::target_user_id.eq(&new_notification.target_user_id))
            .get_result::<models::notifications::Notification>(conn)
            .optional()?;
        match existing_notification {
            Some(notification) => Ok(notification),
            None => {
                let created_notification = diesel::insert_into(notifications::table)
                    .values(new_notification)
                    .get_result::<models::notifications::Notification>(conn)?;
                Ok(created_notification)
            }
        }
    }

    /// Adds the given `new_notification` to the database
    ///
    /// Note : When using this method within another action, ensure that they are wrapped together into a
    /// transaction using [`diesel::connection::Connection::transaction`]
    ///
    /// Note : this will clean all unread notifications with the same [`models::notifications::Notification::internal_description`], for this user id
    pub fn add_notification(
        new_notification: models::notifications::NewNotification,
        conn: &PgConnection,
    ) -> Result<models::notifications::Notification, IntertextualError> {
        // Remove notifications with the same key
        clean_unread_notifications_with_internal_description_for_user(
            &new_notification.internal_description,
            new_notification.target_user_id,
            conn,
        )?;

        let created_notification = diesel::insert_into(notifications::table)
            .values(new_notification)
            .get_result::<models::notifications::Notification>(conn)?;
        Ok(created_notification)
    }

    /// Clean all unread notifications with the same [`models::notifications::Notification::internal_description`] for the given `user_id`
    ///
    /// Note : When using this method within another action, ensure that they are wrapped together into a
    /// transaction using [`diesel::connection::Connection::transaction`]
    pub fn clean_unread_notifications_with_internal_description_for_user(
        internal_description: &str,
        user_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        diesel::delete(
            notifications::table
                .filter(notifications::internal_description.eq(internal_description))
                .filter(notifications::target_user_id.eq(user_id))
                .filter(
                    notifications::notification_time
                        .ge(chrono::Utc::now().naive_utc())
                        .or(notifications::read.eq(false)),
                ),
        )
        .get_result::<models::notifications::Notification>(conn)
        .optional()?;
        Ok(())
    }

    /// Clean all unread notifications with the same [`models::notifications::Notification::internal_description`], regardless of the target user id
    ///
    /// Note : When using this method within another action, ensure that they are wrapped together into a
    /// transaction using [`diesel::connection::Connection::transaction`]
    pub fn clean_all_unread_notifications_with_internal_description(
        internal_description: &str,
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        diesel::delete(
            notifications::table
                .filter(notifications::internal_description.eq(internal_description))
                .filter(
                    notifications::notification_time
                        .ge(chrono::Utc::now().naive_utc())
                        .or(notifications::read.eq(false)),
                ),
        )
        .get_result::<models::notifications::Notification>(conn)
        .optional()?;
        Ok(())
    }

    /// Clean all unread notifications with the same [`models::notifications::Notification::internal_description`] for all users,
    /// except for the user whose IDs are in the list
    ///
    /// Note : When using this method within another action, ensure that they are wrapped together into a
    /// transaction using [`diesel::connection::Connection::transaction`]
    pub fn clean_all_unread_notifications_with_internal_description_for_all_users_except(
        internal_description: &str,
        user_id_list: &[uuid::Uuid],
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        diesel::delete(
            notifications::table
                .filter(notifications::internal_description.eq(internal_description))
                .filter(diesel::dsl::not(
                    notifications::target_user_id.eq(diesel::dsl::any(user_id_list)),
                ))
                .filter(
                    notifications::notification_time
                        .ge(chrono::Utc::now().naive_utc())
                        .or(notifications::read.eq(false)),
                ),
        )
        .get_result::<models::notifications::Notification>(conn)
        .optional()?;
        Ok(())
    }

    /// Set the given notification as unread by the user
    pub fn set_unread(
        notification: models::notifications::Notification,
        conn: &PgConnection,
    ) -> Result<models::notifications::Notification, IntertextualError> {
        let updated_notification = diesel::update(
            notifications::table
                .filter(notifications::notification_time.lt(chrono::Utc::now().naive_utc()))
                .find(notification.id),
        )
        .set(notifications::read.eq(false))
        .get_result::<models::notifications::Notification>(conn)?;
        Ok(updated_notification)
    }

    /// Set the given notification as read by the user
    pub fn set_read(
        notification: models::notifications::Notification,
        conn: &PgConnection,
    ) -> Result<models::notifications::Notification, IntertextualError> {
        let updated_notification = diesel::update(
            notifications::table
                .filter(notifications::notification_time.lt(chrono::Utc::now().naive_utc()))
                .find(notification.id),
        )
        .set(notifications::read.eq(true))
        .get_result::<models::notifications::Notification>(conn)?;
        Ok(updated_notification)
    }

    /// Set all notifications of the user `user_id` as read
    pub fn set_all_read(user_id: uuid::Uuid, conn: &PgConnection) -> Result<(), IntertextualError> {
        diesel::update(
            notifications::table
                .filter(notifications::notification_time.lt(chrono::Utc::now().naive_utc()))
                .filter(notifications::target_user_id.eq(user_id)),
        )
        .set(notifications::read.eq(true))
        .get_result::<models::notifications::Notification>(conn)
        .optional()?;
        Ok(())
    }
}

/// These methods should be called automatically by a background thread or a script, but not manually by an user or moderator action
pub mod automated_actions {
    use chrono::NaiveDateTime;
    use diesel::prelude::*;

    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::schema::*;

    /// Represents a notification stored in the database
    #[derive(Debug, Identifiable, Queryable)]
    #[table_name = "pending_notification_generation_requests"]
    pub struct PendingNotificationGenerationRequest {
        /// The ID of the notification
        pub id: uuid::Uuid,
        /// The date at which the request has been made
        pub request_time: NaiveDateTime,
        /// The internal description of the notification. Used for filtering, should not be shown in the UI
        pub request_type: String,
        /// The ID to use as basis for the request.
        pub target_id: uuid::Uuid,
        /// The worker ID (if it is set), or null if not.
        pub currently_handled_by_task_worker_index: Option<i32>,
    }

    pub fn get_next_request(
        task_worker_index: i32,
        conn: &PgConnection,
    ) -> Result<Option<PendingNotificationGenerationRequest>, IntertextualError> {
        conn.transaction::<Option<PendingNotificationGenerationRequest>, IntertextualError, _>(|| {
            let result = pending_notification_generation_requests::table
                .filter(pending_notification_generation_requests::currently_handled_by_task_worker_index.eq(task_worker_index))
                .first::<PendingNotificationGenerationRequest>(conn)
                .optional()?;
            if result.is_some() {
                return Ok(result);
            }
            let result = pending_notification_generation_requests::table
                .filter(pending_notification_generation_requests::currently_handled_by_task_worker_index.is_null())
                .order_by(pending_notification_generation_requests::request_time.asc())
                .first::<PendingNotificationGenerationRequest>(conn)
                .optional()?;
            if let Some(value) = result {
                diesel::update(pending_notification_generation_requests::table)
                    .filter(pending_notification_generation_requests::id.eq(value.id))
                    .set(pending_notification_generation_requests::currently_handled_by_task_worker_index.eq(task_worker_index))
                    .execute(conn)?;
                return Ok(Some(value))
            }
            Ok(None)
        })
    }

    pub fn handle_request(
        request_id: uuid::Uuid,
        task_worker_index: i32,
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        let request = pending_notification_generation_requests::table
            .filter(pending_notification_generation_requests::id.eq(request_id))
            .filter(
                pending_notification_generation_requests::currently_handled_by_task_worker_index
                    .eq(task_worker_index),
            )
            .first::<PendingNotificationGenerationRequest>(conn);

        if request.is_err() {
            diesel::delete(pending_notification_generation_requests::table.find(request_id))
                .execute(conn)?;
        }

        let request = request?;

        match request.request_type.as_str() {
            "chapter_created" => {
                if let Ok(chapter) = chapters::table
                    .find(request.target_id)
                    .first::<models::stories::Chapter>(conn)
                {
                    let _ =
                        crate::actions::follows::notifications::notify_chapter_publication_change(
                            &chapter.show_publicly_after_date,
                            &chapter,
                            conn,
                        );
                }
            }
            "chapter_updated" => {
                if let Ok(chapter) = chapters::table
                    .find(request.target_id)
                    .first::<models::stories::Chapter>(conn)
                {
                    let _ = crate::actions::follows::notifications::notify_chapter_updated(
                        &chapter, conn,
                    )?;
                }
            }
            "recommendation_created" => {
                if let Ok(recommendation) = recommendations::table
                    .find(request.target_id)
                    .first::<models::recommendations::Recommendation>(conn)
                {
                    let _ = crate::actions::follows::notifications::notify_new_recommendation(
                        &recommendation,
                        conn,
                    );
                }
            }
            any => {
                log::error!(
                    "Could not handle request '{}' : request type {} is not a known request type",
                    request.id,
                    any
                );
                // Delete the task
                diesel::delete(pending_notification_generation_requests::table.find(request_id))
                    .execute(conn)?;
                return Err(IntertextualError::InternalServerError);
            }
        }

        // Delete the task
        diesel::delete(pending_notification_generation_requests::table.find(request_id))
            .execute(conn)?;
        Ok(())
    }
}
