use diesel::prelude::*;

use crate::models::error::IntertextualError;
use crate::schema::*;

/// Finds the number of hits for the chapter `chapter_id`
pub fn find_hit_counts_from_several(
    chapter_ids: &[uuid::Uuid],
    conn: &PgConnection,
) -> Result<i64, IntertextualError> {
    let result = story_hits::table
        .filter(story_hits::chapter_id.eq_any(chapter_ids))
        .select(diesel::dsl::count(story_hits::id))
        .get_result::<i64>(conn)?;
    Ok(result)
}

/// Finds the timestamp for every hit for the chapter `chapter_id`
pub fn find_hits_through_time(
    chapter_ids: &[uuid::Uuid],
    conn: &PgConnection,
) -> Result<Vec<chrono::NaiveDateTime>, IntertextualError> {
    let result = story_hits::table
        .filter(story_hits::chapter_id.eq_any(chapter_ids))
        .select(story_hits::hit_date)
        .load::<chrono::NaiveDateTime>(conn)?;
    Ok(result)
}

/// These methods are used to handle modifications of the story hit counts
pub mod modifications {
    use diesel::prelude::*;

    use crate::models::error::IntertextualError;
    use crate::schema::*;

    /// Represents a new hit on a chapter, for insertion in the database
    #[derive(Insertable)]
    #[table_name = "story_hits"]
    pub struct NewHit {
        /// The chapter that got a hit
        pub chapter_id: uuid::Uuid,
    }

    /// Add a hit for the given chapter.
    pub fn add_hit(chapter_id: uuid::Uuid, conn: &PgConnection) -> Result<(), IntertextualError> {
        diesel::insert_into(story_hits::table)
            .values(NewHit { chapter_id })
            .get_result::<(i32, uuid::Uuid, chrono::NaiveDateTime)>(conn)?;
        Ok(())
    }
}

/// These methods are used by administrators to check user approvals
pub mod admin {
    use diesel::prelude::*;

    use crate::models::error::IntertextualError;
    use crate::schema::*;

    /// Gets the total number of hits
    pub fn find_total_hit_count(conn: &PgConnection) -> Result<i64, IntertextualError> {
        let result = story_hits::table.count().get_result::<i64>(conn)?;
        Ok(result)
    }

    /// Gets the times where any chapter received a hit
    pub fn find_all_hit_times_between(
        start: chrono::NaiveDateTime,
        end: chrono::NaiveDateTime,
        conn: &PgConnection,
    ) -> Result<Vec<chrono::NaiveDateTime>, IntertextualError> {
        let result = story_hits::table
            .filter(story_hits::hit_date.gt(start))
            .filter(story_hits::hit_date.le(end))
            .select(story_hits::hit_date)
            .load::<chrono::NaiveDateTime>(conn)?;
        Ok(result)
    }
}
