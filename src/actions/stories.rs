//! # Stories
//!
//! This module contains methods used to retrieve stories or chapters, or edit these stories and chapters
use diesel::prelude::*;

use crate::models;
use crate::models::error::IntertextualError;
use crate::models::filter::FilterMode;
use crate::models::stories::CreativeRole;
use crate::models::stories::StorySortMode;
use crate::schema::*;

use super::access;

macro_rules! add_story_id_filter {
    ($query: ident, $story_id: ident, $filter: ident, $conn: ident) => {
        match $filter {
            FilterMode::Standard => {
                $query = $query
                    .filter(chapters::show_publicly_after_date.lt(chrono::Utc::now().naive_utc()))
                    .filter(chapters::moderator_locked.eq(false));
            }
            FilterMode::IncludeDirectAccess { own_user_id } => {
                if user_to_story_associations::table
                    .filter(user_to_story_associations::story_id.eq($story_id))
                    .filter(user_to_story_associations::user_id.eq(own_user_id))
                    .filter(
                        user_to_story_associations::creative_role
                            .ge(CreativeRole::min_unpublished_access()),
                    )
                    .select((
                        user_to_story_associations::story_id,
                        user_to_story_associations::user_id,
                    ))
                    .first::<(uuid::Uuid, uuid::Uuid)>($conn)
                    .optional()?
                    .is_none()
                {
                    $query = $query.filter(
                        chapters::show_publicly_after_date
                            .lt(chrono::Utc::now().naive_utc())
                            .and(chapters::moderator_locked.eq(false)),
                    );
                }
            }
            FilterMode::BypassFilters => {}
        }
    };
}

macro_rules! add_existing_id_filter {
    ($query: ident, $filter: ident) => {
        match $filter {
            FilterMode::Standard => {
                $query = $query
                    .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
                    .filter(users::deactivated_by_user.eq(false))
                    .filter(
                        users::banned_until
                            .lt(chrono::Utc::now().naive_utc())
                            .or(users::banned_until.is_null()),
                    )
                    .filter(chapters::show_publicly_after_date.lt(chrono::Utc::now().naive_utc()))
                    .filter(chapters::moderator_locked.eq(false));
            }
            FilterMode::IncludeDirectAccess { own_user_id } => {
                $query = $query
                    .filter(
                        user_to_story_associations::creative_role
                            .eq(CreativeRole::author())
                            .and(users::deactivated_by_user.eq(false))
                            .or(users::id.eq(own_user_id)),
                    )
                    .filter(
                        user_to_story_associations::creative_role
                            .eq(CreativeRole::author())
                            .and(
                                users::banned_until
                                    .lt(chrono::Utc::now().naive_utc())
                                    .or(users::banned_until.is_null()),
                            )
                            .or(users::id.eq(own_user_id)),
                    )
                    .filter(
                        user_to_story_associations::creative_role
                            .eq(CreativeRole::author())
                            .and(
                                chapters::show_publicly_after_date
                                    .lt(chrono::Utc::now().naive_utc())
                                    .and(chapters::moderator_locked.eq(false)),
                            )
                            .or(user_to_story_associations::user_id.eq(own_user_id)),
                    );
            }
            FilterMode::BypassFilters => {}
        }
    };
}

pub(crate) use add_existing_id_filter;

/// Find the overall number of stories on the website, using the given `filter`
pub fn find_stories_quantity(
    filter: &FilterMode,
    conn: &PgConnection,
) -> Result<i64, IntertextualError> {
    let mut query = stories::table
        .inner_join(chapters::table)
        .inner_join(user_to_story_associations::table)
        .filter(
            user_to_story_associations::creative_role.ge(CreativeRole::min_unpublished_access()),
        )
        .inner_join(users::table.on(user_to_story_associations::user_id.eq(users::id)))
        .into_boxed();
    add_existing_id_filter!(query, filter);
    let result = query
        .select(stories::id)
        .distinct()
        .load::<uuid::Uuid>(conn)?;
    Ok(result.len() as i64)
}

/// Find the next `limit` stories on the website, skipping the first `start`, using the given `filter` and ordered based on `sort_mode`
pub fn find_stories_by_sort_mode(
    start: i64,
    limit: i64,
    sort_mode: StorySortMode,
    filter: &FilterMode,
    conn: &PgConnection,
) -> Result<Vec<models::stories::Story>, IntertextualError> {
    let mut query = stories::table
        .inner_join(chapters::table)
        .inner_join(user_to_story_associations::table)
        .filter(
            user_to_story_associations::creative_role.ge(CreativeRole::min_unpublished_access()),
        )
        .inner_join(users::table.on(user_to_story_associations::user_id.eq(users::id)))
        .into_boxed();
    add_existing_id_filter!(query, filter);
    let query = query.select(stories::all_columns).distinct();
    let query = match sort_mode {
        StorySortMode::TitleAsc => query.order(stories::title.asc()),
        StorySortMode::TitleDesc => query.order(stories::title.desc()),
        StorySortMode::InternalCreationAsc => {
            query.order(stories::story_internally_created_at.asc())
        }
        StorySortMode::InternalCreationDesc => {
            query.order(stories::story_internally_created_at.desc())
        }
        StorySortMode::PublicationAsc => query.order(stories::cached_first_publication_date.asc()),
        StorySortMode::PublicationDesc => {
            query.order(stories::cached_first_publication_date.desc())
        }
        StorySortMode::LastUpdateAsc => query.order(stories::cached_last_update_date.asc()),
        StorySortMode::LastUpdateDesc => query.order(stories::cached_last_update_date.desc()),
    };
    let query = query.offset(start).limit(limit);
    let result = query.load::<models::stories::Story>(conn)?;
    Ok(result)
}

/// Find all stories written by `author_id`, using the given `filter`
///
/// Note: This method doesn't need to check if the author is enabled and not banned,
/// as we assume that the author ID cannot be provided unless the user can already
/// access the author's information
pub fn find_all_stories_by_author(
    author_id: uuid::Uuid,
    filter: &FilterMode,
    conn: &PgConnection,
) -> Result<Vec<models::stories::Story>, IntertextualError> {
    let result = match filter {
        FilterMode::BypassFilters => user_to_story_associations::table
            .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
            .filter(user_to_story_associations::user_id.eq(author_id))
            .inner_join(stories::table.on(user_to_story_associations::story_id.eq(stories::id)))
            .select(stories::all_columns)
            .load::<models::stories::Story>(conn)?,
        FilterMode::IncludeDirectAccess { own_user_id } if &author_id == own_user_id => {
            user_to_story_associations::table
                .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
                .filter(user_to_story_associations::user_id.eq(author_id))
                .inner_join(stories::table.on(user_to_story_associations::story_id.eq(stories::id)))
                .select(stories::all_columns)
                .load::<models::stories::Story>(conn)?
        }
        FilterMode::IncludeDirectAccess { own_user_id } => {
            // Hmm. How to do that???
            let all_story_ids = user_to_story_associations::table
                .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
                .filter(user_to_story_associations::user_id.eq(author_id))
                .inner_join(stories::table.on(user_to_story_associations::story_id.eq(stories::id)))
                .select(stories::id)
                .load::<uuid::Uuid>(conn)?;
            let mut stories = Vec::<models::stories::Story>::with_capacity(all_story_ids.len());
            for story_id in all_story_ids {
                let available_directly_count = user_to_story_associations::table
                    .filter(user_to_story_associations::story_id.eq(story_id))
                    .filter(user_to_story_associations::user_id.eq(own_user_id))
                    .filter(
                        user_to_story_associations::creative_role
                            .ge(CreativeRole::min_unpublished_access()),
                    )
                    .count()
                    .first::<i64>(conn)?;
                if available_directly_count == 0 {
                    let available_overall_count = stories::table
                        .filter(stories::id.eq(story_id))
                        .inner_join(chapters::table)
                        .filter(
                            chapters::show_publicly_after_date.lt(chrono::Utc::now().naive_utc()),
                        )
                        .filter(chapters::moderator_locked.eq(false))
                        .count()
                        .first::<i64>(conn)?;
                    if available_overall_count == 0 {
                        continue;
                    }
                }
                stories.push(
                    stories::table
                        .find(story_id)
                        .first::<models::stories::Story>(conn)?,
                );
            }
            stories
        }
        FilterMode::Standard => stories::table
            .inner_join(
                user_to_story_associations::table
                    .on(user_to_story_associations::story_id.eq(stories::id)),
            )
            .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
            .filter(user_to_story_associations::user_id.eq(author_id))
            .inner_join(chapters::table)
            .filter(chapters::show_publicly_after_date.lt(chrono::Utc::now().naive_utc()))
            .filter(chapters::moderator_locked.eq(false))
            .distinct_on(stories::id)
            .select(stories::all_columns)
            .load::<models::stories::Story>(conn)?,
    };
    Ok(result)
}

/// Find the number of stories written by `author_id`, using the given `filter`
pub fn find_story_count_by_author(
    author_id: uuid::Uuid,
    filter: &FilterMode,
    conn: &PgConnection,
) -> Result<i64, IntertextualError> {
    let result = match filter {
        FilterMode::BypassFilters => stories::table
            .inner_join(
                user_to_story_associations::table
                    .on(user_to_story_associations::story_id.eq(stories::id)),
            )
            .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
            .filter(user_to_story_associations::user_id.eq(author_id))
            .select(stories::id)
            .distinct()
            .load::<uuid::Uuid>(conn)?
            .len(),
        FilterMode::IncludeDirectAccess { own_user_id } if &author_id == own_user_id => {
            stories::table
                .inner_join(
                    user_to_story_associations::table
                        .on(user_to_story_associations::story_id.eq(stories::id)),
                )
                .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
                .filter(user_to_story_associations::user_id.eq(author_id))
                .select(stories::id)
                .distinct()
                .load::<uuid::Uuid>(conn)?
                .len()
        }
        FilterMode::Standard | FilterMode::IncludeDirectAccess { own_user_id: _ } => stories::table
            .inner_join(
                user_to_story_associations::table
                    .on(user_to_story_associations::story_id.eq(stories::id)),
            )
            .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
            .filter(user_to_story_associations::user_id.eq(author_id))
            .inner_join(chapters::table)
            .filter(chapters::show_publicly_after_date.lt(chrono::Utc::now().naive_utc()))
            .filter(chapters::moderator_locked.eq(false))
            .select(stories::id)
            .distinct()
            .load::<uuid::Uuid>(conn)?
            .len(),
    };
    Ok(result as i64)
}

/// Find the number of collaboration stories on the website, using the given `filter`
pub fn find_all_collaboration_stories(
    filter: &FilterMode,
    conn: &PgConnection,
) -> Result<Vec<models::stories::Story>, IntertextualError> {
    let result = match filter {
        FilterMode::BypassFilters => stories::table
            .filter(stories::is_collaboration.eq(true))
            .inner_join(
                user_to_story_associations::table
                    .on(user_to_story_associations::story_id.eq(stories::id)),
            )
            .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
            .inner_join(users::table.on(user_to_story_associations::user_id.eq(users::id)))
            .distinct_on(stories::id)
            .select(stories::all_columns)
            .load::<models::stories::Story>(conn)?,
        FilterMode::IncludeDirectAccess { own_user_id } => stories::table
            .filter(stories::is_collaboration.eq(true))
            .inner_join(
                user_to_story_associations::table
                    .on(user_to_story_associations::story_id.eq(stories::id)),
            )
            .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
            .inner_join(users::table.on(user_to_story_associations::user_id.eq(users::id)))
            .filter(
                users::deactivated_by_user
                    .eq(false)
                    .or(users::id.eq(own_user_id)),
            )
            .filter(
                users::banned_until
                    .lt(chrono::Utc::now().naive_utc())
                    .or(users::banned_until.is_null())
                    .or(users::id.eq(own_user_id)),
            )
            .inner_join(chapters::table)
            .filter(chapters::show_publicly_after_date.lt(chrono::Utc::now().naive_utc()))
            .filter(chapters::moderator_locked.eq(false))
            .distinct_on(stories::id)
            .select(stories::all_columns)
            .load::<models::stories::Story>(conn)?,
        FilterMode::Standard => stories::table
            .filter(stories::is_collaboration.eq(true))
            .inner_join(
                user_to_story_associations::table
                    .on(user_to_story_associations::story_id.eq(stories::id)),
            )
            .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
            .inner_join(users::table.on(user_to_story_associations::user_id.eq(users::id)))
            .filter(users::deactivated_by_user.eq(false))
            .filter(
                users::banned_until
                    .lt(chrono::Utc::now().naive_utc())
                    .or(users::banned_until.is_null()),
            )
            .inner_join(chapters::table)
            .filter(chapters::show_publicly_after_date.lt(chrono::Utc::now().naive_utc()))
            .filter(chapters::moderator_locked.eq(false))
            .distinct_on(stories::id)
            .select(stories::all_columns)
            .load::<models::stories::Story>(conn)?,
    };
    Ok(result)
}

/// Find the story with the id `story_id`
pub fn find_story_by_id(
    story_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<Option<models::stories::Story>, IntertextualError> {
    let result = stories::table
        .find(story_id)
        .get_result::<models::stories::Story>(conn)
        .optional()?;
    Ok(result)
}

/// Find the story and chapter with the chapter id `chapter_id`
pub fn find_chapter_by_id(
    chapter_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<Option<(models::stories::Chapter, models::stories::Story)>, IntertextualError> {
    let result = chapters::table
        .find(chapter_id)
        .inner_join(stories::table)
        .get_result::<(models::stories::Chapter, models::stories::Story)>(conn)
        .optional()?;
    Ok(result)
}

/// Find the authors of story `story_id`
pub fn find_author_ids_by_story(
    story_id: uuid::Uuid,
    filter: &FilterMode,
    conn: &PgConnection,
) -> Result<Vec<uuid::Uuid>, IntertextualError> {
    let result = match filter {
        FilterMode::BypassFilters => user_to_story_associations::table
            .filter(user_to_story_associations::story_id.eq(story_id))
            .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
            .inner_join(users::table.on(user_to_story_associations::user_id.eq(users::id)))
            .select(users::id)
            .load::<uuid::Uuid>(conn)?,
        FilterMode::IncludeDirectAccess { own_user_id } => user_to_story_associations::table
            .filter(user_to_story_associations::story_id.eq(story_id))
            .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
            .inner_join(users::table.on(user_to_story_associations::user_id.eq(users::id)))
            .filter(
                users::deactivated_by_user
                    .eq(false)
                    .or(users::id.eq(own_user_id)),
            )
            .filter(
                users::banned_until
                    .lt(chrono::Utc::now().naive_utc())
                    .or(users::banned_until.is_null())
                    .or(users::id.eq(own_user_id)),
            )
            .select(users::id)
            .load::<uuid::Uuid>(conn)?,
        FilterMode::Standard => user_to_story_associations::table
            .filter(user_to_story_associations::story_id.eq(story_id))
            .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
            .inner_join(users::table.on(user_to_story_associations::user_id.eq(users::id)))
            .filter(users::deactivated_by_user.eq(false))
            .filter(
                users::banned_until
                    .lt(chrono::Utc::now().naive_utc())
                    .or(users::banned_until.is_null()),
            )
            .select(users::id)
            .load::<uuid::Uuid>(conn)?,
    };
    Ok(result)
}

/// Find the authors of story `story_id`
pub fn find_authors_by_story(
    story_id: uuid::Uuid,
    filter: &FilterMode,
    conn: &PgConnection,
) -> Result<Vec<models::users::User>, IntertextualError> {
    let result = match filter {
        FilterMode::BypassFilters => user_to_story_associations::table
            .filter(user_to_story_associations::story_id.eq(story_id))
            .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
            .inner_join(users::table.on(user_to_story_associations::user_id.eq(users::id)))
            .select(users::all_columns)
            .load::<models::users::User>(conn)?,
        FilterMode::IncludeDirectAccess { own_user_id } => user_to_story_associations::table
            .filter(user_to_story_associations::story_id.eq(story_id))
            .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
            .inner_join(users::table.on(user_to_story_associations::user_id.eq(users::id)))
            .filter(
                users::deactivated_by_user
                    .eq(false)
                    .or(users::id.eq(own_user_id)),
            )
            .filter(
                users::banned_until
                    .lt(chrono::Utc::now().naive_utc())
                    .or(users::banned_until.is_null())
                    .or(users::id.eq(own_user_id)),
            )
            .select(users::all_columns)
            .load::<models::users::User>(conn)?,
        FilterMode::Standard => user_to_story_associations::table
            .filter(user_to_story_associations::story_id.eq(story_id))
            .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
            .inner_join(users::table.on(user_to_story_associations::user_id.eq(users::id)))
            .filter(users::deactivated_by_user.eq(false))
            .filter(
                users::banned_until
                    .lt(chrono::Utc::now().naive_utc())
                    .or(users::banned_until.is_null()),
            )
            .select(users::all_columns)
            .load::<models::users::User>(conn)?,
    };
    Ok(result)
}

pub fn is_story_ongoing(
    story_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<bool, IntertextualError> {
    let ongoing_status = stories::table
        .filter(stories::id.eq(story_id))
        .select(stories::ongoing_status)
        .first::<i32>(conn)?;
    if models::stories::OngoingState::AlwaysOngoing.matches(&ongoing_status) {
        return Ok(true);
    }
    if models::stories::OngoingState::NeverOngoing.matches(&ongoing_status) {
        return Ok(false);
    }

    let chapters_count = chapters::table
        .filter(chapters::story_id.eq(story_id))
        .count()
        .first::<i64>(conn)?;
    let published_chapters_count = chapters::table
        .filter(chapters::story_id.eq(story_id))
        .filter(chapters::show_publicly_after_date.lt(chrono::Utc::now().naive_utc()))
        .count()
        .first::<i64>(conn)?;
    return Ok(published_chapters_count < chapters_count);
}

/// Check if the given `url_fragment` can be used for a new story by author `author_id`
pub fn is_author_story_url_available(
    author_id: uuid::Uuid,
    url_fragment: &str,
    conn: &PgConnection,
) -> Result<bool, IntertextualError> {
    let matched_count = stories::table
        .inner_join(
            user_to_story_associations::table
                .on(user_to_story_associations::story_id.eq(stories::id)),
        )
        .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
        .filter(user_to_story_associations::user_id.eq(author_id))
        .filter(stories::url_fragment.eq(url_fragment))
        .count()
        .first::<i64>(conn)?;
    Ok(matched_count == 0)
}

/// Check if the given `url_fragment` can be used for a new collaboration story
pub fn is_collaboration_story_url_available(
    url_fragment: &str,
    conn: &PgConnection,
) -> Result<bool, IntertextualError> {
    let matched_count = stories::table
        .filter(stories::is_collaboration.eq(true))
        .filter(stories::url_fragment.eq(url_fragment))
        .count()
        .first::<i64>(conn)?;
    Ok(matched_count == 0)
}

/// Find the non-collaboration story written by author `author_id` and with URL `url_fragment`
pub fn find_story_by_author_and_url(
    author_id: uuid::Uuid,
    url_fragment: &str,
    filter: &FilterMode,
    conn: &PgConnection,
) -> Result<Option<models::stories::Story>, IntertextualError> {
    let result = match filter {
        FilterMode::BypassFilters => stories::table
            .inner_join(
                user_to_story_associations::table
                    .on(user_to_story_associations::story_id.eq(stories::id)),
            )
            .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
            .filter(user_to_story_associations::user_id.eq(author_id))
            .filter(stories::url_fragment.eq(url_fragment))
            .select(stories::all_columns)
            .first::<models::stories::Story>(conn)
            .optional()?,
        FilterMode::IncludeDirectAccess { own_user_id } if own_user_id == &author_id => {
            stories::table
                .inner_join(
                    user_to_story_associations::table
                        .on(user_to_story_associations::story_id.eq(stories::id)),
                )
                .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
                .filter(user_to_story_associations::user_id.eq(author_id))
                .filter(stories::url_fragment.eq(url_fragment))
                .select(stories::all_columns)
                .first::<models::stories::Story>(conn)
                .optional()?
        }
        _ => stories::table
            .filter(stories::url_fragment.eq(url_fragment))
            .inner_join(
                user_to_story_associations::table
                    .on(user_to_story_associations::story_id.eq(stories::id)),
            )
            .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
            .inner_join(users::table.on(user_to_story_associations::user_id.eq(users::id)))
            .filter(user_to_story_associations::user_id.eq(author_id))
            .filter(users::deactivated_by_user.eq(false))
            .filter(
                users::banned_until
                    .lt(chrono::Utc::now().naive_utc())
                    .or(users::banned_until.is_null()),
            )
            .select(stories::all_columns)
            .first::<models::stories::Story>(conn)
            .optional()?,
    };
    Ok(result)
}

/// Find the collaboration story with URL `url_fragment`
pub fn find_collaboration_story_by_url(
    url_fragment: &str,
    filter: &FilterMode,
    conn: &PgConnection,
) -> Result<Option<models::stories::Story>, IntertextualError> {
    let result = match filter {
        FilterMode::BypassFilters => stories::table
            .filter(stories::is_collaboration.eq(true))
            .filter(stories::url_fragment.eq(url_fragment))
            .inner_join(
                user_to_story_associations::table
                    .on(user_to_story_associations::story_id.eq(stories::id)),
            )
            .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
            .select(stories::all_columns)
            .first::<models::stories::Story>(conn)
            .optional()?,
        FilterMode::IncludeDirectAccess { own_user_id } => stories::table
            .filter(stories::is_collaboration.eq(true))
            .filter(stories::url_fragment.eq(url_fragment))
            .inner_join(
                user_to_story_associations::table
                    .on(user_to_story_associations::story_id.eq(stories::id)),
            )
            .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
            .inner_join(users::table.on(user_to_story_associations::user_id.eq(users::id)))
            .filter(
                users::deactivated_by_user
                    .eq(false)
                    .or(users::id.eq(own_user_id)),
            )
            .filter(
                users::banned_until
                    .lt(chrono::Utc::now().naive_utc())
                    .or(users::banned_until.is_null())
                    .or(users::id.eq(own_user_id)),
            )
            .select(stories::all_columns)
            .first::<models::stories::Story>(conn)
            .optional()?,
        FilterMode::Standard => stories::table
            .filter(stories::is_collaboration.eq(true))
            .filter(stories::url_fragment.eq(url_fragment))
            .inner_join(
                user_to_story_associations::table
                    .on(user_to_story_associations::story_id.eq(stories::id)),
            )
            .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
            .inner_join(users::table.on(user_to_story_associations::user_id.eq(users::id)))
            .filter(users::deactivated_by_user.eq(false))
            .filter(
                users::banned_until
                    .lt(chrono::Utc::now().naive_utc())
                    .or(users::banned_until.is_null()),
            )
            .select(stories::all_columns)
            .first::<models::stories::Story>(conn)
            .optional()?,
    };

    Ok(result)
}

/// Finds all the chapter metadata of story `story_id`, respecting the given `filter`
pub fn find_chapters_metadata_by_story_id(
    story_id: uuid::Uuid,
    filter: &FilterMode,
    conn: &PgConnection,
) -> Result<Vec<models::stories::ChapterMetadata>, IntertextualError> {
    let mut query = chapters::table
        .filter(chapters::story_id.eq(story_id))
        .into_boxed();
    add_story_id_filter!(query, story_id, filter, conn);
    let result = query
        .select((
            chapters::id,
            chapters::chapter_internally_created_at,
            chapters::story_id,
            chapters::chapter_number,
            chapters::title,
            chapters::word_count,
            chapters::official_creation_date,
            chapters::update_date,
            chapters::show_publicly_after_date,
            chapters::moderator_locked,
        ))
        .order(chapters::chapter_number.asc())
        .load::<models::stories::ChapterMetadata>(conn)?;
    Ok(result)
}

/// Finds all the chapters of story `story_id`, respecting the given `filter`
pub fn find_chapters_by_story_id(
    story_id: uuid::Uuid,
    filter: &FilterMode,
    conn: &PgConnection,
) -> Result<Vec<models::stories::Chapter>, IntertextualError> {
    let mut query = chapters::table
        .filter(chapters::story_id.eq(story_id))
        .into_boxed();
    add_story_id_filter!(query, story_id, filter, conn);
    let result = query
        .order(chapters::chapter_number.asc())
        .load::<models::stories::Chapter>(conn)?;
    Ok(result)
}

/// Finds the chapter `chapter_number` of non-collaboration story by `author_id`, with URL `url_fragment`, respecting the given `filter`
pub fn find_chapter_by_author_url_and_number(
    author_id: uuid::Uuid,
    url_fragment: &str,
    chapter_number: i32,
    filter: &FilterMode,
    conn: &PgConnection,
) -> Result<Option<(models::stories::Story, models::stories::Chapter)>, IntertextualError> {
    let unfiltered_result = stories::table
        .inner_join(
            user_to_story_associations::table
                .on(user_to_story_associations::story_id.eq(stories::id)),
        )
        .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
        .inner_join(users::table.on(user_to_story_associations::user_id.eq(users::id)))
        .inner_join(chapters::table)
        .filter(user_to_story_associations::user_id.eq(author_id))
        .filter(stories::url_fragment.eq(url_fragment))
        .filter(chapters::chapter_number.eq(chapter_number))
        .select((
            users::all_columns,
            stories::all_columns,
            chapters::all_columns,
        ))
        .first::<(
            models::users::User,
            models::stories::Story,
            models::stories::Chapter,
        )>(conn)
        .optional()?;
    let (author, story, chapter) = match unfiltered_result {
        Some(r) => r,
        None => return Ok(None),
    };

    // Check if the chapter is published first
    if !author.deactivated_by_user
        && !author.is_banned()
        && !chapter.moderator_locked
        && chapter.is_published()
    {
        return Ok(Some((story, chapter)));
    }

    // The chapter isn't publsihed, check if the user has special access
    match filter {
        FilterMode::BypassFilters => Ok(Some((story, chapter))),
        FilterMode::IncludeDirectAccess { own_user_id } => {
            let creative_role = access::get_creative_role(story.id, *own_user_id, conn)?;
            if i32::from(creative_role) >= CreativeRole::min_unpublished_access() {
                Ok(Some((story, chapter)))
            } else {
                Ok(None)
            }
        }
        FilterMode::Standard => Ok(None),
    }
}

/// Finds the chapter `chapter_number` of collaboration story with URL `url_fragment`, respecting the given `filter`
pub fn find_collaboration_chapter_url_and_number(
    url_fragment: &str,
    chapter_number: i32,
    filter: &FilterMode,
    conn: &PgConnection,
) -> Result<Option<(models::stories::Story, models::stories::Chapter)>, IntertextualError> {
    let mut query = stories::table
        .filter(stories::is_collaboration.eq(true))
        .filter(stories::url_fragment.eq(url_fragment))
        .inner_join(user_to_story_associations::table)
        .filter(
            user_to_story_associations::creative_role.ge(CreativeRole::min_unpublished_access()),
        )
        .inner_join(chapters::table)
        .inner_join(users::table.on(user_to_story_associations::user_id.eq(users::id)))
        .filter(chapters::chapter_number.eq(chapter_number))
        .into_boxed();
    add_existing_id_filter!(query, filter);
    let result = query
        .select((stories::all_columns, chapters::all_columns))
        .first::<(models::stories::Story, models::stories::Chapter)>(conn)
        .optional()?;
    Ok(result)
}

/// Finds stories published between `start_date` and `end_date`, respecting the given `filter`
pub fn find_stories_in_range(
    start_date: chrono::NaiveDateTime,
    end_date: chrono::NaiveDateTime,
    filter: &FilterMode,
    conn: &PgConnection,
) -> Result<Vec<(models::stories::Story, Vec<models::stories::Chapter>)>, IntertextualError> {
    let mut query = stories::table
        .inner_join(user_to_story_associations::table)
        .filter(
            user_to_story_associations::creative_role.ge(CreativeRole::min_unpublished_access()),
        )
        .inner_join(chapters::table)
        .inner_join(users::table.on(user_to_story_associations::user_id.eq(users::id)))
        .filter(chapters::official_creation_date.gt(start_date))
        .filter(chapters::official_creation_date.le(end_date))
        .into_boxed();
    add_existing_id_filter!(query, filter);
    let results = query
        .order(stories::title)
        .select((stories::all_columns, chapters::all_columns))
        .distinct()
        .load::<(models::stories::Story, models::stories::Chapter)>(conn)?;
    let mut result: Vec<(models::stories::Story, Vec<models::stories::Chapter>)> = vec![];
    for (story, chapter) in results {
        match result.iter_mut().find(|(s, _)| s.id == story.id) {
            Some((_, ref mut chapters)) => {
                chapters.push(chapter);
            }
            None => {
                result.push((story, vec![chapter]));
            }
        }
    }
    Ok(result)
}

/// Finds whether the story `story_id` has a single chapter, respecting the given `filter`
pub fn story_is_standalone(
    story_id: uuid::Uuid,
    filter: &FilterMode,
    conn: &PgConnection,
) -> Result<bool, IntertextualError> {
    let story_is_multi_chapter = stories::table
        .find(story_id)
        .select(stories::is_multi_chapter)
        .get_result::<bool>(conn)?;
    if story_is_multi_chapter {
        return Ok(false);
    }
    let mut query = chapters::table
        .filter(chapters::story_id.eq(story_id))
        .into_boxed();
    add_story_id_filter!(query, story_id, filter, conn);
    let chapter_count = query.limit(2).load::<models::stories::Chapter>(conn)?.len();
    Ok(chapter_count <= 1)
}

/// Finds the previous chapter of the story `story_id`, starting from `chapter_number` and respecting the given `filter`
pub fn find_previous_chapter(
    story_id: uuid::Uuid,
    chapter_number: i32,
    filter: &FilterMode,
    conn: &PgConnection,
) -> Result<Option<models::stories::Chapter>, IntertextualError> {
    let mut query = chapters::table
        .filter(chapters::story_id.eq(story_id))
        .filter(chapters::chapter_number.lt(chapter_number))
        .into_boxed();
    add_story_id_filter!(query, story_id, filter, conn);
    let previous_chapter = query
        .order_by(chapters::chapter_number.desc())
        .get_result::<models::stories::Chapter>(conn)
        .optional()?;
    Ok(previous_chapter)
}

/// Finds the next chapter of the story `story_id`, starting from `chapter_number` and respecting the given `filter`
pub fn find_next_chapter(
    story_id: uuid::Uuid,
    chapter_number: i32,
    filter: &FilterMode,
    conn: &PgConnection,
) -> Result<Option<models::stories::Chapter>, IntertextualError> {
    let mut query = chapters::table
        .filter(chapters::story_id.eq(story_id))
        .filter(chapters::chapter_number.gt(chapter_number))
        .into_boxed();
    add_story_id_filter!(query, story_id, filter, conn);
    let next_chapter = query
        .order_by(chapters::chapter_number.asc())
        .get_result::<models::stories::Chapter>(conn)
        .optional()?;
    Ok(next_chapter)
}

/// These methods are used to handle modifications of the stories
pub mod modifications {
    use diesel::prelude::*;
    use std::ops::Add;

    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::models::formats::{RichTagline, SanitizedHtml};
    use crate::models::stories::CreativeRole;
    use crate::schema::*;

    /// Represents the raw data of a new story, for insertion in the database
    #[derive(Insertable)]
    #[table_name = "stories"]
    struct NewStoryRawData {
        /// The title of the story
        pub title: String,
        /// The URL of the story
        pub url_fragment: String,
        /// Whether this story is a collaboration or from a single author
        pub is_collaboration: bool,
        /// The description of the story
        pub description: RichTagline,
        /// Whether recommendations are enabled, enabled and not published, or disabled (see [`models::stories::RecommendationsState`] for details)
        pub recommendations_enabled: i32,
        /// Whether comments are enabled for this story
        pub comments_enabled: bool,
        /// Whether to show the "chapter index" screen even there is a single chapter for this story or not
        pub is_multi_chapter: bool,
        /// Whether to show the "ongoing" tag on this story (see [`models::stories::OngoingState`] for details)
        pub ongoing_status: i32,
    }

    /// Represents the raw data of a new chapter, for insertion in the database
    #[derive(Insertable)]
    #[table_name = "chapters"]
    pub struct NewChapterRawData {
        /// The story linked to this chapter
        pub story_id: uuid::Uuid,
        /// The chapter number (index of the chapter in the story).
        pub chapter_number: i32,
        /// The optional title of the story
        pub title: Option<String>,
        /// The chapter's contents, formatted.
        pub content: SanitizedHtml,
        /// The number of words in this chapter
        pub word_count: i32,
        /// Author's foreword as sanitized HTML
        pub foreword: SanitizedHtml,
        /// Author's afterword as sanitized HTML
        pub afterword: SanitizedHtml,
    }

    /// Create a new individual story with the given contents
    pub fn create_new_single_author_story(
        new_story: models::stories::NewSingleAuthorStory,
        conn: &PgConnection,
    ) -> Result<(models::stories::Story, models::stories::Chapter), IntertextualError> {
        conn.transaction::<(models::stories::Story, models::stories::Chapter), IntertextualError, _>(|| {
            // Check if the author already has a story with this URL fragment
            let author_id = new_story.author_id;
            let url_fragment = new_story.url_fragment.clone();
            let existing_story_with_url = user_to_story_associations::table
                .filter(user_to_story_associations::user_id.eq(author_id))
                .filter(user_to_story_associations::creative_role
                        .eq(CreativeRole::author()))
                .inner_join(stories::table)
                .filter(stories::url_fragment.eq(url_fragment))
                .select(stories::all_columns)
                .first::<models::stories::Story>(conn)
                .optional()?;
            if existing_story_with_url.is_some() {
                // The story exists, return a dummy duplicate error
                // TODO : this is hackish. Don't do this ?
                return Err(IntertextualError::DBError(
                    diesel::result::Error::DatabaseError(
                        diesel::result::DatabaseErrorKind::UniqueViolation,
                        Box::new("".to_string()),
                    ),
                ));
            }

            let new_story_internal = NewStoryRawData {
                title: new_story.title,
                url_fragment: new_story.url_fragment,
                is_collaboration: false,
                description: new_story.description,
                recommendations_enabled: new_story.recommendations_enabled.into(),
                comments_enabled: new_story.comments_enabled,
                is_multi_chapter: new_story.is_multi_chapter,
                ongoing_status: new_story.ongoing_status.into(),
            };
            let created_story = diesel::insert_into(stories::table)
                .values(new_story_internal)
                .get_result::<models::stories::Story>(conn)?;
            let _ = diesel::insert_into(user_to_story_associations::table)
                .values((
                    user_to_story_associations::user_id.eq(author_id),
                    user_to_story_associations::story_id.eq(created_story.id),
                    user_to_story_associations::creative_role.eq(CreativeRole::author()),
                ))
                .execute(conn)?;
            let new_chapter = models::stories::NewChapter {
                story_id: created_story.id,
                title: None,
                foreword: new_story.foreword,
                content: new_story.content,
                afterword: new_story.afterword,
                word_count: new_story.word_count,
            };
            let created_chapter = create_new_chapter(new_chapter, conn)?;
            Ok((created_story, created_chapter))
        })
    }

    /// Create a new chapter for a given existing story
    pub fn create_new_chapter(
        new_chapter: models::stories::NewChapter,
        conn: &PgConnection,
    ) -> Result<models::stories::Chapter, IntertextualError> {
        conn.transaction::<models::stories::Chapter, IntertextualError, _>(|| {
            let existing_indices = chapters::table
                .filter(chapters::story_id.eq(new_chapter.story_id))
                .select(chapters::chapter_number)
                .load::<i32>(conn)?;
            let mut next_chapter_number = 1;
            while existing_indices.iter().any(|i| *i == next_chapter_number) {
                next_chapter_number += 1;
            }
            let new_chapter_internal = NewChapterRawData {
                story_id: new_chapter.story_id,
                chapter_number: next_chapter_number,
                title: new_chapter.title,
                foreword: new_chapter.foreword,
                content: new_chapter.content,
                afterword: new_chapter.afterword,
                word_count: new_chapter.word_count,
            };

            let created_chapter = diesel::insert_into(chapters::table)
                .values(new_chapter_internal)
                .get_result::<models::stories::Chapter>(conn)?;

            Ok(created_chapter)
        })
    }

    /// Update the title or description of a story
    pub fn update_story_title_desc(
        story: models::stories::Story,
        conn: &PgConnection,
    ) -> Result<models::stories::Story, IntertextualError> {
        let story_result = diesel::update(stories::table.find(story.id))
            .set((
                stories::title.eq(story.title),
                stories::description.eq(story.description),
            ))
            .get_result::<models::stories::Story>(conn)?;
        Ok(story_result)
    }

    /// Update the story "ongoing" status
    pub fn update_story_ongoing_status(
        story_id: uuid::Uuid,
        state: models::stories::OngoingState,
        conn: &PgConnection,
    ) -> Result<models::stories::Story, IntertextualError> {
        let story_result = diesel::update(stories::table.find(story_id))
            .set(stories::ongoing_status.eq(i32::from(state)))
            .get_result::<models::stories::Story>(conn)?;
        Ok(story_result)
    }

    /// Update the story "multi-chapters" status
    pub fn update_story_multi_chapters(
        story: models::stories::Story,
        is_multi_chapter: bool,
        conn: &PgConnection,
    ) -> Result<models::stories::Story, IntertextualError> {
        conn.transaction::<models::stories::Story, IntertextualError, _>(|| {
            // Do not allow this if the story has several chapters.
            let chapter_count = chapters::table
                .filter(chapters::story_id.eq(story.id))
                .count()
                .get_result::<i64>(conn)?;
            if chapter_count > 1 && !is_multi_chapter {
                return Err(IntertextualError::FormFieldFormatError {
                    form_field_name: "is_multi_chapter",
                    message: format!(
                        "This story has {} chapters and must be set as multi-chapters",
                        chapter_count
                    ),
                });
            }

            let story_result = diesel::update(stories::table.find(story.id))
                .set(stories::is_multi_chapter.eq(is_multi_chapter))
                .get_result::<models::stories::Story>(conn)?;
            Ok(story_result)
        })
    }

    /// Update the collaboration status of a story
    pub fn update_story_collaboration(
        story: models::stories::Story,
        conn: &PgConnection,
    ) -> Result<models::stories::Story, IntertextualError> {
        let story_result = diesel::update(stories::table.find(story.id))
            .set((stories::is_collaboration.eq(story.is_collaboration),))
            .get_result::<models::stories::Story>(conn)?;
        Ok(story_result)
    }

    /// Update the comments or recommendation options of a story
    pub fn update_story_interaction(
        story: models::stories::Story,
        conn: &PgConnection,
    ) -> Result<models::stories::Story, IntertextualError> {
        let story_result = diesel::update(stories::table.find(story.id))
            .set((
                stories::comments_enabled.eq(story.comments_enabled),
                stories::recommendations_enabled.eq(story.recommendations_enabled),
            ))
            .get_result::<models::stories::Story>(conn)?;
        Ok(story_result)
    }

    /// Update the title, author notes, content and word count of a chapter
    pub fn update_chapter(
        chapter: models::stories::Chapter,
        do_not_notify_users: bool,
        conn: &PgConnection,
    ) -> Result<models::stories::Chapter, IntertextualError> {
        conn.transaction::<models::stories::Chapter, IntertextualError, _>(|| {
            let chapter_result = diesel::update(chapters::table.find(chapter.id))
                .set((
                    chapters::title.eq(chapter.title),
                    chapters::foreword.eq(chapter.foreword),
                    chapters::content.eq(chapter.content),
                    chapters::afterword.eq(chapter.afterword),
                    chapters::word_count.eq(chapter.word_count),
                ))
                .get_result::<models::stories::Chapter>(conn)?;

            // If the chapter is already published, notify the users
            let chapter_is_public = matches!(
                &chapter_result.show_publicly_after_date,
                Some(date) if *date < chrono::Utc::now().naive_utc()
            );
            if chapter_is_public && !chapter_result.moderator_locked && !do_not_notify_users {
                crate::actions::notifications::modifications::add_chapter_update_notification(
                    chapter_result.id,
                    conn,
                )?;
            }

            Ok(chapter_result)
        })
    }

    /// Update the chapter_index of a chapter, moving all other chapters around in the expected places
    pub fn move_chapter_to(
        chapter: models::stories::Chapter,
        new_number: i32,
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        conn.transaction::<(), IntertextualError, _>(|| {
            use std::cmp::Ordering;
            use std::convert::TryFrom;

            // Find all our the chapters
            let story_id = chapter.story_id;
            let all_chapters_in_story: Vec<(uuid::Uuid, i32)> = chapters::table
                .filter(chapters::story_id.eq(story_id))
                .select((chapters::id, chapters::chapter_number))
                .order_by(chapters::chapter_number.asc())
                .load::<(uuid::Uuid, i32)>(conn)?;

            let position_current = usize::try_from(chapter.chapter_number).unwrap_or(0);
            let position_target = usize::try_from(new_number).unwrap_or(0);

            // Check if the target position is valid
            let total_chapter_count = all_chapters_in_story.len();
            if !(1 <= position_target || position_target <= total_chapter_count) {
                return Err(IntertextualError::FormFieldFormatError {
                    form_field_name: "Chapter number",
                    message: format!("The number must be between 1 and {}", total_chapter_count),
                });
            }

            match position_current.cmp(&position_target) {
                Ordering::Less => {
                    // Take the items ]current..target]
                    // Note 1 : these are chapter indices, not numbers, so we need to remove one
                    // Note 2 : the default is [a..b[, so we need to add one to get ]a..b]
                    // So it cancels out ^^
                    let items_to_move = &all_chapters_in_story[position_current..position_target];

                    // Temporarily move our wanted chapter to an invalid position
                    diesel::update(chapters::table.find(chapter.id))
                        .set(chapters::chapter_number.eq(-1))
                        .get_result::<models::stories::Chapter>(conn)?;

                    // Move the matched chapters inbetween down one, starting from the lowest one
                    for (chapter_id, chapter_number) in items_to_move {
                        diesel::update(chapters::table.find(*chapter_id))
                            .set(chapters::chapter_number.eq(*chapter_number - 1))
                            .get_result::<models::stories::Chapter>(conn)?;
                    }

                    // Move our wanted chapter to the wanted position
                    diesel::update(chapters::table.find(chapter.id))
                        .set(chapters::chapter_number.eq(new_number))
                        .get_result::<models::stories::Chapter>(conn)?;
                    Ok(())
                }
                Ordering::Greater => {
                    // Take the items ]current...target]
                    // Note 1 : these are chapter indices, not numbers, so we need to remove one
                    let items_to_move =
                        &all_chapters_in_story[position_target - 1..position_current - 1];

                    // Temporarily move our wanted chapter to an invalid position
                    diesel::update(chapters::table.find(chapter.id))
                        .set(chapters::chapter_number.eq(-1))
                        .get_result::<models::stories::Chapter>(conn)?;

                    // Move the chapters inbetween up one, starting from the top one
                    for (chapter_id, chapter_number) in items_to_move.iter().rev() {
                        diesel::update(chapters::table.find(*chapter_id))
                            .set(chapters::chapter_number.eq(*chapter_number + 1))
                            .get_result::<models::stories::Chapter>(conn)?;
                    }

                    // Move our wanted chapter to the wanted position
                    diesel::update(chapters::table.find(chapter.id))
                        .set(chapters::chapter_number.eq(new_number))
                        .get_result::<models::stories::Chapter>(conn)?;
                    Ok(())
                }
                Ordering::Equal => {
                    // We don't need to move ! Everything is okay.
                    Ok(())
                }
            }
        })
    }

    // This is used to allow republishing for a bit of time, and still update the first publication date.
    const GRACE_PERIOD_MN: i64 = 30;

    /// Set the given chapter as public at the expected date. If the date is [`None`], the chapter is set as unpublished
    pub fn set_public_after(
        chapter: models::stories::Chapter,
        publication_date: Option<chrono::NaiveDateTime>,
        conn: &PgConnection,
    ) -> Result<models::stories::Chapter, IntertextualError> {
        conn.transaction::<models::stories::Chapter, IntertextualError, _>(|| {
            let current_date = chrono::Utc::now().naive_utc();

            // Don't allow attempting to publish in the past from more than GRACE_PERIOD_MN
            if let Some(date) = publication_date {
                if date.add(chrono::Duration::minutes(GRACE_PERIOD_MN)) < current_date {
                    return Err(IntertextualError::FormFieldFormatError {
                        form_field_name: "Publication Date",
                        message: format!(
                            "The publication date {} is in the past, which is not allowed.",
                            date.format("%Y-%m-%d %H:%M")
                        ),
                    });
                }
            }

            let mut first_publication_date = chapter.official_creation_date.or(publication_date);
            if let Some(defined_creation_date) = &first_publication_date {
                if defined_creation_date.add(chrono::Duration::minutes(GRACE_PERIOD_MN))
                    > current_date
                {
                    // If the date was in the future, then we never actually showed the story publicly.
                    // Set it to the new public date.
                    first_publication_date = publication_date;
                }
            }

            // Notify the users who are following the story behind this.
            crate::actions::notifications::modifications::add_chapter_publication_notification(
                chapter.id, conn,
            )?;

            let chapter_result = diesel::update(chapters::table.find(chapter.id))
                .set((
                    chapters::official_creation_date.eq(first_publication_date),
                    chapters::show_publicly_after_date.eq(publication_date),
                ))
                .get_result::<models::stories::Chapter>(conn)?;
            Ok(chapter_result)
        })
    }

    /// Delete the given chapter
    ///
    /// Note : this should only be used by one of the authors. Administrators should use [`super::admin`]
    pub fn delete_chapter(
        chapter: models::stories::Chapter,
        conn: &PgConnection,
    ) -> Result<models::stories::Chapter, IntertextualError> {
        conn.transaction::<models::stories::Chapter, IntertextualError, _>(|| {
            let story_id = chapter.story_id;
            let chapter_result = diesel::delete(chapters::table.find(chapter.id))
                .get_result::<models::stories::Chapter>(conn)?;
            let other_chapters_in_story: Vec<(uuid::Uuid, i32)> = chapters::table
                .filter(chapters::story_id.eq(story_id))
                .select((chapters::id, chapters::chapter_number))
                .order_by(chapters::chapter_number.asc())
                .load::<(uuid::Uuid, i32)>(conn)?;
            if !other_chapters_in_story.is_empty() {
                // Reorder the other chapters as needed
                let mut current_index = 1;
                for (chapter_id, chapter_number) in other_chapters_in_story {
                    if chapter_number != current_index {
                        diesel::update(chapters::table.find(chapter_id))
                            .set(chapters::chapter_number.eq(current_index))
                            .get_result::<models::stories::Chapter>(conn)?;
                    }
                    current_index += 1;
                }
            } else {
                // Find the story item itself
                let story = stories::table
                    .find(story_id)
                    .get_result::<models::stories::Story>(conn)?;
                // Delete all linked tags first, to update their counts
                for (_, _, internal_tag, _) in
                    crate::actions::tags::find_internal_tags_by_story_id(story.id, conn)?
                {
                    crate::actions::tags::modifications::remove_tag_from_story(
                        story.id,
                        internal_tag,
                        conn,
                    )?;
                }
                // Delete the story item itself
                diesel::delete(stories::table.find(story.id))
                    .get_result::<models::stories::Story>(conn)?;
            }
            Ok(chapter_result)
        })
    }
}

/// These methods should be called automatically by a background thread or a script, but not manually by an user or moderator action
pub mod automated_actions {
    use diesel::prelude::*;

    use crate::models::error::IntertextualError;
    use crate::schema::*;

    /// Recompute the publication and update date for stories whose values might have been updated
    pub fn recompute_cached_story_values(
        check_date: chrono::NaiveDateTime,
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        let previous_check_date = last_story_cache_check::table
            .select(last_story_cache_check::time)
            .get_result::<chrono::NaiveDateTime>(conn)
            .optional()?;
        let all_concerned_story_ids = match previous_check_date {
            None => {
                // First run ; select all stories (this will take slightly longer, but is only done once)
                stories::table
                    .select(stories::id)
                    .load::<uuid::Uuid>(conn)?
            }
            Some(date) => {
                // Only find chapters whose publication or update date is in the last checked range
                let mut publication_ids = chapters::table
                    .filter(chapters::official_creation_date.ge(date))
                    .filter(chapters::official_creation_date.lt(check_date))
                    .select(chapters::story_id)
                    .distinct()
                    .load::<uuid::Uuid>(conn)?;
                let mut update_ids = chapters::table
                    .filter(chapters::update_date.ge(date))
                    .filter(chapters::update_date.lt(check_date))
                    .select(chapters::story_id)
                    .distinct()
                    .load::<uuid::Uuid>(conn)?;
                publication_ids.append(&mut update_ids);
                let deduplicated = publication_ids
                    .drain(..)
                    .collect::<std::collections::HashSet<uuid::Uuid>>();
                publication_ids.extend(deduplicated.into_iter());
                publication_ids
            }
        };

        if !all_concerned_story_ids.is_empty() {
            log::info!(
                "Recomputing cached publication/update date for {} stories",
                all_concerned_story_ids.len()
            );
        }

        for story_id in all_concerned_story_ids {
            conn.transaction::<(), IntertextualError, _>(|| {
                let published_chapters = chapters::table
                    .filter(chapters::story_id.eq(story_id))
                    .filter(chapters::show_publicly_after_date.lt(check_date))
                    .filter(chapters::moderator_locked.eq(false))
                    .select((chapters::official_creation_date, chapters::update_date))
                    .load::<(Option<chrono::NaiveDateTime>, Option<chrono::NaiveDateTime>)>(conn)?;
                let official_creation_date = published_chapters
                    .iter()
                    .filter_map(|(creation, _)| creation.as_ref())
                    .min()
                    .unwrap_or(&check_date);
                let latest_update_date = published_chapters
                    .iter()
                    .filter_map(|(creation, update)| update.as_ref().or(creation.as_ref()))
                    .max()
                    .unwrap_or(official_creation_date);
                diesel::update(stories::table.find(story_id))
                    .set((
                        stories::cached_first_publication_date.eq(official_creation_date),
                        stories::cached_last_update_date.eq(latest_update_date),
                    ))
                    .execute(conn)?;
                Ok(())
            })?;
        }

        match previous_check_date {
            Some(_) => {
                // We already had a value. Update the existing value.
                diesel::update(last_story_cache_check::table)
                    .set(last_story_cache_check::time.eq(check_date))
                    .execute(conn)?;
            }
            None => {
                // We didn't have a value. Create it.
                diesel::insert_into(last_story_cache_check::table)
                    .values(last_story_cache_check::time.eq(check_date))
                    .execute(conn)?;
            }
        }

        Ok(())
    }
}

/// Methods used to administrate stories
pub mod admin {
    use diesel::prelude::*;

    use crate::actions::utils::log_mod_action;
    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::schema::*;

    /// Note : This method is admin-only because only administration panels should have a raw story ID without an attached story.
    /// Other screens should use methods that directly return stories such as [super::find_story_by_author_and_url] or [super::find_collaboration_story_by_url].
    pub fn find_story_by_id(
        story_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<models::stories::Story, IntertextualError> {
        let result: models::stories::Story = stories::table.find(story_id).first(conn)?;
        Ok(result)
    }

    /// Note : This method is admin-only because only administration panels should have a raw chapter ID without an attached story.
    /// Other screens should use methods that directly return a chapter.
    pub fn find_chapter_by_id(
        chapter_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<models::stories::Chapter, IntertextualError> {
        let result: models::stories::Chapter = chapters::table.find(chapter_id).first(conn)?;
        Ok(result)
    }

    /// Find a list of all chapters that are currently not published on the website
    pub fn find_all_unpublished_chapters(
        conn: &PgConnection,
    ) -> Result<Vec<(models::stories::Story, models::stories::Chapter)>, IntertextualError> {
        Ok(stories::table
            .inner_join(chapters::table)
            .filter(
                chapters::show_publicly_after_date
                    .ge(chrono::Utc::now().naive_utc())
                    .or(chapters::show_publicly_after_date
                        .is_null()
                        .or(chapters::moderator_locked.eq(true))),
            )
            .select((stories::all_columns, chapters::all_columns))
            .load::<(models::stories::Story, models::stories::Chapter)>(conn)?)
    }

    /// Gets the story internal creation times between the given start and end dates
    pub fn find_all_story_internal_creation_times_between(
        start: chrono::NaiveDateTime,
        end: chrono::NaiveDateTime,
        conn: &PgConnection,
    ) -> Result<Vec<chrono::NaiveDateTime>, IntertextualError> {
        let result = stories::table
            .filter(stories::story_internally_created_at.gt(start))
            .filter(stories::story_internally_created_at.le(end))
            .select(stories::story_internally_created_at)
            .load::<chrono::NaiveDateTime>(conn)?;
        Ok(result)
    }

    /// Add and remove "always displayed" tags to a given story
    pub fn add_and_remove_always_displayed_checkable_tags(
        moderator: models::users::ModeratorUserWrapper,
        authors: &[models::users::User],
        story: &models::stories::Story,
        selected_canonical_tag_ids: &[uuid::Uuid],
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            let story_tags = crate::actions::tags::find_internal_tags_by_story_id(story.id, conn)?;
            let categories = crate::actions::tags::get_all_categories(conn)?;
            for category in categories {
                if !category.is_always_displayed {
                    // We don't want to manage non-always-displayed categories
                    continue;
                }

                let tags = crate::actions::tags::get_checkable_tags_by_category(category.id, conn)?;
                for canonical_tag in tags {
                    if !canonical_tag.is_checkable {
                        // We don't want to manage non-checkable tags
                        continue;
                    }

                    let canonical_tag_id = canonical_tag.id;
                    let is_tag_in_selected_list = selected_canonical_tag_ids.contains(&canonical_tag_id);
                    let maybe_tag_in_story = story_tags.iter().find(|(_, t, _, _)| t.id == canonical_tag_id);
                    if let Some((_, _, internal_tag, _)) = maybe_tag_in_story {
                        // The tag already exists on this story. Check if we should remove it
                        if !is_tag_in_selected_list {
                            // Get the internal tag to remove
                            let internal_tag = crate::actions::tags::find_internal_tag_by_id(internal_tag.internal_id, conn)?;
                            if let Some(internal_tag) = internal_tag {
                                crate::actions::tags::modifications::remove_tag_from_story(
                                    story.id, internal_tag, conn,
                                )?;
                            }
                        }
                    } else {
                        // The tag doesn't exist on this story. Check if we should add it.
                        if is_tag_in_selected_list {
                            // Get a possible internal tag to add
                            let internal_tag = crate::actions::tags::find_internal_tags_equivalent_to(&canonical_tag, conn)?
                                .into_iter()
                                .next();
                            if let Some(internal_tag) = internal_tag {
                                crate::actions::tags::modifications::add_tag_to_story(
                                    story.id,
                                    internal_tag,
                                    models::tags::TagAssociationType::Standard,
                                    conn,
                                )?;
                            }
                        }

                    }
                }
            }

            // Show the action as modmail
            for author in authors {
                crate::actions::modmail::admins::send_new_system_modmail(
                    moderator.clone(),
                    author.id,
                    format!(
                        "System message. Tags for your story {} have been modified for the following reason: {}",
                        story.title,
                        log_entry.message,
                    ),
                    conn,
                )?;
            }

            // Store the action in the moderation log
            let mod_action = log_mod_action(log_entry, conn)?;

            // Notify the user
            for author in authors {
                let notification = models::notifications::NewNotification {
                    target_user_id: author.id,
                    notification_time: chrono::Utc::now().naive_utc(),
                    internal_description: format!(
                        "administrator_story_tags_modified_u{}_s{}",
                        author.id,
                        story.id
                    ),
                    message: format!("The tags for your story {} have been modified by a moderator", story.title),
                    link: "/moderation_contact/".to_string(),
                    category: models::notifications::NotificationCategory::SystemMessage.into(),
                };
                crate::actions::notifications::modifications::add_notification(notification, conn)?;
            }

            Ok(mod_action)
        })
    }

    /// Lock the given list of chapters
    pub fn lock_chapter_list(
        moderator: models::users::ModeratorUserWrapper,
        authors: &[models::users::User],
        chapter_list: &[models::stories::Chapter],
        story_title: &str,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            for chapter in chapter_list {
                diesel::update(chapters::table.find(chapter.id))
                .set((
                    chapters::moderator_locked.eq(true),
                    chapters::moderator_locked_reason.eq(Some(log_entry.message.as_str())),
                ))
                .get_result::<models::stories::Chapter>(conn)?;
            }

            // Show the action as modmail
            for author in authors {
                crate::actions::modmail::admins::send_new_system_modmail(
                    moderator.clone(),
                    author.id,
                    format!(
                        "System message. Your story {} has been locked by a moderator for the following reason : {}",
                        story_title,
                        log_entry.message
                    ),
                    conn,
                )?;
            }

            // Store the action in the moderation log
            let mod_action = log_mod_action(log_entry, conn)?;

            // Notify the users
            for author in authors {
                let notification = models::notifications::NewNotification {
                    target_user_id: author.id,
                    notification_time: chrono::Utc::now().naive_utc(),
                    internal_description: format!("administrator_chapter_locked_u{}", author.id),
                    message: format!("Your story {} has been locked by a moderator", story_title),
                    link: "/moderation_contact/".to_string(),
                    category: models::notifications::NotificationCategory::SystemMessage.into(),
                };
                crate::actions::notifications::modifications::add_notification(notification, conn)?;
            }

            Ok(mod_action)
        })
    }

    /// Unlock the given list of chapters
    pub fn unlock_chapter_list(
        moderator: models::users::ModeratorUserWrapper,
        authors: &[models::users::User],
        chapter_list: &[models::stories::Chapter],
        story_title: &str,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            for chapter in chapter_list {
                diesel::update(chapters::table.find(chapter.id))
                .set((
                    chapters::moderator_locked.eq(false),
                    chapters::moderator_locked_reason.eq(Option::<String>::None),
                ))
                .get_result::<models::stories::Chapter>(conn)?;
            }

            // Show the action as modmail
            for author in authors {
                crate::actions::modmail::admins::send_new_system_modmail(
                    moderator.clone(),
                    author.id,
                    format!(
                        "System message. Your story {} has been unlocked for the following reason : {}",
                        story_title,
                        log_entry.message
                    ),
                    conn,
                )?;
            }

            // Store the action in the moderation log
            let mod_action = log_mod_action(log_entry, conn)?;

            // Notify the user
            for author in authors {
                let notification = models::notifications::NewNotification {
                    target_user_id: author.id,
                    notification_time: chrono::Utc::now().naive_utc(),
                    internal_description: format!("administrator_chapter_unlocked_u{}", author.id),
                    message: format!("Your story {} has been unlocked", story_title),
                    link: "/moderation_contact/".to_string(),
                    category: models::notifications::NotificationCategory::SystemMessage.into(),
                };
                crate::actions::notifications::modifications::add_notification(notification, conn)?;
            }

            Ok(mod_action)
        })
    }

    /// Delete the given chapter
    pub fn disable_collaboration_link(
        moderator: models::users::ModeratorUserWrapper,
        authors: &[models::users::User],
        story: models::stories::Story,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<(models::stories::Story, models::admin::ModerationAction), IntertextualError> {
        conn.transaction::<(models::stories::Story, models::admin::ModerationAction), IntertextualError, _>(|| {
            let story_id = story.id;
            let story_title = story.title.clone();
            let mut story = story;
            story.is_collaboration = false;
            let response = super::modifications::update_story_collaboration(story, conn)?;

            // Show the action as modmail
            for author in authors {
                crate::actions::modmail::admins::send_new_system_modmail(
                    moderator.clone(),
                    author.id,
                    format!(
                        "System message. The collaboration link for your story {} has been disabled for the following reason: {}",
                        story_title,
                        log_entry.message,
                    ),
                    conn,
                )?;
            }

            // Store the action in the moderation log
            let mod_action = log_mod_action(log_entry, conn)?;

            // Notify the user
            for author in authors {
                let notification = models::notifications::NewNotification {
                    target_user_id: author.id,
                    notification_time: chrono::Utc::now().naive_utc(),
                    internal_description: format!("administrator_story_collaboration_removed_u{}_s{}", author.id, story_id),
                    message: format!("The collaboration link for your story {} has been removed by a moderator", story_title),
                    link: "/moderation_contact/".to_string(),
                    category: models::notifications::NotificationCategory::SystemMessage.into(),
                };
                crate::actions::notifications::modifications::add_notification(notification, conn)?;
            }

            Ok((response, mod_action))
        })
    }

    /// Delete the given chapter
    pub fn delete_chapter_administrator(
        moderator: models::users::ModeratorUserWrapper,
        authors: &[models::users::User],
        chapter: models::stories::Chapter,
        story_title: &str,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<(models::stories::Chapter, models::admin::ModerationAction), IntertextualError>
    {
        conn.transaction::<(models::stories::Chapter, models::admin::ModerationAction), IntertextualError, _>(|| {
            let chapter_id = chapter.id;
            let response = super::modifications::delete_chapter(chapter, conn)?;

            // Show the action as modmail
            for author in authors {
                crate::actions::modmail::admins::send_new_system_modmail(
                    moderator.clone(),
                    author.id,
                    format!(
                        "System message. A chapter of your story {} has been deleted for the following reason : {}",
                        story_title,
                        log_entry.message
                    ),
                    conn,
                )?;
            }

            // Store the action in the moderation log
            let mod_action = log_mod_action(log_entry, conn)?;

            // Notify the user
            for author in authors {
                let notification = models::notifications::NewNotification {
                    target_user_id: author.id,
                    notification_time: chrono::Utc::now().naive_utc(),
                    internal_description: format!("administrator_chapter_deleted_u{}_c{}", author.id, chapter_id),
                    message: format!("A chapter of your story {} has been deleted by a moderator", story_title),
                    link: "/moderation_contact/".to_string(),
                    category: models::notifications::NotificationCategory::SystemMessage.into(),
                };
                crate::actions::notifications::modifications::add_notification(notification, conn)?;
            }

            Ok((response, mod_action))
        })
    }
}
