//! # Latest Stories
//!
//! This module contains methods used to retrieve the list of latest stories in order to display them in terse lists (e.g. the front page)
use std::convert::TryFrom;

use diesel::prelude::*;

use crate::models;
use crate::models::error::IntertextualError;
use crate::models::filter::FilterMode;
use crate::models::stories::CreativeRole;
use crate::schema::*;

use super::stories::add_existing_id_filter;

/// Options used to compute the list of latest stories
pub struct LatestStoriesOptions<'a> {
    /// The maximum number of entries to provide.
    /// The actual number is guaranteed to be equal or less than this.
    pub entries_count: usize,
    /// The maximum number of stories sharing the same author.
    ///
    /// If more than this amount of stories are found, the last entries
    /// will be merged into a single entry until there is only one remaining
    ///
    /// Note: The value `Some(0)` is not valid, and will be handled as `None`.
    pub aggregate_same_author_after_count: Option<usize>,

    /// The canonical tags that should not show up in the latest stories
    pub hidden_canonical_tags: &'a [uuid::Uuid],
}

impl<'a> Default for LatestStoriesOptions<'a> {
    fn default() -> Self {
        Self {
            entries_count: 32,
            aggregate_same_author_after_count: None,
            hidden_canonical_tags: &[],
        }
    }
}

/// Describes the result of a single story update
pub struct StoryUpdateEntry {
    /// The Uuids of the authors of this story, based on the provided filter's visibility
    pub author_ids: Vec<uuid::Uuid>,
    /// The story information
    pub story: models::stories::Story,
    /// The chapter information.
    pub chapters: Vec<models::stories::ChapterMetadata>,
}

/// Describes the result of several story updates from a single author or authors.
pub struct AggregatedStoryUpdatesEntry {
    /// The Uuids of the authors of this story, based on the provided filter's visibility
    pub author_ids: Vec<uuid::Uuid>,
    /// The list of story updates that were matched for this author combination
    pub entries: Vec<StoryUpdateEntry>,
}

/// Describes a story update, either single or aggregated
pub enum LatestStoryResult {
    /// Created from a single update of an author
    SingleUpdate(StoryUpdateEntry),
    /// Created from several updates of a same author combination
    AggregatedUpdate(AggregatedStoryUpdatesEntry),
}

/// Finds the `minimum_limit` latest updated stories, respecting the given `filter`
///
/// Note : If a story has been updated several non-consecutive times, then it will appear several times in the result list
pub fn find_latest_stories(
    options: LatestStoriesOptions,
    filter: &FilterMode,
    conn: &PgConnection,
) -> Result<Vec<LatestStoryResult>, IntertextualError> {
    let mut results: Vec<LatestStoryResult> = Vec::with_capacity(options.entries_count);
    let mut start_offset = 0;
    while results.len() < options.entries_count {
        /* Heuristics 1 : On average, stories shouldn't have more than a burst of 3 chapters one after the other. */
        /* Heuristics 2 : One random story shouldn't post more than 45 chapters at once */
        let latest_chapters_limit = 42 + i64::try_from(options.entries_count).unwrap_or(0) * 3;
        let mut query = stories::table
            .inner_join(chapters::table)
            .inner_join(user_to_story_associations::table)
            .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
            .inner_join(users::table.on(user_to_story_associations::user_id.eq(users::id)))
            .into_boxed();
        add_existing_id_filter!(query, filter);
        let chapter_list = query
            .order(chapters::official_creation_date.desc())
            .select((
                stories::all_columns,
                (
                    chapters::id,
                    chapters::chapter_internally_created_at,
                    chapters::story_id,
                    chapters::chapter_number,
                    chapters::title,
                    chapters::word_count,
                    chapters::official_creation_date,
                    chapters::update_date,
                    chapters::show_publicly_after_date,
                    chapters::moderator_locked,
                ),
            ))
            .distinct()
            .offset(start_offset)
            .limit(latest_chapters_limit)
            .load::<(models::stories::Story, models::stories::ChapterMetadata)>(conn)?;

        if chapter_list.is_empty() {
            // There wasn't enough stories overall. Break now.
            break;
        }

        start_offset += chapter_list.len() as i64;

        for (story, chapter) in chapter_list {
            let tags = crate::actions::tags::find_all_canonical_tags_by_story_id(story.id, conn)?;
            if tags
                .iter()
                .any(|(_, t)| options.hidden_canonical_tags.contains(&t.id))
            {
                // Skip this story
                continue;
            }

            merge_card_in_list(
                StoryUpdateEntry {
                    author_ids: crate::actions::stories::find_author_ids_by_story(
                        story.id, filter, conn,
                    )?,
                    story,
                    chapters: vec![chapter],
                },
                &mut results,
                options.aggregate_same_author_after_count,
            );

            if results.len() >= options.entries_count {
                // We found enough stories
                break;
            }
        }
    }
    Ok(results)
}

fn merge_card_in_list(
    card: StoryUpdateEntry,
    results: &mut Vec<LatestStoryResult>,
    max_author_cards: Option<usize>,
) {
    let mut card = card;
    if let Some(last_entry) = results.last_mut() {
        match last_entry {
            LatestStoryResult::SingleUpdate(last_update) => {
                if last_update.story.id == card.story.id {
                    // Aggregate the chapter into the last standalone story update
                    last_update.chapters.append(&mut card.chapters);
                    return;
                }
            }
            LatestStoryResult::AggregatedUpdate(last_aggregation) => {
                if let Some(last_update) = last_aggregation.entries.last_mut() {
                    if last_update.story.id == card.story.id {
                        // Aggregate the chapter into the last aggregated story update
                        last_update.chapters.append(&mut card.chapters);
                        return;
                    }
                }
            }
        }
    }

    let author_list = card.author_ids.clone();
    results.push(LatestStoryResult::SingleUpdate(card));

    match max_author_cards {
        Some(max_quantity) if max_quantity != 0 => {
            // Clean up the list for this author list specifically.
            let mut match_count = 0;
            let mut last_match_index = None;
            for current_index in 0..results.len() {
                match results.get(current_index) {
                    Some(LatestStoryResult::SingleUpdate(update)) => {
                        if !author_ids_matches(&author_list, &update.author_ids) {
                            continue;
                        }
                    }
                    Some(LatestStoryResult::AggregatedUpdate(update)) => {
                        if !author_ids_matches(&author_list, &update.author_ids) {
                            continue;
                        }
                    }
                    None => continue,
                }
                match_count += 1;
                if match_count <= max_quantity {
                    last_match_index = Some(current_index);
                    continue;
                }
                if last_match_index.is_none() {
                    // This shouldn't happen, but just in case
                    break;
                }
                let recent_index = last_match_index.unwrap_or(0);
                let current_match = results.remove(current_index);
                let recent_match = results.remove(recent_index);
                let entries = match (recent_match, current_match) {
                    (
                        LatestStoryResult::AggregatedUpdate(recent_aggregated),
                        LatestStoryResult::AggregatedUpdate(current_aggregated),
                    ) => {
                        let mut entries = recent_aggregated.entries;
                        let mut others = current_aggregated.entries;
                        entries.append(&mut others);
                        entries
                    }
                    (
                        LatestStoryResult::SingleUpdate(recent_single),
                        LatestStoryResult::AggregatedUpdate(current_aggregated),
                    ) => {
                        let mut entries = current_aggregated.entries;
                        entries.insert(0, recent_single);
                        entries
                    }
                    (
                        LatestStoryResult::AggregatedUpdate(recent_aggregated),
                        LatestStoryResult::SingleUpdate(current_single),
                    ) => {
                        let mut entries = recent_aggregated.entries;
                        entries.push(current_single);
                        entries
                    }
                    (
                        LatestStoryResult::SingleUpdate(recent_single),
                        LatestStoryResult::SingleUpdate(current_single),
                    ) => {
                        vec![recent_single, current_single]
                    }
                };
                results.insert(
                    recent_index,
                    LatestStoryResult::AggregatedUpdate(AggregatedStoryUpdatesEntry {
                        author_ids: author_list,
                        entries,
                    }),
                );
                break;
            }
        }
        _ => {}
    }
}

fn author_ids_matches(first_list: &[uuid::Uuid], second_list: &[uuid::Uuid]) -> bool {
    if first_list.len() != second_list.len() {
        return false;
    }

    for id in first_list {
        if !second_list.contains(id) {
            return false;
        }
    }

    true
}
