//! # Database Actions
//!
//! List of small operations on the database. Combines some operations that should
//! always be executed together, but there is not generally any check for access rights.
//!
//! For advanced database access, see the [`crate::data`] module

pub mod access;
pub mod announcements;
pub mod comments;
pub mod denylist;
pub mod follows;
pub mod hits;
pub mod latest_stories;
pub mod marked_for_later;
pub mod modmail;
pub mod notifications;
pub mod recommendations;
pub mod reports;
pub mod search;
pub mod static_pages;
pub mod stories;
pub mod tags;
pub mod user_approvals;
pub mod users;
pub mod utils;
