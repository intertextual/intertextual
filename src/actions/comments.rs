//! # Comments
//!
//! This module contains methods related to reading and writing comments on a chapter
use diesel::prelude::*;

use crate::models;
use crate::models::error::IntertextualError;
use crate::schema::*;

/// Find the `limit` comments starting from `start` for the chapter with id `chapter_id`
pub fn find_comments_for_chapter(
    chapter_id: uuid::Uuid,
    start: i64,
    limit: i64,
    conn: &PgConnection,
) -> Result<Vec<(models::comments::Comment, models::users::User)>, IntertextualError> {
    let result = comments::table
        .filter(comments::chapter_id.eq(chapter_id))
        .inner_join(users::table)
        .filter(users::deactivated_by_user.eq(false))
        .filter(
            users::banned_until
                .lt(chrono::Utc::now().naive_utc())
                .or(users::banned_until.is_null()),
        )
        .order(comments::created.desc())
        .offset(start)
        .limit(limit)
        .load(conn)?;
    Ok(result)
}

/// Find the total quantity of comments for the chapter with id `chapter_id`
pub fn find_comment_quantity_for_chapter(
    chapter_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<i64, IntertextualError> {
    let result = comments::table
        .filter(comments::chapter_id.eq(chapter_id))
        .inner_join(users::table)
        .filter(users::deactivated_by_user.eq(false))
        .filter(
            users::banned_until
                .lt(chrono::Utc::now().naive_utc())
                .or(users::banned_until.is_null()),
        )
        .count()
        .get_result(conn)?;
    Ok(result)
}

/// Find the total quantity of comments for the chapter with id `chapter_id`
pub fn find_comment_quantity_for_chapters(
    chapter_ids: &[uuid::Uuid],
    conn: &PgConnection,
) -> Result<i64, IntertextualError> {
    let result = comments::table
        .filter(comments::chapter_id.eq_any(chapter_ids))
        .inner_join(users::table)
        .filter(users::deactivated_by_user.eq(false))
        .filter(
            users::banned_until
                .lt(chrono::Utc::now().naive_utc())
                .or(users::banned_until.is_null()),
        )
        .count()
        .get_result(conn)?;
    Ok(result)
}

/// Find the complete list of timestamp of comments for the chapter with id `chapter_id`
pub fn find_comments_through_time(
    chapter_ids: &[uuid::Uuid],
    conn: &PgConnection,
) -> Result<Vec<chrono::NaiveDateTime>, IntertextualError> {
    let result = comments::table
        .filter(comments::chapter_id.eq_any(chapter_ids))
        .inner_join(users::table)
        .filter(users::deactivated_by_user.eq(false))
        .filter(
            users::banned_until
                .lt(chrono::Utc::now().naive_utc())
                .or(users::banned_until.is_null()),
        )
        .select(comments::created)
        .load(conn)?;
    Ok(result)
}

/// Find the `limit` next comments by the user with id `user_id`, starting at the `start` comment
pub fn find_comments_by_user(
    user_id: uuid::Uuid,
    start: i64,
    limit: i64,
    conn: &PgConnection,
) -> Result<
    Vec<(
        models::stories::Story,
        models::stories::ChapterMetadata,
        models::comments::Comment,
    )>,
    IntertextualError,
> {
    let comments = comments::table
        .filter(comments::commenter_id.eq(user_id))
        .inner_join(chapters::table.on(chapters::id.eq(comments::chapter_id)))
        .inner_join(stories::table.on(stories::id.eq(chapters::story_id)))
        .order(comments::created.desc())
        .select((
            stories::all_columns,
            (
                chapters::id,
                chapters::chapter_internally_created_at,
                chapters::story_id,
                chapters::chapter_number,
                chapters::title,
                chapters::word_count,
                chapters::official_creation_date,
                chapters::update_date,
                chapters::show_publicly_after_date,
                chapters::moderator_locked,
            ),
            comments::all_columns,
        ))
        .offset(start)
        .limit(limit)
        .load::<(
            models::stories::Story,
            models::stories::ChapterMetadata,
            models::comments::Comment,
        )>(conn)?;
    Ok(comments)
}

/// Find the total quantity of comments by the user with id `user_id`
pub fn find_comment_quantity_by_user(
    user_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<i64, IntertextualError> {
    let result = comments::table
        .filter(comments::commenter_id.eq(user_id))
        .count()
        .get_result(conn)?;
    Ok(result)
}

/// Find the timestamps for all the comments by the user with id `user_id`
pub fn find_commentbyby_user_through_time(
    user_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<Vec<chrono::NaiveDateTime>, IntertextualError> {
    let result = comments::table
        .filter(comments::commenter_id.eq(user_id))
        .select(comments::created)
        .load(conn)?;
    Ok(result)
}

/// Represents the return type of the [`find_comment`] method.
pub type CommentTuple = (
    models::comments::Comment,
    models::users::User,
    models::stories::Chapter,
    models::stories::Story,
);

/// Find the data related to the comment with id `comment_id`
pub fn find_comment(
    comment_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<Option<CommentTuple>, IntertextualError> {
    let result = comments::table
        .find(comment_id)
        .inner_join(users::table)
        .filter(users::deactivated_by_user.eq(false))
        .filter(
            users::banned_until
                .lt(chrono::Utc::now().naive_utc())
                .or(users::banned_until.is_null()),
        )
        .inner_join(chapters::table.on(chapters::id.eq(comments::chapter_id)))
        .inner_join(stories::table.on(stories::id.eq(chapters::story_id)))
        .get_result(conn)
        .optional()?;
    Ok(result)
}

/// Find the index of the comment `comment`
///
/// The index of a comment is the start position needed to read this comment.
pub fn find_comment_index_for_story(
    comment: &models::comments::Comment,
    conn: &PgConnection,
) -> Result<i64, IntertextualError> {
    let result = comments::table
        .filter(comments::chapter_id.eq(comment.chapter_id))
        .inner_join(users::table)
        .filter(users::deactivated_by_user.eq(false))
        .filter(
            users::banned_until
                .lt(chrono::Utc::now().naive_utc())
                .or(users::banned_until.is_null()),
        )
        .filter(comments::created.gt(comment.created))
        .count()
        .get_result(conn)?;
    Ok(result)
}

/// This module is used for retrieving the data linked to an account in raw format
pub mod gdpr {
    use diesel::prelude::*;

    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::schema::*;

    /// Get all the comments and linked data for a given user
    pub fn all_comments_from_user(
        user_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<
        Vec<(
            models::stories::Story,
            models::stories::Chapter,
            models::comments::Comment,
        )>,
        IntertextualError,
    > {
        let all_approvals = comments::table
            .filter(comments::commenter_id.eq(user_id))
            .inner_join(chapters::table.on(chapters::id.eq(comments::chapter_id)))
            .inner_join(stories::table.on(stories::id.eq(chapters::story_id)))
            .select((
                stories::all_columns,
                chapters::all_columns,
                comments::all_columns,
            ))
            .load::<(
                models::stories::Story,
                models::stories::Chapter,
                models::comments::Comment,
            )>(conn)?;
        Ok(all_approvals)
    }
}

/// This module contains all actions related to modifying a chapter's comment list by an user
pub mod modifications {
    use diesel::prelude::*;

    use crate::actions::notifications::modifications::add_notification;
    use crate::actions::notifications::modifications::add_notification_if_not_already_sent;
    use crate::actions::notifications::modifications::clean_all_unread_notifications_with_internal_description;
    use crate::actions::notifications::modifications::clean_all_unread_notifications_with_internal_description_for_all_users_except;
    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::models::formats::RichBlock;
    use crate::prelude::*;
    use crate::schema::*;

    /// Add a new comment, written by the given user.
    ///
    /// This function should only be called if the identity of the comment's author is confirmed.
    pub fn add_new_comment(
        new_comment: models::comments::NewComment,
        conn: &PgConnection,
    ) -> Result<models::comments::Comment, IntertextualError> {
        conn.transaction::<models::comments::Comment, IntertextualError, _>(|| {
            let created_comment = diesel::insert_into(comments::table)
                .values(new_comment)
                .get_result::<models::comments::Comment>(conn)?;

            let comment_information = get_comment_information(&created_comment, conn)?;
            notify_authors(&comment_information, conn)?;
            notify_mentioned_users_if_needed(&created_comment, &comment_information, conn)?;

            Ok(created_comment)
        })
    }

    /// Change the message of a given comment, written by the given user.
    ///
    /// This function should only be called if the identity of the comment's author is confirmed.
    pub fn edit_comment(
        comment_id: uuid::Uuid,
        message: RichBlock,
        conn: &PgConnection,
    ) -> Result<models::comments::Comment, IntertextualError> {
        let comment_result = diesel::update(comments::table.find(comment_id))
            .set((
                comments::message.eq(message),
                comments::updated.eq(Some(chrono::Utc::now().naive_utc())),
            ))
            .get_result(conn)?;

        let comment_information = get_comment_information(&comment_result, conn)?;
        notify_mentioned_users_if_needed(&comment_result, &comment_information, conn)?;

        Ok(comment_result)
    }

    /// Delete a given comment.
    ///
    /// This function should only be called if the identity of the comment's author is confirmed, or if the identity of the
    /// chapter's author is confirmed.
    pub fn delete_comment(
        comment: models::comments::Comment,
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        conn.transaction::<(), IntertextualError, _>(|| {
            // Remove all pending notifications for this comment
            let comment_information = get_comment_information(&comment, conn)?;
            clean_all_unread_notifications_with_internal_description(
                &comment_information.internal_description,
                conn,
            )?;

            // Delete this comment
            diesel::delete(comments::table.find(comment.id))
                .get_result::<models::comments::Comment>(conn)?;

            Ok(())
        })
    }

    /// Contains metadata about the comment (comment author, story information, etc...)
    struct CommentInformation {
        /// Author of the comment
        pub comment_author: models::users::User,
        /// Story the comment is commenting on
        pub story: models::stories::Story,
        /// List of authors for this story
        pub author_list: Vec<models::users::User>,
        /// Internal description of the comment, used to deduplicate notifications
        pub internal_description: String,
        /// Link to the comment
        pub comment_link: String,
    }

    /// Get some metadata about the comment (comment author, story information, etc...)
    fn get_comment_information(
        comment: &models::comments::Comment,
        conn: &PgConnection,
    ) -> Result<CommentInformation, IntertextualError> {
        let comment_author: models::users::User =
            users::table.find(&comment.commenter_id).first(conn)?;

        let chapter_id = comment.chapter_id;
        let chapter: models::stories::Chapter = chapters::table.find(chapter_id).first(conn)?;
        let story: models::stories::Story = stories::table.find(chapter.story_id).first(conn)?;
        let author_list = crate::actions::stories::find_authors_by_story(
            story.id,
            &FilterMode::BypassFilters,
            conn,
        )?;

        let internal_description = match author_list.as_slice() {
            [author] if story.is_collaboration => format!(
                "a{}s{}c{}u{}comm{}",
                author.id, story.id, chapter.id, comment_author.id, comment.id
            ),
            _ => format!(
                "collabs{}c{}u{}comm{}",
                story.id, chapter.id, comment_author.id, comment.id
            ),
        };

        let comment_link = format!(
            "/{}/{}/{}/comments/{}",
            story.author_path(&author_list),
            story.url_fragment,
            chapter.chapter_number,
            comment.id
        );

        Ok(CommentInformation {
            comment_author,
            story,
            author_list,
            internal_description,
            comment_link,
        })
    }

    /// Notify all the authors of the story that the comment has been created
    fn notify_authors(
        comment_information: &CommentInformation,
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        for author in &comment_information.author_list {
            if comment_information.comment_author.id == author.id {
                // Note : if we're writing a comment in our own story, don't notify outself
                continue;
            }
            let message = format!(
                "The user {} wrote a comment for your story {} !",
                comment_information.comment_author.display_name, comment_information.story.title,
            );
            let notification = models::notifications::NewNotification {
                target_user_id: author.id,
                notification_time: chrono::Utc::now().naive_utc(),
                internal_description: comment_information.internal_description.clone(),
                message,
                link: comment_information.comment_link.clone(),
                category: models::notifications::NotificationCategory::Comment.into(),
            };
            add_notification(notification, conn)?;
        }

        Ok(())
    }

    /// Notify all the users mentioned in the comment that they were mentioned
    ///
    /// Note : notifications will only be sent if there isn't already a notification for this comment. As such,
    /// this method is safe to call several times.
    fn notify_mentioned_users_if_needed(
        created_comment: &models::comments::Comment,
        comment_information: &CommentInformation,
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        let mut notified_user_ids: Vec<uuid::Uuid> = comment_information
            .author_list
            .iter()
            .map(|u| u.id)
            .collect();
        let mentioned_usernames = created_comment.message.mentions();
        for username in mentioned_usernames {
            let mentioned_user = users::table
                .filter(users::username.ilike(username))
                .first::<models::users::User>(conn)
                .optional()?;
            if let Some(mentioned_user) = mentioned_user {
                // Note : if we're mentioning ourselves, don't notify
                if comment_information.comment_author.id == mentioned_user.id {
                    continue;
                }

                // Check if the comment author isn't in the denylist of the mentioned user.
                if crate::actions::denylist::check_if_user_ignored_other_user(
                    mentioned_user.id,
                    comment_information.comment_author.id,
                    conn,
                )? {
                    continue;
                }

                let message = format!(
                    "The user {} mentioned you in a comment !",
                    comment_information.comment_author.display_name,
                );
                let notification = models::notifications::NewNotification {
                    target_user_id: mentioned_user.id,
                    notification_time: chrono::Utc::now().naive_utc(),
                    internal_description: comment_information.internal_description.clone(),
                    message,
                    link: comment_information.comment_link.clone(),
                    category: models::notifications::NotificationCategory::Comment.into(),
                };
                add_notification_if_not_already_sent(notification, conn)?;
                notified_user_ids.push(mentioned_user.id);
            }
        }

        // Remove any existing mention for users with no notifications, if it exists
        clean_all_unread_notifications_with_internal_description_for_all_users_except(
            &comment_information.internal_description,
            &notified_user_ids,
            conn,
        )?;

        Ok(())
    }
}

/// This module contains all actions related to editing the comments of a chapter using administrative rights.
pub mod admin {
    use diesel::prelude::*;

    use crate::actions::utils::log_mod_action;
    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::models::users::ModeratorUserWrapper;
    use crate::schema::*;

    #[derive(Clone, Copy, PartialEq, Eq)]
    pub enum CommentSortMode {
        CreationAsc,
        CreationDesc,
    }

    /// Gets the total number of comments
    pub fn find_total_comments_count(conn: &PgConnection) -> Result<i64, IntertextualError> {
        let result = comments::table.count().get_result::<i64>(conn)?;
        Ok(result)
    }

    /// Gets the comments written between the two given times
    pub fn find_all_comment_times_between(
        start: chrono::NaiveDateTime,
        end: chrono::NaiveDateTime,
        conn: &PgConnection,
    ) -> Result<Vec<chrono::NaiveDateTime>, IntertextualError> {
        let result = comments::table
            .filter(comments::created.gt(start))
            .filter(comments::created.le(end))
            .select(comments::created)
            .load::<chrono::NaiveDateTime>(conn)?;
        Ok(result)
    }

    /// Gets the comments ordered by most recently edited
    pub fn find_comments_by_sort_mode(
        start: i64,
        limit: i64,
        sort_mode: CommentSortMode,
        conn: &PgConnection,
    ) -> Result<Vec<models::comments::Comment>, IntertextualError> {
        let result = match sort_mode {
            CommentSortMode::CreationAsc => comments::table
                .offset(start)
                .limit(limit)
                .order_by(comments::created.asc())
                .load::<models::comments::Comment>(conn)?,
            CommentSortMode::CreationDesc => comments::table
                .offset(start)
                .limit(limit)
                .order_by(comments::created.desc())
                .load::<models::comments::Comment>(conn)?,
        };
        Ok(result)
    }

    /// Represents the return type of a comment report request
    pub type ReportedCommentTuple = (
        models::comments::CommentReport,
        models::users::User,
        models::comments::Comment,
        models::stories::Chapter,
        models::stories::Story,
    );

    /// Gets the report information for a given comment ID
    pub fn get_reported_comment(
        comment_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<Option<ReportedCommentTuple>, IntertextualError> {
        let result = comment_reports::table
            .filter(comment_reports::comment_id.eq(comment_id))
            .inner_join(users::table.on(users::id.eq(comment_reports::reporter_id)))
            .inner_join(comments::table.on(comments::id.eq(comment_reports::comment_id)))
            .inner_join(chapters::table.on(chapters::id.eq(comments::chapter_id)))
            .inner_join(stories::table.on(stories::id.eq(chapters::story_id)))
            .get_result::<ReportedCommentTuple>(conn)
            .optional()?;
        Ok(result)
    }

    /// Deletes a given comment
    pub fn delete_comment(
        moderator: ModeratorUserWrapper,
        comment: models::comments::Comment,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            let user_id = comment.commenter_id;
            super::modifications::delete_comment(comment, conn)?;

            // Show the action as modmail
            crate::actions::modmail::admins::send_new_system_modmail(
                moderator,
                user_id,
                format!(
                    "System message. One of your comments has been deleted for the following reason : {}",
                    log_entry.message
                ),
                conn,
            )?;

            // Store the action in the moderation log
            let mod_action = log_mod_action(log_entry, conn)?;

            // Notify the user
            let notification = models::notifications::NewNotification {
                target_user_id: user_id,
                notification_time: chrono::Utc::now().naive_utc(),
                internal_description: format!("administrator_comment_deleted_u{}", user_id),
                message: "One of your comments has been deleted by a moderator".to_string(),
                link: "/moderation_contact/".to_string(),
                category: models::notifications::NotificationCategory::SystemMessage.into(),
            };
            crate::actions::notifications::modifications::add_notification(notification, conn)?;

            Ok(mod_action)
        })
    }
}
