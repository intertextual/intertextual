//! # Utilities : HTMLization
//!
//! This module contains methods used to convert user-submitted story data into sanitized html.

use std::boxed::Box;
use std::io::{Read, Write};
use std::path::Path;

use comrak::{markdown_to_html, ComrakOptions};
use log::info;

use crate::models::error::IntertextualError;
use crate::models::formats::SanitizedHtml;

pub fn markdown_to_sanitized_html(md: &str) -> SanitizedHtml {
    let mut options = ComrakOptions::default();
    options.extension.strikethrough = true;
    options.extension.superscript = true;
    options.parse.smart = true;
    SanitizedHtml::from(&markdown_to_html(md, &options))
}

pub fn pandoc_wrapped_and_sanitized(
    input_kind: pandoc::InputKind,
    input_format: Option<pandoc::InputFormat>,
) -> Result<SanitizedHtml, IntertextualError> {
    let mut pandoc = pandoc::new();
    pandoc.add_option(pandoc::PandocOption::Sandbox);
    pandoc.add_option(pandoc::PandocOption::RuntimeSystem(vec![
        pandoc::PandocRuntimeSystemOption::MaximumHeapMemory("512M".to_string()),
    ]));
    pandoc.set_input(input_kind);
    if let Some(f) = input_format {
        pandoc.set_input_format(f, vec![]);
    }
    pandoc.set_output(pandoc::OutputKind::Pipe);
    pandoc.set_output_format(pandoc::OutputFormat::Html5, vec![]);
    let unsanitized_output = pandoc.execute()?;
    if let pandoc::PandocOutput::ToBuffer(str) = unsanitized_output {
        info!("Pandoc parsing completed successfully!");
        Ok(SanitizedHtml::from(&str))
    } else {
        Err(IntertextualError::InternalServerError)
    }
}

lazy_static! {
    static ref BBCODE_REGEX: regex::Regex = regex::Regex::new(r"\[/?[biu]\]").unwrap();
    static ref BBCODE_BOLD_REGEX: regex::Regex = regex::Regex::new(r"\[/?b\]").unwrap();
    static ref BBCODE_ITALICS_REGEX: regex::Regex = regex::Regex::new(r"\[/?i\]").unwrap();
}

pub fn file_to_sanitized_html<P: AsRef<Path>>(
    filepath: P,
) -> Result<SanitizedHtml, IntertextualError> {
    let mut filepath: Box<dyn AsRef<Path>> = Box::new(filepath);

    let ext_as_str = (*filepath)
        .as_ref()
        .extension()
        .and_then(std::ffi::OsStr::to_str)
        .map(str::to_ascii_lowercase);
    let mut input_format = None;

    if let Some(format) = ext_as_str {
        if format == "txt" || format == "md" || format == "htm" || format == "html" {
            // We need to check the file's encoding, and create a new UTF-8 encoded file if needed.
            info!("Checking the file encoding for {:?}", (*filepath).as_ref());
            let mut file = std::fs::File::open((*filepath).as_ref())?;
            let mut file_contents: Vec<u8> = Vec::new();
            file.read_to_end(&mut file_contents)?;

            let mut detector = chardetng::EncodingDetector::new();
            detector.feed(&file_contents, true);
            let encoding = detector.guess(None, true);
            info!(
                "Encoding guessed by chardetng: {:?} ({} bytes checked)",
                encoding,
                file_contents.len()
            );

            if encoding != encoding_rs::UTF_8 {
                let mut new_file = tempfile::Builder::new()
                    .suffix(&(".".to_owned() + &format))
                    .tempfile_in("./tmp/")?;

                // Write the decoded data to the new file.
                info!("Writing decoded UTF-8 data to {:?}", new_file.path());
                let mut decoder = encoding.new_decoder();
                let mut intermediate_buffer = [0u8; 4096];
                let mut input_start = 0;

                loop {
                    // Decode the next chunk of the input.
                    let (decoder_result, decoder_read, decoder_written, _) = decoder
                        .decode_to_utf8(
                            &file_contents[input_start..],
                            &mut intermediate_buffer,
                            true,
                        );
                    input_start += decoder_read;
                    // Write the intermediate buffer to disk.
                    new_file.write_all(&intermediate_buffer[..decoder_written])?;
                    match decoder_result {
                        encoding_rs::CoderResult::InputEmpty => {
                            break;
                        }
                        encoding_rs::CoderResult::OutputFull => {
                            continue;
                        }
                    }
                }

                filepath = Box::new(new_file.into_temp_path());
            }
        }

        if format == "txt" {
            // We need to check if the file contains BBCode and convert it to markdown if so
            info!("Checking for BBCode in {:?}", (*filepath).as_ref());
            let mut file = std::fs::File::open((*filepath).as_ref())?;
            let mut file_contents = String::new();
            file.read_to_string(&mut file_contents)?;

            if BBCODE_REGEX.is_match(&file_contents) {
                info!("BBCode detected, converting to markdown");
                let mut new_file = tempfile::Builder::new()
                    .suffix(".md")
                    .tempfile_in("./tmp/")?;
                let file_contents = BBCODE_BOLD_REGEX.replace_all(&file_contents, "**");
                let file_contents = BBCODE_ITALICS_REGEX.replace_all(&file_contents, "*");
                let file_contents = file_contents
                    .replace("[u]", "[")
                    .replace("[/u]", "]{.underline}");

                new_file.write_all(file_contents.as_bytes())?;
                filepath = Box::new(new_file.into_temp_path());
            }

            input_format = Some(pandoc::InputFormat::Other(String::from(
                "markdown-pandoc_title_block-yaml_metadata_block",
            )));
        }

        if format == "html" || format == "htm" {
            info!(
                "Skipping pandoc processing for html file {:?}",
                (*filepath).as_ref()
            );
            let mut file = std::fs::File::open((*filepath).as_ref())?;
            let mut file_contents = String::new();
            file.read_to_string(&mut file_contents)?;
            return Ok(SanitizedHtml::from(&file_contents));
        }
    }

    info!(
        "Starting pandoc parsing of the temporary file {:?}",
        (*filepath).as_ref()
    );
    let result = pandoc_wrapped_and_sanitized(
        pandoc::InputKind::Files(vec![(*filepath).as_ref().to_path_buf()]),
        input_format,
    )?;
    Ok(remove_leading_whitespaces_from_paragraphs_in_html(result))
}

/// Remove all leading extra whitespaces from all the paragraphs in the given sanitized HTML,
/// and return a new Sanitized HTML
///
/// # Examples
///
/// ```
/// use intertextual::models::formats::SanitizedHtml;
/// use intertextual::utils::htmlize::remove_leading_whitespaces_from_paragraphs_in_html;
/// let block = SanitizedHtml::from("
///     <p><span>&nbsp;&nbsp;&nbsp;&nbsp;</span><span>This is a test</span><span>&nbsp;&nbsp;&nbsp;&nbsp;</span><span>For removing leading spaces</span></p>
///     <p>&nbsp;&nbsp;&nbsp;&nbsp;<span>This is another test</span></p>
///     "
/// );
/// let clean_block = remove_leading_whitespaces_from_paragraphs_in_html(block);
/// assert_eq!(
///     "<p><span>This is a test</span><span>&nbsp;&nbsp;&nbsp;&nbsp;</span><span>For removing leading spaces</span></p>
///     <p><span>This is another test</span></p>",
///     clean_block.as_str(),
/// );
/// ```
pub fn remove_leading_whitespaces_from_paragraphs_in_html(html: SanitizedHtml) -> SanitizedHtml {
    lazy_static! {
        /// Used to remove all leading non-breaking spaces in a paragraph
        ///
        /// Note : The `&nbsp;` inside the regex is an escaped non-breaking space, while the "space-looking space" is an actual non-breaking space.
        static ref SPACES_REMOVAL_RE: regex::Regex = {
            let mut builder = regex::RegexBuilder::new("<p>(\\s|&nbsp;|\u{009f}|<span>(&nbsp;|\\s|\u{009f})*</span>)+");
            builder.case_insensitive(true);
            builder.build().unwrap()
        };
    }
    html.map(|raw_string| {
        SPACES_REMOVAL_RE
            .replace_all(&raw_string, "<p>")
            .to_string()
    })
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::models::formats::SanitizedHtml;

    #[test]
    fn remove_leading_escaped_nonbreaking_whitespaces_from_p() {
        let block = SanitizedHtml::from("<p>&nbsp;&nbsp;&nbsp;&nbsp;This is a test</p>");
        assert_eq!(
            "<p>This is a test</p>",
            remove_leading_whitespaces_from_paragraphs_in_html(block).as_str(),
        );
    }

    #[test]
    fn remove_leading_nonbreaking_whitespaces_from_p() {
        let block = SanitizedHtml::from("<p>\u{009f}\u{009f}\u{009f}\u{009f}This is a test</p>");
        assert_eq!(
            "<p>This is a test</p>",
            remove_leading_whitespaces_from_paragraphs_in_html(block).as_str(),
        );
    }

    #[test]
    fn remove_leading_whitespace_spans_from_p() {
        let block = SanitizedHtml::from("<p><span>    </span>This is a test</p>");
        assert_eq!(
            "<p>This is a test</p>",
            remove_leading_whitespaces_from_paragraphs_in_html(block).as_str(),
        );
    }

    #[test]
    fn remove_leading_encoded_nonbreaking_whitespace_spans_from_p() {
        let block =
            SanitizedHtml::from("<p><span>&nbsp;&nbsp;&nbsp;&nbsp;</span>This is a test</p>");
        assert_eq!(
            "<p>This is a test</p>",
            remove_leading_whitespaces_from_paragraphs_in_html(block).as_str(),
        );
    }

    #[test]
    fn remove_leading_nonbreaking_whitespace_spans_from_p() {
        let block = SanitizedHtml::from(
            "<p><span>\u{009f}\u{009f}\u{009f}\u{009f}</span>This is a test</p>",
        );
        assert_eq!(
            "<p>This is a test</p>",
            remove_leading_whitespaces_from_paragraphs_in_html(block).as_str(),
        );
    }

    #[test]
    fn remove_leading_repeated_whitespace_spans_from_p() {
        let block =
            SanitizedHtml::from("<p><span>    </span>    <span>    </span>This is a test</p>");
        assert_eq!(
            "<p>This is a test</p>",
            remove_leading_whitespaces_from_paragraphs_in_html(block).as_str(),
        );
    }

    #[test]
    fn remove_leading_prefixed_whitespace_spans_from_p() {
        let block =
            SanitizedHtml::from("<p>&nbsp;<span></span>&nbsp;&nbsp;&nbsp;&nbsp;<span>    </span>&nbsp;&nbsp;This is a test</p>");
        assert_eq!(
            "<p>This is a test</p>",
            remove_leading_whitespaces_from_paragraphs_in_html(block).as_str(),
        );
    }

    #[test]
    fn remove_leading_whitespace_with_line_breaks_from_p() {
        let block = SanitizedHtml::from(
            "<p>
                &nbsp;<span></span>
                &nbsp;&nbsp;&nbsp;&nbsp;<span>    </span>
                &nbsp;&nbsp;
                This is a test</p>",
        );
        assert_eq!(
            "<p>This is a test</p>",
            remove_leading_whitespaces_from_paragraphs_in_html(block).as_str(),
        );
    }

    #[test]
    fn remove_leading_case_insensitive_whitespace_breaks_from_p() {
        let block = SanitizedHtml::from(
            "<P>
                &nbsp;<SPAN></SPAN>
                &nbsp;&nbsp;&nbsp;&nbsp;<span>    </span>
                &nbsp;&nbsp;
                This is a test</p>",
        );
        assert_eq!(
            "<p>This is a test</p>",
            remove_leading_whitespaces_from_paragraphs_in_html(block).as_str(),
        );
    }

    #[test]
    fn do_not_remove_nonempty_trailing_spans_from_p() {
        let block = SanitizedHtml::from("<p>This is a test<span>Test </span></p>");
        assert_eq!(
            "<p>This is a test<span>Test </span></p>",
            remove_leading_whitespaces_from_paragraphs_in_html(block).as_str(),
        );
    }

    #[test]
    fn do_not_remove_nonempty_leading_spans_from_p() {
        let block = SanitizedHtml::from("<p><span>Test </span>This is a test</p>");
        assert_eq!(
            "<p><span>Test </span>This is a test</p>",
            remove_leading_whitespaces_from_paragraphs_in_html(block).as_str(),
        );
    }
}
