ALTER TABLE users ADD COLUMN theme TEXT DEFAULT NULL;

UPDATE users SET theme = (
     CASE WHEN style_prefs @> '{"color_scheme": "dark"}' THEN 'd' ELSE 'l' END
  || CASE WHEN style_prefs @> '{"font_family": "sans_serif"}' THEN 'n' ELSE 's' END
  || CASE WHEN style_prefs @> '{"text_alignment": "justify"}' THEN 'j' ELSE 'l' END
  || (style_prefs ->> 'font_size')
  || CASE WHEN style_prefs @> '{"content_width": "narrow"}' THEN 'n' ELSE 'w' END
  || CASE WHEN style_prefs @> '{"paragraph_layout": "modern"}' THEN 'm' ELSE 'c' END
);

ALTER TABLE users DROP COLUMN style_prefs;
