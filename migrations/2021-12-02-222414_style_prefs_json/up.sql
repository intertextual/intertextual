/*
 * Replace the brittle string-based (and ambiguously-named) 'theme'
 * column, with a JSON object storing style prefs in a more readable and
 * extensible way
 */
ALTER TABLE users ADD COLUMN style_prefs JSONB DEFAULT '{}';

UPDATE users SET style_prefs = jsonb_set(style_prefs, '{color_scheme}', '"light"') WHERE substring(theme FROM 1 FOR 1) = 'l';
UPDATE users SET style_prefs = jsonb_set(style_prefs, '{color_scheme}', '"dark"') WHERE substring(theme FROM 1 FOR 1) = 'd';

UPDATE users SET style_prefs = jsonb_set(style_prefs, '{font_family}', '"serif"') WHERE substring(theme FROM 2 FOR 1) = 's';
UPDATE users SET style_prefs = jsonb_set(style_prefs, '{font_family}', '"sans_serif"') WHERE substring(theme FROM 2 FOR 1) = 'n';

UPDATE users SET style_prefs = jsonb_set(style_prefs, '{text_alignment}', '"left"') WHERE substring(theme FROM 3 FOR 1) = 'l';
UPDATE users SET style_prefs = jsonb_set(style_prefs, '{text_alignment}', '"justify"') WHERE substring(theme FROM 3 FOR 1) = 'j';

UPDATE users SET style_prefs = jsonb_set(style_prefs, '{font_size}', substring(theme FROM 4 FOR 2)::jsonb);

UPDATE users SET style_prefs = jsonb_set(style_prefs, '{content_width}', '"wide"') WHERE substring(theme FROM 6 FOR 1) = 'w';
UPDATE users SET style_prefs = jsonb_set(style_prefs, '{content_width}', '"narrow"') WHERE substring(theme FROM 6 FOR 1) = 'n';

UPDATE users SET style_prefs = jsonb_set(style_prefs, '{paragraph_layout}', '"classic"') WHERE substring(theme FROM 7 FOR 1) = 'c';
UPDATE users SET style_prefs = jsonb_set(style_prefs, '{paragraph_layout}', '"modern"') WHERE substring(theme FROM 7 FOR 1) = 'm';

ALTER TABLE users DROP COLUMN theme;
