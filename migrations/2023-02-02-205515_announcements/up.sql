/*
 * This table contains the list of announcements on the website
 */
CREATE TABLE announcements (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  /* The URL fragment of the announcement */
  url_fragment VARCHAR(32) NOT NULL,
  /* The title of the announcement */
  title VARCHAR(128),
  /* Only used internally */
  announcement_internally_created_at timestamp without time zone NOT NULL DEFAULT NOW(),
  /* This field is used to determine when this will be made available to the public. */
  show_publicly_after_date timestamp without time zone DEFAULT NULL,
  /* The actual text inside the announcement */
  content TEXT NOT NULL
);

/*
 * This table contains the single announcement shown on the front page.
 */
CREATE TABLE frontpage_announcement (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  /* Only used internally */
  announcement_internally_created_at timestamp without time zone NOT NULL DEFAULT NOW(),
  /* This field is used to determine when this will be made available to the public. */
  show_publicly_after_date timestamp without time zone DEFAULT NULL,
  /* After this date is passed, the announcement will be deleted from the database.  */
  remove_after_date timestamp without time zone DEFAULT NULL,
  /* The actual text inside of the announcement */
  content TEXT NOT NULL
);
