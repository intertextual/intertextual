/*
 * This table contains canonical tags excluded by an user
 */
CREATE TABLE excluded_canonical_tags_for_user (
  user_id uuid NOT NULL REFERENCES users ON DELETE CASCADE,
  canonical_tag_id uuid NOT NULL REFERENCES canonical_tags ON DELETE CASCADE,
  PRIMARY KEY (user_id, canonical_tag_id)
);
