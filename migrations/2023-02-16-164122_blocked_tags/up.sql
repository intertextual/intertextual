ALTER TABLE canonical_tags ADD block_publication BOOLEAN NOT NULL DEFAULT FALSE;
ALTER TABLE canonical_tags ADD block_publication_message TEXT DEFAULT NULL;
