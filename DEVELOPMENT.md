# Development Instructions

## Set up your local environment

### Linux/MacOS

- [Install Nix](https://nixos.org/guides/install-nix.html)
- Clone this repo and `cd` into it
- Run `nix develop` to enter a development environment with all the required tools
- Set up a Postgres database somewhere you can access - locally, inside a container, or on another machine you can connect to

### Windows

Install the following softwares :

- postgres from the [postgresql.org](https://www.postgresql.org/download/) website
- The latest release of pandoc from [github](https://github.com/jgm/pandoc/releases/)
- Rust and cargo from the [rust-lang.org](https://www.rust-lang.org/tools/install) website (via rustup)
- npm and nodejs from the [nodejs.org](https://nodejs.org/en/download/) website
- Git for Windows from the [gitforwindows.org](https://gitforwindows.org/) website
- clang for windows from the [llvm.org](https://releases.llvm.org/download.html#10.0.0) website

Set up your environment variables properly (needed to make the diesel connection work properly)

- Set your env variable `LIB` to your postgres `lib/` folder (usually `C:/Program Files/PostgreSQL/12/lib/`)
- Update your `PATH` env variable, and add the `lib/` folder to it
- Update your `PATH` env variable, and add the `bin/` folder to it as well (usually `C:/Program Files/PostgreSQL/12/bin/`)
- In the `lib/` directory, copy the `libpq.dll` file to `pq.dll` (see [here](https://github.com/diesel-rs/diesel/issues/487) for details about why)

`git clone` this repo

## Set up your database

- Create e.g. an `Superuser` user with password `Superpassword` in your database with "superuser" rights
- replace username / password by what you want
- This user will be used to create tables and extensions needed while migrating the DB
- Create e.g. an `Access` user in your database with password `AccessPassword` and only access rights (no database creation rights)
- replace username / password by what you want
- This user will be used to access the data during normal operation
- Auto-grant the rights for the `Access` user for all tables created by your superuser.
```sql
ALTER DEFAULT PRIVILEGES FOR ROLE "Superuser" GRANT EXECUTE ON FUNCTIONS TO "Access";
ALTER DEFAULT PRIVILEGES FOR ROLE "Superuser" GRANT ALL ON TABLES TO "Access";
ALTER DEFAULT PRIVILEGES FOR ROLE "Superuser" GRANT ALL ON SEQUENCES TO "Access";
```
- Create an `intertextual` database in your server

## Set up your variables

- copy `env.example` to `.env` and edit the values for your own setup
  - Replace the `DATABASE_URL` value with `postgres://Access:AccessPassword@localhost:5432/intertextual` in the `DATABASE_URL` line
  - Use your own username and password as needed
- `cargo install diesel_cli --no-default-features --features postgres`

## Install the diesel DB and

- Run the `DATABASE_URL=postgres://Superuser:Superpassword@localhost:5432/intertextual diesel setup` command to setup the diesel connection
  - Replace username/password as needed
- Run the `DATABASE_URL=postgres://Superuser:Superpassword@localhost:5432/intertextual diesel migration run` command to initialize the database
  - Replace username/password as needed
- Run `npm install` to install the tinymce javascript library
- Start the project via `cargo run`

## Update your environment after a pull

After a `git pull`, you should execute :

- `DATABASE_URL=postgres://Superuser:Superpassword@localhost:5432/intertextual diesel migration run`
  - Replace username/password as needed
  - This will adjust the database contents as needed
- `cargo run`
