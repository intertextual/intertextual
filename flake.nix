{
  description = "Intertextual web fiction server";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    utils.url = "github:numtide/flake-utils";
    rust-overlay.url = "github:oxalica/rust-overlay";
    crate2nix = {
      url = "github:kolloch/crate2nix";
      flake = false;
    };
    npmlock2nix = {
      url = "github:nix-community/npmlock2nix";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, utils, rust-overlay, crate2nix, npmlock2nix }: let
    name = "intertextual";
  in
    utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = import nixpkgs {
            inherit system;
            overlays = [
              rust-overlay.overlays.default
              (self: super: {
                rustc = self.rust-bin.stable.latest.default;
                cargo = self.rust-bin.stable.latest.default;
                npmlock2nix = self.callPackage npmlock2nix {};
              })
            ];
          };
          node_modules = pkgs.npmlock2nix.node_modules { src = ./.; };
          tinymce = "${node_modules}/node_modules/tinymce";
          inherit (import "${crate2nix}/tools.nix" { inherit pkgs; })
            generatedCargoNix;
          project = import
            (generatedCargoNix {
              inherit name;
              src = ./.;
            })
            {
              inherit pkgs;
              defaultCrateOverrides = pkgs.defaultCrateOverrides // {
                ${name} = oldAttrs: {
                  inherit buildInputs nativeBuildInputs;
                } // buildEnvVars;
                argonautica = oldAttrs: {
                  inherit nativeBuildInputs;
                } // buildEnvVars;
              };
            };
          buildInputs = with pkgs; [ postgresql.lib ];
          nativeBuildInputs = with pkgs; [ rustc cargo pkgconfig clang ];
          buildEnvVars = with pkgs; {
            LIBCLANG_PATH = "${llvmPackages.libclang.lib}/lib";
          };
        in rec {
          packages.${name} = pkgs.symlinkJoin {
            inherit name;
            paths = [
              packages."${name}-unwrapped"
              (pkgs.writeShellScriptBin "diesel-setup" ''
                ${pkgs.diesel-cli}/bin/diesel setup --migration-dir ${./migrations}
                ${pkgs.diesel-cli}/bin/diesel migration run --migration-dir ${./migrations}
              '')
            ];
            buildInputs = [ pkgs.makeWrapper ];
            postBuild = ''
              wrapProgram $out/bin/${name} \
                --set TINYMCE_DIR ${tinymce} \
                --set STATIC_DIR ${./static} \
                --prefix PATH : ${pkgs.pandoc}/bin
            '';
          };

          defaultPackage = packages.${name};

          packages."${name}-unwrapped" = project.rootCrate.build;

          devShell = pkgs.mkShell ({
            inherit buildInputs;
            nativeBuildInputs = with pkgs; nativeBuildInputs ++ [
              cargo-audit
              pandoc
              stdenv.cc
              nodejs-16_x
              diesel-cli
            ];
            RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";
            TINYMCE_DIR = tinymce;
          } // buildEnvVars);
        }
      );
}
