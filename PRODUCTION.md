# Deploying Intertextual in production

## Prerequisites

This guide assumes that you have a freshly-installed Debian 11 server,
and root ssh access.

We haven't nailed down how much memory/disk/CPU is optimal for
Intertextual, but:
- Building intertextual server-side will require several GB of memory,
  and probably a decent amount of CPU as well.
- The site data is basically all text, so the disk footprint will be
  pretty tiny.  The database on our production server is still well
  under 1GB.
- However, the Nix store may grow to a few tens of GB over time. You
  may need to clean up old build artifacts periodically, see below.

You'll also need [Ansible](https://ansible.com) installed on your
local machine in order to run our deployment playbook.

## Configuration

Go into the `ansible` directory, and add the following files:
- `hosts.yaml`: this defines your "inventory" of servers - in this
  case, the single server that you'll be installing Intertextual
  on. Copy `hosts.example.yaml` and change the hostname from
  `example.com` to whatever your actual hostname is.
- `host_vars/[your hostname].yaml`: this defines all the important
  system settings for your specific instance of Intertextual.
  `example.com.yaml` is a template, see the comments there for
  documentation of all the config options.

  Depending on where you're keeping your config files, you may want to
  use `ansible-vault` to encrypt some of the passwords and secrets
  kept in `host_vars/[hostname].yaml`.  All of these values will be
  unencrypted on the server, and just protected by unix file
  permissions; but if you're sharing your config over unencrypted
  channels or you're concerned about somebody getting into your
  personal computer, it doesn't hurt to encrypt things.  Choose a
  strong vault password to use for all of the secrets in this file,
  and run `ansible-vault encrypt_string` to encrypt things.  I've been
  entering strings into `ansible-vault` without trailing newlines; I'm
  not sure whether it actually makes a difference though.

You can also optionally add
- A custom theme for your Intertextual instance.  You can add theme
  files to `files/theme`, see the README in there for details.
- A custom logging configuration in `files/log4rs.yaml`, you probably
  don't need this.

## Deployment

Still in the `ansible` directory, just run:

```
ansible-playbook -i hosts.yaml playbook.yaml
```

This will run the entire deployment process from start to finish; once
it completes, your Intertextual server should be up and running!  It
may take quite a while, *especially* if this is your first time
deploying, since there's a lot of stuff that will need to be built
from scratch the first time.

You can also run

```
ansible-playbook --diff -i hosts.yaml playbook.yaml
```

to see how files are actually being changed; this can help catch
errors sometimes.  Bear in mind that if the diffs include secret data,
that will be included in the output.

## Updating

You can rerun the same command you used to deploy Intertextual at any
time.  If there are new commits to Intertextual, or you've changed any
of your configuration variables, those will be updated and
Intertextual will be restarted. Anything that doesn't need updating
will be left as-is.

## Optional pre-deploy preparation

If your local machine runs x86-64 Linux and has Nix tools, and has more
computational resources than your server (or has already built the
latest Intertextual), you can save some time/work during deploys by
pre-building Intertextual locally and copying it up to your production
server:

```
nix copy --to ssh://root@example.com git+https://gitlab.com/intertextual/intertextual?ref=main
```

This takes advantage of Nix's reproducible builds and advanced
dependency management, copying the completed build and all of its
recursive runtime dependencies from your local Nix store directly to
the remote Nix store.  The remote Nix may still rebuild a few
interstitial artifacts during deployment, but most of the build
outputs will be used directly without rebuilding.

If your production server uses a non-standard SSH port, you can set an
environment variable for `nix copy` like so: `NIX_SSHOPTS="-p 2222"`

## Nix store cleanup

The Nix store retains all the old versions of everything you've
built/installed indefinitely, so it can get pretty big.  You can clean
up data that's no longer in use by running this command as root on
your server:

```
nix-collect-garbage --delete-older-than 30d
```
