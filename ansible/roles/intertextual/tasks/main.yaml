- name: install git
  package:
    name: git

- name: add intertextual to flake registry
  command:
    cmd: "{{ nix_command }} registry add --registry /etc/nix/registry.json intertextual git+https://gitlab.com/intertextual/intertextual?ref=main"
    creates: /etc/nix/registry.json

- name: install intertextual
  command:
    cmd: "nice {{ nix_command }} profile install --profile /nix/var/nix/profiles/intertextual intertextual"
    creates: /nix/var/nix/profiles/intertextual

- name: upgrade intertextual
  command: "nice {{ nix_command }} profile upgrade --refresh --profile /nix/var/nix/profiles/intertextual flake:intertextual#defaultPackage.x86_64-linux"

- name: create system group
  group:
    name: intertextual
    state: present
    system: yes
- name: create system user
  user:
    name: intertextual
    group: intertextual
    state: present
    system: yes
    home: /opt/intertextual
    password_lock: yes

- name: configure intertextual
  template:
    src: intertextual.env.j2
    dest: /opt/intertextual/.env
    owner: intertextual
    group: intertextual
    mode: '0600'

- name: configure intertextual logging (custom config)
  copy:
    src: log4rs.yaml
    dest: /opt/intertextual/log4rs.yaml
    owner: intertextual
    group: intertextual
    mode: '0644'
  ignore_errors: yes
  register: intertextual_custom_logging

- name: configure intertextual logging (fallback config)
  copy:
    src: log4rs.default.yaml
    dest: /opt/intertextual/log4rs.yaml
    owner: intertextual
    group: intertextual
    mode: '0644'
  when: intertextual_custom_logging is failed

- name: install theme
  copy:
    src: theme/
    dest: /opt/intertextual/theme
    owner: intertextual
    group: intertextual
    mode: '0644'
    directory_mode: '0755'

- name: create temporary storage directory
  file:
    state: directory
    path: /opt/intertextual/tmp
    owner: intertextual
    group: intertextual
    mode: '0700'

- name: set up postgres
  become: yes
  become_user: postgres
  block:
    - name: create database user
      postgresql_user:
        name: intertextual
        state: present
    - name: create database
      postgresql_db:
        name: intertextual
        state: present
        owner: intertextual
    - name: create extension for database
      postgresql_ext:
        db: intertextual
        name: uuid-ossp
        state: present

- name: create intertextual systemd service
  copy:
    src: intertextual.service
    dest: /etc/systemd/system/intertextual.service
    owner: root
    group: root
    mode: '0644'

- name: enable intertextual service
  systemd:
    name: intertextual.service
    daemon-reload: yes
    enabled: yes

- name: stop intertextual if running
  systemd:
    name: intertextual.service
    state: stopped

- name: set up database and run migrations
  command:
    cmd: /nix/var/nix/profiles/intertextual/bin/diesel-setup
    chdir: /opt/intertextual
  become: yes
  become_user: intertextual

- name: start intertextual
  systemd:
    name: intertextual.service
    state: started

- name: set up fail2ban
  copy:
    src: "{{ item }}.conf"
    dest: "/etc/fail2ban/{{ item }}.d/intertextual.conf"
    owner: root
    group: root
    mode: '0644'
  loop:
    - filter
    - jail
  notify:
    - restart fail2ban

- name: set up nginx config
  template:
    src: nginx.conf.j2
    dest: /etc/nginx/sites-available/intertextual
    owner: root
    group: root
    mode: '0644'
  notify:
    - restart nginx
- name: enable nginx config
  file:
    src: /etc/nginx/sites-available/intertextual
    dest: /etc/nginx/sites-enabled/intertextual
    state: link
    owner: root
    group: root
  notify:
    - restart nginx
- name: disable default nginx config
  file:
    path: /etc/nginx/sites-enabled/default
    state: absent
  notify:
    - restart nginx
